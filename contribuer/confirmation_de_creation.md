---
layout: form_confirmation
confirmation_message: |
  La nouvelle notice a été envoyée à l'équipe de Framalibre qui la
  validera dans les jours à venir.
---
