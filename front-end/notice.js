//@ts-check

const copyButton = document.querySelector(".html-copy");

if (!copyButton) {
  throw new TypeError("Bouton pour copier le code HTML manquant (.html-copy)");
}

const toCopyHTMLElement = document.querySelector(".framalibre-notice-to-copy");

if (!toCopyHTMLElement) {
  throw new TypeError("Template HTML manquant (.framalibre-notice-to-copy");
}

const toCopy = toCopyHTMLElement.innerHTML;

copyButton.addEventListener("click", (e) => {
  copyButton.classList.add("animate");
  copyButton.addEventListener("animationend", () =>
    copyButton.classList.remove("animate"),
  );

  navigator.clipboard
    .writeText(toCopy)
    .then(() => {
      copyButton.textContent = "Copié dans le presse-papier !";
    })
    .catch((error) => {
      console.error("Une erreur s'est produite lors de la copie : ", error);
    });
});
