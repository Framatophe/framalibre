# Framalibre

La version en ligne est disponible ici : https://framalibre.org/


## Contexte

[Framalibre](https://framalibre.org/) est un annuaire de logiciels libre. 

À l'origine et jusque 2023, framalibre est/était hébergé sur un Drupal. À l'occasion de la fin de vie de la version de Drupal utilisée, un choix s'est présenté : mettre à jour Drupal ou passer sur une autre technologie. Pour le contexte de framalibre, il a été jugé qu'une autre option viable était de faire un site statique. Ainsi, chaque "notice" (ou "fiche") décrivant un logiciel pourrait être un fichier markdown avec des meta-données (site web, licence, tags, etc.) en yaml/front-matter en haut du fichier markdown

Tout le contenu serait sur un repo gitlab sur framagit.org plutôt que dans une base de données


## Architecture

Ce dépôt contient :
- un site statique généré par Jekyll dans le dossier `public` avec la page d'accueil, les notices (en tant que collection Jekyll dans [`_notices`](./_notices)) et le formulaire de contribution,
- des [outils](./tools) pour récupérer les notices existantes sur le site Drupal (pour s'économiser une conversion des ~900 notices à la main)
- un [petit serveur en Node.js](./server/main.js) qui gère la création d'une `MergeRequest` sur le repo à partir des données envoyées via le formulaire de contribution.


### Contenu et lecture

Le plus gros usage du site, c'est des personnes qui cherchent des notices (textuellement ou via les tags) et les lisent et naviguent\
Cette partie est assurée par le site statique qui génère une page web par notice dans la collection Jekyll


### Contribution

Il est ensuite possible de contribuer des nouvelles notices ou modifier les existantes

Il y a une poignée d'approches possibles : 
1) Contribution avec validation a posteriori
2) Contribution avec validation a priori

Pour le moment, l'approche choisie est la 2) : quand une personne contribue via le formulaire de contribution, sa contribution est transformée en *merge request* sur framagit
(le passage à 1) consistera à faire des commits directement sur le repo)

Côté technique, dans tous les cas, nous pourrions permettre ces changements depuis directement le côté client vers l'API gitlab/framagit, mais les options offertes par Gitlab ne permettent pas de créer des token avec une granularité assez fine pour ne permettre que de "créer une MR" ou "ajouter des commits avec des changements seulement dans dans tel dossier".\
Les token créés embarquent forcément des pouvoir destructifs (modifier la branche `main`, notamment) largement supérieurs à nos besoins et potentiellement dangereux.\
Nous ne souhaitons pas prendre le risque de laisser ce genre de token de manière publique côté client. Alors nous avons un petit serveur qui a le token et qui se restreint à l'utiliser pour seulement les usages souhaités


## Pré-requis

- [Node.js](https://nodejs.org/en/download) or [for Linux](https://nodejs.org/en/download/package-manager)

## Installation de Ruby/Jekyll

Ce site est basé sur Jekyll. Nous avons eu beaucoup de problèmes de gestion de versions de Ruby/Gem/Bundler/Jekyll et donc,
nous proposons deux manières de faire pour installer les bonnes versions des
dépendances, soit en installant Ruby directement, soit via une image Docker.

### Docker

Nous avons créé une image docker à `build`-er au début et qui contient les bonnes versions de tout. Si des versions changent, nous changeons le `Gemfile.lock` et chaque développeur.eresse doit re-builder l'image docker

Tout la gestion de docker est cachée dans des script npm

Vous pouvez installer Docker en suivant cette [documentation](https://docs.docker.com/engine/install/).

Puis, installer les dépendances :
```sh
npm run dev:build-jekyll-docker-image
npm install
npm run dev:prepare
```

### Sans Docker

Installer [Ruby](https://www.ruby-lang.org/en/documentation/installation/) >= 3.0.2

Et également :
```sh
sudo apt-get install build-essential
sudo apt-get install ruby-dev
sudo gem install bundler # Bundler >= 2.4.10
```

Installer les dépendances JavaScript :
```
npm install
npm run dev:prepare
```

## Développement

En dev, ouvrir dans 3 consoles différentes avec chacune :
- avec Docker : `mkdir public/ && npm run dev:docker-jekyll`
- sans Docker : `npm run dev:jekyll`
- `npm run dev:rollup`

Le build peut prendre un peu de temps, jusqu'à 2-3 minutes. 

Le site peut être visité sur `http://localhost:4000/`.

Le formulaire de contribution est accessible sur `http://localhost:4000/contribuer`. On peut lire les logs de la soumission du formulaire dans la console qui a lancé `npm run dev:server`. Il n'y a pas de message de confirmation côté client pour l'instant.

Pour lancer une version locale du serveur de contribution:
- `FRAMAGIT_TOKEN=<token> npm run dev:server`

### Fonctionnement et configuration du serveur de contribution

Le serveur pour le formulaire de contribution a besoin de divers configurations fournies via des variables d'environnement :
- `FRAMAGIT_ORIGIN` : origine du serveur de framagit (instance de gitlab qui contient les notices et autres contenus de framalibre)
- `FRAMAGIT_TOKEN` : Token gitlab généré sur framagit pour donner le droit au serveur de faire de merge-requests. Ce token peut être généré dans [`Settings > Access Tokens`](https://framagit.org/framasoft/framalibre/-/settings/access_tokens) avec :
    - le scope `api` 
    - le rôle `maintainer`.
- `FRAMALIBRE_FRAMAGIT_PROJECT_ID` : Identifiant du projet (`Project ID`) gitlab qui contient les notices et autres contenus
- `HOST` : argument `host` du serveur fastify/node.js. Il peut s'agir de toutes les options valides de l'option `host` de la fonction node.js [server.listen](https://nodejs.org/api/net.html#serverlistenoptions-callback). Notamment une adresse IPv4, IPv6 ou `localhost`
- `PORT` : le port sur lequel le serveur écoute

Dans la commande `npm run dev:server`, toutes les variables d'environnement sauf `FRAMAGIT_TOKEN` sont hardcodées pour simplifier. Pour éviter de versionner ces tokens qui ont le pouvoir notamment de supprimer le repo, ce token doit être créé par chaque dev et caché. D'où le fait de passer le token directement en ligne de commande via `FRAMAGIT_TOKEN=<token> npm run dev:server` plutôt que directement `npm run dev:server`

Pour le moment, le serveur de contribution dans l'environnement de développement contribue sur le même repo que sur l'environnement de production. **Il n'y a pas de repo distinct** (et c'est un compromis qui a l'air ok pour le moment)




## Déploiement en production

Il y a 2 morceaux déployés : 
- le site statique hébergé en gitlab pages par framagit. Ce déploiement est géré par le [`.gitlab-ci.yml`](.gitlab-ci.yml)
- le serveur de contribution qui est hebergé par framasoft sur un serveur dédié


### Serveur de contribution

Le serveur de contribution est déployé sur un serveur de Framasoft et accessible sur le domaine `contribuer.framalibre.org`

Pour le mettre en place, il faut configurer les variables CI/CD du repo (Settings => CI/CD => Variables) : 
- `DEPLOYMENT_HOST` : host du serveur framasoft où le serveur de contribution est déployé
- `DEPLOYMENT_USER` : nom d'utilisateur sur le serveur
- `DEPLOYMENT_KEY` : clef ssh pour accéder au serveur

Le fichier `.service` est [disponible dans ce repo](contribuer-framalibre.service)

En cas de modification de ce fichier, pour le mettre à jour sur le serveur, faire les choses suivantes : 

```sh
sudo /usr/bin/cp /var/www/clients/client3/web46/private/contribuer-framalibre.service /etc/systemd/system/contribuer-framalibre.service
sudo /bin/systemctl restart contribuer-framalibre.service
```



## Commandes disponibles

Ces commandes ne devraient être nécessaires que lors des premiers déploiements en production pour permettre d'avoir les mêmes informations que sur le site actuel de Framalibre.

Pour générer un export des notices à partir des données sur Drupal :
```sh
npm run export
```

Pour créer les notices au nouveau format mardown à partir de l'export Drupal : 
```sh
npm run make-notices-from-export
```



## Documentation

Plus de documentation peut être trouvée [dans le dossier `documentation`](./documentation/index.md)
