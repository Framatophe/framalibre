---
nom: "Vym"
date_creation: "Mercredi, 4 janvier, 2017 - 19:10"
date_modification: "lundi, 19 février, 2024 - 11:16"
logo:
    src: "images/logo/Vym.png"
site_web: "http://www.insilmaril.de/vym/"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
description_courte: "Logiciel de mind mapping simple, efficace et élégant. Idéal pour démarrer en mind mapping."
createurices: "Uwe Drechse"
alternative_a: "imindmap, mindjet, novamind, personal brain"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
    - "carte heuristique"
    - "organisation"
    - "mind mapping"
    - "gtd"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/vym"
---

Vym pour "View your mind" est un logiciel de mind mapping qui se veut très simple à prendre en main.
Il accepte en importation les signets kde4, les cartes freemind ou mind manager et il est capable d'exporter dans 14 formats dont html, texte, pdf, svg, xml, odf, orgmode...
Il accepte les commentaires, les liens internes et externes, le formatage html, le formatage des branches et possède un mode diapositive. L'ensemble des fonctions sont pilotables via des raccourcis clavier.
Il possède également une fonction de gestion des tâches pour des méthodes du type Getting Things Done.
Si vous cherchez un outil de mind mapping dont la prise en main est immédiate, c'est vraiment l'outil idéal.
