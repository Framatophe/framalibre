---
nom: "Nextcloud"
date_creation: "Mardi, 14 mars, 2017 - 16:28"
date_modification: "lundi, 15 janvier, 2024 - 10:00"
logo:
    src: "images/logo/Nextcloud.png"
site_web: "https://nextcloud.com/"
plateformes:
    - "le web"
    - "Apple iOS"
    - "Android"
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
    - "Autre"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Nextcloud est une plate-forme auto-hébergée de services de stockage et d’applications diverses dans les nuages."
createurices: "Frank Karlitschek"
alternative_a: "Dropbox, Google Drive, One Drive, Apple iCloud, Google Docs, Google Apps"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "stockage"
    - "agenda"
    - "tâche"
    - "marque-pages"
    - "partage de fichiers"
    - "synchronisation"
    - "auto-hébergement"
    - "décentralisation"
    - "fediverse"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Nextcloud"
lien_exodus: ""
identifiant_wikidata: "Q25874683"
mis_en_avant: "oui"
redirect_from: "/content/nextcloud"
---

Nextcloud vous permet de stocker vos fichiers (documents, photos, vidéos, etc.), vos contacts, vos calendriers, vos tâches et favoris sur votre propre cloud auto-hébergé (nécessite un serveur ou un hébergement).
Toutes vos données sont ainsi accessibles depuis n'importe quel navigateur. Il est également possible de les synchroniser grâce aux logiciels clients (Windows, Mac, Linux, iOS, Android, Windows Mobile).
Des fonctionnalités avancées de partage sont proposées (à durée limitée et/ou avec mot de passe par exemple).
De nombreux plugins sont proposés par la communauté afin d'étendre les fonctionnalités : ajout d'une suite bureautique, mise en place d'un gestionnaire de mots de passe, visioconférence, etc.
Il propose aussi de nombreuses fonctionnalités à destination des entreprises (intégration dans l'infrastructure de l'entreprise, sécurité avancée, gestion des droits, workflow, etc.).
Il s'agit d'un fork du logiciel Owncloud.



