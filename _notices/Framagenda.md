---
nom: "Framagenda"
date_creation: "Vendredi, 17 mars, 2017 - 22:13"
date_modification: "Jeudi, 22 septembre, 2022 - 22:31"
logo:
    src: "images/logo/Framagenda.png"
site_web: "https://framagenda.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "FirefoxOS"
    - "Windows Mobile"
    - "Apple iOS"
    - "Autre"
    - "le web"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Framagenda est un agenda en ligne partageable."
createurices: "Thomas Citharel"
alternative_a: "Google Agenda, Outlook OWA"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "agenda"
    - "tâche"
    - "synchronisation"
    - "décentralisation"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/framagenda"
---

Framagenda a été développé par Thomas Citharel à la demande de l’association Framasoft, pour proposer une alternative libre aux services privateurs d’agenda « dans les nuages » des grands acteurs du web.
Le service est basé sur Nextcloud, dans lequel il s’intègre parfaitement, y ajoutant une nouvelle brique.

