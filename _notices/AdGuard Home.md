---
nom: "AdGuard Home"
date_creation: "mercredi, 17 avril, 2024 - 09:36"
date_modification: "mercredi, 17 avril, 2024 - 09:36"
logo:
    src: "images/logo/AdGuard Home.svg"
site_web: "https://github.com/AdguardTeam/AdguardHome"
plateformes:
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
description_courte: "Un moyen rapide, simple et efficace de bloquer les publicités sur n'importe quel appareil, et de protéger votre vie privée ainsi que les enfants en ligne"
createurices: "https://github.com/AdguardTeam/AdGuardHome/graphs/contributors"
alternative_a: "Pi-Hole"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bloqueur de publicité"
    - "sécurité"
    - "serveur"
lien_wikipedia: "https://fr.wikipedia.org/wiki/AdGuard"
lien_exodus: ""
identifiant_wikidata: "Q4033398"
mis_en_avant: "non"

---

- De nombreux filtres anti-publicitaires régulièrement mis à jour ;
- Une mise à l’abri des pisteurs en ligne et des systèmes d’analyse de données ;
- Un module de protection familiale pour bloquer l'accès à tous les sites Web ayant du contenu réservé aux adultes ;
- Possibilité de choisir les serveurs DNS et d'activer le chiffrement des communications ;
- Mise à disposition d'un tableau de bord et de statistiques ;
- Pas d'installation d'autres applications : vous pouvez l'utiliser sur des appareils sous Windows, GNU/Linux, macOS, Android ou encore iOS.
