---
nom: "Pa11y"
date_creation: "Mercredi, 23 octobre, 2019 - 17:31"
date_modification: "Lundi, 10 mai, 2021 - 13:46"
logo:
    src: "images/logo/Pa11y.png"
site_web: "https://pa11y.org/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
description_courte: "Trouver les points problématiques pour l’accessibilité d'un site web"
createurices: ""
alternative_a: "tenon.io"
licences:
    - "Licence publique générale limitée GNU (LGPL)"
tags:
    - "développement"
    - "web"
    - "création de site web"
    - "wcag"
    - "rgaa"
    - "accessibilité"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/pa11y"
---

Pa11y prend la forme d'un ensemble d'outils qui servent à vérifier l'accessibilité de pages web. Nous avons un outil en ligne de commande, une interface web, une interface de bureau multi-plateforme, ainsi que d'autres services pour développeurs.

