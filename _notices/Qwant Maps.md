---
nom: "Qwant Maps"
date_creation: "Samedi, 16 juillet, 2022 - 00:45"
date_modification: "mardi, 26 décembre, 2023 - 14:13"
logo:
    src: "images/logo/Qwant Maps.jpg"
site_web: "https://www.qwant.com/maps/"
plateformes:
    - "le web"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Pour une alternative libre à Google Maps !"
createurices: ""
alternative_a: "Google Maps"
licences:
    - "Licence Apache (Apache)"
tags:
    - "internet"
    - "carte géographique"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Qwant_Maps"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/qwant-maps"
---

Qwant Maps est basé principalement sur la base de données OpenStreetMap avec Pages Jaunes et OpenAdresses. Les autres composants du site sont sous licence libre.
Comme avec Google Maps, on peut mettre des adresses en favoris et avoir les informations tel que le numéro de téléphone, l'adresse, les horaires, l'adresse-mail, le site web et les réseaux sociaux d'un lieu. Après on peut connaître la durée et l'itinéraire d'un trajet qu'on veut faire.
Enfin, on peut connaître les arrêts de bus d'une ville, ce qu'on ne trouve pas sur Google Maps.


