---
nom: "Node.js"
date_creation: "Jeudi, 12 janvier, 2017 - 17:25"
date_modification: "Jeudi, 12 janvier, 2017 - 17:25"
logo:
    src: "images/logo/Node.js.PNG"
site_web: "https://nodejs.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
description_courte: "Serveur web et framework en Javascript"
createurices: "Ryan Lienhart Dahl"
alternative_a: ""
licences:
    - "Licence MIT/X11"
tags:
    - "système"
    - "serveur"
    - "web"
    - "javascript"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Node.js"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/nodejs"
---

Node.js est une plateforme Javascript construite pour créer des réseaux d’applications rapide et évolutif. Node.js utilise un modèle événementiel asychrone, sans blocage I/O qui le rend léger et efficace, il est avant tout utilisé pour créer des applications web en temps réel avec un volume de données important.
Node.js contient une bibliothèque de serveur HTTP intégrée, ce qui lui permet de faire tourner un serveur web sans avoir besoin d'un logiciel externe comme Apache.

