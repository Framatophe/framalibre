---
nom: "OpenProject"
date_creation: "dimanche, 14 janvier, 2024 - 17:19"
date_modification: "dimanche, 14 janvier, 2024 - 17:19"
logo:
    src: "images/logo/OpenProject.png"
site_web: "https://www.openproject.org/fr/"
plateformes:
    - "le web"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Logiciel web de gestion de projet pour un usage collaboratif"
createurices: "OpenProject"
alternative_a: "Microsoft Project"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "planification"
    - "gestion de projet"
    - "gantt"
    - "conduite de projet"
lien_wikipedia: "https://fr.wikipedia.org/wiki/OpenProject"
lien_exodus: ""
identifiant_wikidata: "Q14468709"
mis_en_avant: "non"

---

Ce logiciel de gestion de projet est dédié aux organisations (entreprises ou associations). Il offre un support à la collaboration de projets : planification, gestion et attribution des tâches (portefeuille de projets), gestion du temps, allocations budgétaires et d'autres ressource, etc. Il dispose aussi d'une intégration avec Nextcloud (accès aux fichiers et dossiers, lecture, notifications).
