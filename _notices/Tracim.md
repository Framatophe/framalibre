---
nom: "Tracim"
date_creation: "Vendredi, 15 décembre, 2017 - 11:34"
date_modification: "Lundi, 13 septembre, 2021 - 11:04"
logo:
    src: "images/logo/Tracim.png"
site_web: "http://www.tracim.fr"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "FirefoxOS"
    - "Windows Mobile"
    - "Apple iOS"
    - "Autre"
    - "le web"
langues:
    - "Français"
    - "English"
description_courte: "Le socle de votre travail en équipe."
createurices: "Algoo, Damien Accorsi"
alternative_a: "Dropbox, trello, Google Drive, Wiki, Slack"
licences:
    - "Licence MIT/X11"
tags:
    - "travail collaboratif"
    - "gestion documentaire"
    - "forum de discussion"
    - "todo-list"
    - "gestion de tickets"
    - "base de connaissances"
    - "communication"
    - "communauté"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/tracim"
---

Notre plateforme est un logiciel libre, accessible à tous. Sa simplicité en fait un outil adapté à tous les publics, toutes les activités, de l’animation associative à la documentation technique en passant par l’enseignement et les métiers du conseil.
Tracim est conçu pour être flexible et pour s’intégrer avec les outils que vous avez l’habitude d’utiliser : navigateur web et mobile, client de messagerie email, explorateur de fichiers.
La démarche de Tracim se distingue des autres plateformes collaboratives en ce qu'elle s'adresse prioritairement à des équipes. L’activité d’un espace est partagée avec tous les membres ; chacun voit vivre l'ensemble des espaces dont il est membre.
À quoi sert Tracim ?
Centraliser les documents et toute l’information de vos projets,
Partager des fichiers et documents et les consulter directement en ligne,
Lancer des discussions riches et réagir,
Créer des documents bureautiques à plusieurs,
Organiser des agendas

