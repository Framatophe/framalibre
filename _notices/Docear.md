---
nom: "Docear"
date_creation: "Mercredi, 22 mars, 2017 - 09:44"
date_modification: "Jeudi, 23 mars, 2017 - 08:52"
logo:
    src: "images/logo/Docear.png"
site_web: "http://www.docear.org/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Suite Logicielle Académique : bibliographie + carte heuristique de nos notes de lecture."
createurices: "Joeran Beel, Stefan Langer, Marcel Genzmehr, Bela Gipp"
alternative_a: "EndNote"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
    - "documentation"
    - "java"
lien_wikipedia: "https://en.wikipedia.org/wiki/Docear"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/docear"
---

Ce logiciel fonctionne par projet, permettant de séparer nos thèmes de lecture.Principe
En lisant un fichier pdf, l'annoter ou stabilobosser des passages, Docear importera ces notes et ces surlignages sous forme de carte heuristique au format de Freeplane).
Sauver la bibliographie (sous format de JabRef, BibTex...) : la recherche des champs se fait automatiquement sur le web.
Conseils
J'utilise le logiciel pdfxchange gratuit, mais non libre pour que le stabilobossage copie le texte surligné.  (cf. mon site).
Je synchronise les fichiers entre mes PC via mon propre cloud (OwnCloud). Cette facilité m'a fait quitter Zotero.

