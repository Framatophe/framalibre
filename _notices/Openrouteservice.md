---
nom: "Openrouteservice"
date_creation: "Samedi, 27 octobre, 2018 - 15:35"
date_modification: "Mercredi, 12 mai, 2021 - 15:57"
logo:
    src: "images/logo/Openrouteservice.png"
site_web: "https://maps.openrouteservice.org/"
plateformes:
    - "Autre"
    - "le web"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Un service de calcul d'itinéraires (voiture, vélo, piéton, etc) basé sur les données OpenStreetMap !"
createurices: "GIScience Research Group"
alternative_a: "Google Maps"
licences:
    - "Licence MIT/X11"
tags:
    - "carte géographique"
    - "osm"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/openrouteservice"
---

Openrouteservice est un service web de calcul d'itinéraire créé en 2017 par des membres de l'université de Heidelberg, en Allemagne. Il repose sur les données collectées par OpenStreetMap.
Les itinéraires peuvent être calculés pour une grande diversité de moyens de transports : voitures, poids lourds, cyclistes, piétons. Les trajets sont configurables sur de nombreux aspects : plus court / plus rapide, type de routes à éviter, largeur / hauteur maximale, etc. Les informations sur le dénivelé sont disponibles (cf illustration).
Un mode isochrones est également proposé, avec ses propres options de configuration.

