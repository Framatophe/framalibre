---
nom: "LeCoinDuProf"
date_creation: "Dimanche, 28 avril, 2019 - 15:46"
date_modification: "Lundi, 10 mai, 2021 - 14:14"
logo:
    src: "images/logo/LeCoinDuProf.jpeg"
site_web: "http://lecoinduprof.com/"
plateformes:
    - "Android"
    - "Apple iOS"
langues:
    - "Français"
description_courte: "Une application mobile gratuite pour partager, échanger ou troquer le matériel pédagogique inutilisé."
createurices: "Maryon Quétel-Crevon, Alan Crevon"
alternative_a: "Le Bon Coin"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "éducation"
    - "école"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/lecoinduprof"
---

Enseignants, vous avez changé de niveau, de méthode pédagogique... ? Alors vous avez certainement du matériel, des manuels, logiciels... pédagogiques qui dorment dans vos placards.
LeCoinDuProf est fait pour vous. Il s'agit d'une application mobile qui permet de proposer du matériel à partager, échanger, troquer. Vous pouvez même y déposer vos demandes.
L'application est gratuite et le code a été libéré par ses auteurs.

