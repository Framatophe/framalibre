---
nom: "Ikki Boot"
date_creation: "Samedi, 27 janvier, 2018 - 08:22"
date_modification: "Vendredi, 1 décembre, 2023 - 10:24"
logo:
    src: "images/logo/Ikki Boot.png"
site_web: "https://ikkiboot.tuxfamily.org"
plateformes:
    - "GNU/Linux"
    - "Windows"
langues:
    - "Français"
    - "English"
description_courte: "Ikki Boot un live DVD/USB multiboot destiné au dépannage"
createurices: "Julien Billard"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
    - "live usb"
    - "linux"
    - "multiboot"
    - "dépannage"
    - "sauvegarde"
    - "restauration"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/ikki-boot"
---

Ikki Boot est un live DVD/USB multi-distribution destiné au dépannage et à l’administration. Il contient actuellement 3 live CD : Gparted,  Clonezilla et une version personnalisée de Porteus.
Porteus : un lice cd rapide, léger et modulaire basé sur Slackware Linux.
GParted : un éditeur de partition en mode graphique
Clonezilla : un logiciel de sauvegarde/restauration de partition ou de disques
Il inclut les utilitaires suivant :
MemTest86+ pour tester la mémoire vive
ShredOS qui supprime définitivement les données d’un disque
Super Grub Disk2 site : restauration de Grub
Hardware Detection Tool site : détection du matériel du poste

