---
nom: "Projet SK1"
date_creation: "Lundi, 13 mai, 2019 - 09:19"
date_modification: "Vendredi, 5 août, 2022 - 12:32"
logo:
    src: "images/logo/Projet SK1.png"
site_web: "https://sk1project.net"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Windows"
langues:
    - "English"
    - "Autres langues"
description_courte: "Programme d'illustration de qualité professionnelle"
createurices: ""
alternative_a: "CorelDraw, Adobe FreeHand, Adobe Illustrator"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "pao"
    - "vectoriel"
    - "graphisme"
    - "infographie"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/projet-sk1"
---

SK1 Projet est une suite graphique libre ayant pour objectif de remplacer efficacement les logiciels utilisés dans le domaine prépresse (préimpression). Bien qu'ayant une ambition professionnelle, la prise en main est réputée facile. Outre le format SVG, la suite prend en charge les format PDF, Postscript (couleurs CMJN). Elle est capable d'utiliser les formats CorelDraw ou AdobeIllistrator.

