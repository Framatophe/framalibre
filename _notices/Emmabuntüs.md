---
nom: "Emmabuntüs"
date_creation: "Jeudi, 22 décembre, 2016 - 14:44"
date_modification: "Vendredi, 4 janvier, 2019 - 16:40"
logo:
    src: "images/logo/Emmabuntüs.png"
site_web: "http://www.emmabuntus.org"
plateformes:
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Fruit de la rencontre entre l’humanitaire et le libre, une distribution GNU/Linux pour tous."
createurices: "Collectif Emmabuntüs"
alternative_a: "Microsoft Windows, MacOS"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
    - "distribution gnu/linux"
    - "debian"
    - "ubuntu"
    - "système d'exploitation (os)"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Emmabunt%C3%BCs"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/emmabunt%C3%BCs"
---

Fruit de la rencontre entre l’humanitaire et le libre, une distribution GNU/Linux pour tous.
Emmabuntüs est une distribution de bureau basée sur les versions d'Ubuntu ayant un support à long terme, et maintenant Debian Stable. Elle est accessible aux débutants et suffisamment légère en ressources pour être utilisée sur de vieux ordinateurs fonctionnant à l'origine sous Windows XP, Vista, ou 7. Elle inclut un grand nombre de programmes modernes pré-configurés pour une utilisation quotidienne dans le cadre d'un usage familial, un dock (barre de lancement d'applications), une installation aisée des logiciels non-libres et des codecs multimédias, et des scripts (programmes d'automatisation) pour une installation rapide sans recours à internet. La distribution propose les langues suivantes : l'allemand, l'anglais, l'arabe, l'espagnol, le français, l'italien, le portugais.

