---
nom: "Kore"
date_creation: "Samedi, 8 avril, 2017 - 14:28"
date_modification: "Dimanche, 3 décembre, 2017 - 19:08"
logo:
    src: "images/logo/Kore.png"
site_web: "http://kodi.tv/"
plateformes:
    - "Android"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Contrôlez votre media center Kodi depuis votre smartphone."
createurices: ""
alternative_a: ""
licences:
    - "Licence Apache (Apache)"
tags:
    - "internet"
    - "contrôle à distance"
    - "réseau"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/kore"
---

Kore vous permet de contrôler à distance votre instance Kodi depuis votre appareil Android. Naviguez dans votre liste de fichiers, films, séries, musiques, et contrôlez leur lecture (marche, arrêt, volume, etc), notamment avec les touches physiques.
L'application permet aussi de gérer les listes de lectures, obtenir des informations sur les fichiers, charger des sous-titres, et plus encore.
Kore nécessite un accès au réseau local ainsi qu'un paramétrage dans Kodi.

