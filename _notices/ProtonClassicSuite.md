---
nom: "ProtonClassicSuite"
date_creation: "Vendredi, 16 mars, 2018 - 12:44"
date_modification: "Jeudi, 29 mars, 2018 - 18:30"
logo:
    src: "images/logo/ProtonClassicSuite.png"
site_web: "https://protonclassic.com"
plateformes:
    - "GNU/Linux"
    - "Windows"
langues:
    - "Français"
description_courte: "ProtonClassicSuite cerne l'ensemble des données budgétaires de la collectivité territoriale"
createurices: "Thibault Mondary, Gilbert Mondary"
alternative_a: ""
licences:
    - "Licence CECILL (Inria)"
tags:
    - "métiers"
    - "reporting"
    - "collectivités territoriales"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/protonclassicsuite"
---

ProtonClassicSuite est un logiciel d'audit sur mesure, d'allocation de crédits et de reporting infra-annuel à destination des collectivités territoriales. Le passé récent, le présent et le futur proche sont indissociables en terme de gestion. Chaque période alimente ou puise sa justification dans une autre selon un cycle immuable :
1/ Une bonne connaissance du passé récent (ProtonClassicAuditeur) permet d'anticiper le meilleur niveau possible des prochaines allocations de crédits (ProtonClassicBudget)
2/ Une surveillance attentive de l'exécution budgétaire permet de terminer l'exercice dans les meilleures conditions (ProtonClassicReporting)
3/ Un exercice achevé devient partie intégrante du passé récent (ProtonClassicAuditeur)
4/ Ces nouvelles données permettent d'anticiper les prochaines allocations de crédits en tenant compte des derniers éléments ayant influencé la politique budgétaire de la collectivité (ProtonClassicBudget)
5/ etc.

