---
nom: "FlightAirMap"
date_creation: "Mercredi, 28 juin, 2017 - 09:46"
date_modification: "Mercredi, 12 mai, 2021 - 15:57"
logo:
    src: "images/logo/FlightAirMap.png"
site_web: "https://www.flightairmap.fr/"
plateformes:
    - "GNU/Linux"
    - "Windows"
    - "le web"
langues:
    - "Français"
    - "English"
description_courte: "Statistiques et suivi des avions, navires et traceurs GPS sur une carte 2D et 3D"
createurices: "ycarus"
alternative_a: "flightradar24"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "tracker"
    - "gps"
    - "carte géographique"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/flightairmap"
---

FlightAirMap est un projet libre permettant d'afficher les avions, navires et traceurs GPS sur une carte 2D et 3D.
Explorez les statistiques et les données sur un avion, compagnie aérienne, aéroport, navire, véhicule,...
De nombreuses sources sont compatibles : ADS-B au format SBS1 (dump1090, Radarcape,...), ou Virtual Radar Server, des simulations de vols (VATSIM, IVAO whazzup.txt, phpvms,...), les messages ACARS (acarsdec, acarsdeco2), AIS pour les navires, APRS pour les traceurs GPS,...
FlightAirMap est écrit en PHP et peut utiliser une base de données MySQL/MariaDB ou PostgreSQL.

