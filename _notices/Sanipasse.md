---
nom: "Sanipasse"
date_creation: "Mardi, 24 août, 2021 - 11:30"
date_modification: "Mardi, 24 août, 2021 - 11:30"
logo:
    src: "images/logo/Sanipasse.png"
site_web: "https://sanipasse.fr/"
plateformes:
    - "Autre"
    - "le web"
langues:
    - "Français"
description_courte: "Logiciel de vérification des certificats de test ou de vaccination covid-19"
createurices: "Ophir LOJKINE"
alternative_a: ""
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:

lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/sanipasse"
---

"Sanipasse est un logiciel libre de vérification des certificats de test ou de vaccination, et d'organisation d’événements respectueux des règles sanitaires.
Sanipasse a deux fonctions principales :
la création de listes d'invités qui peuvent confirmer eux-mêmes leur présence en scannant leur passe sanitaire,
la lecture de vos propres certificats, pour vérifier les informations qu'ils contiennent."
[extrait de la page à propos]

