---
nom: "Opale"
date_creation: "Vendredi, 21 avril, 2017 - 14:14"
date_modification: "Jeudi, 5 janvier, 2023 - 19:19"
logo:
    src: "images/logo/Opale.png"
site_web: "https://doc.scenari.software/Opale"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Opale est une chaîne éditoriale dédiée à la création de cours pour les enseignements secondaire et supérieur."
createurices: ""
alternative_a: "Microsoft Office, Microsoft Word"
licences:
    - "Licence CECILL (Inria)"
    - "Licence Publique Générale GNU (GNU GPL)"
    - "Licence publique générale limitée GNU (LGPL)"
    - "Licence Publique Mozilla (MPL)"
tags:
    - "éducation"
    - "chaîne éditoriale"
    - "diaporama"
    - "création de site web"
    - "traitement de texte"
    - "exerciseur"
    - "formation"
    - "enseignement"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/opale"
---

Opale est une chaîne éditoriale dédiée à la création de cours pour les enseignements du secondaire et du supérieur.
À partir d'une même source documentaire (texte + multimédia) Opale va permettre de générer des supports de cours papier, des cours multimédia en ligne, des diaporama de présentation pour l'enseignant, des exercices d'auto-évaluation…
Opale est le modèle documentaire Scenari qui regroupe le plus d'utilisateurs ; il possède de nombreuses extensions permettant des exports supplémentaires, des designs différents…
Opale est un des modèles documentaires de Scenari.

