---
nom: "easyappointments"
date_creation: "Jeudi, 11 mars, 2021 - 12:24"
date_modification: "Jeudi, 11 mars, 2021 - 12:24"
logo:
    src: "images/logo/easyappointments.png"
site_web: "https://easyappointments.org/"
plateformes:
    - "GNU/Linux"
    - "le web"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Logiciel permettant de prendre des rendez-vous en ligne dans le style de doctolib."
createurices: "Alex Tselegidi"
alternative_a: "doctolib"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:

lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/easyappointments"
---

Logiciel permettant de prendre des rendez-vous en ligne dans le style de doctolib.
La partie "backend" permet de gérer les utilisateurs, de 4 types:
 - administrateurs - permet d'ajouter les utilisateurs, de définir les plages horaires
 - secretaire peut ajouter / supprimer des rendez-vous de plusieurs exécutants
 - executant peut ajouter / supprimer des rendez-vous
 - clients
possiblité de synchroniser avec un agenda google.
easyappointment  envoie l'e-mail de confirmation.
Ecrit pour apache / php / mysql

