---
nom: "299Ko"
date_creation: "Mercredi, 31 août, 2022 - 16:46"
date_modification: "Dimanche, 19 mars, 2023 - 11:05"
logo:
    src: "images/logo/299Ko.png"
site_web: "https://299ko.ovh"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "FirefoxOS"
    - "Windows Mobile"
    - "Apple iOS"
    - "Autre"
langues:
    - "Français"
description_courte: "299Ko est un CMS libre flat-file écrit en PHP, modulable, et très facile à prendre en main."
createurices: "Jonathan C., Maxence C."
alternative_a: "wix, blogger, Blogspot, Wordpress"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "cms"
    - "blog"
    - "gestionnaire de contenus"
    - "création site web"
    - "flat-file"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/299ko"
---

299Ko est un CMS (gestionnaire de contenus) écrit en PHP, et open source.
Il est la reprise du CMS 99ko, duquel il a gardé la philosophie : L'anti usine à gaz.
En effet, 299Ko se veut simple à administrer, à utiliser et à modifier, grâce à un code source allant au plus simple, sans pour autant négliger sur la sécurité.
Pas besoin de base de données : 299Ko stocke tout le contenu de votre site dans des fichiers .json, qui facilitent grandement les sauvegardes et font de lui un CMS très rapide.
Il est livré avec plusieurs plugins indispensables permettant de créer un blog, une galerie photo, des pages, des menus ou un formulaire de contact. Le tout pourra être rédigé avec un éditeur WYSIWYG (TinyMCE), mais un plugin pour rédiger vos articles en MarkDown est déjà disponible.
Coté thème, celui par défaut est simple et aisément personnalisable, mais vous pourrez en télécharger d'autres ou même créer le votre.

