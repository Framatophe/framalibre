---
nom: "XML Copy Editor"
date_creation: "Jeudi, 17 décembre, 2015 - 09:42"
date_modification: "Mardi, 10 janvier, 2017 - 04:20"
logo:
    src: "images/logo/XML Copy Editor.jpg"
site_web: "http://xml-copy-editor.sourceforge.net/"
plateformes:
    - "GNU/Linux"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "XML Copy Editor est un éditeur XML qui a de nombreuses caractéristiques..."
createurices: "Gerald Schmidt, Zane U. Ji"
alternative_a: "Oxygen XML Editor"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "développement"
    - "éditeur"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/xml-copy-editor"
---

XML Copy Editor est un éditeur XML qui a les caractéristiques suivantes pour la version 1.2.1.3 (current)
DTD/XML Schema/RELAX NG validation
XSLT
XPath
Impression rapide
La coloration syntaxique
Plie ou déplie les balises
Complétion des tags
Verrouillage des tags
Édition libre de tag
Vérification orthographique et du style
Intègre la prise en charge des formats XHTML, XSL, DocBook et TEI
Lossless importe et exporte des documents de Microsoft Word (Windows seulement)

