---
nom: "ComiXed"
date_creation: "Jeudi, 1 août, 2019 - 12:45"
date_modification: "Lundi, 10 mai, 2021 - 14:36"
logo:
    src: "images/logo/ComiXed.png"
site_web: "https://github.com/mcpierce/comixed/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "ComiXed veut être le système de gestion numérique de bandes dessinées ultime"
createurices: "Darryl L. Pierce"
alternative_a: "ComicBookLover"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "bande dessinée"
    - "cbr"
    - "cbz"
    - "comics"
    - "gestionnaire de fichiers"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/comixed"
---

Une application complète de gestion de bandes dessinées numériques.
Avec ComiXed, vous pourrez:
* importer vos bandes dessinées numériques,
* standardisez vos bandes dessinées sur un seul format préféré,
* trouver et supprimer facilement les pages en double sur les bandes dessinées,
* bloquer les pages, en marquant automatiquement les publicités pour suppression,
* extraire des méta-données de sources en ligne telles que ComicVine, et
* accédez à vos bandes dessinées numériques à partir d'un lecteur compatible OPDS.
Tout cela dans un programme libre et open source.
ComiXed s'exécute en arrière-plan et toutes les interactions des utilisateurs se font via un navigateur.

