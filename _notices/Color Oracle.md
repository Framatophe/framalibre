---
nom: "Color Oracle"
date_creation: "Lundi, 1 mai, 2017 - 15:19"
date_modification: "Lundi, 1 mai, 2017 - 16:09"
logo:
    src: "images/logo/Color Oracle.png"
site_web: "http://colororacle.org/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
description_courte: "Simulateur de daltonisme sur tout l'écran"
createurices: "Bernhard Jenny, Nathaniel Vaughn Kelso"
alternative_a: ""
licences:
    - "Licence MIT/X11"
tags:
    - "création"
    - "simulation"
    - "design"
    - "couleur"
    - "handicap"
    - "accessibilité"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/color-oracle"
---

Color Oracle est un simulateur de daltonisme pour Linux*, Mac OS et Windows*. Il applique un "filtre" sur tout l'écran pour simuler la vision des daltoniens, les trois variantes du daltonisme sont utilisables.
On peut lancer automatiquement Color Oracle au démarrage du système ou bien l'utiliser ponctuellement.
Color Oracle est indispensable aux designers et créateurs qui veulent s'assurer de la bonne perception de leurs créations par le plus grand nombre.
(* Requiert Java 6 ou version ultérieure)

