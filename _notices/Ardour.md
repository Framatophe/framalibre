---
nom: "Ardour"
date_creation: "Jeudi, 29 décembre, 2016 - 08:39"
date_modification: "jeudi, 4 janvier, 2024 - 09:26"
logo:
    src: "images/logo/Ardour.png"
site_web: "https://ardour.org/"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
    - "Autre"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "La station audio-numérique libre de référence, pour dire au revoir à Cubase et Pro Tools !"
createurices: "Paul Davis"
alternative_a: "Cubase, Pro Tools, Ableton Live"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "création"
    - "musique"
    - "daw"
    - "enregistrement"
    - "mixage"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Ardour"
lien_exodus: ""
identifiant_wikidata: "Q640004"
mis_en_avant: "non"
redirect_from: "/content/ardour"
---

Ardour est une station audio-numérique (DAW) qui permet de gérer ses créations audios depuis l'enregistrement jusqu'au mixage et au mastering. L'interface est claire et complète, le logiciel permet de séquencer autant de pistes que nécessaire, de créer des bus, d'interfacer du MIDI, d'appliquer des effets avec ou sans l'aide de plugins LADSPA ou LV2, etc. Import/export avec les formats audios usuels.
Le tout est compatible avec le serveur audio Jack.


