---
nom: "Xpdf"
date_creation: "Samedi, 27 avril, 2019 - 18:52"
date_modification: "Jeudi, 20 mai, 2021 - 16:39"
logo:
    src: "images/logo/Xpdf.png"
site_web: "https://www.xpdfreader.com/index.html"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
description_courte: "Lecteur PDF multiplateforme"
createurices: "Derek Noonburg"
alternative_a: "Adobe Acrobat Reader"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
    - "lecteur pdf"
    - "convertisseur"
    - "extracteur"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Xpdf"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/xpdf"
---

Xpdf a été publié pour la première fois en 1995. Outre sa fonction de lecteur de fichiers PDF, ce petit programme simple d'utilisation dispose de plusieurs outils de conversion :
pdftotext: convertit le PDF en texte
pdftops: convertit le PDF en PostScript
pdftoppm: convertit les pages PDF en netpbm (PPM / PGM / PBM)
pdftopng: convertit les pages PDF en images PNG
pdftohtml: convertit le PDF en HTML
pdfinfo: extrait les métadonnées PDF
pdfimages: extrait les images brutes des fichiers PDF
pdffonts: répertorie les polices utilisées dans les fichiers PDF

