---
nom: "Planning IAE"
date_creation: "Lundi, 13 mars, 2023 - 15:58"
date_modification: "Lundi, 13 mars, 2023 - 17:22"
logo:
    src: "images/logo/Planning IAE.png"
site_web: "https://www.philnoug.com/planning"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "FirefoxOS"
    - "Windows Mobile"
    - "Apple iOS"
    - "Autre"
    - "Web"
langues:
    - "Français"
description_courte: "Planning à l’usage d’établissement de l’enseignement supérieur"
createurices: "Philippe NOUGAILLON"
alternative_a: "Google Agenda"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "planning"
    - "agenda"
    - "université"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Collaborative, flexible et simple d'utilisation, cette application a été initialement développée pour l'IAE de Paris (Sorbonne Business School - Université Paris 1 Panthéon-Sorbonne) dans le but d'offrir une alternative à l'outil Hyperplanning, qui était complexe et surdimensionné pour un usage interne.
De la création des planning à l’édition des feuille d'émargement, en passant par les états de services, cette application assure la gestion complète et collaborative des processus de planification des cours, formation, intervenants, etc.

