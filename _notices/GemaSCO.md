---
nom: "GemaSCO"
date_creation: "lundi, 15 avril, 2024 - 16:29"
date_modification: "lundi, 15 avril, 2024 - 16:29"
logo:
    src: "images/logo/GemaSCO.png"
site_web: "http://openacademie.fr/static/book.png"
plateformes:
    - "Windows"
langues:

description_courte: "GemaSCO assure l'inventaire des manuels scolaires prêtés aux élèves"
createurices: "OpenAcadémie, startup d'Etat de l'Education nationale"
alternative_a: "BCDI"
licences:
    - "Licence Publique Union Européenne (EUPL)"
tags:
    - "Manuels scolaires"
    - "Prêt"
    - "Inventaire"
    - "Code-barres"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

Importez la base élèves de l'établissement, créez votre catalogue de manuels, imprimez les code-barres que vous collerez sur les couvertures des ouvrages : vous êtes en pôle position pour prêter des milliers de manuels aux élèves à la rentrée, les récupérer en juin, avec une gestion fine de l'inventaire, des dégradations, etc.
