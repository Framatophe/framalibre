---
nom: "Fotowall"
date_creation: "Vendredi, 14 avril, 2017 - 01:04"
date_modification: "dimanche, 28 janvier, 2024 - 12:07"
logo:
    src: "images/logo/Fotowall.png"
site_web: "http://www.enricoros.com/opensource/fotowall/index.html"
plateformes:
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Créez des patchworks de photos !"
createurices: ""
alternative_a: "Picasa"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "création"
    - "image"
    - "graphisme"
    - "photo"
    - "manipulation d'images"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/fotowall"
---

Fotowall est un logiciel, très facile à prendre en main, qui permet de créer des patchworks de photos: on choisit des photos qu'on dispose comme on veut, on ajoute très facilement du texte qu'on peut tordre dans tous les sens, puis des effets, des cadres, des transitions, etc, à partir de modèles si on veut. Trop facile de créer des assemblages délires et colorés pour des évènements festifs ou des souvenirs.
Si vous n'avez jamais réussi à créer des images avec des logiciels plus complexes tels que the Gimp, Fotowall vous ravira ! Pour tout public.

**Le logiciel n'a plus de nouvelle version depuis 2017, parce que ses mainteneur·ices considèrent qu'il est terminé.**

Voir aussi : Hugin pour créer des photomontages panoramiques ou sphériques, sans raccord visible.
