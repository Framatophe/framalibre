---
nom: "Aegis Authenticator"
date_creation: "Samedi, 25 avril, 2020 - 16:59"
date_modification: "Samedi, 25 avril, 2020 - 16:59"
logo:
    src: "images/logo/Aegis Authenticator.png"
site_web: "https://getaegis.app/"
plateformes:
    - "Android"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Aegis est une application libre de gestion de l'authentification à deux facteurs pour Android."
createurices: ""
alternative_a: "Google Authenticator, Authy"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "sécurité"
    - "authentification"
lien_wikipedia: ""
lien_exodus: "https://reports.exodus-privacy.eu.org/fr/reports/com.beemdevelopment.aegis/lates…"
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/aegis-authenticator"
---

Aegis est une application libre de gestion de l'authentification à deux facteurs pour Android. L'application est compatible avec les algorithmes TOTP et HOTP, ce qui permet d'assurer une compatibilité avec la majorité des services.
Les identifiants stockés dans l'application peuvent être chiffrés avec l'algorithme de chiffrement AES-256, et l'application peut être verrouillée grâce à un mot de passe, ou au verrouillage biométrique (empreinte digitale, reconnaissance faciale, selon le modèle de l'appareil).
Aegis offre également quelques options de personnalisation, comme un thème sombre activable à souhait, et la possiblité de définir des icônes personnalisées sur les entrées, pour une meilleure visibilité.

