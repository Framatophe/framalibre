---
nom: "Pinta"
date_creation: "Mercredi, 22 mars, 2017 - 00:55"
date_modification: "Mercredi, 22 mars, 2017 - 00:56"
logo:
    src: "images/logo/Pinta.png"
site_web: "https://pinta-project.com"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Logiciel de graphisme simple et intuitif."
createurices: ""
alternative_a: "Paint.net, Microsoft Paint"
licences:
    - "Licence MIT/X11"
tags:
    - "création"
    - "graphisme"
    - "infographie"
    - "dessin"
    - "brosse"
    - "peinture"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Pinta_(logiciel)"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/pinta"
---

Logiciel de graphisme simple et intuitif, permettant de dessiner ou d'éditer des images facilement.
Tous les outils de dessin standard sont présents : lignes, polygones, dessin à main levée, courbes de Béziers, texte... De même qu'un système de calques et de nombreux effets : croquis à l'encre ou au crayon, peinture à l'huile, détection des contours, suppression des yeux rouges, nuages, différents type de flou, et bien d'autres encore.

