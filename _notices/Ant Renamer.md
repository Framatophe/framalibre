---
nom: "Ant Renamer"
date_creation: "Dimanche, 2 septembre, 2018 - 16:25"
date_modification: "Lundi, 25 mai, 2020 - 11:03"
logo:
    src: "images/logo/Ant Renamer.png"
site_web: "http://www.antp.be/software/renamer/fr"
plateformes:
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Ant Renamer est un programme de renommage de fichiers et de dossier"
createurices: "Antoire Potten"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
    - "renommage de fichiers"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Ant_Renamer"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/ant-renamer"
---

Ant Renamer est un programme permettant de renommer facilement de grandes quantités de fichiers et dossiers selon des critères définis. Il permet ainsi de renommer en modifiant et/ou ajoutant des chaînes de caractères, par énumération ou depuis un fichier. Il permet également d'extraire certaines métadonnées des fichiers concernés (date/heure de modification, tags mp3 ou infos EXIF).

