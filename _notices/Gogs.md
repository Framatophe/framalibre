---
nom: "Gogs"
date_creation: "Jeudi, 23 février, 2017 - 14:53"
date_modification: "Jeudi, 23 février, 2017 - 16:01"
logo:
    src: "images/logo/Gogs.png"
site_web: "https://gogs.io/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "FirefoxOS"
    - "Windows Mobile"
    - "Apple iOS"
    - "Autre"
    - "le web"
langues:
    - "English"
description_courte: "Gogs est une interface web basée sur git et une bonne alternative à GitHub."
createurices: "Jiahua Chen"
alternative_a: "Github"
licences:
    - "Licence MIT/X11"
tags:
    - "git"
    - "gestion de version"
    - "forge logicielle"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Gogs_(logiciel)"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/gogs"
---

Gogs est une interface web au gestionnaire de version git. Il est très léger et fonctionne très bien sur des micro-ordinateurs type Raspberry Pie. Très pratique pour auto-héberger ses différents projets. Gogs a une interface proche de GitHub.

