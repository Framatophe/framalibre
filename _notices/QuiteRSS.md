---
nom: "QuiteRSS"
date_creation: "Lundi, 29 juillet, 2019 - 10:19"
date_modification: "Mercredi, 12 mai, 2021 - 16:40"
logo:
    src: "images/logo/QuiteRSS.png"
site_web: "https://quiterss.org"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Un agrégateur de flux RSS et Atom facile et libre"
createurices: "Aleksey Khokhryakov, Shilyaev Egor"
alternative_a: "Feedly, feedreader"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
    - "lecteur de flux rss"
    - "agrégateur"
    - "atom"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/quiterss"
---

QuiteRSS est un agrégateur de flux RSS/Atom multiplateforme. Il dispose d’une interface simple permettant de classer ses flux dans des dossiers et sous-dossiers, mais également de fonctionnalités plus avancées permettant d’interagir avec les articles : étiquetage, partage (via email, ou des services comme Twitter, Pocket, Linkedin…), filtres, etc.
Il intègre également une fonctionnalité "Adblock" autorisant le filtrage des publicités.
Enfin, ses nombreuses options permettent de régler le fonctionnement des notifications, la fréquence de mise à jour des flux, ou encore l’apparence du logiciel.

