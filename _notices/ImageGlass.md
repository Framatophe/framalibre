---
nom: "ImageGlass"
date_creation: "Mercredi, 14 octobre, 2020 - 23:10"
date_modification: "Mardi, 3 mai, 2022 - 18:45"
logo:
    src: "images/logo/ImageGlass.png"
site_web: "https://imageglass.org/"
plateformes:
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Une visionneuse rapide et polyvalente."
createurices: "Dương Diệu Pháp"
alternative_a: "Windows 10 Photos, Photos"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "photo"
    - "image"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/imageglass"
---

Windows 10 Photos est très lent! Voici donc une alternative simple et rapide pour ouvrir plus de 70 formats d'images.
La navigation entre les images est tout aussi rapide, avec des miniatures en aperçu.
Les images peuvent aussi être pivotées, redimensionnées ou ajustées à l'écran.

