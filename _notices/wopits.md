---
nom: "wopits"
date_creation: "Lundi, 20 juillet, 2020 - 14:14"
date_modification: "mercredi, 24 janvier, 2024 - 15:10"
logo:
    src: "images/logo/wopits.png"
site_web: "https://github.com/esaracco/wopits"
plateformes:
    - "le web"
    - "GNU/Linux"
    - "Autre"
langues:
    - "Français"
    - "English"
description_courte: "Une application avec laquelle vous pouvez gérer toutes sortes de projets via des post-its."
createurices: "Emmanuel Saracco"
alternative_a: "Padlet, PopLab, Lino, trello"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "gestion de projet"
    - "tableau interactif"
    - "memo"
    - "travail collaboratif"
    - "édition collaborative"
    - "décentralisation"
    - "planning"
    - "kanban"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/wopits"
---

Une application pour gérer des projets en ligne en utilisant des post-its pour partager et collaborer.
Vous pouvez éditer plusieurs murs de post-its en même temps, déposer des pièces jointes, insérer des images, créer des relations etc.
Une gestion des groupes permet de contrôler finement le partage des données, et un espace de discussion est disponible pour chaque mur.
wopits est exempt de Node.js & React.js et utilise Swoole comme serveur de Tâches & WebSocket.
