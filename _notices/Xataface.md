---
nom: "Xataface"
date_creation: "Lundi, 28 mars, 2016 - 20:04"
date_modification: "Dimanche, 13 mars, 2022 - 20:15"
logo:
    src: "images/logo/Xataface.png"
site_web: "https://shannah.github.io/xataface-manual"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Xataface est un simpliciel comparé aux framework"
createurices: "Steve Hannah"
alternative_a: "Filemaker, Microsoft Access"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "développement"
    - "cms"
    - "base de données"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/xataface"
---

Xataface est un simpliciel comparé aux frameworks existants :
créez vos tables en base de données
dézippez la dernière version de Xataface
adaptez 2 variables et la connexion BD à votre environnement
vous venez de terminer 80% de votre application !
C'est à la fois un framework
gérer des utilisateurs
des listings
des formulaires saisie/consultation/recherche
des dépôts de pièces-jointes
des modules : calendrier, mail, PDF, etc.
et une API
manipuler les instances et collections PHP
Ressources :
l'ancien site officiel (wiki et documentation de base) : http://xataface.com
communauté : https://groups.google.com/g/xataface

