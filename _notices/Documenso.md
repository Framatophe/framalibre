---
nom: "Documenso"
date_creation: "jeudi, 11 avril, 2024 - 17:05"
date_modification: "jeudi, 11 avril, 2024 - 17:05"
logo:
    src: "images/logo/Documenso.webp"
site_web: "https://documenso.com/"
plateformes:
    - "le web"
langues:
    - "English"
description_courte: "L'alternative OpenSource pour signer vos documents"
createurices: ""
alternative_a: ""
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "signature"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---


