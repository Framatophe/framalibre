---
nom: "ZeGrapher"
date_creation: "Dimanche, 3 février, 2019 - 23:04"
date_modification: "Lundi, 10 mai, 2021 - 14:28"
logo:
    src: "images/logo/ZeGrapher.png"
site_web: "https://zegrapher.com"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "ZeGrapher est un logiciel de tracé de courbes sur le plan, rapide et simple d'utilisation."
createurices: "Adel Kara Slimane"
alternative_a: "Sinequanon, Desmos"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "science"
    - "2d"
    - "donnée"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/zegrapher"
---

ZeGrapher est un logiciel de tracé de courbes léger, simple et rapide. Disponible en français et anglais, il propose les fonctionnalités suivantes:
 Le tracé de fonctions, suites et équations paramétriques.
 Le tracé de données expérimentales. Il permet l'interpolation polynomiale, l'import et l'export de fichiers CSV. 
 Navigation et interaction avec le graphique à la souris: déplacer la vue, zoomer de différentes manières.
 Export au format PDF ou image.
 Personnalisable: couleurs, police. 

