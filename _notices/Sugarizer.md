---
nom: "Sugarizer"
date_creation: "Dimanche, 12 décembre, 2021 - 22:16"
date_modification: "Dimanche, 12 décembre, 2021 - 22:16"
logo:
    src: "images/logo/Sugarizer.png"
site_web: "https://sugarizer.org"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "Apple iOS"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Sugarizer est une suite pédagogique libre et gratuite."
createurices: "Lionel Laské"
alternative_a: ""
licences:
    - "Licence Apache (Apache)"
tags:
    - "éducation"
    - "pédagogie"
    - "enseignement"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/sugarizer"
---

Sugarizer est une suite pédagogique libre et gratuite. Son interface reprend les principes ergonomiques de Sugar, la suite logicielle développée par le projet One Laptop Per Child, et aujourd'hui utilisée par des milliers d'enfants dans le monde.
Sugarizer a été conçu pour les enfants entre 6 et 12 ans. L'interface de Sugarizer est intuitive et emploie des métaphores que peuvent comprendre les enfants sans savoir encore lire.
Sugarizer intègre plus de 50 activités pédagogiques différentes pour jouer, créer, simuler, ...

