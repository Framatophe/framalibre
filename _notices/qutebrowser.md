---
nom: "qutebrowser"
date_creation: "Mercredi, 19 avril, 2017 - 17:36"
date_modification: "Mercredi, 12 mai, 2021 - 14:52"
logo:
    src: "images/logo/qutebrowser.png"
site_web: "https://www.qutebrowser.org/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
description_courte: "Navigateur web simple et rapide orienté clavier avec une interface graphique minimale."
createurices: "Florian Bruhin"
alternative_a: "Google Chrome, Chrome, Opera"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
    - "navigateur web"
    - "commande clavier"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/qutebrowser"
---

Qutebrowser est un navigateur web qui permet d'avoir une navigation orientée clavier plus rapide et plus efficace.
Ce navigateur fonctionne de manière modale: il est notamment inspiré du logiciel Vim ou des extensions Vimperator pour Firefox ou Vimium pour Chrome et Chromium. Ce mode de fonctionnement peut être déroutant à prendre en main pour un néophyte mais, avec le temps, permet d'être plus productif.
Par exemple, une fonctionnalité typique est de pouvoir suivre ou de copier les liens hypertexte sans toucher à la souris. Il est aussi possible d'utiliser des touches du clavier (ou des commandes) pour, par exemple, se déplacer dans l'historique de navigation de l'onglet courant.
Une autre particularité de Qutebrowser est qu'on peut l'étendre en écrivant des commandes avec le langage Python.
Code source: https://github.com/qutebrowser/qutebrowser/

