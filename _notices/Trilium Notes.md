---
nom: "Trilium Notes"
date_creation: "Vendredi, 10 mars, 2023 - 21:10"
date_modification: "Vendredi, 10 mars, 2023 - 21:10"
logo:
    src: "images/logo/Trilium Notes.png"
site_web: "https://github.com/zadam/trilium"
plateformes:
    - "GNU/Linux"
    - "Windows"
    - "Autre"
langues:
    - "English"
description_courte: "Trilium Notes est une application hiérarchique de prise de notes."
createurices: "zadam"
alternative_a: "Notion"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "bureautique"
    - "note"
    - "notes"
    - "prise de notes"
    - "gestion de tâches"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/trilium-notes"
---

Trilium Notes est une application hiérarchique de prise de notes. Elle permet non seulement des notes, mais aussi de les organiser de manière hiérarchique. Elle dispose aussi de nombreuses autres fonctionnalités comme la gestion de tâches et la création de relations entre chaque note (Exemple : faire un arbre de famille). L'application donne aussi la possibilité de l'héberger sur un serveur pour ainsi relier toutes vos instances ensemble.

