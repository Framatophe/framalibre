---
nom: "MARP"
date_creation: "Vendredi, 6 janvier, 2017 - 01:48"
date_modification: "Jeudi, 10 décembre, 2020 - 11:30"
logo:
    src: "images/logo/MARP.png"
site_web: "https://marp.app"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
description_courte: "MARP est un logiciel sous licence MIT permetant de faire des présentations en MarkDown"
createurices: "Yhatt"
alternative_a: "Microsoft PowerPoint, Prezi"
licences:
    - "Licence MIT/X11"
tags:
    - "bureautique"
    - "présentation"
    - "markdown"
    - "diaporama"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/marp"
---

MARP est un logiciel qui vous permet de créer vos présentations en Markdown.
Via ce logiciel, vous écrivez votre texte en Markdown et vous avez en direct les modifications.
Vous pouvez utiliser des formules mathématiques et des émoticones.
Exportation en PDF
2 thèmes sont disponibles.

