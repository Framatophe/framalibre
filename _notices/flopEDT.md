---
nom: "flopEDT"
date_creation: "Samedi, 16 juin, 2018 - 08:10"
date_modification: "Lundi, 15 juillet, 2019 - 23:44"
logo:
    src: "images/logo/flopEDT.png"
site_web: "https://framagit.org/flopedt/FlOpEDT"
plateformes:
    - "GNU/Linux"
langues:
    - "Français"
description_courte: "flopEDT: Créateur et gestionnaire d'emplois du temps Flexible et OpenSource"
createurices: "Iulian OBER, Paul RENAUD-GOUD, Pablo SEBAN"
alternative_a: "EDT, Edt-soft, UnDeuxTEMPS"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "éducation"
    - "emploi du temps"
    - "agenda"
    - "temps"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/flopedt"
---

flopEDT est un outil de gestion d'emplois du temps comprenant :
- une application web permettant aux utilisateurs
  * d'exprimer leurs contraintes et préférences
  * de modifier l'emploi du temps
- un moteur de génération d'emplois du temps qui respectent les contraintes et maximisent la satisfaction générale.

