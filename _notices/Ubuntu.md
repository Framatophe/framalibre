---
nom: "Ubuntu"
date_creation: "Dimanche, 7 juillet, 2019 - 15:28"
date_modification: "mercredi, 13 mars, 2024 - 11:58"
logo:
    src: "images/logo/Ubuntu.png"
site_web: "https://ubuntu.com/"
plateformes:
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Système d'exploitation libre facile à installer et à administrer."
createurices: "Mark Shuttleworth, Canonical"
alternative_a: "Microsoft Windows, MacOS"
licences:
    - "Multiples licences"
tags:
    - "système"
    - "système d'exploitation (os)"
    - "distribution gnu/linux"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Ubuntu_(syst%C3%A8me_d%27exploitation)"
lien_exodus: ""
identifiant_wikidata: "Q381"
mis_en_avant: "oui"
redirect_from: "/content/ubuntu"
---

Basé sur Debian, Ubuntu est un système d'exploitation libre et gratuit axé sur la simplicité d'installation et d'utilisation. Il a permis (et permet toujours) à de nombreuses personnes de passer à un système GNU/Linux sans trop de complications.
Les mises à jour du système sortent tous les six mois.
