---
nom: "WordsMatter"
date_creation: "Samedi, 8 août, 2020 - 17:08"
date_modification: "Samedi, 23 juillet, 2022 - 10:21"
logo:
    src: "images/logo/WordsMatter.png"
site_web: "https://gitlab.com/julien.wilhelm/WordsMatter"
plateformes:
    - "GNU/Linux"
    - "Windows"
    - "Android"
    - "Autre"
langues:
    - "Français"
    - "English"
description_courte: "Rédigez depuis l'Application ; partagez grâce à l'API connectée."
createurices: "Julien Wilhelm"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "cms"
    - "blog"
    - "écoresponsable"
    - "création de site web"
    - "flat-file"
    - "responsive"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/wordsmatter"
---

Rédigez depuis l'Application ; partagez grâce à l'API connectée. WordsMatter est un module de gestion de blog 100% autonome, libre et gratuit. Son efficience en fait une alternative écoresponsable aux Systèmes de Gestion de Contenu traditionnels (CMS).

