---
nom: "UltraFastCopy"
date_creation: "samedi, 16 mars, 2024 - 13:59"
date_modification: "samedi, 16 mars, 2024 - 13:59"
logo:
    src: "images/logo/UltraFastCopy.jpg"
site_web: "https://ultrafastcopy.com/"
plateformes:
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Permet des copies de fichiers/dossiers extrêmement rapide, en remplaçant le copier-coller classique de manière transparente"
createurices: "https://www.tr-softwares.com/"
alternative_a: "TeraCopy"
licences:
    - "Autre"
tags:
    - "copie"
    - "document"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

La copie est optimisée en fonction du support d'origine et de destination (SSD, disque dur, Samba...)

La gestion des copies des liens symboliques est également plus optimisée que la fonction de base de Windows.

Vous bénéficierez d'options pour la copie que vous pourrez personnaliser : nouveau non, copie ou non des attributs, dates, droits...

Le programme permet également le glisser-déposer depuis le logiciel 7Zip.

A noter que la version professionnelle payante rajoute la fonctionnalité consistant à supprimer une grosse quantité de fichiers très rapidement (avec gestion automatique des erreurs : fichiers en cours d'utilisation, droits insuffisants, chemin racine trop long...).
