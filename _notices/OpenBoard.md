---
nom: "OpenBoard"
date_creation: "Lundi, 2 janvier, 2017 - 17:51"
date_modification: "Mardi, 14 mars, 2017 - 21:50"
logo:
    src: "images/logo/OpenBoard.png"
site_web: "http://openboard.ch/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Logiciel pour tableau numérique interactif"
createurices: ""
alternative_a: "Interwrite Workspace, SMART Notebook"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "éducation"
    - "tableau interactif"
    - "école"
lien_wikipedia: "https://fr.wikipedia.org/wiki/OpenBoard"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/openboard"
---

OpenBoard est un logiciel pouvant être utilisé avec n'importe quel tableau interactif quelque soit sa marque.
Comme tout logiciel de TNI, OpenBoard propose un espace où peuvent s’afficher textes, images, vidéos, annotations manuelles, etc. Travailler sur plusieurs pages est évidemment possible, de même que préparer un cours entier. Le logiciel comporte différentes applications et outils qui apportent un plus indéniable.
OpenBoard est un fork du logiciel OpenSankoré qui est lui-meême l'évolution, libérée, d'Uniboard.

