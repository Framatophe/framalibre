---
nom: "Pioneer Spacesim"
date_creation: "Jeudi, 19 novembre, 2020 - 21:00"
date_modification: "Jeudi, 19 novembre, 2020 - 21:00"
logo:
    src: "images/logo/Pioneer Spacesim.png"
site_web: "https://pioneerspacesim.net/"
plateformes:
    - "GNU/Linux"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Un clone de Fontier : Elite."
createurices: ""
alternative_a: "Elite : Dangerous"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "jeu"
    - "3d"
    - "science-fiction"
    - "espace"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/pioneer-spacesim"
---

Pioneer est une tentative de re-création du jeu mythique "Frontier : Elite II".
Pioneer est un jeu de "space opera" mono-joueur, se déroulant dans une galaxie comprenant des milions de planètes.
Dans ce jeu, vous êtes libre de choisir votre destinée, soit en partant à l'exploration de planètes lointaines, soit en menant une vie de pirate, ou en faisant du commerce entre planètes, ou encore en effectuant des mission parfois dangereuses.

