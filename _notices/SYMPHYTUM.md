---
nom: "SYMPHYTUM"
date_creation: "Dimanche, 24 novembre, 2019 - 22:49"
date_modification: "Dimanche, 24 novembre, 2019 - 22:49"
logo:
    src: "images/logo/SYMPHYTUM.png"
site_web: "https://github.com/giowck/symphytum#symphytum"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
description_courte: "Symphytum permet de concevoir et d’éditer des bases de données maison de façon intuitive."
createurices: "GIOWISYS Software UG"
alternative_a: "Filemaker Access"
licences:
    - "Berkeley Software Distribution License (BSD)"
tags:
    - "bureautique"
    - "base de données"
    - "gestionnaire de base de données"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/symphytum"
---

Symphytum permet de concevoir et d’éditer des bases de données maison de façon intuitive. Ce qui rend ce logiciel accessible à tous est l’interface claire qui guide pas à pas la création de la première base de données.
Le logiciel est libre et multi-plateforme (basé sur QT5 et SQLite pour gérer les données)
12 Modèles de champ peut être mis en place dans l’enregistrement (Texte, Case à Cocher, Images, Fichiers …)

