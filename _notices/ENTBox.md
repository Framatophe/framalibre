---
nom: "ENTBox"
date_creation: "Vendredi, 4 septembre, 2020 - 10:19"
date_modification: "Lundi, 10 mai, 2021 - 14:26"
logo:
    src: "images/logo/ENTBox.jpg"
site_web: "http://entbox.ticedu.fr/"
plateformes:
    - "Windows"
    - "Autre"
langues:
    - "Français"
description_courte: "Créer, partager et collaborer dans un environnement de travail numérique."
createurices: "Marc-Aurélien Chardine"
alternative_a: "Salle de Classe"
licences:
    - "Creative Commons -By-Sa"
tags:
    - "éducation"
    - "espace numérique de travail (ent)"
    - "école"
    - "windows"
    - "raspberry pi"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Espace_num%C3%A9rique_de_travail"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/entbox"
---

ENTBox est une interface permettant aux élèves de collaborer et de créer des productions numériques avec une grande efficacité.
Retrouvez la documentation sur :http://entbox.ticedu.fr/downloads/entbox.pdf
Quelques ressources supplémentaireshttps://www.youtube.com/watch?v=EtZ93UECNLM

