---
nom: "OsmAnd~"
date_creation: "mercredi, 27 décembre, 2023 - 13:55"
date_modification: "jeudi, 11 avril, 2024 - 10:24"
logo:
    src: "images/logo/OSMAnd~.svg"
site_web: "https://osmand.net/"
plateformes:
    - "Apple iOS"
    - "Android"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Application libre de cartes hors ligne, navigation et enregistrement de traces GPS, basée sur les données OpenStreetMap"
createurices: "OsmAnd BV"
alternative_a: "Google Maps"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "cartographie"
    - "android"
    - "gps"
    - "OpenStreetMap"
    - "routing"
lien_wikipedia: "https://fr.wikipedia.org/wiki/OsmAnd"
lien_exodus: "https://reports.exodus-privacy.eu.org/fr/reports/net.osmand.plus/latest/"
identifiant_wikidata: "Q3087512"
mis_en_avant: "non"

---

OsmAnd~ est une application complète pour la cartographie sur mobile, ses cartes sont consultables hors-ligne une fois téléchargées. Elle permet également la navigation, l'affichage et la recherche des points d'intérêts par thématique. Il est également possible d'enregistrer ses traces GPS et les exporter dans divers formats. 

Elle dispose de nombreuses extensions, notamment celle pour l'édition des données d'OpenStreetMap.

Les mises à jour des cartes sont mensuelles. La mise à jour toutes les heures est disponible sur abonnement et pour les contributeurs·trices OpenStreetMap réguliers (30 modifications dans les 2 derniers mois).

Une version gratuite et limitée est disponible sur Google Play et les versions complètes sont payantes sur Google Play et le App Store d'Apple (voir le comparatif dans ce [tableau](https://osmand.net/docs/user/purchases/android) - en anglais)
OsmAnd~ est gratuite sur le dépôt d'applications libres [F-Droid](https://f-droid.org).
