---
nom: "VNote"
date_creation: "mercredi, 10 janvier, 2024 - 15:47"
date_modification: "mercredi, 10 janvier, 2024 - 17:01"
logo:
    src: "images/logo/VNote.png"
site_web: "https://vnotex.github.io/vnote"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "English"
    - "Autres langues"
description_courte: "Un éditeur markdown léger offrant une expérience confortable"
createurices: "Le Tan"
alternative_a: "MSWord"
licences:
    - "Licence publique générale limitée GNU (LGPL)"
tags:
    - "markdown"
    - "éditeur"
    - "écriture"
    - "note"
    - "rédaction"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

Cet éditeur markdown est avant tout léger et d'utilisation fort simple grâce à sa barre d'outil et ses panneau latéraux. L'aide fournie permet de comprendre comment écrire en markdown et surtout comment entrer le texte si vous désirez créer des schémas, des diagrammes ou des formules mathématiques. Pour cela VSCode utilise des bibliothèques javascript variées. L'export en PDF est propre et d'un rendu assez satisfaisant pour se passer d'autres outils. Dans les options de configuration, il est tout aussi possible de régler l'export, l'apparence et divers autres options de manière à ce que VNote corresponde à vos besoins.

