---
nom: "Ultracopier"
date_creation: "Samedi, 12 septembre, 2020 - 14:11"
date_modification: "Samedi, 12 septembre, 2020 - 14:24"
logo:
    src: "images/logo/Ultracopier.png"
site_web: "http://ultracopier-fr.first-world.info/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Remplace le système de copie/déplacement par défaut avec des fonctions supplémentaires."
createurices: "BRULE Herman"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Ultracopier"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/ultracopier"
---

Ultracopier permet de remplacer le système de gestion des copies/déplacements par défaut de l'explorateur de fichiers avec des fonctions en plus :
Gère les erreurs et les collisions  (toujours écraser, écraser si plus récent, etc.)
permet de modifier la liste des copies
permet de régler la vitesse de copie
etc.
Il existe un autre logiciel pour la gestion du copie/déplacement, développé aussi par BRULE Herman : Supercopier. C'est en fait Ultracopier avec un skin personnalisé en CSS.
A noter un forum d'aide en français : forum-ultracopier.first-world.info

