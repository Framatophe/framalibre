---
nom: "Code::Blocks"
date_creation: "Dimanche, 10 mars, 2019 - 11:01"
date_modification: "Jeudi, 17 août, 2023 - 21:08"
logo:
    src: "images/logo/Code::Blocks.png"
site_web: "http://www.codeblocks.org/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
description_courte: "Un IDE pour la programmation en C, C++ et Fortran."
createurices: ""
alternative_a: "Microsoft VisualStudio"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "développement"
    - "code"
    - "traitement de texte"
    - "compilation"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Code::Blocks"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/codeblocks"
---

Code::Blocks est un Environnement de Développement Intégré (IDE) conçu pour le développement de logiciels en C, C++ et Fortran. Il supporte de nombreux compilateurs, la coloration syntaxique, l'autocomplétion de code, un gestionnaire de classes, une ToDo list et support de nombreuses extensions.

