---
nom: "Geary"
date_creation: "Mercredi, 22 mars, 2017 - 00:19"
date_modification: "Mercredi, 12 mai, 2021 - 16:13"
logo:
    src: "images/logo/Geary.png"
site_web: "https://wiki.gnome.org/Apps/Geary"
plateformes:
    - "GNU/Linux"
    - "BSD"
langues:
    - "Autres langues"
description_courte: "Client de messagerie simple et moderne"
createurices: ""
alternative_a: "Microsoft Outlook, Mail"
licences:
    - "Licence publique générale limitée GNU (LGPL)"
tags:
    - "internet"
    - "client mail"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Geary_(logiciel)"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/geary"
---

Client de messagerie accès sur le protocole IMAP, avec prise en charge simplifiée des principaux services de messagerie (Gmail, Yahoo! Mail, Outlook.com...), qui présente les courriers sous forme de conversation.

