---
nom: "Mobilizon"
date_creation: "Mercredi, 15 mai, 2019 - 10:13"
date_modification: "Mardi, 11 mai, 2021 - 23:17"
logo:
    src: "images/logo/Mobilizon.png"
site_web: "https://joinmobilizon.org/fr/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Windows"
    - "le web"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Un outil libre et fédéré pour se rassembler, s'organiser, se mobiliser et agir."
createurices: ""
alternative_a: "Facebook, Meetup, Eventbrite"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "évènements"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Mobilizon"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/mobilizon"
---

Mobilizon est un logiciel actuellement en cours de développement par Framasoft.
Il permettra à chaque organisation ou individu de proposer sa propre plateforme de mobilisation.
Il sera possible de :
Publier des évènements,
Créer des groupes,
Dialoguer par messagerie,
Avoir plusieurs identités avec un seul compte (pour séparer les évènements familiaux ou activités de loisirs de son activité militante, par exemple)
Fédérer plusieurs instances Mobilizon pour échanger avec d’autres instances, afficher plus d’événements que juste ceux de son instance et favoriser les interactions (utilisation du protocole ActivityPub)
Mobilizon sera un logiciel éthique permettant de s'émanciper de Facebook, Meetup, Eventbrite ou tout autre plateforme à but lucratif. Il sera respectueux de la vie privée et des données personnelles de ses utilisateurs.
Mobilizon est actuellement en cours de financement : il est possible de faire un don pour aider à ce que toutes les fonctionnalités ci-dessus puissent être développées et incluses dans le logiciel.

