---
nom: "KeepassX"
date_creation: "Mardi, 13 décembre, 2016 - 10:24"
date_modification: "Lundi, 10 mai, 2021 - 13:39"
logo:
    src: "images/logo/KeepassX.png"
site_web: "https://www.keepassx.org/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
    - "Android"
langues:
    - "Autres langues"
description_courte: "Pour gérer vos mots de passe, un coffre-fort centralisé peut s'avérer utile."
createurices: ""
alternative_a: "lastpass, 1password"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "sécurité"
    - "gestionnaire de mots de passe"
    - "chiffrement"
    - "mot de passe"
    - "authentification"
lien_wikipedia: "https://en.wikipedia.org/wiki/KeePassX"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/keepassx"
---

KeePassX a débuté comme étant un portage sous Linux du logiciel Keepass, qui était un gestionnaire de mot de passe pour Windows. Maintenant les deux sont multiplateformes. KeePassX utilise les librairies graphiques QT, les versions récentes de Keepass elles utilisent .NET / Mono.
KeepassX est multiplateforme et tourne donc sur  GNU/Linux, Windows, et Mac OS X.
KeePassX utilise le format de bases de données de mot de passe de KeePass 2 (.kdbx) comme format natif. Il peut aussi importer (via une conversion) les anciens fichiers de KeePass 1 (.kdb).

