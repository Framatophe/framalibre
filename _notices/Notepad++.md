---
nom: "Notepad++"
date_creation: "Lundi, 29 septembre, 2014 - 15:24"
date_modification: "Samedi, 16 novembre, 2019 - 11:25"
logo:
    src: "images/logo/Notepad++.png"
site_web: "https://notepad-plus-plus.org"
plateformes:
    - "Windows"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Notepad++ est un éditeur de texte et de code pour Windows"
createurices: "Don Ho"
alternative_a: "Notepad, UltraEdit, Sublime Text"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "développement"
    - "traitement de texte"
    - "texte"
    - "développement web"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Notepad%2B%2B"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/notepad%2B%2B"
---

Notepad++ est un éditeur de texte et de code pour Windows basé sur Scintilla (tout comme le célèbre et puissant SciTE).
Très peu gourmand en ressources système, il prend en charge de multiples langages, il est doté d'un système de coloration syntaxique, et de multiples fonctionnalités qui raviront toute personne recherchant un bon éditeur de texte sous Windows.

