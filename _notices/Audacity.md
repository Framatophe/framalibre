---
nom: "Audacity"
date_creation: "Jeudi, 29 décembre, 2016 - 16:30"
date_modification: "mercredi, 10 avril, 2024 - 19:38"
logo:
    src: "images/logo/Audacity.png"
site_web: "https://www.audacityteam.org"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Autres langues"
description_courte: "Le logiciel vedette pour l'édition audio"
createurices: "Dominic Mazzoni"
alternative_a: "Adobe Audition"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "retouche audio"
    - "logiciel audio"
    - "mao"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Audacity"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/audacity"
---

Audacity est un logiciel libre pour la manipulation de données audio numériques. Audacity permet d'enregistrer du son numérique par le biais des entrées ligne/micro/cd des cartes sons. Il permet d'éditer (copier, coller, sectionner…) les sons sur plusieurs pistes, et il est accompagné de divers filtres et effets : pitch, tempo, réduction de bruit, égaliseur, filtres de Fourier, augmentation de fréquences précises, compression, amplification, normalisation, écho, phaser, wahwah, inversion…
Une [liste Framaliste a été créée](https://framalistes.org/sympa/subscribe/audacity/)
et un [salon Jabber](https://candy.jabberfr.org/audacity@chat.jabberfr.org/).
Il est à noter que les binaires téléchargeables depuis audacityteam.org contiennent des fonctionnalités de télémétrie proposant par défaut de signaler les erreurs du logiciel à l'équipe de développement, selon leur politique de confidentialité. Le projet Tenacity est un fork d'Audacity sans cette particularité, mais n'est pas disponible sous tous les systèmes.
