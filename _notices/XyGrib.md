---
nom: "XyGrib"
date_creation: "Jeudi, 13 septembre, 2018 - 11:32"
date_modification: "Jeudi, 13 septembre, 2018 - 11:32"
logo:
    src: "images/logo/XyGrib.png"
site_web: "https://opengribs.org/fr/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "XyGrib est un lecteur de fichier GRIB, il permet de télécharger et visualiser des fichiers météo"
createurices: ""
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "science"
    - "sig"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/xygrib"
---

Fork de zygrib (projet qui semble abandonné), XyGrib est un lecteur de fichier GRIB, il permet principalement de télécharger des fichiers météo au format GRIB et de visionner les données météo récupérées.
Selon les modèles, les fichiers de prévisions peuvent être d'une résolution 0.25° , intervalle 3 heures, période : 1 à 10 jours 
Forum : https://opengribs.org/fr/forum-fr
Modèles atmosphériques :
	• GFS - fourni par NOAA
	• ICON (ICON GLOBAL)- fourni par Deutschewetterdienst (DWD)
	• Arpège - fourni par MétéoFrance
Modèles de vagues :
	• WW3 (mondial sans la Méditerranée) - fourni par NOAA
	• GWAM (mondial y compris la Méditerranée) - fourni par DWD
	• EWAM (eaux européennes et la Méditerranée) - fourni par DWD

