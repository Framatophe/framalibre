---
nom: "Clavier+"
date_creation: "Dimanche, 15 janvier, 2017 - 15:02"
date_modification: "Lundi, 16 janvier, 2017 - 16:38"
logo:
    src: "images/logo/Clavier+.png"
site_web: "http://utilfr42.free.fr/util/Clavier.php"
plateformes:
    - "Windows"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Clavier+ pour créer des raccourcis clavier facilement"
createurices: "Guillaume Ryder"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
    - "clavier"
    - "raccourci"
    - "utilitaire"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/clavier"
---

Clavier+ est un petit utilitaire bien pratique qui permet de créer simplement des raccourcis clavier.
Grâce aux raccourcis créés par cet exécutable vous pourrez :
- lancer une application ;
- écrire un texte prédéfini (ex:adresse courriel) ;
- lancer une action windows (ex:extinction du système) ;
- lancer un script ;
- etc...Clavier+ est également portable donc vous pouvez l'utiliser depuis une clé usb.

