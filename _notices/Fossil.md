---
nom: "Fossil"
date_creation: "Dimanche, 9 avril, 2017 - 01:39"
date_modification: "Dimanche, 9 avril, 2017 - 01:39"
logo:
    src: "images/logo/Fossil.gif"
site_web: "https://www.fossil-scm.org"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
description_courte: "Forge logicielle, wiki et gestion de ticket en un exécutable léger."
createurices: "D. Richard Hipp"
alternative_a: "Github"
licences:
    - "Berkeley Software Distribution License (BSD)"
tags:
    - "développement"
    - "forge logicielle"
    - "gestion de version"
    - "wiki"
    - "gestion de tickets"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Fossil_(logiciel)"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/fossil"
---

Fossil est assez surprenant: c'est un logiciel de gestion de versions décentralisé (comme git) mais qui intègre une gestion de tickets, un wiki et une gestion de documentation, aussi versionnés, avec une interface web, le tout en un exécutable léger. Une sorte de Github tout en un, facile à installer.
Une autre différence par rapport à git (la référence actuelle), est que tout le contenu du dépôt Fossil est stocké dans une base de données SQLite. D'ailleurs, le projet SQLite utilise Fossil. Ils sont du même créateur.
La page de documentation vous expliquera les différences de philosophie et d'usage avec Git, et on peut lire des avis et retours d'expérience (en anglais).

