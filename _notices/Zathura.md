---
nom: "Zathura"
date_creation: "Lundi, 22 avril, 2019 - 18:52"
date_modification: "Lundi, 22 avril, 2019 - 18:52"
logo:
    src: "images/logo/Zathura.png"
site_web: "http://pwmt.org/projects/zathura/"
plateformes:
    - "GNU/Linux"
langues:
    - "English"
description_courte: "Un visionneur de document minimaliste"
createurices: "Moritz Lipp, Sebastian Ramacher"
alternative_a: ""
licences:
    - "Licence Zlib"
tags:
    - "bureautique"
    - "pdf"
lien_wikipedia: "https://en.wikipedia.org/wiki/Zathura_(document_viewer)"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/zathura"
---

Zathura est une visionneuse de documents gratuite basée sur un plugin. Les plugins sont disponibles pour les formats PDF (via poppler ou MuPDF), PostScript, DjVu et EPUB. Il a été écrit pour être léger et contrôlé avec des raccourcis clavier similaires à ceux de vi. La personnalisation de Zathura le rend populaire auprès de nombreux utilisateurs de Linux.

