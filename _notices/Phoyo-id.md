---
nom: "Phoyo-id"
date_creation: "Jeudi, 19 octobre, 2017 - 17:19"
date_modification: "Jeudi, 14 décembre, 2017 - 20:40"
logo:
    src: "images/logo/Phoyo-id.png"
site_web: "https://www.phoyosystem.com/fr/logiciel-cabine-photo/#loutil-dimpression-de-phot…"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
description_courte: "Utilitaire d’impression de photos d’identité"
createurices: "Olivier T."
alternative_a: "Emjysoft Photo d'identité"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/phoyo-id"
---

Phoyo-id permet d’imprimer (depuis chez soi et sans utiliser un logiciel de cabine photo) des photographies d'identité conformes.
Les photos d'identités doivent respecter certaines normes dépendant du pays pour servir à la confection de documents officiels (passeport, carte d'identité, ...) . De nos jours, ces normes (prises en compte par Phoyo-id) sont pratiquement dans tous les pays, basées sur celles des passeports biométriques.

