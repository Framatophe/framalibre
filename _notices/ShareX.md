---
nom: "ShareX"
date_creation: "Dimanche, 9 juillet, 2017 - 16:02"
date_modification: "Dimanche, 21 juillet, 2019 - 20:47"
logo:
    src: "images/logo/ShareX.png"
site_web: "https://getsharex.com/"
plateformes:
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "ShareX est un logiciel de capture d'image pour Windows."
createurices: "Jaex Delpach, Michael Delpach"
alternative_a: "Snagit, WinSnap"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "screencast"
    - "capture d'écran"
    - "capture vidéo"
    - "utilitaire"
lien_wikipedia: "https://fr.wikipedia.org/wiki/ShareX"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/sharex"
---

ShareX permet de capturer des images en tout facilité. Il permet aussi plusieurs traitements d'images et de facilement créer des compilations d'images en une nouvelle image.
De plus, il permet de partager directement les enregistrements d'images sur plusieurs sites (dont les gros propriétaires et quelques opensource tel que uguu.se) il offre aussi la possibilité d'ajouter ses propres services d'hébergements.
Anciens noms : ZScreen et ZUploader.

