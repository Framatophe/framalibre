---
nom: "MediaInfo"
date_creation: "Vendredi, 5 octobre, 2018 - 00:02"
date_modification: "Mercredi, 12 mai, 2021 - 16:07"
logo:
    src: "images/logo/MediaInfo.png"
site_web: "https://mediaarea.net/fr/MediaInfo"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Afficher rapidement des informations sur un fichier son ou vidéo (auteur, durée, dimension, codec, débit, etc)"
createurices: ""
alternative_a: ""
licences:
    - "Berkeley Software Distribution License (BSD)"
tags:
    - "multimédia"
    - "métadonnées"
    - "vidéo"
    - "son"
    - "codecs"
    - "format"
lien_wikipedia: "https://en.wikipedia.org/wiki/MediaInfo"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/mediainfo"
---

Cet utilitaire permet de visualiser rapidement de nombreuses informations sur un fichier son ou vidéo. Un clic droit sur un fichier permet de lancer l'utilitaire et d'afficher:
Informations générales : titre, auteur, réalisateur, album, numéro de piste, date, durée...
Video : codec, dimension, nombre d'images par secondes, débit...
Audio : codec, fréquence d’échantillonnage, nombre de canaux, langue, débit...
Sous-titres: format, langue
Chapitres : nombre de chapitres, liste des chapitres
Note: cet utilitaire est également inclus dans le K-Lite Codec Pack: https://fr.wikipedia.org/wiki/K-Lite_Codec_Pack

