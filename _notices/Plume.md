---
nom: "Plume"
date_creation: "Dimanche, 14 octobre, 2018 - 10:46"
date_modification: "lundi, 18 décembre, 2023 - 16:33"
logo:
    src: "images/logo/Plume.png"
site_web: "https://joinplu.me"
plateformes:
    - "le web"
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
description_courte: "Un moteur de blog décentralisé et fédéré !"
createurices: "Ana Gelez"
alternative_a: "Medium"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "blog"
    - "fediverse"
    - "activitypub"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: "Q65272161"
mis_en_avant: "oui"
redirect_from: "/content/plume"
---

Plume est un moteur de blog décentralisé et fédéré en cours de développement.
Décentralisé : Chaque instance peut suivre une ou plusieurs autres instances Plume afin de permettre à ses utilisateur·rices de visionner les vidéos de celles-ci.
Fédéré : Via le protocole ActivityPub, Plume peut interagir avec d'autres logiciels qui font partie du Fediverse, comme Mastodon ou PixelFed par exemple.
Une version de démonstration est disponible.


