---
nom: "liferea"
date_creation: "Jeudi, 30 août, 2018 - 22:32"
date_modification: "Mercredi, 12 mai, 2021 - 16:40"
logo:
    src: "images/logo/liferea.png"
site_web: "https://lzone.de/liferea/"
plateformes:
    - "GNU/Linux"
    - "BSD"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Un aggrégateur de flux gérant aussi les comptes distants (Tiny Tiny RSS par exemple)."
createurices: "Lars Windolf"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
    - "lecteur de flux rss"
lien_wikipedia: "https://en.wikipedia.org/wiki/Liferea"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/liferea"
---

Liferea est un aggrégateur de flux rss local gérant aussi les comptes distants comme Tiny Tiny RSS, theOldReader, Reedah, InoReader, Planet et BlogRoll. Il permet aussi les imports/exports en OPML ainsi qu'une recherche parmi les flux.

