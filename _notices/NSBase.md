---
nom: "NSBase"
date_creation: "Lundi, 11 décembre, 2017 - 09:21"
date_modification: "Vendredi, 23 février, 2018 - 09:08"
logo:
    src: "images/logo/NSBase.png"
site_web: "http://www.nsbase.neuts.fr"
plateformes:
    - "GNU/Linux"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "NSBase  système de gestion de base de données alternatif à MSAccess"
createurices: "Neuts J.-L."
alternative_a: "Microsoft Access"
licences:
    - "Berkeley Software Distribution License (BSD)"
tags:
    - "bureautique"
    - "sqlite"
    - "base de données"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/nsbase"
---

NSBase est un système de gestion de base de données.
Il permet de construire rapidement des applications complètes de base de données.
Son interface moderne est simple et ergonome. Son scripting en LUA permet de dynamiser vos applications.
Il est une véritable alternative à MSAccess, kexi et autre
NSBase est construit avec LAZARUS IDE, de SQLITE et de LUA
Importez votre base de données: Fichiers texte, MS Access (fichier MDB), SQLite et Kexi
Construisez vos applications rapidement, grâce aux assistants intégrés
Caractéristiques :
Exécution possible de NSBase à partir de votre clé USB
Rapports avancés: qrcodes, codes à barres, formes, images ...
LUA Scripting
Multi langues
Importation de bases de données: fichiers texte, MS Access (fichier MDB), SQLite et Kexi
Assistants de formulaire, d'importation
Multi plateformes: Windows et Linux

