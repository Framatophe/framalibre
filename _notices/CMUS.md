---
nom: "CMUS"
date_creation: "Mercredi, 4 janvier, 2017 - 04:20"
date_modification: "Samedi, 31 juillet, 2021 - 10:40"
logo:
    src: "images/logo/CMUS.png"
site_web: "https://cmus.github.io"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Android"
langues:
    - "Autres langues"
description_courte: "Lecteur de musique pour le terminal de Linux."
createurices: "Timo Hirvonen"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "lecteur multimédia"
    - "terminal"
    - "cli"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/cmus"
---

CMUS (C* Music Player) fonctionne dans un terminal d'un système GNU/Linux. Non seulement il est très véloce, mais ses raccourcis claviers, inspirés de Vim, rendent son utilisation encore plus rapide. De plus, il existe une extension, "cmus-remote", qui permet de le contrôler en étant en-dehors du terminal, par des raccourcis définis sous Gnome par exemple.
Il possède plusieurs vues:
bibliothèque de morceaux,
bibliothèque triée,
listes de lectures,
queue de lecture,
navigateur de fichiers,
filtres,
et préférences.
Chacune de ces sept vues est accessible par son chiffre correspondant, de 1 à 7. Les raccourcis sont somme toute classiques: taper "a" dans le navigateur de fichiers permet d'ajouter le dossier à la bibliothèque, taper Entrée permet de le lire immédiatement.
Super lecteur pour amateurs du terminal.

