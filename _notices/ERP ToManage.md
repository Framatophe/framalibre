---
nom: "ERP ToManage"
date_creation: "Mercredi, 5 avril, 2017 - 13:00"
date_modification: "Mardi, 11 mai, 2021 - 23:14"
logo:
    src: "images/logo/ERP ToManage.png"
site_web: "https://www.tomanage.fr"
plateformes:
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
description_courte: "Mesurez, pilotez, Managez.
logiciel ERP open source - logiciel de gestion personnalisable et connecté."
createurices: "Hervé PROT, Régis CHARNACE"
alternative_a: "wavesoft, Sage, Divalto"
licences:
    - "Licence Apache (Apache)"
tags:
    - "métiers"
    - "erp"
    - "crm"
    - "iot"
    - "réseau"
    - "gestion de la relation client"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/erp-tomanage"
---

ToManage est un logiciel ERP libre (licence OSL v3) pour gérer vos activités ou celles de vos clients (pré-comptabilité, achats, ventes, projets, contacts...).
Le logiciel ERP ToManage permet aux entreprises de construire leur logiciel sur mesure à partir d’un catalogue de modules personnalisables et d’intégrer des spécificités métier sur mesure. ToManage est un ERP personnalisable!
De plus, le réseau de communication avec les objets connectés intégré au logiciel ERP ToManage offre la possibilité aux entreprises de piloter en temps réel en mettant en place des alertes, des remontées d'informations, des collectes de données sur machine ...
Caractéristiques techniques du logiciel ERP open source ToManage :
- Node JS,
- Angular JS,
- Mongo DB,
- Total JS web framework.

