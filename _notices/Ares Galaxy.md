---
nom: "Ares Galaxy"
date_creation: "Vendredi, 17 mars, 2017 - 21:10"
date_modification: "Lundi, 10 mai, 2021 - 13:56"
logo:
    src: "images/logo/Ares Galaxy.png"
site_web: "https://aresgalaxy.io/"
plateformes:
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Ares Galaxy est un logiciel p2p utilisant son propre réseau et passant les firewalls."
createurices: ""
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
    - "p2p"
    - "réseau"
    - "décentralisation"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Ares_Galaxy"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/ares-galaxy"
---

Ares Galaxy ne requiert aucune configuration et se trouve être simple d'utilisation. Il embarque un système de chat similaire à un salon IRC mais de type décentralisé puisque chaque salon est hébergé par les utilisateurs. La fonction centrale du client est le partage de fichiers mais autour de celle-ci il existe des fonctions de lecture audio, radio, vidéo et un navigateur sommaire.
La notion de vie privée est importante pour les développeurs du logiciel, l'aspect légal également puisqu'à travers le logiciel l'équipe tend à vouloir promouvoir les torrents légaux.

