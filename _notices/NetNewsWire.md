---
nom: "NetNewsWire"
date_creation: "Dimanche, 13 novembre, 2022 - 12:40"
date_modification: "Vendredi, 13 janvier, 2023 - 14:22"
logo:
    src: "images/logo/NetNewsWire.png"
site_web: "https://netnewswire.com"
plateformes:
    - "Mac OS X"
    - "Apple iOS"
langues:
    - "English"
description_courte: "NetNewsWire est un lecteur RSS gratuit et open source pour Mac, iPhone et iPad"
createurices: "Brent Simmons"
alternative_a: ""
licences:
    - "Licence MIT/X11"
tags:
    - "internet"
    - "lecteur de flux rss"
    - "actualités"
lien_wikipedia: "https://en.wikipedia.org/wiki/NetNewsWire"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/netnewswire"
---

NetNewsWire vous montre des articles de vos blogs et sites d'actualités préférés et garde une trace de ce que vous avez lu.
Cela signifie que vous pouvez arrêter d'aller de page en page dans votre navigateur à la recherche de nouveaux articles à lire. Faites-le plutôt simplement: laissez NetNewsWire vous apporter les nouvelles.
Et, si vous recevez vos actualités via Facebook – avec ses publicités, ses algorithmes, son suivi des utilisateurs, son indignation et sa désinformation – vous pouvez passer à NetNewsWire pour obtenir des informations directement et de manière plus fiable à partir des sites auxquels vous faites confiance.

