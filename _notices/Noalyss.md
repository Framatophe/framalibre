---
nom: "Noalyss"
date_creation: "Mercredi, 11 janvier, 2017 - 21:17"
date_modification: "Mardi, 11 mai, 2021 - 23:14"
logo:
    src: "images/logo/Noalyss.png"
site_web: "http://www.noalyss.eu"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "le web"
langues:
    - "Français"
description_courte: "Noalyss est un serveur libre de comptabilité et ERP en ligne et de qualité professionnelle"
createurices: "Dany De Bontridder"
alternative_a: "winbook, exactonline, yooz"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "erp"
    - "comptabilité"
    - "entreprise"
    - "crm"
    - "gestion de la relation client"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Phpcompta"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/noalyss"
---

C'est un logiciel serveur libre de gestion et de comptabilité, de qualité professionnelle auquel on accède grâce à un navigateur internet
Utilisé par des universités pour l'enseignement de la comptabilité, des associations, des syndic de copropriétaires, PME et indépendants. Souple, puissant et flexible, des extensions sont disponibles.
Il peut être utilisé pour plusieurs entités (personnes morales, indépendants) et multi-utilisateurs.
Liste des principales fonctionnalités :
comptabilité analytique,
comptabilité Générale,
prévision,
déclaration fiscale (certaines à paramétrer soi-même),
bilan et ratios,
CRM Suivi Client, Fournisseurs et même administrations,
gestion des bons commandes, devis, projets,...
gestion de stock,
suivi des contacts,
facturation (génération, conversion en PDF et envoi par courriel).

