---
nom: "LibreTorrent"
date_creation: "Dimanche, 26 mars, 2017 - 12:45"
date_modification: "Mercredi, 12 mai, 2021 - 15:08"
logo:
    src: "images/logo/LibreTorrent.png"
site_web: "https://github.com/proninyaroslav/libretorrent"
plateformes:
    - "Android"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Client torrent simple et léger."
createurices: "Yaroslav Pronin"
alternative_a: "µTorrent, BitTorrent"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
    - "torrent"
    - "client bittorrent"
    - "réseau"
    - "téléchargement"
    - "p2p"
    - "bittorrent"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/libretorrent"
---

LibreTorrent est un client torrent pour Android 4+. Il permet entre autre le filtrage par IP, supporte les liens aimantés et le chiffrement.
Il est capable de modifier l'emplacement de stockage en cours de téléchargement ou d'enclencher le déplacement à la fin du téléchargement.

