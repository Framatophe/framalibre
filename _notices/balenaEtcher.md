---
nom: "balenaEtcher"
date_creation: "Samedi, 14 janvier, 2017 - 14:34"
date_modification: "mercredi, 8 mai, 2024 - 11:28"
logo:
    src: "images/logo/balenaEtcher.png"
site_web: "https://www.balena.io/etcher/"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "English"
description_courte: "Etcher pour graver vos distributions sans prise de tête"
createurices: ""
alternative_a: "Nero"
licences:
    - "Licence Apache (Apache)"
tags:
    - "système"
    - "utilitaire"
    - "gravure"
    - "iso"
    - "usb"
    - "live usb"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Etcher"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/balenaetcher"
---

Etcher est un utilitaire de copie d'images disque (zip, img, iso) sur différents supports (clé USB, carte SD, microSD). Etcher est très simple d'utilisation et assure une copie fiable grâce à sa fonction de vérification de l'image gravée.
Il a de plus l'avantage d'être portable et peut donc être transporté sur une clé USB.
https://debian-facile.org/doc:install:usb-boot:etcher

Point de vigilance : d’après la page Wikipédia dédiée à ce logiciel, ce dernier intègre des outils visant à capter les données des utilisateurs (type Google Analytics, GoSquared et Mixpanel), même sans leur consentement ! Nous vous conseillons de chercher une alternative parmi les autres logiciels de gravure recensés dans Framalibre.
