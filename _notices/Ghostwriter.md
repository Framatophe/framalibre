---
nom: "Ghostwriter"
date_creation: "Lundi, 19 mars, 2018 - 20:39"
date_modification: "mardi, 16 avril, 2024 - 00:16"
logo:
    src: "images/logo/Ghostwriter.png"
site_web: "https://ghostwriter.kde.org/"
plateformes:
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Éditeur de markdown en mode plein écran et dans une interface propre."
createurices: "wereturtle"
alternative_a: "Markdownpad"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
    - "markdown"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: "Q115797394"
mis_en_avant: "oui"
redirect_from: "/content/ghostwriter"
---

Un éditeur de Markdown simple, libre et gratuit.
Il permet de prévisualiser le document Markdown en HTML. L'aperçu ne se met à jour que lorsque vous arrêtez de taper, ce qui vous permet de continuer à travailler sur un document volumineux sans que l'application ne gèle.
Le processeur intégré, Sundown, permet d'exporter en HTML mais on peut également installer l'un des éléments suivants pour exporter vers plusieurs autres formats:
* Pandoc ;
* MultiMarkdown ;
* Discount ;
* CommonMark.

Ghostwriter détectera automatiquement leur installation, vous permettant d'exporter en HTML, Word, ODT, PDF, et plusieurs autres formats.
Il incorpore un dictionnaire orthographique.
Si les deux thèmes intégrés ne suffisent pas, vous pouvez créer le vôtre.
