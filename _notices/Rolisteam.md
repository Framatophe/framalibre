---
nom: "Rolisteam"
date_creation: "Mardi, 27 décembre, 2016 - 01:47"
date_modification: "Mardi, 27 décembre, 2016 - 08:45"
logo:
    src: "images/logo/Rolisteam.png"
site_web: "http://www.rolisteam.org"
plateformes:
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Libérez vos parties (de jeu de rôle) avec Rolisteam."
createurices: "Renaud Guezennec"
alternative_a: "roll20.net, fantasy ground"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "jeu"
    - "jeux de rôle"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Rolisteam"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/rolisteam"
---

Rolisteam vous aide à maîtriser une partie de jeu de rôle avec des joueurs distants. Il fournit de nombreuses fonctionnalités, telles que le partage de carte/plan ou le partage de photo. Il contient également des outils de communication avec vos joueurs. Le but étant de rendre une partie gérée avec Rolisteam aussi facile qu'une partie à votre table. Pour cela, Rolisteam propose plusieurs fonctionnalités comme :
un lecteur audio,
des outils de dessin,
le partage d'images et de plans,
la gestion du brouillard de guerre sur un plan,
le positionnement des PJ ou PNJ sur le plan,
la prise de notes,
le lancement de dés (pleins d’opérations possible: tri, compte…),
une messagerie instantanée,
etc.

