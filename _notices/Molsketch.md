---
nom: "Molsketch"
date_creation: "dimanche, 17 mars, 2024 - 16:57"
date_modification: "lundi, 18 mars, 2024 - 12:39"
logo:
    src: "images/logo/Molsketch.png"
site_web: "https://sourceforge.net/projects/molsketch/"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "English"
description_courte: "Bon logiciel de dessin 2D de molécules. Très pratique pour la réalisation des schémas d'un cours ou d'un devoir."
createurices: "Hendrik Vennekate"
alternative_a: "MarvinSketch, ChemSketch, ChemDraw, EDraw Software"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "dessin"
    - "dessin technique"
    - "chimie"
    - "molécule"
    - "science"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

Le logiciel Molsketch est un logiciel de modélisation 2D (dessin) sous licence libre, permettant de dessiner des molécules chimiques en 2D. Ce logiciel permet l'import de dessins existants sous plusieurs formats. La [chaîne Youtube Molsketch](https://www.youtube.com/channel/UCIYnNzSnI9AHpB_c48BR7fQ) présente le fonctionnement de ce logiciel.
