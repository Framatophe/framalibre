---
nom: "Processwire"
date_creation: "lundi, 28 août, 2023 - 11:51"
date_modification: "lundi, 28 août, 2023 - 11:51"


site_web: "https://processwire.com"
plateformes:
    - "Web"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Un CMS simple, versatile, sécurisé, mis à jour, riche et échelonnable."
createurices: "Ryan Cramer"
alternative_a: "Wordpress"
licences:
    - "Licence Publique Mozilla (MPL)"
tags:
    - "création de site web"
    - "php"
    - "développement web"
lien_wikipedia: "https://en.wikipedia.org/wiki/ProcessWire"
lien_exodus: ""
identifiant_wikidata: "Q3021710"
mis_en_avant: "non"
---

Ce CMS permet de de créer un site web d'une manière simple et intuitive, il est quand même préférable de connaître le langage PHP.  Il s'adapte à tous les sites. Il est sécurisé, ainsi par exemple la page de login de  l'administrateur aura l'url que vous décidez. Ses mises à jour se font dans un répertoire séparé de celui de votre site. Il suit la structure de Symphony. Pour un utilisateur non averti la mise à jour des contenus est simple et demande juste une courte démo.
Il sera ensuite facile de changer l'échelle du site ou de le modifier. Il répond à n'importe quelle demande du client. La plupart des modules sont gratuits, certains qui facilitent le travail du créateur du site sont payants mais vous font gagner du temps. Je le plébiscite, je l'utilise depuis plus de 10 ans. Il est mis à jour très régulièrement et le support est très réactif par la communauté dans les forums ou par le créateur lui-même qui est très disponible.
