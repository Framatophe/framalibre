---
nom: "Shotcut"
date_creation: "Mardi, 11 avril, 2017 - 12:48"
date_modification: "vendredi, 29 mars, 2024 - 15:47"
logo:
    src: "images/logo/Shotcut.png"
site_web: "https://www.shotcut.org/"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Montage vidéo multiplateforme et portable."
createurices: "Charlie Yates, Dan Dennedy (créateurs de MLT)"
alternative_a: "Windows Movie Maker"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "vidéo"
    - "éditeur vidéo"
    - "montage vidéo"
    - "webcam"
    - "animation"
    - "créativité"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Shotcut"
lien_exodus: ""
identifiant_wikidata: "Q21977736"
mis_en_avant: "non"
redirect_from: "/content/shotcut"
---

Shotcut est un logiciel de montage vidéo non-linéaire qui fonctionne sous GNU/Linux, Windows ou MacOS et qui possède une version portable (utilisable sans installation).
Néanmoins, Shotcut, créé et développé par les créateurs du framework de montage vidéo MLT (Medio Lovin' Toolkit), sur lequel reposent d'autres logiciels de montage vidéo sous Linux.
* Éditeur multipistes avec formes d'onde
* Effets vidéo et audio
* Capture de la webcam
* Animation vectorielle avec l'intégration de Glaxnimate
* Nombreux formats audio et vidéo
* Bonne prise en charge du matériel (multiprocesseurs, accélération vidéo, etc)
Shotcut est moins connu que KDEnlive mais est plus avancé sur différents points.
