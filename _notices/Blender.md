---
nom: "Blender"
date_creation: "Mercredi, 18 mars, 2015 - 21:46"
date_modification: "Mercredi, 18 octobre, 2023 - 11:02"
logo:
    src: "images/logo/Blender.png"
site_web: "http://www.blender.org"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Blender est un puissant logiciel de modélisation, d'animation et de rendu 3D."
createurices: ""
alternative_a: "Autodesk 3ds Max, Adobe After Effects, Maya"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "création"
    - "3d"
    - "animation"
    - "python"
    - "développement de jeu vidéo"
    - "créativité"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Blender"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/blender"
---

Blender est un puissant logiciel de modélisation, d'animation et de rendu 3D. Il contient de nombreuses fonctionnalités avancées. Il dispose d'un moteur de jeu intégré (Blender game engine) qui permet de créer des jeux ou des applications 3D.
Il permet également le montage vidéo via son module Video Sequence Editor (VSE), la composition vidéo cinématographique (''compositing''), entre autres.
Blender est un logiciel permettant d’obtenir (avec le travail et le savoir-faire) des résultats professionnels. Il reste néanmoins utilisable pour un usage amateur, de nombreux tutoriels existant sur internet.

