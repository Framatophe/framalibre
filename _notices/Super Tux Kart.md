---
nom: "Super Tux Kart"
date_creation: "Samedi, 24 décembre, 2016 - 03:17"
date_modification: "Dimanche, 26 janvier, 2020 - 11:15"
logo:
    src: "images/logo/Super Tux Kart.png"
site_web: "https://supertuxkart.net"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "Autre"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "SuperTuxKart est un jeu de course avec une variétés de modes, de cartes et de karts. Plus une campagne solo !"
createurices: "Joerg Henrichs, Marianne Gagnon, Eduardo Hernandez Munoz, Steve Baker"
alternative_a: "Mario kart"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "jeu"
    - "course"
lien_wikipedia: "https://en.wikipedia.org/wiki/SuperTuxKart"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/super-tux-kart"
---

Bien qu'étant un clone de Mario Kart, le jeu innove, par exemple avec un mode de jeu Suivre le meneur. En constant développement, il dispose d'une communauté active et reçoit de la part des médias du web des critiques plutôt positives.
Le projet dispose d'une communauté passionnée et active, notamment sur le forum officiel du projet. Elle participe entre autres à la traduction du jeu et à la création de ressources (karts, circuits et aussi musiques).
Les extensions proposées sont l'œuvre de la communauté et permettent d'allonger considérablement la durée de vie du jeu. Plus d'une trentaine de karts supplémentaires sont disponibles ainsi qu'une vingtaine de circuits.
Ces karts et ces circuits ainsi que ceux disponibles par défaut sont réalisés sous le logiciel libre de modélisation et de rendu 3D Blender.

