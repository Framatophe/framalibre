---
nom: "Pleroma"
date_creation: "Dimanche, 14 octobre, 2018 - 12:05"
date_modification: "Samedi, 20 juillet, 2019 - 00:22"
logo:
    src: "images/logo/Pleroma.png"
site_web: "https://pleroma.social/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "Apple iOS"
    - "le web"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Un réseau social de microblogging décentralisé et fédéré."
createurices: ""
alternative_a: "Twitter"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "réseau social"
    - "microblog"
    - "décentralisation"
    - "activitypub"
    - "ostatus"
    - "fediverse"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/pleroma"
---

Pleroma est un réseau social de microblogging décentralisé et fédéré créé en 2017. Il a la particularité d'être économe en ressources. Les posts sont limités par défaut à 5000 caractères.
Pleroma peut être utilisé via deux interfaces : une interface native (cf illustration) hautement personnalisable, ou une interface similaire à celle de Mastodon. L'utilisateur·rice a également la possibilité de passer par le protocole Gopher pour une interface super légère.
Un chat local, interne à l'instance, est également proposé.
Décentralisé : Chaque instance peut suivre une ou plusieurs autres instances Pleroma afin de permettre à ses membres de visionner les vidéos de celles-ci.
Fédéré : Via les protocoles ActivityPub et OStatus, Pleroma peut interagir avec d'autres logiciels qui font partie du Fediverse, comme Mastodon ou GNUSocial par exemple.

