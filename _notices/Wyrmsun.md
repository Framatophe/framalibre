---
nom: "Wyrmsun"
date_creation: "Lundi, 13 mars, 2017 - 18:58"
date_modification: "Mercredi, 16 août, 2017 - 12:31"
logo:
    src: "images/logo/Wyrmsun.png"
site_web: "https://andrettin.github.io/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Autre"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Wyrmsun est un jeu de stratégie en temps réel s'inspirant de Warcraft 2."
createurices: "Andrettin"
alternative_a: "Warcraft"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "jeu"
    - "rts"
    - "2d"
    - "stratégie"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/wyrmsun"
---

Entrez dans l'univers de Wyrmsun et de ses nombreuses planètes. Nous retrouvons les humains sur la planète terre ainsi que les nains marchant sur le sol de Nidavellir, les elfes pour leur part nourrissent leur monde Alfheim. Ces trois peuples luttent pour se tailler une place avec leurs armes et leurs outils de pierre, de bronze et de fer. Grâce à cette volonté peut-être qu'un jour ils se rencontreront par delà les étoiles.
Le style graphique de Wyrmsun est ce qu'on peux qualifier de "retro", c'est en effet un jeu 2D de stratégie s'inspirant clairement de la série Warcraft sur le gameplay et l'ambiance générale du jeu. Vous avez la possibilité de jouer sur Internet, en local ou d'effectuer les quêtes disponibles en solo. À la manière de Warcraft 3 un système de héro (plus simpliste) permet de rajouter un peu de micro-gestion supplémentaire ce qui fera plaisir aux joueurs de RTS. On peut citer également la présence de nombreuses cartes et environnements qui rajoutent un plus au jeu.

