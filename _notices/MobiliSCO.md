---
nom: "MobiliSCO"
date_creation: "lundi, 15 avril, 2024 - 16:25"
date_modification: "lundi, 15 avril, 2024 - 16:25"
logo:
    src: "images/logo/MobiliSCO.png"
site_web: "http://openacademie.fr/mobilisco/"
plateformes:
    - "Windows"
langues:
    - "Français"
description_courte: "MobiliSCO organise les voyages pédagogiques de votre EPLE"
createurices: "OpenAcadémie, startup d'Etat de l'Education nationale"
alternative_a: "Rien !"
licences:
    - "Licence Publique Union Européenne (EUPL)"
tags:
    - "Voyages pédagogiques"
    - "Sorties scolaires"
    - "école"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

Importez la base élèves, et définissez votre voyage : il n'y a plus qu'à sélectionner quels élèves partent, et l'ensemble des documents pré-remplis peuvent être communiqués aux familles. La lettre d'engagement, l'autorisation de sortie du territoire, la fiche infirmerie ; mais aussi le budget et le compte-rendu financier, l'ordre de mission des accompagnateurs...

MobiliSCO est interfacé avec l'application comptable GFC des établissements scolaires, pour y importer les droits constatés, et permettre un suivi financier transparent et sans risque d'erreur.

Application primée par le premier prix national du concours de l'innovation administrative Impulsions 2015
