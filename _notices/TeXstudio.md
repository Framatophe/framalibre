---
nom: "TeXstudio"
date_creation: "Samedi, 27 avril, 2019 - 23:53"
date_modification: "Samedi, 27 avril, 2019 - 23:53"
logo:
    src: "images/logo/TeXstudio.png"
site_web: "http://texstudio.sourceforge.net/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "TeXstudio est disponible pour tous les principaux systèmes d'exploitation."
createurices: "Benito van der Zander, Jan Sundermeyer, Daniel Braun, Tim Hoffmann"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
    - "traitement de texte"
lien_wikipedia: "https://en.wikipedia.org/wiki/TeXstudio"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/texstudio"
---

TeXstudio est un environnement d'écriture intégré pour la création de documents LaTeX. Son objectif est de rendre la rédaction de LaTeX aussi simple et confortable que possible. Par conséquent, TeXstudio dispose de nombreuses fonctionnalités telles que la coloration syntaxique, la visionneuse intégrée, la vérification des références et divers assistants.

