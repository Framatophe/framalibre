---
nom: "OpenWrt"
date_creation: "Mercredi, 17 octobre, 2018 - 21:35"
date_modification: "mercredi, 24 janvier, 2024 - 12:08"
logo:
    src: "images/logo/OpenWrt.png"
site_web: "https://openwrt.org/"
plateformes:
    - "Autre"
langues:
    - "Autres langues"
description_courte: "OpenWrt est un microprogramme libre pour routeur"
createurices: ""
alternative_a: "Linksys, LEDE, pfsense, OPNsense"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
    - "internet"
    - "réseau"
lien_wikipedia: "https://fr.wikipedia.org/wiki/OpenWrt"
lien_exodus: ""
identifiant_wikidata: "Q1140401"
mis_en_avant: "non"
redirect_from: "/content/openwrt"
---

OpenWrt permet de remplacer le microprogramme de certains routeurs grand public. Par défaut, il vient avec une interface graphique et l’ensemble des fonctionnalités que l’on peut attendre d’un routeur grand public (gestion du Wi-Fi, DHCP, etc) et quelques fonctionnalités plus avancées tels que des graphiques d’utilisations réseau. Un gestionnaire de paquet embarqué permet d’augmenter grandement les possibilités.
