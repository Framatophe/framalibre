---
nom: "Kate"
date_creation: "Lundi, 2 janvier, 2017 - 10:12"
date_modification: "mardi, 26 décembre, 2023 - 13:20"
logo:
    src: "images/logo/Kate.png"
site_web: "https://kate-editor.org/"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Autres langues"
description_courte: "C'est un éditeur de texte simple mais puissant, multi-documents et polyvalent."
createurices: ""
alternative_a: "Sublime Text, UltraEdit"
licences:
    - "Licence publique générale limitée GNU (LGPL)"
tags:
    - "développement"
    - "texte"
    - "script"
    - "programmation"
    - "traitement de texte"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Kate_(logiciel)"
lien_exodus: ""
identifiant_wikidata: "Q261933"
mis_en_avant: "non"
redirect_from: "/content/kate"
---

Kate est un éditeur de texte multi-documents offrant de nombreuses fonctionnalités pour le développement. Il gère la coloration syntaxique, le pliage du code, le complètement automatique du code, la sélection par blocs, la recherche et le remplacement par expressions rationnelles et bien plus encore.
Il s'intègre avec le terminal, permet de travailler sur des fichiers distants via de nombreux protocoles (ftp, http, webdav, etc.). Il propose également l’enregistrement des sessions de travail, permettant de retrouver ainsi l'ensemble des documents précédemment ouverts.
Les fonctionnalités de Kate peuvent être étendues grâce à des modules externes.


