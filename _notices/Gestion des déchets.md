---
nom: "Gestion des déchets"
date_creation: "Lundi, 6 juillet, 2020 - 16:12"
date_modification: "Jeudi, 1 décembre, 2022 - 16:08"
logo:
    src: "images/logo/Gestion des déchets.gif"
site_web: "https://www.mon-bureau.yo.fr/"
plateformes:
    - "GNU/Linux"
    - "Windows"
langues:
    - "Français"
description_courte: "Un logiciel pour gérer la production et l'élimination des déchets industriels."
createurices: "LGH"
alternative_a: ""
licences:
    - "Licence CECILL (Inria)"
tags:
    - "métiers"
    - "gestion"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/gestion-des-d%C3%A9chets"
---

Ce logiciel permet de rassembler au sein de 5 utilitaires, très simples, un ensemble d'informations destinées à créer et conserver à jour, un plan de prévention et de gestion des déchets, des fiches d'identifications de déchets, un catalogue de matériels, et un registre d'enlèvement des déchets d'un site d'exploitation, de production ou de services.
Il vous permettra une bonne évaluation de votre situation, d'éviter autant que faire se peut des accidents environnementaux toujours possibles.
Les caractéristiques et enregistrements choisis sont ceux qui sont obligatoires. Les documents que vous pourrez éditer avec le logiciel, plan, fiches d'identification, étiquettes de transport, bordereaux de suivis des déchets ou registre de l'enlèvement des déchets sont conformes à la législation.
(Bases de données conservée 5 ans, nomenclature des déchets complète, édition de documents règlementaires, enregistrements automatisés, statistiques etc).
Nécessite un serveur et php 5 à 7.

