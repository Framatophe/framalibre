---
nom: "W"
date_creation: "Dimanche, 18 septembre, 2022 - 23:45"
date_modification: "Lundi, 19 septembre, 2022 - 10:55"
logo:
    src: "images/logo/W.png"
site_web: "https://w.club1.fr"
plateformes:
    - "GNU/Linux"
langues:
    - "English"
description_courte: "Un petit CMS encourageant la creation de page spontanée. Ideal pour des notes perso et des petits sites web."
createurices: "Vincent Peugnet"
alternative_a: ""
licences:
    - "Licence MIT/X11"
tags:
    - "cms"
    - "web"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/w"
---

W est un CMS flat-file (sans base de donnée SQL) codé en PHP.
Il permet de gérer une base de données de pages indépendantes. Libre à vous de les relier, ajouter des menus, regrouper certaines pages et non d'autres...
La spécificité de W est d'être à la fois un outil de prise de note et un outil de publication de site web. Il peut donc servir à stocker des idées, présenter des recherches, créer des narrations interactives, et surtout : de tout mélanger !
C'est donc un CMS plus utile pour des projets expérimentaux, artistiques, qui recherchent une liberté créative importante, plutôt que pour faire des sites classiques ou plus complexes.

dispose d'un gestionnaire de medias.
peut fonctionner entièrement sans Javascript.
multi utilisateurs
léger et rapide


