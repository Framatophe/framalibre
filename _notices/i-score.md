---
nom: "i-score"
date_creation: "Mercredi, 4 janvier, 2017 - 17:22"
date_modification: "Vendredi, 6 janvier, 2017 - 06:22"
logo:
    src: "images/logo/i-score.png"
site_web: "http://www.i-score.org"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
description_courte: "Un séquenceur inter-médias libre et open-source pour le creative coding."
createurices: "LaBRI, Blue Yeti, L'Arboretum"
alternative_a: "QLab, Max/MSP, Vezér"
licences:
    - "Licence CECILL (Inria)"
tags:
    - "création"
    - "midi"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/i-score"
---

Un séquenceur inter-médias libre et open-source pour le creative coding.
i-score rend possible une écrite précise et flexible de partitions et scénarios interactifs.
Il est possible de contrôler et d'automatiser dans une timeline n'importe quel logiciel
compatible OSC ou MIDI : PureData, OpenFrameworks, Processing, Max/MSP...

