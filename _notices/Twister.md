---
nom: "Twister"
date_creation: "Lundi, 13 février, 2017 - 10:08"
date_modification: "Lundi, 13 février, 2017 - 10:08"
logo:
    src: "images/logo/Twister.png"
site_web: "http://twister.net.co/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
langues:
    - "English"
description_courte: "Microblogging décentralisé basé sur les technologies Bitcoin et Bittorrent."
createurices: "Miguel Freitas"
alternative_a: "Twitter"
licences:
    - "Berkeley Software Distribution License (BSD)"
    - "Licence MIT/X11"
tags:
    - "internet"
    - "twitter"
    - "microblog"
    - "p2p"
    - "communication chiffrée"
lien_wikipedia: "https://en.wikipedia.org/wiki/Twister_(software)"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/twister"
---

Twister est une plateforme de microblogging décentralisée et chiffrée.
personne ne peut censurer de messages ou bloquer de comptes
pas d'espionnage: les communications privées sont chiffrées de bout en bout (y compris les méta-données, telles que l'adresse du destinataire)
Twister n'enregistre pas votre adresse IP. Votre présence en ligne n'est pas annoncée.
Twister peut s'utiliser avec un client bureautique ou avec une interface web.

