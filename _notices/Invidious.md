---
nom: "Invidious"
date_creation: "Samedi, 13 juin, 2020 - 18:42"
date_modification: "lundi, 8 janvier, 2024 - 18:30"
logo:
    src: "images/logo/Invidious.png"
site_web: "https://github.com/iv-org/invidious"
plateformes:
    - "le web"
    - "Autre"
langues:
    - "Français"
description_courte: "Une interface alternative pour accéder aux vidéos YouTube sans confier ses données au géant !"
createurices: "Omar Roth"
alternative_a: "Youtube"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "streaming"
    - "vidéo"
    - "client youtube"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/invidious"
---

Invidious est une interface permettant d'accéder aux vidéos Youtube sans passer par youtube.com
En plus de constituer un avantage sur le plan de la confidentialité (les données ne transitent pas directement par les services du géant), cette interface offre plusieurs fonctionnalités :
- Mode audio seul,
- Mode sombre,
- Possibilité d'afficher les commentaires Reddit plutôt que les commentaires YouTube,
- Possibilité de s'abonner aux chaines sans créer de compte Google
...
Une liste des instances proposant Invidious est consultable à cette adresse : https://redirect.invidious.io ; ou à celle-ci, moins actuelle : https://instances.invidio.us/


