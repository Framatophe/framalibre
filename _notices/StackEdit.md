---
nom: "StackEdit"
date_creation: "Mercredi, 14 février, 2018 - 19:49"
date_modification: "Jeudi, 29 mars, 2018 - 17:55"
logo:
    src: "images/logo/StackEdit.png"
site_web: "https://stackedit.io/"
plateformes:
    - "GNU/Linux"
langues:
    - "English"
description_courte: "StackEdit est un éditeur de Markdown open-source. Il permet donc la rédaction de contenu en format Markdown."
createurices: "Benoit Schweblin"
alternative_a: "OneNote, Evernote"
licences:
    - "Licence Apache (Apache)"
tags:
    - "bureautique"
    - "prise de notes"
    - "notes"
    - "markdown"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/stackedit"
---

L’avantage de cet outil pour rédiger ses contenus textuels est sa double interface : StackEdit est divisé en deux fenêtre, la première pour écrire et la deuxième pour lire. On écrit en Markdown dans la fenêtre de gauche et on lit le rendu dans la fenêtre de droite.
Le service propose l’écriture de documents, la synchronisation dans Google Docs ou Dropbox et la publication via Blogger, WordPress ou d’autres services. De plus, StackEdit propose un mode d’utilisation hors-ligne qui vous permet de continuer d’écrire sans avoir internet (il faut juste avoir la page déjà chargée).
Vous pouvez même rédiger vos documents en temps réel à plusieurs.
Tout cela dans le but de publier vos documentations dans vos repositories internes sous forme de Readme, ou de fournir des spécifications fonctionnelles au format PDF ou HTML facilement.

