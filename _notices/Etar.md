---
nom: "Etar"
date_creation: "Mercredi, 23 janvier, 2019 - 12:03"
date_modification: "Dimanche, 24 février, 2019 - 10:52"
logo:
    src: "images/logo/Etar.png"
site_web: "https://github.com/Etar-Group/Etar-Calendar/"
plateformes:
    - "Android"
    - "le web"
langues:
    - "Autres langues"
description_courte: "Etar est l'agenda Material design et open source pour tous!"
createurices: ""
alternative_a: "Google Agenda"
licences:
    - "Licence Apache (Apache)"
tags:
    - "agenda"
lien_wikipedia: ""
lien_exodus: "https://reports.exodus-privacy.eu.org/fr/reports/search/ws.xsoh.etar/"
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/etar"
---

Etar est une app de calendrier simple et efficace qui propose les traditionnelles vue jour/semaine/mois/agenda et permet de synchroniser plusieurs calendriers :
- le calendrier local de l'ordiphone
- abonnement ics
- nextcloud / owncloud et autre CalDAV
- exchange / google agenda

