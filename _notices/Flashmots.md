---
nom: "Flashmots"
date_creation: "Dimanche, 29 octobre, 2023 - 11:51"
date_modification: "dimanche, 31 mars, 2024 - 09:13"
logo:
    src: "images/logo/Flashmots.png"
site_web: "https://educajou.forge.apps.education.fr/flashmots/"
plateformes:
    - "Apple iOS"
    - "Android"
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
    - "Autre"
langues:
    - "Français"
description_courte: "Aide à la mémorisation orthographique"
createurices: "Arnaud Champollion"
alternative_a: "Je lis puis j'écris"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "éducation"
    - "dictée"
    - "orthographe"
    - "mémoire"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/flashmots"
---

Le fonctionnement est très simple : un mot est affiché (temps de lecture), puis caché (temps de mémorisation), l'élève doit ensuite l'écrire à l'identique.
Les délais de lecture et de mémorisation sont réglables via des curseurs. Ils s'adaptent automatiquement à la longueur du mot.
Cinq listes de mots invariables, plus une viden sont proposées par niveaux du CP au CM2, à titre indicatif. Chaque liste est éditable, on peut ainsi ajouter et supprimer des mots.
Les listes éditées sont partageables très facilement par lien.
