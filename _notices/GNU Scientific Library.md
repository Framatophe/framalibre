---
nom: "GNU Scientific Library"
date_creation: "Vendredi, 17 juillet, 2020 - 20:23"
date_modification: "Vendredi, 17 juillet, 2020 - 20:23"
logo:
    src: "images/logo/GNU Scientific Library.png"
site_web: "https://www.gnu.org/software/gsl/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Autre"
langues:
    - "English"
description_courte: "Une librairie de calcul complète écrite en C"
createurices: ""
alternative_a: "Matlab"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "science"
    - "calcul"
lien_wikipedia: "https://en.wikipedia.org/wiki/GNU_Scientific_Library"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/gnu-scientific-library"
---

La GNU Scientific Library (GSL) est une librairie de calcul écrite en C qui contient un grand nombre d'outils utiles dans le domaine du calcul scientifique. La librairie est fournie avec une librairie CBLAS spécialement écrite qui peut être remplacée par une CBLAS optimisée pour le processeur de la machine utilisée. Les algorithmes ne sont pas parallèlisés mais l'utilisation d'une CBLAS parallèlisée permet d'exploiter toute la puissance d'un processeur multi-coeur.

