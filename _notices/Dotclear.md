---
nom: "Dotclear"
date_creation: "Mercredi, 22 mars, 2017 - 08:39"
date_modification: "Dimanche, 26 mars, 2017 - 17:04"
logo:
    src: "images/logo/Dotclear.png"
site_web: "https://fr.dotclear.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Autre"
langues:
    - "Autres langues"
description_courte: "Ne fait que des blogs, mais le fait bien !"
createurices: "Olivier Meunier"
alternative_a: "wix"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "cms"
    - "blog"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Dotclear"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/dotclear"
---

Dotclear est un logiciel libre de publication web publié en 2003 par Olivier Meunier. Fruit du travail d'une seule personne à l'origine, le projet s'est rapidement doté d'une équipe constituée de personnalités diverses et d'horizons variés.
L'objectif du projet est de fournir un outil simple d'emploi permettant à tout un chacun de publier sur le web et ce, quel que soit son niveau de connaissances techniques.
Dotclear est un logiciel libre conçu avant tout pour ses utilisateurs et recevant des contributions régulières de ceux-ci. N'importe qui peut l'utiliser et le modifier selon les termes de la licence d'utilisation.

