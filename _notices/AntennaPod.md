---
nom: "AntennaPod"
date_creation: "Mercredi, 22 mars, 2017 - 10:05"
date_modification: "Mardi, 26 février, 2019 - 17:13"
logo:
    src: "images/logo/AntennaPod.png"
site_web: "http://antennapod.org/"
plateformes:
    - "Android"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Un gestionnaire de Podcast pour Android."
createurices: "Daniel Oeh"
alternative_a: ""
licences:
    - "Licence MIT/X11"
tags:
    - "multimédia"
    - "podcast"
    - "audio"
    - "musique"
lien_wikipedia: "https://en.wikipedia.org/wiki/AntennaPod"
lien_exodus: "https://reports.exodus-privacy.eu.org/fr/reports/search/de.danoeh.antennapod/"
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/antennapod"
---

AntennaPod est un lecteur et gestionnaire de podcast permettant l'accès à des millions de podcast gratuits ou payants produit aussi bien par des podcasters indépendants que de gros éditeurs.
Ajoutez, importez et exportez leurs flux facilement à partir d'ITunes, de fichiers OPML ou simplement à partir de liens RSS.
Grace à son lecteur RSS, il vous avertira également de tous les nouvelles vidéos qui seront disponible sur vos chaînes Youtube et Dailymotion favorites.

