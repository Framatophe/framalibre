---
nom: "libcaca"
date_creation: "Mercredi, 23 janvier, 2019 - 21:02"
date_modification: "Lundi, 10 mai, 2021 - 14:54"
logo:
    src: "images/logo/libcaca.png"
site_web: "http://caca.zoy.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
description_courte: "Bibliothèque graphique en mode texte."
createurices: "Samuel Hocevar"
alternative_a: ""
licences:
    - "Licence publique f***-en ce que vous voulez (WTFPL)"
tags:
    - "développement"
    - "graphisme"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Libcaca"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/libcaca"
---

La libcaca est une bibliothèque graphique qui génère des caractères colorés au lieu de pixels, la rendant ainsi compatible avec les vielles cartes graphiques et terminaux. Elle remplace l'ancienne bibliothèque AAlib.
Cette bibliothèque est compatible avec la plupart des terminaux textes sur les systèmes Linux, Windows ou MacOS.
Elle supporte de nombreuses opérations de rendus telles que des rotations ou des symétries.

