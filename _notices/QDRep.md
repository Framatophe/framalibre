---
nom: "QDRep"
date_creation: "Mardi, 26 mai, 2020 - 12:18"
date_modification: "Jeudi, 22 juillet, 2021 - 11:47"
logo:
    src: "images/logo/QDRep.png"
site_web: "http://projet.eu.org/pedago/sin/project/QDRep/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "FirefoxOS"
    - "Windows Mobile"
    - "Apple iOS"
    - "le web"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "QDRep, une solution libre pour l’échange et le partage de fichiers."
createurices: "Dominique Laporte"
alternative_a: "Dropbox"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "partage de fichiers"
    - "éducation"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/qdrep"
---

QDrep se veut une solution alternative, légère et sans prétention, à des plates‑formes comme Dropbox.
L'initiative est survenue suite au confinement du COVID-19 et au besoin de partager des documents dans le milieu éducatif dans le respect du RGPD de façon simple, rapide et légère.
QDrep est écrit en PHP et s’installe sur un serveur, sans avoir recours à une base de données, en dézippant tout simplement l’archive sous la racine. Il peut aussi être utilisé comme serveur d'instances de lui-même.
Quelques caractéristiques de QDREp :
* Léger (~2000 lignes de code)
* Simple à installer (paste ‘n’ play)
* Sans base de données
* Partage direct par URL
* Libre et gratuit
* Compatible RGPD
* Responsive (ie : utilisable sur SMART)
* Disponible en 6 langues
* Avec ou sans connexion anonyme
* Nécessite PHP 5+
Le bac à sable

