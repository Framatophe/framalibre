---
nom: "Flus"
date_creation: "vendredi, 22 décembre, 2023 - 11:15"
date_modification: "vendredi, 8 mars, 2024 - 16:35"
logo:
    src: "images/logo/Flus.svg"
site_web: "https://flus.fr/"
plateformes:
    - "le web"
langues:
    - "Français"
description_courte: "Une plateforme pour agréger, stocker et partager votre veille autour de vous."
createurices: "Marien Fressinaud"
alternative_a: "Google Reader, Feedly, Pocket"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "lecteur de flux RSS"
    - "veille"
    - "agrégateur"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

Flus est un outil qui permet :
- de stocker des liens pour les lire plus tard
- de suivre les actualités d'autres sites

Mais surtout, qui propose une façon apaisante de vider sa pile de lectures (ou de vidéos !) grâce au journal, qui sélectionne aléatoirement quelques liens pour ne pas se sentir submergé par tout ce qui nous reste à lire. Quel plaisir de vider son journal et de demander 3 nouveaux liens !

Le créateur réalise aussi des articles explicatifs, par exemple pour vulgariser comment il est possible de [suivre l'actualité d'autres sites sans passer par les médias sociaux](https://flus.fr/carnet/a-quoi-servent-les-flux.html)

Et sinon, pêle-mêle, de [la transparence sur les coûts et un prix libre](https://flus.fr/tarifs), un [détecteur de pisteurs](https://flus.fr/carnet/luttez-contre-les-pisteurs-avec-flus.html), de [la collaboration entre les utilisateurs](https://flus.fr/carnet/publiez-votre-veille-a-plusieurs.html)
