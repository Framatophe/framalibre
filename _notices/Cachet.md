---
nom: "Cachet"
date_creation: "Jeudi, 8 février, 2018 - 21:00"
date_modification: "Jeudi, 8 février, 2018 - 21:00"
logo:
    src: "images/logo/Cachet.png"
site_web: "https://cachethq.io"
plateformes:
    - "GNU/Linux"
    - "le web"
langues:
    - "Autres langues"
description_courte: "Un système de page d'état opensource pour tout le monde"
createurices: ""
alternative_a: "StatusHub"
licences:
    - "Berkeley Software Distribution License (BSD)"
tags:
    - "cms"
    - "monitoring"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/cachet"
---

Cachet est un système de page d'état simple à utiliser et facile à gérer. Il peut être facilement installé et utilisé pour suivre le service d'un tiers en informant vos utilisateurs de ce qui se passe.
Cachet est 100% open source, ouvrant un monde de contributions et de nouvelles fonctionnalités.
Livré avec une API RESTful, Cachet offre des possibilités infinies.
Cachet est construit en gardant à l'esprit l'expérience utilisateur, en veillant à ce que chaque détail ressemble et agit comme vous l'espérez.

