---
nom: "KeePassDX"
date_creation: "Mercredi, 4 avril, 2018 - 12:52"
date_modification: "Vendredi, 15 avril, 2022 - 19:38"
logo:
    src: "images/logo/KeePassDX.png"
site_web: "https://f-droid.org/packages/com.kunzisoft.keepass.libre/"
plateformes:
    - "Android"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Keepass DX est un client KeePass Android de gestion d'identités numériques et de mots de passe."
createurices: "J-Jamet"
alternative_a: "lastpass, 1password, dashlane"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "sécurité"
    - "chiffrement"
    - "gestionnaire de mots de passe"
    - "mot de passe"
    - "formulaire"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/keepassdx"
---

Gestionnaire de fichiers KeePass multiformats, l'application permet de créer des clés et mots de passe de manière sécurisée en s'intégrant parfaitement aux normes de design Android.
Fonctionnalités :
 * Création de bases de données / entrées et groupes
 * Support des fichiers .kdb et .kdbx (version 1 à 4) avec algorithme AES - Twofish - ChaCha20 - Argon2
 * Compatible avec la majorité des programmes alternatifs (KeePass, KeePassX, KeePassXC...)
 * Permet la copie rapide de champs et l'ouverture d'URI /URL
 * Reconnaissance biométrique pour un déblocage rapide (Empreintes digitales / Déverouillage par visage / ...)
 * Gestion des mots de passe à usage unique (One-Time Password HOTP / TOTP) pour l'authentification à deux facteurs (2FA)
 * Material design avec thèmes
 * Remplissage automatique de champs
 * Clavier de remplissage de champs
 * Gestion précise des paramètres
 * Code écrit en langage natif (Kotlin / Java / JNI / C)

