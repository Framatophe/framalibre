---
nom: "GIMP UP"
date_creation: "Jeudi, 1 août, 2019 - 19:23"
date_modification: "Vendredi, 2 août, 2019 - 17:31"
logo:
    src: "images/logo/GIMP UP.jpg"
site_web: "https://fr.tuto.com/gimp/gratuit-gimp-up-une-version-portable-augmentee-de-gimp-…"
plateformes:
    - "Windows"
langues:
    - "Français"
    - "Autres langues"
description_courte: "GIMP version portable contenant de nombreuses extensions."
createurices: "Spencer Kimball, Peter Mattis"
alternative_a: "Adobe Photoshop, Affinity photo, Affinity designer"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "création"
    - "photo"
    - "graphisme"
    - "image"
    - "retouche"
    - "dessin"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/gimp-0"
---

GIMP UP est une version portable de GIMP 2.10 pour Windows 64bits, enrichie de nombreuses extensions, que vous pouvez aisément glisser sur une clé USB d’où vous pourrez la démarrer sans avoir à l’installer sur votre système.
Disposant d’une interface conviviale, GIMP UP contient de nombreuses extensions lui conférant des fonctions absentes de la version de base de GIMP :
Separate et Cyan pour l’export en CMJN
BIMP pour le traitement d’images par lot
Pandora pour la création de panoramas
Liquid Rescale pour étirer une image sans en déformer certains objets
Resynthesizer pour supprimer un objet d’une image
UFRaw pour le traitement des fichiers RAW
Webexport pour exporter rapidement au format jpeg optimisé
… et de nombreuses autres extensions !
Cette version portable, pour peu que vous la laissiez sur clé USB, cohabitera parfaitement avec la version officielle de GIMP, dont elle se veut complémentaire.

