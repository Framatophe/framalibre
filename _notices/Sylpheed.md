---
nom: "Sylpheed"
date_creation: "Mardi, 21 mars, 2017 - 20:52"
date_modification: "Mercredi, 12 mai, 2021 - 16:21"
logo:
    src: "images/logo/Sylpheed.png"
site_web: "http://sylpheed.sraoss.jp/en/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
description_courte: "Sylpheed, un client de messagerie qui prend en charge le stockage MH"
createurices: "Hiroyuki Yamamoto"
alternative_a: "Microsoft Outlook"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
    - "client mail"
    - "pgp"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Sylpheed"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/sylpheed"
---

Sylpheed possède trois grands intérêts. Le 1er, c'est que chaque mail est un fichier. Le 2e est qu'il prend en charge nativement les expressions régulières ou rationnelles au niveau des règles de filtrage anti-spam. La 3e, c'est la simplicité de prise en charge du chiffrage PGP.

