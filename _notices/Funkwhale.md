---
nom: "Funkwhale"
date_creation: "Mardi, 4 septembre, 2018 - 15:58"
date_modification: "Vendredi, 26 février, 2021 - 19:31"
logo:
    src: "images/logo/Funkwhale.png"
site_web: "https://funkwhale.audio/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
    - "le web"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Un logiciel de streaming musical décentralisé et fédéré alliant hébergement et fonctionnalités de partage."
createurices: "Agate Berriot"
alternative_a: "Deezer, Spotify, Soundcloud"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "musique"
    - "streaming"
    - "hébergement"
    - "décentralisation"
    - "fediverse"
    - "activitypub"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Funkwhale"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/funkwhale"
---

Funkwhale est un logiciel de streaming musical décentralisé et fédéré.
Le nom de Funkwhale rend hommage au logiciel Grooveshark qui l'a grandement inspiré : interface, approche mettant l'accent sur l'interaction entre utilisateur·rice·s, création de radios, etc.
Funkwhale va toutefois plus loin, avec un fonctionnement :
Décentralisé : Chaque instance peut suivre une ou plusieurs autres instances Funkwhale afin de permettre à leurs utilisateur·rice·s d'interagir et de partager les contenus hébergés.
Fédéré : Via le protocole ActivityPub, Funkwhale peut interagir avec d'autres logiciels qui font partie du Fediverse, comme Mastodon ou PeerTube par exemple.
Plus d'infos :
Un article détaillé de NextInpact
La version de démonstration (utilisateur : demo, mot de passe : demo)

