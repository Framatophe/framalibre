---
nom: "GestAssoPhp"
date_creation: "Dimanche, 1 janvier, 2017 - 17:57"
date_modification: "Mardi, 11 mai, 2021 - 23:14"
logo:
    src: "images/logo/GestAssoPhp.png"
site_web: "http://gestassophp.free.fr/cms/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Windows Mobile"
langues:
    - "Français"
description_courte: "Gestion en ligne des adhérents d'une association et de leur cotisation développée en  PHP/MySql ou PostgreSql."
createurices: "JC Etiemble"
alternative_a: ""
licences:
    - "Creative Commons -By-Sa"
tags:
    - "métiers"
    - "association"
    - "gestion"
    - "gestion de la relation client"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/gestassophp"
---

Une gestion en ligne depuis un site Web (PHP et MySql ou PostgreSql) pour les associations comprenant la gestion des adhérents et de leurs cotisations :
- Un accès pour chacun des membres par mot de passe,
- Un annuaire pour tous les membres qui ont donné leur accord,
- Une gestion multi-administrateurs par priorité pour les accès.
- Un tableau de bord,
- Une liste des adhérents avec leurs informations,
- Une liste des cotisants avec la création (gestion multi-cotisations),
- Une liste des fichiers des adhérents.
Et une aide sur chaque page.
Installation très simple sur serveur mutualisé, possibilité d'ajuster les couleurs et les paramètres d'affichage.
Il est possible d'accéder à une version de démonstration sur demande.

