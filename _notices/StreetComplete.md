---
nom: "StreetComplete"
date_creation: "jeudi, 26 octobre, 2023 - 14:31"
date_modification: "jeudi, 26 octobre, 2023 - 14:31"
logo:
    src: "images/logo/SreetComplete.svg"
site_web: "https://streetcomplete.app/"
plateformes:
    - "Android"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Contribuez à OpenStreetMap en répondant à des questions simples."
createurices: "Tobias Zwick"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "cartographie"
    - "édition de cartes géographiques"
    - "OpenStreetMap"
    - "gps"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: "Q56221667"
mis_en_avant: "oui"
---

Cette application recense les informations incomplètes et à améliorer autour de vous et les affiche sur une carte sous forme de marqueurs. 
L’information liée à chaque marqueur peut être complétée en répondant à une question simple.

Les informations que vous saisissez sont ensuite directement ajoutées à OpenStreetMap, à votre nom, sans qu’il soit nécessaire d’utiliser un autre éditeur de carte. 
