---
nom: "Sphinx"
date_creation: "Dimanche, 10 avril, 2016 - 22:48"
date_modification: "Mardi, 10 janvier, 2017 - 04:09"
logo:
    src: "images/logo/Sphinx.png"
site_web: "http://www.sphinx-doc.org"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Sphinx permet de générer très facilement une documentation structurée à partir de fichiers sources."
createurices: "Georg Brandl"
alternative_a: ""
licences:
    - "Berkeley Software Distribution License (BSD)"
tags:
    - "développement"
    - "documentation"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Sphinx_%28g%C3%A9n%C3%A9rateur_de_documentation%29"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/sphinx"
---

Générateur de documentation
Basé sur Python, permet de générer très facilement une documentation structurée à partir de fichiers sources en RestructuredText.
Parmi les formats de sortie :
HTML (ensemble de pages statiques et arborescence de répertoire)
LaTeX / PDF
Initialement développé pour une documentation technique de Python, Sphinx peut être utilisé dans le cadre d'une documentation utilisateur, d'une procédure...

