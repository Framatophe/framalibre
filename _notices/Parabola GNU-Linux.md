---
nom: "Parabola GNU/Linux"
date_creation: "Jeudi, 29 décembre, 2016 - 17:17"
date_modification: "Mardi, 1 septembre, 2020 - 11:30"
logo:
    src: "images/logo/Parabola GNU-Linux.png"
site_web: "https://www.parabola.nu/"
plateformes:
    - "GNU/Linux"
langues:
    - "Autres langues"
description_courte: "Une distribution GNU/Linux 100% Libre !"
createurices: ""
alternative_a: "Microsoft Windows, MacOS"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
    - "système d'exploitation (os)"
    - "linux"
    - "distribution gnu/linux"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Parabola_GNU/Linux-libre"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/parabola-gnulinux"
---

Parabola (dont le nom officiel est Parabola GNU/Linux-libre) est un système d'exploitation basé sur la distribution Arch Linux.
Le développement de Parabola se concentre sur la simplicité pour l'administrateur, l'implication de la communauté et l'utilisation des logiciels libres les plus récents du fait d'un modèle de publication continue. Parabola GNU/Linux-libre fait partie de la liste des systèmes d'exploitation libres de la Fondation pour le logiciel libre (Free Software Foundation).
Cette distribution est vraiment faite pour des utilisateurs experts, il n'est pas facile de l'installer. Elle n'utilise pas le noyau Linux officiel car il comporte depuis quelques années du code non-libre. La FSF a donc créé une version du noyau 100% libre, appelé : Noyau Linux-libre.

