---
nom: "2FAuth"
date_creation: "mercredi, 3 janvier, 2024 - 17:08"
date_modification: "mercredi, 3 janvier, 2024 - 17:08"
logo:
    src: "images/logo/2FAuth.png"
site_web: "https://docs.2fauth.app/"
plateformes:
    - "le web"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Gérer vos comptes à double authentification (2FA) et générer leurs mots de passe uniques"
createurices: "Bubka"
alternative_a: "Google Authenticator, Authy, Aegis, 2FAS, MS Authenticator"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "2FA"
    - "double authentification"
    - "sécurité"
    - "auto-hébergement"
    - "MFA"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

2FAuth est une application Web dont la mission première est de générer des mots de passe uniques, aussi appelés One-Time Passwords (OTP). Sa simplicité d'utilisation et sa disponibilité sur tous les écrans du quotidien (smartphones, tablettes, bureaux) en font le compagnon idéal pour la connexion à vos comptes protégés par une double authentification (2FA/MFA).

Alternative directe à Google Authenticator, Authy ou encore MS Authenticator, 2FAuth va au delà et s'intègre parfaitement dans une démarche de dégooglelisation. Il s'agit d'une solution open source, auto-hébergeable, offrant une maitrise totale sur vos données 2FA et leur sécurité. Multi-utilisateur, une instance de 2FAuth peut être partagée entre amis, membres d'une famille ou entre collègues. Une API est également disponible pour permettre une intégration dans un système existant.
