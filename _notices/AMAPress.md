---
nom: "AMAPress"
date_creation: "Mercredi, 29 juillet, 2020 - 01:14"
date_modification: "Vendredi, 7 mai, 2021 - 11:02"
logo:
    src: "images/logo/AMAPress.png"
site_web: "https://amapress.fr/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "FirefoxOS"
    - "Windows Mobile"
    - "Apple iOS"
    - "Autre"
langues:
    - "Français"
description_courte: "Logiciel de gestion et de communication (externe et interne) pour les AMAP basé sur Wordpress"
createurices: "Le comptoir des appli"
alternative_a: "La ruche qui dit oui"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "métiers"
    - "amap"
    - "circuit court"
    - "système de commande de nourriture en ligne"
    - "gestion"
    - "agriculture"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/amapress"
---

Plugin pour Wordpress permettant de gérer différents besoins des Associations de Maintient de l'Agriculture Paysanne allant du suivi des contrats à l'échange de paniers, le partage de recettes, l'édition d'un site web, d'infolettres.

