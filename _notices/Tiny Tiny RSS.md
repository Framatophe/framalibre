---
nom: "Tiny Tiny RSS"
date_creation: "Mardi, 14 mars, 2017 - 15:37"
date_modification: "Mercredi, 12 mai, 2021 - 16:40"
logo:
    src: "images/logo/Tiny Tiny RSS.png"
site_web: "https://tt-rss.org/gitlab/fox/tt-rss"
plateformes:
    - "GNU/Linux"
    - "le web"
langues:
    - "English"
description_courte: "Tiny Tiny RSS (TTRSS) est un lecteur et agrégateur RSS/ATOM auto-hébergé."
createurices: "Andrew Dolgov"
alternative_a: "Netvibes, Feedly"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "lecteur de flux rss"
    - "atom"
    - "agrégateur"
    - "auto-hébergement"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Tiny_Tiny_RSS"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/tiny-tiny-rss"
---

Tiny Tiny RSS  (souvent abrégé en TTRSS) est un lecteur et agrégateur RSS/ATOM à installer sur un serveur LAMP.
Accessible ensuite depuis n'importe quel navigateur, il permet de s'abonner aux flux RSS ou ATOM des sites que vous souhaitez suivre afin de recevoir les derniers articles publiés. Une application officielle android est également disponible.
Il propose de nombreux fonctionnalités telles que l'import / export OPML, le partage via RSS ou réseaux sociaux, l'ajout de plugins ou de thèmes, le support des podcasts, le filtrage des flux RSS, etc.

