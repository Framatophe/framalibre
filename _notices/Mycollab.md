---
nom: "Mycollab"
date_creation: "Vendredi, 21 août, 2020 - 10:41"
date_modification: "Lundi, 10 mai, 2021 - 13:04"
logo:
    src: "images/logo/Mycollab.png"
site_web: "https://mycollab.com/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "le web"
langues:
    - "Autres langues"
description_courte: "Mycollab c'est trois modules de collaboration pour les petites et moyennes entreprises : gestion de projet,"
createurices: ""
alternative_a: "Basecamp, Microsoft Project, Asana"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "gestion de projet"
    - "conduite de projet"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/mycollab"
---

Il existe deux options de licence : une édition commerciale "ultime", qui est plus rapide et peut être exécutée sur site ou dans le nuage, et l'édition open source "communautaire", qui est la version qui nous intéresse ici.
L'édition communautaire n'a pas d'option "cloud" et est plus lente, car elle n'utilise pas de cache de requêtes, mais offre des fonctionnalités essentielles de gestion de projet, notamment les tâches, la gestion des problèmes, le flux d'activité, la vue de la feuille de route et un tableau Kanban pour les équipes agiles. Bien qu'il n'ait pas d'application mobile séparée, il fonctionne sur les appareils mobiles ainsi que sur Linux, Unix, Windows et MacOS

