---
nom: "OpenKeychain"
date_creation: "Dimanche, 3 décembre, 2017 - 18:55"
date_modification: "Lundi, 4 décembre, 2017 - 11:16"
logo:
    src: "images/logo/OpenKeychain.png"
site_web: "https://www.openkeychain.org/"
plateformes:
    - "Android"
langues:
    - "Autres langues"
description_courte: "Chiffrez vos fichiers et communications. Compatible avec le standard OpenPGP."
createurices: ""
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "sécurité"
    - "openpgp"
    - "pgp"
    - "chiffrement"
    - "authentification"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/openkeychain"
---

OpenKeychain vous aide à communiquer de manière plus confidentielle et sécurisée. Il utilise le chiffrement pour s'assurer que vos messages ne peuvent être lus que par les personnes à qui vous les envoyez, d'autres peuvent vous envoyer des messages que vous seul pouvez lire, et ces messages peuvent être signés numériquement afin que les personnes qui les reçoivent soient sûres qui les a envoyés. OpenKeychain est basé sur la norme bien établie OpenPGP rendant le chiffrement compatible entre vos appareils et systèmes.

