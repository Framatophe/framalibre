---
nom: "WAPT"
date_creation: "Mardi, 3 janvier, 2017 - 17:29"
date_modification: "Mercredi, 12 mai, 2021 - 15:37"
logo:
    src: "images/logo/WAPT.png"
site_web: "https://www.tranquil.it/gerer-parc-informatique/decouvrir-wapt/"
plateformes:
    - "GNU/Linux"
    - "Windows"
langues:
    - "Français"
    - "English"
description_courte: "WAPT est un logiciel Open Source de déploiement d'applications."
createurices: ""
alternative_a: "SCCM, LANDESK management suite, DELL Kace"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
    - "installation"
    - "administration système"
    - "système d'exploitation (os)"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Wapt_(logiciel)"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/wapt"
---

WAPT est l’équivalent d’apt-get pour Windows, il vous permet d’automatiser votre gestion de parc grâce à sa console de gestion centralisée. Déployez vos logiciels en toute tranquillité grâce à la version Enterprise qualifiée par l'ANSSI.
Avec WAPT, vous pouvez installer, mettre à jour et désinstaller vos logiciels et configurations sur votre parc rapidement et avec une remontée d’information fiable sur le déroulement de vos déploiements.
Pourquoi WAPT ?
Tranquil IT  est une entreprise qui a la vocation d’aider les entreprises et collectivités avec la gestion de leurs systèmes informatiques et d’assister les administrateurs systèmes dans leurs tâches quotidiennes. L'entreprise développe également une expertise unique en France sur Samba Active Directory.
WAPT bénéficie de plusieurs ressources comme un Forum et même une chaîne Youtube

