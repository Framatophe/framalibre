---
nom: "DNS66"
date_creation: "Samedi, 25 mars, 2017 - 12:27"
date_modification: "Mercredi, 12 mai, 2021 - 15:08"
logo:
    src: "images/logo/DNS66.png"
site_web: "https://jak-linux.org/projects/dns66/"
plateformes:
    - "Android"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Régulez votre trafic DNS."
createurices: "Julian Andres Klode"
alternative_a: "DNSet, DNS Changer"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
    - "réseau"
    - "bloqueur de publicité"
    - "vie privée"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/dns66"
---

DNS66 fonctionne à partir d'une interface VPN (aucune connexion à un quelconque VPN) afin de filtrer tout le trafic de votre smartphone. A la manière d'AdAway, l'application filtre les requêtes à l'aide de listes. Il est possible d'y ajouter les vôtres. DNS66 propose l'exclusion d'applications du filtrage, ces dernières étant toutes concernées par défaut.
Elle permet aussi d'utiliser ses propres résolveurs DNS, en y intégrant quelques-uns par défaut.

