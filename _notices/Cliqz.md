---
nom: "Cliqz"
date_creation: "Vendredi, 21 avril, 2017 - 15:14"
date_modification: "Vendredi, 29 juillet, 2022 - 00:32"
logo:
    src: "images/logo/Cliqz.png"
site_web: "https://cliqz.com/fr/"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "FirefoxOS"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Une version de Firefox orientée protection des données personnelles."
createurices: ""
alternative_a: "Google Chrome, Opera, Internet Explorer, Apple Safari"
licences:
    - "Licence Publique Mozilla (MPL)"
tags:
    - "internet"
    - "navigateur web"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Cliqz"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/cliqz"
---

[Attention, ce navigateur a été abandonné le 29 avril 2020] Ce navigateur internet (une version dérivée de Firefox) est orienté protection de la vie privée. Il contient ainsi un anti-trackers (similaire à l'extension Ghostery) et un système de suggestion de sites qui construit ses propositions d'après le surf de tous les utilisateurs (et pas d'après "des robots [d'indexation]").

