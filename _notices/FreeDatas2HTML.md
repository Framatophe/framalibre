---
nom: "FreeDatas2HTML"
date_creation: "Mercredi, 9 mars, 2022 - 17:09"
date_modification: "Mercredi, 9 mars, 2022 - 17:09"
logo:
    src: "images/logo/FreeDatas2HTML.png"
site_web: "https://freedatas2html.le-fab-lab.com"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Autre"
langues:
    - "Français"
description_courte: "Module TypeScript/JavaScript permettant de parser et visualiser des données CSV, JSON ou HTML."
createurices: "Fabrice PENHOËT"
alternative_a: ""
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "développement"
    - "json"
    - "typescript"
    - "javascript"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/freedatas2html"
---

Partant du besoin d'une association souhaitant afficher le contenu d'un fichier CSV sur son site web, FreeDatas2HTML permet au final d'extraire et afficher des données au format CSV, JSON ou HTML.
Les données peuvent se situer dans la page elle-même, dans un fichier ou encore dans une source distante (API).
Une fois extraites, leur affichage propose de nombreuses options : choix des champs/colonnes à (ré)afficher, mise en forme (tableau, liste HTML), compteur d'enregistrements, etc.
Au-delà de simplement lister les données, il est possible d'ajouter des outils pour classer les données, les paginer ou encore les filtrer, par exemple via un moteur de recherche. Tout se fait côté client, sans nouvel appel au serveur web.
Le code source est écrit en TypeScript, mais une version compilée en JavaScript est proposée. Il est pensé par faciliter son adaptation à votre besoin.
Le site propose quelques exemples d'utilisation et une documentation en français.

