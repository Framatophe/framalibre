---
nom: "Glaxnimate"
date_creation: "Lundi, 3 octobre, 2022 - 19:42"
date_modification: "Mardi, 17 octobre, 2023 - 21:50"
logo:
    src: "images/logo/Glaxnimate.png"
site_web: "https://glaxnimate.mattbas.org/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Programme d'animation graphique vectoriel rapide et facile à utiliser, supportant de nombreux formats."
createurices: "Mattia Basaglia"
alternative_a: "Adobe Animate CC, Flash (animation)"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "animation"
    - "vectoriel"
    - "dessin vectoriel"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/glaxnimate"
---

Glaxnimate est un logiciel est un éditeur d'animation, orienté vectoriel, orienté vers les formats récents d'animation, notamment pour le web. Il est compatible avec les formats GIF, WebP, Lottie (dont sticker animés Telegram), SVG (dont SVG animé), synfig, RIVE, vidéo et feuille de sprites.
Il permet également d'aller plus loin en codant ses propres scripts en Python. Une bibliothèque indépendante python (glaxnimate) est également disponible pour être utilisée dans des scripts indépendant de l'éditeur. Enfin, il peut être utilisé via MLT avec les éditeurs vidéos KDEnlive et Shotcut pour ajouter des éléments vectoriels animés aux effets.
Quelques points clefs:
- Logique vectorielle
- Système de calques et groupes
- Interpolation d'image pour des animations plus fluide
- vectorisation d'animations
- Interface fortement modifiable
Le logiciel est codé en C++ et utilise Qt5. On trouve pour Linux un AppImage, un paquet Debian et AUR

