---
nom: "inventaire"
date_creation: "Jeudi, 22 décembre, 2016 - 17:11"
date_modification: "Mercredi, 12 mai, 2021 - 18:55"
logo:
    src: "images/logo/inventaire.jpg"
site_web: "https://inventaire.io"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "FirefoxOS"
    - "Windows Mobile"
    - "Autre"
    - "le web"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Vos amis et communautés sont votre meilleure bibliothèque !"
createurices: "maxlath, jums"
alternative_a: ""
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "partage"
    - "livre"
    - "savoir libre"
    - "collection"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/inventaire"
---

Faites l'inventaire de vos livres - et de ce que vous souhaitez en faire : "ce livre je peux le prêter, ce livre je peux le donner, celui-là je le vends..."
Partagez vos inventaires entres amis et groupes - Vous avez toujours rêvé d'une bibliothèque collaborative pour votre coworking / association / club de lecture ? C'est ici !
Garder la trace des livres que vous prêtez et empruntez - Maintenant, les livres que vous prêtez pourraient même revenir !
inventaire.io

