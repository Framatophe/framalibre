---
nom: "Libertempo"
date_creation: "Mercredi, 17 octobre, 2018 - 15:16"
date_modification: "Lundi, 10 mai, 2021 - 13:22"
logo:
    src: "images/logo/Libertempo.png"
site_web: "http://libertempo.tuxfamily.org"
plateformes:
    - "GNU/Linux"
langues:
    - "Français"
description_courte: "Libertempo : Application libre de gestion des congés"
createurices: "wouldsmina, prytoegrian"
alternative_a: "opentime, leavedays, figgo"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "métiers"
    - "congés"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/libertempo"
---

Libertempo est une application web de gestion des congés du personnel d’un service, une association, une entreprise, une administration, etc. Elle a pour vocation d'inclure les règles métier liées à la législation française.

