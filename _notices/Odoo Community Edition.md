---
nom: "Odoo Community Edition"
date_creation: "Mardi, 9 janvier, 2018 - 16:31"
date_modification: "Mardi, 11 mai, 2021 - 23:14"
logo:
    src: "images/logo/Odoo Community Edition.png"
site_web: "https://www.odoo.com/page/community"
plateformes:
    - "GNU/Linux"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Un PGI (ERP) extensible, piles fournies !"
createurices: ""
alternative_a: "SAP ERP, Sage, CIEL, Odoo Enterprise Edition"
licences:
    - "Licence publique générale limitée GNU (LGPL)"
tags:
    - "métiers"
    - "erp"
    - "crm"
    - "pgi"
    - "gestion"
    - "gestion de la relation client"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Odoo"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/odoo-community-edition"
---

Odoo Community (précédemment OpenERP) est un progiciel de gestion intégré. Il permet de gérer (entre autres) la clientèle, les commandes, les devis, la facturation, des sites web (oui, il intègre un CMS complet), etc.
De plus, Odoo pousse la modularité à l’extrême, permettant de développer rapidement des modules pour ajouter ou modifier des fonctionnalités (il existe d’ailleurs de nombreux modules d’extension libres). Le développement se fait majoritairement en XML (pour la partie déclarative) et en Python (pour le fonctionnel).
Odoo représente un investissement conséquent au début : plus lourd que d’autres PGI à l’installation et un peu plus difficile d’abord, il est en revanche plus facilement personnalisable, grâce à la possibilité d’écrire facilement des modules qui interagissent entre eux ou avec des modules tiers sans avoir à modifier le code source de ces derniers.

