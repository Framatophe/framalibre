---
nom: "WebArchives"
date_creation: "Samedi, 13 janvier, 2018 - 11:49"
date_modification: "Vendredi, 7 mai, 2021 - 16:58"
logo:
    src: "images/logo/WebArchives.png"
site_web: "https://birros.github.io/projects/web-archives.html"
plateformes:
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Español"
description_courte: "Un lecteur d'archives web permettant de parcourir hors ligne des projets tels que Wikipédia ou Wikisource."
createurices: ""
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
    - "web"
    - "archives"
    - "wiki"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/webarchives"
---

Cette petite application sans prétention est là pour répondre à un besoin précis, celui de pouvoir accéder au contenu de projets communautaires, tels que Wikipédia, Wiktionnaire ou encore Wikisource, partout tout le temps.
Elle est loin d'être un foudre de guerre mais a l'avantage de parfaitement s'intégrer à l'environnement GNOME et ainsi lui fournir le support des fichiers au format ZIM.
Cette application étant directement inspirée de l'application Kiwix et partageant avec elle l'utilisation des fichiers ZIM, il est conseiller de se tourner vers cette dernière pour un usage similaire sur d'autres plateformes.

