---
nom: "LightZone"
date_creation: "Samedi, 19 décembre, 2020 - 13:52"
date_modification: "Vendredi, 15 décembre, 2023 - 21:53"
logo:
    src: "images/logo/LightZone.png"
site_web: "http://lightzoneproject.org/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "LightZone est conçu pour la photographie et permet d'éditer des photos rapidement et facilement."
createurices: "Fabio Riccardi"
alternative_a: "Adobe Lightroom"
licences:
    - "Berkeley Software Distribution License (BSD)"
tags:
    - "création"
    - "photo"
    - "raw"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/lightzone"
---

LightZone est spécialement conçu pour la photographie et permet à quiconque, indépendamment de ses compétences photographiques, d'éditer des photos rapidement et facilement. Le traitement d'image breveté de LightZone analyse chaque photo et permet aux éditeurs de photos d'effectuer des modifications visuelles intelligentes, instantanément et facilement. Les nouveaux styles instantanés intégrés de LightZone, tels que noir et blanc, plage dynamique élevée, contraste local, tonification, donnent aux éditeurs de photos les résultats instantanés qu'ils souhaitent. Modifiez un style ou plongez-vous directement et utilisez les outils intelligents de LightZone pour le personnaliser.

