---
nom: "Noethys"
date_creation: "Lundi, 19 mars, 2018 - 23:26"
date_modification: "Vendredi, 15 décembre, 2023 - 10:34"
logo:
    src: "images/logo/Noethys.png"
site_web: "https://www.noethys.com/"
plateformes:
    - "GNU/Linux"
    - "Windows"
langues:
    - "Français"
description_courte: "Noethys est un logiciel de gestion d'activités diverses : crèche, MJC, ALSH, ALAE, club sportif..."
createurices: "Ivan LUCAS"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "métiers"
    - "cantine"
    - "gestion"
    - "facturation"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/noethys"
---

Noethys est un logiciel permettant de gérer des activités. De la saisie des familles et individus à la facturation en passant par le pointage et l'encaissement. Noethys vous permet d'effectuer toutes les tâches administratives quotidiennes avec efficacité et rapidité.
Il s'agit bien d'un "logiciel métier" : un investissement lors de la prise en main et du paramétrage. En effet Noéthys est très finement paramétrable pour s'adapter à des fonctionnements variés.
Attestation de présence, devis, location, statistiques, comptabilité sommaire permettant l'exportation, gestion des collaborateurs, ordres de prélèvement compatible avec le trésor public, paiement par internet, envois de SMS, etc. Le logiciel est très riche en fonctionnalités et en constante évolution.
Il s'est doté d'un système de pointage sur tablette (Nomadhys), un portail internet (Connecthys) pour les usagers et une version full web (Noethysweb) en SAAS par le développeur ou en autohébergement.

