---
nom: "Steam Trader Tools"
date_creation: "Jeudi, 31 août, 2017 - 11:56"
date_modification: "samedi, 6 janvier, 2024 - 19:53"
logo:
    src: "images/logo/Steam Trader Tools.png"
site_web: "https://steam-trader-tools.matthieu.app/"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
description_courte: "Gérer vos clés Steam en double !"
createurices: "Matthieu42"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "jeu"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/steam-trader-tools"
---

Logiciel java qui permet de gérer ses clés Steam en double, les échanger plus facilement, etc.
Vous pouvez associer plusieurs clés pour chaque jeux, lui donner différents états.
Possibilité d'importer une liste sous format CSV (voir FAQ du site officiel)


