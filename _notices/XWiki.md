---
nom: "XWiki"
date_creation: "Mardi, 27 décembre, 2016 - 16:40"
date_modification: "Jeudi, 30 mars, 2017 - 13:19"
logo:
    src: "images/logo/XWiki.png"
site_web: "http://www.xwiki.org"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "La meilleure façon d'organiser l'information"
createurices: "XWiki Development Team"
alternative_a: "Confluence"
licences:
    - "Licence publique générale limitée GNU (LGPL)"
tags:
    - "cms"
    - "wiki"
    - "web"
    - "développement"
lien_wikipedia: "https://fr.wikipedia.org/wiki/XWiki"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/xwiki"
---

XWiki est une plate-forme pour travail collaboratif et moteur de wiki aux fonctionnalités et aux possibilités de programmation avancées vous permettant de personnaliser votre interface, de structurer vos données et de répondre précisément à vos besoins métiers.

