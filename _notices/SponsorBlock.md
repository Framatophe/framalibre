---
nom: "SponsorBlock"
date_creation: "jeudi, 28 décembre, 2023 - 18:43"
date_modification: "jeudi, 28 décembre, 2023 - 18:43"
logo:
    src: "images/logo/SponsorBlock.png"
site_web: "https://sponsor.ajay.app/"
plateformes:
    - "le web"
    - "Apple iOS"
    - "Android"
langues:
    - "English"
description_courte: "SponsorBlock est une extension et une API pour sauter des segments de sponsor dans des vidéos YouTube."
createurices: "Ajay Ramachandran"
alternative_a: ""
licences:
    - "Licence publique générale limitée GNU (LGPL)"
tags:
    - "internet"
    - "bloqueur de publicité"
    - "add-on"
    - "extension"
    - "navigateur web"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: "Q103887994"
mis_en_avant: "non"

---

SponsorBlock utilise une base de données publique, pour laquelle chaque utilisateurice peut contribuer, qui permet de catégoriser différentes sections de vidéos sur YouTube, afin de sauter les catégories gênantes telles que les sponsors.
