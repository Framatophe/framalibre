---
nom: "Turtl"
date_creation: "Samedi, 31 décembre, 2016 - 14:54"
date_modification: "Mercredi, 12 mai, 2021 - 15:51"
logo:
    src: "images/logo/Turtl.png"
site_web: "https://turtlapp.com/"
plateformes:
    - "Android"
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
    - "le web"
langues:
    - "Français"
    - "English"
description_courte: "Une web-app auto-hébergée permettant de prendre des notes et offrant une alternative à Evernote."
createurices: ""
alternative_a: "Evernote, Google Keep"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "productivité"
    - "auto-hébergement"
    - "notes"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/turtl"
---

Turtl est une application de gestion de notes en ligne, dans la lignée d'Evernote. Elle vous permet d'éditer vos notes sur tous vos appareils, tout en profitant d'une synchronisation des modifications sur chacun d'entre eux. Turtl est basé sur un modèle client-serveur, c'est-à-dire que pour que vos appareils se connectent entre eux, il faut d'abord mettre en place un serveur, puis y connecter vos appareils. Vous pouvez ensuite éditer, créer et même collaborer sur des notes, images, marque-pages ou même mots de passe.
Elle propose une alternative simple et sécurisée à Evernote : une fois que vous avez créé votre compte, vos notes sont chiffrées avec votre mot de passe, ce qui signifie que vous êtes seul maître de vos données. De plus, avoir le pouvoir de l'héberger soi-même vous assure un contrôle total de vos données : elles sont chez vous (sauf si vous utilisez un serveur déjà prêt). Turtl offre les mêmes fonctionnalités qu'Evernote, l'espionnage en moins et le contrôle en plus.

