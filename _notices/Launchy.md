---
nom: "Launchy"
date_creation: "Mardi, 27 décembre, 2016 - 08:34"
date_modification: "Mercredi, 12 mai, 2021 - 16:35"
logo:
    src: "images/logo/Launchy.png"
site_web: "http://www.launchy.net/"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Autres langues"
description_courte: "Un lanceur d'applications et de fichiers customisable remplaçant le menu démarrer et les icônes sur le bureau."
createurices: "Josh Karlin"
alternative_a: "LaunchBar, Alfred"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
    - "lanceur d'applications"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Launchy"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/launchy"
---

Launchy est un lanceur d'applications qui se matérialise après avoir entré un raccourci clavier (par défaut Alt+Espace). Sa fonction première est de venir remplacer le menu démarrer : au lieu de naviguer dans les menus à la recherche de l'application que l'on souhaite lancer, Launchy propose de le faire en tapant quelques lettres dans sa zone de recherche. Il permet aussi d'ouvrir des dossiers, des fichiers, des raccourcis divers (vers des pages web, vers les marques-pages de votre navigateur internet, vers des applications avec des paramètres particuliers, etc.). Par défaut Launchy répertorie les dossiers du menu démarrer mais il est possible d'y ajouter ses propres répertoires avec une possibilité de récursivité ainsi que du choix des types de fichiers à prendre . Il dispose aussi d'un système de plugins supportant l'ajout de nouvelles fonctions comme éteindre ou redémarrer l'ordinateur, enrichir la calculatrice ou encore tuer des applications. Son apparence peut être customisée.

