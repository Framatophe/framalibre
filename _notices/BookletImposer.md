---
nom: "BookletImposer"
date_creation: "mercredi, 13 mars, 2024 - 14:00"
date_modification: "mercredi, 13 mars, 2024 - 14:00"
logo:
    src: "images/logo/BookletImposer.svg"
site_web: "https://kjo.herbesfolles.org/bookletimposer/"
plateformes:
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Créer des livrets pour imprimer des PDF en multipages (4 pages sur une seule feuille par exemple)"
createurices: "Kjö"
alternative_a: "Adobe Acrobat"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "manipulation de pdf"
    - "pdf"
    - "livres"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

Excellente application pour produire une nouvelle version de PDF adaptée à l'impression en livret (par exemple 4 pages sur une seule feuille en recto-verso). Cela règle automatiquement la disposition des pages du PDF d'origine pour enchaîner les pages au sein d'un document livret de plusieurs feuilles. Il y a vraiment beaucoup de réglage possible (4 pages par feuille, 8 pages…)
