---
nom: "Skreen"
date_creation: "Mercredi, 26 août, 2020 - 01:48"
date_modification: "Mercredi, 26 août, 2020 - 01:48"
logo:
    src: "images/logo/Skreen.png"
site_web: "https://github.com/DegrangeM/Skreen"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
description_courte: "Skreen est une application qui permet à un enseignant de voir les écrans de ses élèves."
createurices: "Mathieu Degrange"
alternative_a: ""
licences:
    - "Licence MIT/X11"
tags:
    - "éducation"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/skreen"
---

Skreen est une application qui permet à un enseignant de voir les écrans de ses élèves.
Skreen est une application web qui ne nécessite donc aucune installation que ce soit sur l'ordinateur de l'enseignant ou des élèves.
L'application a cependant besoin d'être hébergé sur internet afin que l'enseignant et les élèves puissent y accéder via leur navigateur internet.
L'enseignant ne peut accéder aux écrans de ses élèves que lorsque ces derniers ont la page web de Skreen ouverte et peuvent donc cesser à tout moment la diffusion.
L'enseignant ne peut donc pas accéder aux écrans de ses élèves à leur insu.

