---
nom: "Gmusicbrowser"
date_creation: "Samedi, 31 décembre, 2016 - 13:32"
date_modification: "Vendredi, 6 janvier, 2017 - 06:14"
logo:
    src: "images/logo/Gmusicbrowser.png"
site_web: "http://gmusicbrowser.org/"
plateformes:
    - "GNU/Linux"
langues:
    - "Autres langues"
description_courte: "Lecteur de musique conçu pour écouter et pour gérer de larges collections musicales."
createurices: "Quentin Sculo"
alternative_a: "iTunes"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "lecteur musique"
    - "musique"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/gmusicbrowser"
---

Gmusicbrowser permet d'écouter et de gérer au mieux sa bibliothèque musicale numérique. Il a été conçu pour les larges bibliothèques, par exemple de plus de 10 000 chansons. Il permet l'utilisation de listes de lecture dynamiques sous forme de filtres très configurables. La lecture peut suivre plusieurs critères que l'on peut définir. Notamment la lecture aléatoire qui peut être pondérée en fonction, par exemple, de la note du genre,…
Très pratique pour une soirée musicale, il permet de remplir une file d'attente qui peut aussi être auto-remplie. Sa fonction de recherche puissante et rapide permettra de mettre la main sur le morceau que vous souhaitez écouter en quelques cliques.  Il permet aussi de restreindre temporairement le filtre de lecture à un artiste ou un album très facilement.

