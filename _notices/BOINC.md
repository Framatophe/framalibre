---
nom: "BOINC"
date_creation: "Lundi, 27 mars, 2017 - 04:45"
date_modification: "Vendredi, 14 avril, 2017 - 03:11"
logo:
    src: "images/logo/BOINC.png"
site_web: "https://boinc.berkeley.edu/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "BOINC permet de soutenir divers projets scientifiques grâce à sa plate-forme de calcul partagé."
createurices: "David P. Anderson"
alternative_a: ""
licences:
    - "Licence publique générale limitée GNU (LGPL)"
tags:
    - "science"
    - "astronomie"
    - "biologie"
    - "astrophysique"
    - "calcul"
    - "ordinateur"
    - "médecine"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Berkeley_Open_Infrastructure_for_Network_Computing"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/boinc"
---

BOINC est une plate-forme de calcul distribué (ou calcul volontaire) sur Internet. Elle permet aux utilisateurs s'y connectant de fournir la puissance de calcul de leur propre machine afin de soutenir divers projets scientifiques.
Créée à l'origine pour "SETI @ Home", projet crée par le célèbre programme du même nom : Search for Extra-Terrestrial Intelligence (Recherche d'une intelligence extra-terrestre), la plate-forme BOINC s'est élargie au fil des années à de nombreux autres projets et de domaines scientifiques tels que l'astronomie, la biologie, la médecine, la physique...
Elle rassemble aujourd'hui une grande communauté d'utilisateurs désirant apporter leur aide au monde scientifique.

