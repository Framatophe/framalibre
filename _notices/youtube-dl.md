---
nom: "youtube-dl"
date_creation: "Mardi, 6 avril, 2021 - 18:58"
date_modification: "jeudi, 28 décembre, 2023 - 17:42"
logo:
    src: "images/logo/youtube-dl.png"
site_web: "https://ytdl-org.github.io/youtube-dl/"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "English"
description_courte: "Télécharger les vidéos de Youtube et de nombreux autres sites"
createurices: ""
alternative_a: ""
licences:
    - "Unlicense"
tags:
    - "internet"
    - "téléchargement de youtube"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Youtube-dl"
lien_exodus: ""
identifiant_wikidata: "Q28401317"
mis_en_avant: "oui"
redirect_from: "/content/youtube-dl"
---

Youtube-dl est un logiciel en ligne de commande qui permet de télécharger les vidéos de Youtube et de nombreux autres sites. Il est capable de télécharger les sous-titres ou des playlistes et possède de nombreuses options.
Pour l'installer, l'utiliser et le mettre à jour (ce qui est nécessaire de faire régulièrement, pour suivre les changements internes des plateformes), il faut, par défaut, savoir taper des commandes dans un terminal.
Mais de nombreuses interfaces graphiques (GUI) plus ou moins officielles et plus ou moins complètes existent. Nous citerons media-downloader (propose des paquets .deb), you2ber, une extension pour Gnome Shell, Tartube (propose des paquets .deb) ou encore youtube-dl-gui (Electron et AppImage).


