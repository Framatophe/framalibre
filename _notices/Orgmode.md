---
nom: "Orgmode"
date_creation: "Vendredi, 30 décembre, 2016 - 00:40"
date_modification: "Mercredi, 12 mai, 2021 - 15:51"
logo:
    src: "images/logo/Orgmode.png"
site_web: "http://orgmode.org/"
plateformes:
    - "Android"
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "English"
description_courte: "Votre vie entière dans un fichier en texte brut !
Rédaction, planning de vie, publication."
createurices: "Carsten Dominik"
alternative_a: "Wunderlist, Remenberthemilk, Microsoft Word"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
    - "notes"
    - "organisation"
    - "agenda"
    - "tâche"
    - "écriture"
    - "projet"
    - "rédaction"
    - "publication"
    - "gtd"
    - "productivité"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Org-mode"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/orgmode"
---

Orgmode permet de saisir de longs textes de façon structurée.
Orgmode est utile à toute personne qui écrit du texte. Pour un blog, un journal, une présentation, un article, une thèse, un livre, de la documentation... Concentrez vous sur le texte et orgmode l'exportera dans le format qui vous convient.
Orgmode est aussi redoutable pour quiconque souhaite organiser sa vie selon des méthodes du type Getting Things Done. Tâches, agenda, rendez-vous, contextes, priorités, feuille de temps, tableaux, archivage, capture... Vous pouvez également le synchroniser avec un agenda ou une application mobile.
Orgmode n'est pas tout à fait un logiciel indépendant puisque c'est un mode d'Emacs. Il est pourtant si autonome et si puissant qu'il mérite une fiche à lui tout seul. Comme Emacs, il y a une courbe d'apprentissage et vous passerez beaucoup de temps à "fignoler" votre fichier de configuration mais ensuite vous découvrirez la puissance d'un texte brut !

