---
nom: "MPC-HC"
date_creation: "Dimanche, 26 mars, 2017 - 15:26"
date_modification: "Jeudi, 28 octobre, 2021 - 17:50"
logo:
    src: "images/logo/MPC-HC.png"
site_web: "https://mpc-hc.org/"
plateformes:
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Un lecteur multimédia pour Windows"
createurices: "MPC-HC Team"
alternative_a: "Windows Media Player"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "lecteur vidéo"
    - "vidéo"
    - "lecteur multimédia"
    - "lecteur audio"
    - "musique"
    - "audio"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Media_Player_Classic"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/mpc-hc"
---

MPC-HC Media Player Classic Home Cinema est un lecteur multimédia pour les systèmes Windows, qui prend en charge tous les formats audio, vidéo. Il lit aussi bien les DVD que les Blu-Ray, dès lors qu'ils auront été "déprotégés" préalablement.

