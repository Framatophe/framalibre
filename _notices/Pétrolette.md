---
nom: "Pétrolette"
date_creation: "Mardi, 2 mars, 2021 - 13:27"
date_modification: "Mardi, 8 juin, 2021 - 02:18"
logo:
    src: "images/logo/Pétrolette.png"
site_web: "https://petrolette.space/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "FirefoxOS"
    - "Windows Mobile"
    - "Apple iOS"
    - "Autre"
    - "le web"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "La page d'actu qui ignore tout de toi."
createurices: "yPhil"
alternative_a: "Netvibes"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "lecteur de flux rss"
    - "actualités"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/p%C3%A9trolette"
---

Pétrolette est une page d'accueil de lecture d'actualités, libre. Elle est immédiatement utilisable sans inscription avec la même URL dans le navigateur du bureau ou celui d'un appareil mobile.
Les flux d'actualité sont organisés en onglets, qui peuvent contenir un nombre infini de colonnes ; tout est ré-organisable, et sauvé directement dans le cache du navigateur.
Pour retrouver les même flux sur son téléphone, soit exporter / importer le fichier petrolette.conf, soit (recommandé) utiliser la fonctionnalité de synchronisation au nuage personnel de l'utilisateur.

