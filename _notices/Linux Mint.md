---
nom: "Linux Mint"
date_creation: "Samedi, 25 avril, 2020 - 16:33"
date_modification: "Jeudi, 26 août, 2021 - 11:13"
logo:
    src: "images/logo/Linux Mint.png"
site_web: "https://linuxmint.com"
plateformes:
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Système d'exploitation GNU/Linux pensé pour l’ergonomie et la liberté"
createurices: "Clément Lefèbvre, Jamie Boo Birse, Kendall Weaver"
alternative_a: "Microsoft Windows, MacOS"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
    - "distribution gnu/linux"
    - "système d'exploitation (os)"
lien_wikipedia: "https://en.wikipedia.org/wiki/Linux_Mint"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/linux-mint"
---

Linux Mint est un système d'exploitation ayant plusieurs déclinaisons. Il est basé soit sur Ubuntu, soit sur Debian et il permet aussi de choisir son environnement de bureau (Cinnamon, Gnome). C'est un système d'exploitation très orienté pour ordinateur de bureau et qui tente d'être le plus ergonomique possible. La devise du projet est "From freedom came elegance" (traduction en français : « Le liberté apporte l'élégance »).

