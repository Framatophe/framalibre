---
layout: notice

nom: "AccessDV Linux"
date_creation: "Mardi, 17 septembre, 2019 - 08:46"
date_modification: "Jeudi, 20 mai, 2021 - 17:06"
logo: 
    src: "https://old.framalibre.org/sites/default/files/leslogos/logo-ADVL-original.png"
site_web: "https://accessdvlinux.fr"
plateformes: "GNU/Linux"
langues: "Français"
description_courte: "Un système simple, gratuit, mettant l'informatique à la portée de tous."
createurices: "Christophe Lévêque"
licences: "Licence Publique Générale GNU (GNU GPL)"
mots_clefs: "déficience visuelle et personnes âgées"
lien_wikipedia: ""
lien_exodus: ""
---

AccessDV Linux est une distribution adaptée aux débutants de tout âge, intégrant des fonctions facilitatrices, une base complète de logiciels pour quasiment tous les domaines. Elle est surtout et principalement une distribution destinée aux personnes en situation de handicap, notamment visuel, et propose pour cela un grand nombre d’outils et de développements pour la compensation du handicap.
Sa simplicité d’utilisation et l’importance des logiciels proposés, en fait une distribution de choix pour tous les utilisateurs, y compris les utilisateurs chevronnés.
Base logicielle : Les principaux :
- navigateur Firefox-ESR avec mode lecture
- messagerieThunderbird.
- lecteur d’écran ORCA,
- suite libre-Office
- éditeur de texte Mouspad
- machine à lire
- lecteur de musique ou de vidéo, lecteur Daisy,
- The Gimp, Audacity,
L'association ACIAH propose en outre des ateliers d'initiation, un accompagnement personnalisé (hot-line), et des formations en informatique adaptée

