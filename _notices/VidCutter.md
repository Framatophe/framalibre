---
nom: "VidCutter"
date_creation: "Vendredi, 5 juillet, 2019 - 21:19"
date_modification: "Vendredi, 5 juillet, 2019 - 21:24"
logo:
    src: "images/logo/VidCutter.png"
site_web: "https://github.com/ozmartian/vidcutter"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
description_courte: "Un outil très rapide de découpe d'extraits d'une vidéo"
createurices: "Pete Alexandrou"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "éditeur vidéo"
    - "vidéo"
    - "utilitaire"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/vidcutter"
---

Un outil très rapide de découpe vidéo, pour extraire d'une vidéo un ou plusieurs passages en autant de fichiers vidéo distincts ou bien en les associant dans un même fichier.
VidCutter ne ré-encode pas les extraits sélectionnés. Ceci a deux avantages majeurs :
- l'extraction est très rapide : quelques secondes là où un ré-encodage aurait pris plusieurs dizaine de minutes,
- la qualité de la vidéo reste exactement la même.
VidCutter arrive à ce résultat en effectuant les découpes sur les image de référence du flux vidéo. Selon l'écart de ces images dans la vidéo, la découpe pourra donc manquer de précision, et les extraits commencer un peu avant ou terminer un peu après la sélection définie. Si dans bien des cas ceci n'est pas gênant, VidCutter propose néanmoins un mode de découpe intelligent, précis à l'image près, qui ré-encode uniquement le début et la fin des extraits.

