---
nom: "RSS-Bridge"
date_creation: "mardi, 26 mars, 2024 - 19:42"
date_modification: "mardi, 26 mars, 2024 - 19:42"
logo:
    src: "images/logo/RSS-Bridge.png"
site_web: "https://github.com/rss-bridge/rss-bridge"
plateformes:
    - "le web"
langues:
    - "English"
description_courte: "RSS Bridge est un générateur de flux RSS pour les site n'en offrant pas"
createurices: "rss-bridge"
alternative_a: ""
licences:
    - "Domaine public"
tags:
    - "RSS"
    - "générateur de flux RSS"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

RSS Bridge est un générateur de flux RSS pour les site n'en offrant pas ou pour les site qui demandent une authentification pour accéder à leur flux.
