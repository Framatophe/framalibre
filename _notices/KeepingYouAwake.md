---
nom: "KeepingYouAwake"
date_creation: "Dimanche, 13 novembre, 2022 - 11:54"
date_modification: "Vendredi, 13 janvier, 2023 - 14:22"
logo:
    src: "images/logo/KeepingYouAwake.png"
site_web: "https://keepingyouawake.app"
plateformes:
    - "Mac OS X"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Empêche votre Mac de se mettre en veille"
createurices: "Marcel Dierkes"
alternative_a: "Caffeine"
licences:
    - "Licence MIT/X11"
tags:
    - "système"
    - "veille"
    - "écran"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/keepingyouawake"
---


empêche votre Mac de se mettre en veille en un clic
durées d'activation prédéfinies
peut se désactiver lorsque le niveau de la batterie est faible
prend en charge les écrans Retina et le mode sombre
prend en charge macOS Big Sur et toutes les versions depuis macOS Sierra
traduit en plusieurs langues par la communauté


