---
nom: "Goupile"
date_creation: "Vendredi, 16 décembre, 2022 - 11:21"
date_modification: "Vendredi, 16 décembre, 2022 - 11:55"
logo:
    src: "images/logo/Goupile.png"
site_web: "https://goupile.fr"
plateformes:
    - "GNU/Linux"
    - "Windows"
    - "FirefoxOS"
    - "Windows Mobile"
langues:
    - "Français"
description_courte: "Pour des communs en santé !"
createurices: "Niels MARTIGNENE"
alternative_a: "Google Forms"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "science"
    - "recherche"
    - "formulaire"
    - "santé"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/goupile"
---

Goupile est parfait pour les études scientifiques notamment en santé. Il permet de réaliser des Cahiers d’observation électronique (ou eCRF).
Goupile est un outil de conception d'eCRF libre qui s'efforce de rendre la création de formulaires et la saisie de données à la fois puissantes et faciles.

