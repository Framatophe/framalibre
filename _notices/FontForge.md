---
nom: "FontForge"
date_creation: "Jeudi, 20 juillet, 2017 - 21:05"
date_modification: "Jeudi, 20 juillet, 2017 - 21:24"
logo:
    src: "images/logo/FontForge.png"
site_web: "https://fontforge.github.io"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "La référence des logiciels de création de polices de caractères."
createurices: ""
alternative_a: ""
licences:
    - "Berkeley Software Distribution License (BSD)"
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "création"
    - "police de caractères"
lien_wikipedia: "https://fr.wikipedia.org/wiki/FontForge"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/fontforge"
---

FontForge est l'outil libre de référence dans le domaine de la création des polices de caractères (fonts). Un livre est  en rédaction sur le FLOSS (voir liens) et un autre sur l'utilisation de FontForge pour la création de polices libres ce qui permet, en plus des nombreux tutoriels que l'on peut trouver de ci de là, de prendre rapidement en main ce logiciel.

