---
nom: "PDF4Teachers"
date_creation: "mardi, 12 mars, 2024 - 09:23"
date_modification: "mercredi, 13 mars, 2024 - 11:26"
logo:
    src: "images/logo/PDF4Teachers.png"
site_web: "https://pdf4teachers.org/"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "PDF4Teachers est une application d'édition de documents PDF libre et open source, permettant la correction de copies PDF de manière optimisée."
createurices: "Clément Grennerat"
alternative_a: "Adobe Acrobat"
licences:
    - "Licence Apache (Apache)"
tags:
    - "manipulation de pdf"
    - "édition"
    - "latex"
    - "enseignement"
    - "évaluation"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

L'application est basée sur un système de mémorisation des annotations les plus utilisées, permettant une correction efficace : chaque annotation saisie est ajoutée à une liste dans laquelle on peut récupérer des annotations précédentes. 

Les annotations peuvent être composées en LaTeX ou LibreOffice Math.

L'application intègre aussi des fonctions de barème, qui permettent la notation chiffrée dans le document (définition de barème, saisie des notes aisée et somme automatique avec export dans un tableur).

La notation par compétence est aussi supportée avec une intégration du logiciel de suivi d'acquisition de compétences SACoche (création d'un devoir, attribution de plusieurs compétences, puis évaluation sur chaque copie avec un tableau récapitulatif généré automatiquement).

Des fonctions de dessin à main levée, d'ajout d'images et d'icônes vectoriel sont aussi disponibles.

PDF4Teachers offre aussi beaucoup de fonctionnalités complémentaires très utiles, comme la conversions de lots d'images en fichiers PDF, ou encore le déplacement, la rotation et la suppression des pages d'un document PDF.
