---
nom: "LemonLDAP::NG"
date_creation: "Mercredi, 22 mars, 2017 - 10:35"
date_modification: "Mercredi, 22 mars, 2017 - 10:50"
logo:
    src: "images/logo/LemonLDAP::NG.png"
site_web: "https://lemonldap-ng.org"
plateformes:
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "LemonLDAP::NG est un logiciel de SSO (authentification un"
createurices: "Xavier Guimard, Clément Oudot"
alternative_a: "Siteminder, Web Access Manager, OpenAM, Sign&Go"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "sécurité"
    - "authentification"
lien_wikipedia: "https://fr.wikipedia.org/wiki/LemonLDAP::NG"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/lemonldapng"
---

LemonLDAP::NG est un logiciel de SSO (authentification unique), contrôle d'accès et fédération des identités. Il permet de gérer la délégation d'authentification des applications Web via des en-têtes HTTP ou les protocoles CAS, SAML 2.0 ou OpenID Connect.

