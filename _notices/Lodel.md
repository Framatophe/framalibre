---
nom: "Lodel"
date_creation: "Dimanche, 10 mai, 2020 - 19:43"
date_modification: "Mercredi, 12 mai, 2021 - 15:12"
logo:
    src: "images/logo/Lodel.png"
site_web: "http://lodel.org/"
plateformes:
    - "GNU/Linux"
langues:
    - "Français"
description_courte: "CMS spécialisé dans l'édition de textes scientifiques longs et structurés"
createurices: "Marin Dacos, Ghislain Picard"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "cms"
    - "édition"
    - "science"
    - "sciences humaines et sociales"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Lodel"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/lodel"
---

Lodel est un CMS développé dans l'objectif d'offrir à l'écosystème des Sciences humaines et sociales un logiciel d'édition électronique prenant en compte les conventions de l'édition scientifique.
Il est particulièrement adapté à l'édition de textes longs dans l'objectif d'une publication sous forme de revue scientifique. Il permet à une personne non informaticienne comme un·e chargé·e d'édition ou secrétaire de rédaction de charger depuis un logiciel de traitement de texte puis de mettre en page des textes scientifiques (notes de bas de pages, mise en page de texte normé et structuré) afin de publier des numéros de revues.
Parmi les sites propulsés par Lodel, vous pouvez par exemple trouver VertigO, une revue scientifique dédiée aux problèmes environnementaux (accessible via ce lien https://journals.openedition.org/vertigo/ ).

