---
nom: "Simulangue"
date_creation: "Mardi, 1 juin, 2021 - 16:59"
date_modification: "Mardi, 1 juin, 2021 - 16:59"
logo:
    src: "images/logo/Simulangue.png"
site_web: "https://app.box.com/s/0hh3kz30ntf5u0qijuda9d6dcfy58f1r"
plateformes:
    - "Windows"
langues:
    - "Français"
    - "English"
description_courte: "Logiciel pour réviser ses connaissances."
createurices: "Thierry Chauve"
alternative_a: ""
licences:
    - "Domaine public"
tags:
    - "éducation"
    - "qcm"
    - "enseignement"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/simulangue"
---

Ce logiciel en Java permet de faire des fiches de révision pour tout type de connaissance, avec des fonctionnalités comme l'interrogation aléatoire, la création de matières, l'insertion d'images et de fichiers audio.

