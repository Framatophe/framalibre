---
nom: "Scratch"
date_creation: "Samedi, 7 janvier, 2017 - 19:53"
date_modification: "Lundi, 10 mai, 2021 - 14:22"
logo:
    src: "images/logo/Scratch.png"
site_web: "https://scratch.mit.edu/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "FirefoxOS"
langues:
    - "Autres langues"
description_courte: "imagine, programme, partage !"
createurices: "Mitchel Resnick"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "éducation"
    - "programmation"
    - "apprentissage"
    - "enfant"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Scratch_(langage)"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/scratch"
---

Scratch est un langage de programmation visuelle à base de blocs. Il facilite la découverte de la programmation et la création de dispositifs interactifs et multimédias, d'animations, de jeux, de musiques, de simulations, etc.
Scratch a été conçu pour sensibiliser les enfants, à partir de 8 ans (pour les 5-7 ans il y a Scratch Junior), aux concepts importants des mathématiques et de l’informatique et les aider à créer, à raisonner et à coopérer. Assez naturellement, il est aussi utilisé dans bien d’autres domaines, notamment comme outil d’expression et d’expérimentation. Des bidouilleurs l’interfacent avec Arduino ou MakeyMakey.
Ses initiateurs du Lifelong Kindergarten research au MIT Media Lab et de nombreux enseignants encouragent une pédagogie active, le partage et la réutilisation des projets Scratch sur le web.
La version initiale a été développée en Smalltalk, la deuxième en ActionScript (requière donc Adobe Air non libre) et la troisième sera en HTML5.

