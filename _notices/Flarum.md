---
nom: "Flarum"
date_creation: "Mardi, 1 octobre, 2019 - 00:36"
date_modification: "Lundi, 10 mai, 2021 - 14:40"
logo:
    src: "images/logo/Flarum.png"
site_web: "https://flarum.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Flarum ré-invente le concept de forum avec une interface moderne et élégante."
createurices: "Toby Zerner, Franz Liedke"
alternative_a: "Invision Community, XenForo, vBulletin"
licences:
    - "Licence MIT/X11"
tags:
    - "internet"
    - "forum de discussion"
    - "discussion"
    - "communauté"
    - "communication"
    - "php"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/flarum"
---

Co-créé par Franz Liedke le fondateur de fluxBB et Toby Zerner le fondateur de esoTalk. Flarum ré-invente le concept de forum avec une interface moderne et élégante.
De nombreux modules sont proposés par la communauté :
https://flagrow.io/
https://friendsofflarum.org
https://github.com/realodix/awesome-flarum
Il est entre autre utilisé par des projets tels que :
Solus (6e rang des distributions Linux les plus consultées en 2018)
Webdeveloper.com (un forum reconnu pour le développement web)

