---
nom: "MJ PDF"
date_creation: "mardi, 13 février, 2024 - 22:27"
date_modification: "mardi, 13 février, 2024 - 22:27"
logo:
    src: "images/logo/MJ PDF.png"
site_web: "https://gitlab.com/mudlej_android/mj_pdf_reader"
plateformes:
    - "Android"
langues:
    - "English"
description_courte: "Consultez vos documents PDF sur Android."
createurices: "Mudlej"
alternative_a: "Adobe Acrobat Reader, Foxit Reader"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "lecteur pdf"
    - "pdf"
lien_wikipedia: ""
lien_exodus: "https://reports.exodus-privacy.eu.org/fr/reports/com.gitlab.mudlej.MjPdfReader/latest/"
identifiant_wikidata: ""
mis_en_avant: "non"

---

MJ PDF est une application de lecture de PDF pour Android.

Rapide, sobre et élégante, une application efficace qui ne fait que ce qu'on lui demande et qui le fait bien !

Fonctionnalités supplémentaires :

- Thème sombre
- Faire défiler le document automatiquement
- Sélection du texte contenu dans le PDF par appui long
