---
nom: "Pytition"
date_creation: "Mercredi, 10 juin, 2020 - 22:00"
date_modification: "dimanche, 4 février, 2024 - 23:36"
logo:
    src: "images/logo/Pytition.png"
site_web: "https://pytition.org"
plateformes:
    - "le web"
    - "Autre"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Un outil libre pour que vos pétitions ne compromettent pas la vie privée des signataires !"
createurices: "Yann Sionneau"
alternative_a: "Change, Avaaz"
licences:
    - "Berkeley Software Distribution License (BSD)"
tags:

lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/pytition"
---

Pytition est un logiciel libre permettant d'héberger des pétitions sur le web.
L'utilisation des données personnelles liées aux pétitions est un enjeu fort, car elles permettent de constituer d'importantes bases de données, et qu'elles traitent souvent de sujets sensibles, notamment politiques.
Parmi les fonctionnalités proposées par Pytition, on trouve :
- L'absence de toute technologie de traçage,
- L'export des signatures au format CSV,
- Une interface multilingue,
- La possibilité de prévisualiser la page de la pétition avant de l'ouvrir au public,
- La possibilité que plusieurs organisations proposent des pétitions sur la même instance
- ...
