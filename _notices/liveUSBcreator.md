---
nom: "liveUSBcreator"
date_creation: "Dimanche, 21 mai, 2017 - 16:32"
date_modification: "Mardi, 9 juillet, 2019 - 11:45"
logo:
    src: "images/logo/liveUSBcreator.png"
site_web: "https://lescahiersdudebutant.fr/tools/"
plateformes:
    - "GNU/Linux"
langues:
    - "Français"
description_courte: "Un petit outil pour transférer facilement une image de type ISO sur une clé USB, créant ainsi un liveUSB."
createurices: "Thuban et Coyotus"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
    - "gravure"
    - "iso"
    - "utilitaire"
    - "installation"
    - "debian"
    - "live usb"
    - "usb"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/liveusbcreator"
---

liveUSBcreator est une alternative graphique à la commande "dd". (différent de LinuxLive USB Creator, pour Windows). C'est un paquet .deb de 18.5 ko, que vous trouverez dans les outils des  cahiers du débutant
Cet utilitaire permet de transférer facilement une image de type ISO sur une clé USB, créant ainsi un liveUSB,
sans avoir besoin d'entrer des commandes dans le terminal.
La clé USB est un moyen pratique pour installer ou tester une distribution GNU/Linux car vous pouvez en changer souvent, et en tester plusieurs.
Elle permet aussi de sécuriser vos tests car une fois la session terminée, aucune trace ni sur votre clé, ni sur l'ordinateur ayant démarré sur la clé.
https://debian-facile.org/doc:install:usb-boot

