---
nom: "DoliSCAN"
date_creation: "Vendredi, 31 juillet, 2020 - 15:11"
date_modification: "Vendredi, 31 juillet, 2020 - 15:11"
logo:
    src: "images/logo/DoliSCAN.png"
site_web: "https://doliscan.org/fr"
plateformes:
    - "GNU/Linux"
    - "Android"
    - "Apple iOS"
langues:
    - "Français"
description_courte: "Assistant numérique pour vos notes de frais."
createurices: "Eric Seigne"
alternative_a: "Spendesk, Onexpense, iZyFrais"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "métiers"
lien_wikipedia: ""
lien_exodus: "https://reports.exodus-privacy.eu.org/fr/reports/fr.caprel.doliscan/latest/"
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/doliscan"
---

DoliSCAN permet de simplifier la gestion des notes de frais pour le ou la collaboratrice et son entreprise.
Il est composé d'une partie client (une application à installer sur smartphone Android ou Apple) et d'une partie serveur (dans l'infrastructure de l'entreprise ou hébergé par DoliSCAN).
Le ou la collaboratrice a juste a prendre en photo ses justificatifs, ajouter quelques informations et le tout est automatiquement transmis au serveur.
Une méthode est incluses pour faciliter le calcul des indemnités kilométriques.
Le service comptabilité ou un expert-comptable peut recevoir automatiquement une note de frais récapitulative chaque mois, avec les justificatifs et un Fichier des Écritures Comptables, importable dans les logiciels métier.
Un service commercial est proposé en plus de la version communautaire : https://doliscan.fr

