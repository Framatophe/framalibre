---
nom: "RenderChan"
date_creation: "Samedi, 30 décembre, 2017 - 23:14"
date_modification: "Lundi, 10 mai, 2021 - 13:42"
logo:
    src: "images/logo/RenderChan.png"
site_web: "https://morevnaproject.org/renderchan/"
plateformes:
    - "GNU/Linux"
langues:
    - "English"
description_courte: "Marchander est un script qui automatise le rendu de film d’animation"
createurices: ""
alternative_a: ""
licences:
    - "Berkeley Software Distribution License (BSD)"
tags:
    - "création"
    - "script"
    - "animation"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/renderchan"
---

RenderChan est un script qui détecte les dépendances modifiées d’un projet d’animation et contrôle les opération de rendus. Ainsi si une modification est faite, par exemple un changement de texture, seul les fichiers impactés seront générés à nouveau, ainsi réduire le temps nécessaire pour rendre un projet. Véritable plateforme, ce script permet une ligne de code de rendre un film d’animation utilisant de multiple moteur de rendu. Ce script peut appeler les moteurs de rendu de ces différents logiciels libres:
- Blender
- Synfig
- Pencil2D
- CGRU/Afanasy
- Krita
- Inkscape
- Gimp
En automatisant le rendu, ce script permet de réduire la taille des sources de projet et facilite donc la collaboration. RenderChan est écrit en python, mais est basé sur « Remake » écrit en Bash. La liste de tache générée peut-être soit envoyer vers une « ferme de rendu », soit gérée en local.
RenderChan un sous projet de « Morevna Project » .

