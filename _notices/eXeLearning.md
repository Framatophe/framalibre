---
nom: "eXeLearning"
date_creation: "Mercredi, 19 avril, 2017 - 11:04"
date_modification: "Jeudi, 11 août, 2022 - 21:41"
logo:
    src: "images/logo/eXeLearning.png"
site_web: "http://exelearning.net/"
plateformes:
    - "GNU/Linux"
    - "Windows"
    - "Apple iOS"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "eXeLearning assiste les enseignants dans la création et la publication d’exercices et de contenus web."
createurices: "Antonio Monje Fernández, Ismail Alí Gago, Jesús Miguel Domínguez"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "éducation"
    - "exerciseur"
    - "scorm"
    - "e-learning"
lien_wikipedia: "https://en.wikipedia.org/wiki/EXeLearning"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/exelearning"
---

eXeLearning est un exerciseur pour l'elearning. eXeLearning permet de créer simplement des séquences d’apprentissage, autonomes ou pour toute plate-forme éducative (LMS ou LCMS).
Les ressources produites avec eXeLearning peuvent être exportées en IMS Content Package, SCORM 1.2, SCORM 2004, IMS Common Cartridge, ePub3 ou simplement en pages web HTML5 (assemblés en un seul paquet).
Alternatives : Opale (Scenari) est un autre logiciel libre qui permet également de produire des exercices en plus de tous les avantages que peuvent présenter les chaînes éditoriales. Moodle est un LCMS permettant de créer directement des exercices (il n'y a pas d'obligation d'utiliser Scenari ou eXeLearning pour approvisionner en exercices Moodle). Logiquiz ou Lumi sont de bonnes alternatives.

