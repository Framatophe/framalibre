---
nom: "Plausible Analycics"
date_creation: "Dimanche, 23 janvier, 2022 - 17:47"
date_modification: "Lundi, 24 janvier, 2022 - 23:06"
logo:
    src: "images/logo/Plausible Analycics.png"
site_web: "https://plausible.io/"
plateformes:
    - "Autre"
langues:
    - "English"
description_courte: "alternative à Google Analytics simple et respectueuse de la vie privée"
createurices: "Uku Taht, Marko Saric"
alternative_a: "Google Analytics, Xiti"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "internet"
    - "analyse"
    - "web analytics"
    - "mesure de statistiques"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/plausible-analycics"
---

Plausible est une solution web analytics légère et open source. Sans cookies, complètement respectueuse du RGPD et d'autres normes internationales autour de la protection de la vie privée. Conçu et hébergé dans l'Union Européenne, sur une infrastructure cloud européenne.

