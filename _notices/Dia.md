---
nom: "Dia"
date_creation: "Vendredi, 4 décembre, 2015 - 12:04"
date_modification: "Dimanche, 3 mars, 2019 - 23:15"
logo:
    src: "images/logo/Dia.gif"
site_web: "https://wiki.gnome.org/Apps/Dia"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Dia s'est inspiré du programme commercial \"Visi"
createurices: ""
alternative_a: "Visio"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "création"
    - "schéma"
    - "diagramme"
    - "dessin vectoriel"
    - "svg"
    - "maquette"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Dia_(logiciel)"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/dia"
---

Dia s'est inspiré du programme commercial "Visio", bien qu'il soit plus orientée vers des schémas informels pour un usage occasionnel. Il peut être utilisé pour la réalisation de diagrammes de type différent. Il a actuellement des objets spéciaux pour aider à la création de diagrammes entité-relation, diagrammes UML, organigrammes, diagrammes de réseau, et de nombreux autres schémas. Il est également possible d'ajouter de nouvelles formes en écrivant de simples fichiers XML et en utilisant un sous-ensemble de SVG pour dessiner la forme. Il peut charger et enregistrer des schémas dans un format XML personnalisé (gzip par défaut). Ces schémas peuvent aussi être exportés dans un certain nombre de format, comme EPS, SVG, XFIG, WMF et PNG.

