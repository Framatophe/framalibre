---
nom: "Gqrx SDR"
date_creation: "Samedi, 17 février, 2018 - 10:26"
date_modification: "Jeudi, 29 mars, 2018 - 18:27"
logo:
    src: "images/logo/Gqrx SDR.jpg"
site_web: "http://gqrx.dk/"
plateformes:
    - "GNU/Linux"
langues:
    - "English"
description_courte: "Récepteur Radio en Logiciel Libre"
createurices: "CSETE"
alternative_a: ""
licences:
    - "Licence Apache (Apache)"
tags:
    - "multimédia"
    - "radio"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/gqrx-sdr"
---

Gqrx supporte la plupart des hardwares SDR disponibles, incluant Airspy, Funcube Dongles, rtl-sdr, HackRF et USRP. Voyez la liste du hardware reconnu pour plus d'informations.
Gqrx permet ceci:
Découvrir ce qui est connecté à l'ordinateur.
Données I/Q pour les équipements
Changer la fréquence, du gain avec corrections (frequence, balance I/Q).
AM, SSB, CW, FM-N and FM-W (mono et stereo) démodulateurs.
FM mode pour NOAA APT.
Filtres de bandes passantes.

