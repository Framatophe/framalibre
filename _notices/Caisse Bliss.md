---
nom: "Caisse Bliss"
date_creation: "Samedi, 11 mai, 2019 - 21:55"
date_modification: "Vendredi, 7 mai, 2021 - 17:06"
logo:
    src: "images/logo/Caisse Bliss.png"
site_web: "https://www.cipherbliss.com/"
plateformes:
    - "GNU/Linux"
    - "Autre"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Une caisse en ligne auto hebergeable permettant de gérer ses stocks, des produits, des catégories. Etc."
createurices: "Baptiste lemoine, tykayn"
alternative_a: ""
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "métiers"
    - "commerce"
    - "caisse enregistreuse"
    - "budget"
    - "achat"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/caisse-bliss"
---

Une caisse en ligne auto hebergeable en php/symfony/js/angular
Ce logiciel de gestion de caisse permet entre autre de gérer ses stocks et les ventes depuis un smartphone, de prévoir les dépenses, de visualiser des statistiques, faire l'historique des achats clients, etc.

