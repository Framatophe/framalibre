---
nom: "Pi-hole"
date_creation: "Dimanche, 20 mai, 2018 - 18:50"
date_modification: "Mardi, 22 mai, 2018 - 11:21"
logo:
    src: "images/logo/Pi-hole.png"
site_web: "https://pi-hole.net/"
plateformes:
    - "GNU/Linux"
    - "le web"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Pi-hole est un bloqueur de publicité."
createurices: ""
alternative_a: ""
licences:
    - "Licence Publique Union Européenne (EUPL)"
tags:
    - "publicité"
    - "bloqueur de publicité"
    - "vie privée"
lien_wikipedia: "https://en.wikipedia.org/wiki/Pi-hole"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/pi-hole"
---

Avec Pi-hole, vous pouvez dire adieu à vos publicité, il suffit d'une commande pour l'installer et 10 minutes (en prenant son temps) pour configurer votre box et hop elles disparaissent. Il vous suffit de configurer le DNS principal de votre box par celui de l'ordinateur équipé de Pi-hole.

