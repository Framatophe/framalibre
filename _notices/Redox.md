---
nom: "Redox"
date_creation: "Vendredi, 5 avril, 2019 - 22:08"
date_modification: "Jeudi, 17 août, 2023 - 21:07"
logo:
    src: "images/logo/Redox.png"
site_web: "https://www.redox-os.org/fr/"
plateformes:
    - "Autre"
langues:
    - "English"
description_courte: "Un système d'exploitation UNIX-compatible écrit en Rust."
createurices: "Jeremy Soller"
alternative_a: "Microsoft Windows"
licences:
    - "Licence MIT/X11"
tags:
    - "système"
    - "système d'exploitation (os)"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Redox_(syst%C3%A8me_d%27exploitation)"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/redox"
---

Redox est un système d'exploitation complet écrit en Rust pour sa sécurité et compatible UNIX. Il utilise un micro-noyau spécialement développé, des pilotes en mode utilisateur ainsi qu'un système de fichiers spécifique. Il dispose également d'une interface graphique optionnelle appelée Orbital.

