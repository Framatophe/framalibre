---
nom: "Radio Tray"
date_creation: "Jeudi, 23 mars, 2017 - 15:52"
date_modification: "Jeudi, 23 mars, 2017 - 15:52"
logo:
    src: "images/logo/Radio Tray.png"
site_web: "http://radiotray.sourceforge.net/"
plateformes:
    - "GNU/Linux"
langues:
    - "Autres langues"
description_courte: "Radio Tray est une petite application d’une extrême simplicité et qui permet d’écouter les radios Internet."
createurices: "Carlos Ribeiro"
alternative_a: "Goodvibes"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "lecteur audio"
    - "lecteur radio"
    - "audio"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/radio-tray"
---

Radio Tray est une petite application légère d’une extrême simplicité et qui permet d’écouter les radios Internet. Cette application écrite en Python s’exécute et s’intègre directement dans le tableau de bord (ou barre des tâches) d’un système Linux. Ces caractéristiques sont :
hyper simple à utiliser et à paramétrer,
s’adapte à la plupart des formats audio (s’appuie sur la librairie « gstreamer »),
supporte le « drag & drop » d’URLs,
supporte les « playlist » au format PLS (Shoutcast/Icecast), M3U, ASX, WAX, WVX,
extensible par le biais de plugins.
NB : On entend par « Radio Internet » des radios FM qui diffusent également au travers d’Internet ou des radios qui n’émettent pas sur la bande FM (ou AM) et qui ne sont disponibles qu’au travers d’Internet.
Installation
Cette application (radiotray) est présente dans les dépôts officiels des principales distributions Linux. Sinon, vous trouverez les sources à cette adresse : http://radiotray.sourceforge.net/

