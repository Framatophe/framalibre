---
nom: "Bibliogram"
date_creation: "Samedi, 4 juin, 2022 - 15:48"
date_modification: "Mardi, 20 juin, 2023 - 12:25"
logo:
    src: "images/logo/Bibliogram.png"
site_web: "https://bibliogram.art/"
plateformes:
    - "Autre"
    - "le web"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Instagram mais en plus respectueux de la vie privée et sans inscription."
createurices: ""
alternative_a: "Instagram"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "frontend"
    - "photo"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/bibliogram"
---

Bibliogram permet de consulter Instagram sans inscription, publicités, Javascript et traçage de l'utilisateur. On peut s'abonner à un compte Instagram avec un flux RSS ou Atom. Aussi, on peut télécharger ses publications. Malheureusement, on peut uniquement consulter une partie des publications d'un compte à cause des restrictions imposés par Instagram.
Attention, le développement de ce logiciel est arrêté.

