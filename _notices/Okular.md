---
nom: "Okular"
date_creation: "Dimanche, 9 avril, 2017 - 17:09"
date_modification: "Jeudi, 20 mai, 2021 - 16:39"
logo:
    src: "images/logo/Okular.png"
site_web: "https://okular.kde.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Un visionneur de documents avec pas mal d'options (surligner, annotations,…)"
createurices: "Piotr Szymanski"
alternative_a: "Adobe Acrobat Reader, PDF-XChange, Microsoft Edge"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
    - "pdf"
    - "lecteur pdf"
    - "epub"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Okular"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/okular"
---

Okular est un visionneur de documents avec quelques options qu'on ne retrouve pas partout, notamment surligner du texte ou prendre des notes. Il propose une série d'annotations rares dans les lecteurs libres. Okular peut lire une quinzaine de documents, parmi lesquels : PDF, EPUB, ODF, DVI...
Il est inclus dans l'environnement de bureau KDE, mais peut aussi être installé dans les autres bureaux.

