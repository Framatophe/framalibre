---
nom: "Freebox zapper"
date_creation: "Jeudi, 20 avril, 2017 - 15:32"
date_modification: "Mercredi, 12 mai, 2021 - 15:16"
logo:
    src: "images/logo/Freebox zapper.png"
site_web: "https://github.com/vhiribarren/freebox-zapper-webapp"
plateformes:
    - "Android"
langues:
    - "Français"
description_courte: "Télécommande Android pour le décodeur français (Free) Freebox v6."
createurices: "Vincent Hiribarren"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "contrôle à distance"
lien_wikipedia: ""
lien_exodus: "https://reports.exodus-privacy.eu.org/fr/reports/143859/"
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/freebox-zapper"
---

C'est une télécommande pour le décodeur Freebox v6, pour contrôler le freeplayer directement avec votre smartphone Android.
Fonctionnalités :
    Freebox v6 supportée
    Dédié au contrôle TV
    Contrôle via Wi-Fi, ne fonctionne pas en 3G/4G
    Complémentaire à votre télécommande, ne se substitue pas à elle
    Mouvements et appuis longs non pris en compte

