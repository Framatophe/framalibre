---
nom: "Vuze"
date_creation: "Mardi, 14 mars, 2017 - 20:26"
date_modification: "Mercredi, 12 mai, 2021 - 15:16"
logo:
    src: "images/logo/Vuze.png"
site_web: "http://www.vuze.com/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Client de téléchargement Torrent"
createurices: ""
alternative_a: "BitTorrent, µTorrent, Xtorrent, BitComet"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
    - "torrent"
    - "téléchargement"
    - "p2p"
    - "tracker"
    - "client bittorrent"
    - "bittorrent"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Vuze_%28logiciel%29"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/vuze"
---

Vuze est un logiciel qui permet de télécharger et de créer des fichiers torrent.
il contient un outil de recherche de fichiers torrent,
il permet prend en charge les protocoles de communications anonymes comme I2P et TOR,
il permet de spécifier les vitesses d'envoi et de réception des différents téléchargements,
il permet la fonction base de données distribuée, permettant d'être plus indépendant des trackers (en obtenant d'autres pairs si le tracker initial est trop lent, voir suspendu).

