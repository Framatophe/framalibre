---
nom: "Tusky"
date_creation: "Lundi, 11 juin, 2018 - 15:48"
date_modification: "Mercredi, 5 septembre, 2018 - 15:21"
logo:
    src: "images/logo/Tusky.png"
site_web: "https://github.com/tuskyapp/Tusky"
plateformes:
    - "Android"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Un client Mastodon pour Android léger."
createurices: ""
alternative_a: "Twitter"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
    - "mastodon"
    - "fediverse"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/tusky"
---

Tusky est un client Mastodon léger pour Android qui peut gérer plusieurs comptes. Par ailleurs il a les fonctions de bases d'un clients Mastodon (lecture de fil d'actualité, pouet et repouet, gestion des comptes utilisateurs…)
Il ne possède pas de navigateur intégré ce qui contribue à sa légèreté.

