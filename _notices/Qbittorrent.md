---
nom: "Qbittorrent"
date_creation: "Vendredi, 30 décembre, 2016 - 15:36"
date_modification: "Mercredi, 12 mai, 2021 - 15:42"
logo:
    src: "images/logo/Qbittorrent.png"
site_web: "https://www.qbittorrent.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
langues:
    - "Autres langues"
description_courte: "Client bittorrent léger avec recherche intégrée et prévisualisation vidéo."
createurices: "Christophe Dumez"
alternative_a: "µTorrent"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
    - "torrent"
    - "téléchargement"
    - "bittorrent"
    - "client bittorrent"
    - "p2p"
    - "partage de fichiers"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Qbittorrent"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/qbittorrent"
---

Qbittorrent est un logiciel qui permet de télécharger et de créer des fichiers torrent. On notera quelques fonctionnalités particulières:
il contient un outil de recherche de fichiers torrent, qui cherche en simultané sur les principaux sites de partage de fichiers BitTorrent,
on peut le contrôler à distance avec une interface web,
il permet de prévisualiser des vidéos en cours de téléchargement, un peu comme du streaming,
il permet de lancer des téléchargements depuis des flux RSS,
il permet d'ordonner des listes de téléchargement,
et quelques trucs en plus !

