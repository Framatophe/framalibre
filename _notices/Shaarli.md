---
nom: "Shaarli"
date_creation: "Mardi, 18 septembre, 2018 - 23:13"
date_modification: "jeudi, 14 mars, 2024 - 10:56"
logo:
    src: "images/logo/Shaarli.png"
site_web: "https://github.com/shaarli/Shaarli"
plateformes:
    - "le web"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Un logiciel léger pour sauvegarder, trier et partager des adresses web."
createurices: "Sébastien Sauvage"
alternative_a: "delicious, Diigo"
licences:
    - "Licence Zlib"
tags:
    - "marque-pages"
    - "gestionnaire de contenus"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/shaarli"
---

Shaarli est un logiciel libre permettant de sauvegarder, trier, synchroniser et partager des adresses web. Il est à la fois léger et simple d'utilisation.

Chaque adresse enregistrée est accompagnée d'un titre, d'une description et de tags. Shaarli peut être utilisé pour prendre des notes : il suffit de laisser le champ de l'adresse vide.

Une [instance de démonstration](https://demo.shaarli.org/) est disponible (nom d'utilisateur·rice : demo, mot de passe : demo).

Des extensions sont disponibles pour rediriger les liens indexés vers des outils de microblogging comme [Mastodon](https://framalibre.org/notices/mastodon.html)…

Installation : il faut simplement un hébergement Web avec PHP pour installer cet outil.
