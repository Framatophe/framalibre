---
nom: "Salle de Classe"
date_creation: "Samedi, 29 août, 2020 - 10:52"
date_modification: "Lundi, 10 mai, 2021 - 14:20"
logo:
    src: "images/logo/Salle de Classe.png"
site_web: "https://www.lecluse.fr/blog/sdc/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
    - "Android"
langues:
    - "Français"
description_courte: "Une application qui permet aux enseignants de gérer la disposition des élèves dans une salle de classe"
createurices: "Thomas, Nicolas et Olivier Lécluse"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "éducation"
    - "enseignement"
    - "école"
    - "collège"
    - "organisation"
    - "pédagogie"
    - "élève"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/salle-de-classe"
---

Salle de Classe permet aux enseignants de gérer la disposition des élèves dans une salle de classe. Outre la gestion de l’espace de salle de cours, l’application propose des fonctionnalités pratiques et dispose d’une excellente documentation exhaustive.
Il existe une version pour mobile qui ne demande pas de passer par le google playstore, qui peut s’installer en flashant un QR code

