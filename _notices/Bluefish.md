---
nom: "Bluefish"
date_creation: "Samedi, 5 novembre, 2016 - 18:25"
date_modification: "Lundi, 4 mars, 2019 - 01:14"
logo:
    src: "images/logo/Bluefish.png"
site_web: "http://bluefish.openoffice.nl/index.html"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Un éditeur de texte simple et efficace."
createurices: ""
alternative_a: "Adobe Dreamweaver"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "développement"
    - "traitement de texte"
    - "éditeur html"
    - "html"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Bluefish"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/bluefish"
---

Produire du code propre et efficace pour votre site web à l’aide d’un éditeur html simple et efficace, ça vous tente ?
Alors allez chercher votre bonheur du côté de Bluefish. Ses multiples fonctionnalités et son interface intuitive, puissante et en français permettent de gérer facilement :
 l’ensemble du projet que constitue un site web ; 
 la coloration syntaxique du code ; prise en charge des fichiers Python, HTML, PHP, C, Java, JavaScript, JSP, SQL, XML, Perl, CSS, ColdFusion, Pascal, R et Octave/MATLAB. Vous pouvez vous-même paramétrer cette coloration à votre goût ! 
 l’ajout rapide de balises à l’aide de menus, de barres d’outils et surtout de très nombreux raccourcis clavier ; 
 le support des scripts PHP et Javascript, du CSS, etc.  
Mais tout ceci se retrouve également dans les nombreux logiciels d’édition de page web. Bluefish se démarque par sa légèreté.

