---
nom: "Syllabux"
date_creation: "Samedi, 23 septembre, 2023 - 09:15"
date_modification: "dimanche, 31 mars, 2024 - 08:49"
logo:
    src: "images/logo/Syllabux.png"
site_web: "https://educajou.forge.apps.education.fr/syllabux/"
plateformes:
    - "Apple iOS"
    - "Android"
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
description_courte: "Application de type \"classeur à syllabes\""
createurices: "Arnaud Champollion"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "éducation"
    - "lecture"
    - "cycle 2"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/syllabux"
---

Cette application reproduit le fonctionnement du syllabaire papier qu'on trouve sous forme de classeurs, avec les pages consonnes et voyelles qui se tournent indépendamment pour former les syllabes. Elle est plutôt destinée aux classes de CP et CE1.
Il s'agit d'une application "en ligne" (fonctionne dans un navigateur web comme Firefox par exemple).
Elle peut néanmoins fonctionner hors ligne, pour cela télécharger les sources sur la Forge.
