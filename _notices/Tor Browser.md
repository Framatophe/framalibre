---
nom: "Tor Browser"
date_creation: "Jeudi, 5 janvier, 2017 - 17:52"
date_modification: "Vendredi, 21 mai, 2021 - 09:54"
logo:
    src: "images/logo/Tor Browser.png"
site_web: "https://www.torproject.org/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
    - "Android"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Un navigateur basé sur Firefox et optimisé pour protéger la vie privée et contourner la censure."
createurices: ""
alternative_a: "Google Chrome, Microsoft Edge, Internet Explorer, Apple Safari, Opera"
licences:
    - "Berkeley Software Distribution License (BSD)"
tags:
    - "internet"
    - "navigateur web"
    - "vie privée"
    - "tor"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Tor_Browser"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/tor-browser"
---

Tor Browser, basé sur Firefox, est un navigateur conçu pour faire passer toute la navigation au travers du réseau Tor. La navigation est envoyée successivement à trois serveurs intermédiaires, chaque fois de façon chiffrée. De cette façon, les sites web visités ne peuvent pas déterminer l'adresse IP ni la localisation de l'utilisateur. Il permet également de contourner la censure mise en place dans certains pays.

