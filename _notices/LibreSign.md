---
nom: "LibreSign"
date_creation: "jeudi, 11 avril, 2024 - 17:10"
date_modification: "jeudi, 11 avril, 2024 - 17:10"
logo:
    src: "images/logo/LibreSign.png"
site_web: "https://libresign.coop/"
plateformes:
    - "le web"
langues:
    - "English"
description_courte: "Application Nextcloud pour signer les documents"
createurices: "LibreCode Cooperative"
alternative_a: ""
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "signature"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---


