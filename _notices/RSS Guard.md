---
nom: "RSS Guard"
date_creation: "dimanche, 5 novembre, 2023 - 16:41"
date_modification: "dimanche, 5 novembre, 2023 - 16:41"
logo:
    src: "images/logo/RSS Guard.png"
site_web: "https://github.com/martinrotter/rssguard/"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
description_courte: "Agrégateur de flux RSS pour lire des nouvelles et des balados (podcasts)."
createurices: "Martin Rotter"
alternative_a: "Feedly  Inoreader"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "lecteur de flux RSS"
    - "nouvelles"
lien_wikipedia: "https://en.wikipedia.org/wiki/RSS_Guard"
lien_exodus: ""
identifiant_wikidata: "Q107242660"
mis_en_avant: "non"
---

RSS Guard est un agrégateur de nouvelles gratuit et en code source libre pour les flux Web et les podcasts. Il est écrit en C++ et utilise Qt.  Il inclut un outil de téléchargement de fichiers, une configuration avancée du proxy réseau et prend en charge les outils de visualisation de médias externes.
Il peut lire les données de flux provenant de divers services en ligne  et API tels que Feedly, Inoreader, FreshRSS, Nextcloud News, Tiny Tiny RSS.
