---
nom: "vim"
date_creation: "Mercredi, 28 décembre, 2016 - 14:19"
date_modification: "Mercredi, 22 mars, 2017 - 21:13"
logo:
    src: "images/logo/vim.png"
site_web: "http://www.vim.org/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Vim est l'éditeur modulaire dont vous avez besoin."
createurices: "Bram Moolenaar"
alternative_a: "Notepad, Sublime Text, PyCharm, UltraEdit, Microsoft VisualStudio"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "développement"
    - "traitement de texte"
    - "texte"
    - "éditeur"
    - "environnement de développement"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Vim"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/vim"
---

Vim est un éditeur modulaire qui peut s'utiliser en ligne de commande ou en graphique. Il est extensible à l'infini grâce à ses très nombreux greffons !
Certes il est dur à prendre à main mais dès que la mémoire musculaire aura fait son travail vous serez plus efficaces que tout vos collègues grâce à son interface très bien pensée.

