---
nom: "KLayout"
date_creation: "Mercredi, 25 novembre, 2020 - 14:25"
date_modification: "Mercredi, 25 novembre, 2020 - 14:25"
logo:
    src: "images/logo/KLayout.png"
site_web: "https://www.klayout.de/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
description_courte: "Logiciel de visualisation/dessin de fichiers GDS utilisés pour la conception de circuits intégrés."
createurices: ""
alternative_a: "Virtuoso Layout Suite, LayoutEditor"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "métiers"
    - "dao"
    - "cao"
    - "2d"
    - "dessin"
    - "électronique"
    - "design"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/klayout"
---

KLayout permet de visualiser des fichiers de type .GDS et .OASIS ainsi que de les éditer. Il est destiné à la réalisation de masques pour la fabrication de circuits intégrés ou d'autres microdispositifs. Il possède également des fonctions avancées comme une vérification automatisée du respect des règles de dessin (DRC) ou le travail avec des cellules paramétrisées.

