---
nom: "Endless Sky"
date_creation: "Dimanche, 25 juin, 2017 - 21:59"
date_modification: "Dimanche, 25 juin, 2017 - 21:59"
logo:
    src: "images/logo/Endless Sky.png"
site_web: "http://endless-sky.github.io/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
description_courte: "Endless Sky est un jeu 2D se situant dans l'espace, avec un grand nombre de gameplay possibles pour le joueur."
createurices: ""
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "jeu"
    - "2d"
    - "science-fiction"
    - "espace"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/endless-sky"
---

Endless Sky est un jeu d'exploration de l'espace en 2D très construit et très complet sur lequel vous passerez des heures le jeu une fois bien pris en main! Il repose essentiellement sur la réalisation de missions, de transport de passager, de commerce spatial et de combats galactiques, mais pas que...
Vous vous retrouvez donc vous, au début du jeu, à la tête d'un petit vaisseau. Votre objectif est de gagner de l'argent par divers moyens, pour ensuite espérer explorer l'univers entier, en achetant de meilleurs vaisseaux, de l'équipement... Mais ce n'est pas tout, vous pouvez également partir à la rencontre d'aliens potentiellement plus évolués que vous, prendre part à une guerre civile, combattre les pirates...
Le jeu dispose d'une bonne et grandissante communauté. Il est actuellement en développement intensif, et un éditeur de carte spatiale est en cours de création.

