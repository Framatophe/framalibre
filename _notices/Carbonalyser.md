---
nom: "Carbonalyser"
date_creation: "Mercredi, 8 juin, 2022 - 19:39"
date_modification: "Mercredi, 8 juin, 2022 - 19:39"
logo:
    src: "images/logo/Carbonalyser.png"
site_web: "https://theshiftproject.org/carbonalyser-extension-navigateur/"
plateformes:
    - "Android"
    - "Autre"
langues:
    - "Français"
description_courte: "Le logiciel pour la sobriété numérique."
createurices: "Richard Hanna"
alternative_a: ""
licences:
    - "Licence MIT/X11"
tags:
    - "internet"
lien_wikipedia: ""
lien_exodus: "https://reports.exodus-privacy.eu.org/fr/reports/com.orange.labs.mobilecarbonaly…"
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/carbonalyser"
---

Ce logiciel est conçu pour visualiser approximativement la consommation électrique et les émissions de gaz à effet de serre (GES) associées à son activité sur le web et à un site précis. Aussi, on a la possibilité d'activer ou de désactiver l'analyse quand on le souhaite.

