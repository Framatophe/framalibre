---
nom: "GNUnet"
date_creation: "Mardi, 14 mars, 2017 - 22:18"
date_modification: "Mardi, 14 mars, 2017 - 23:26"
logo:
    src: "images/logo/GNUnet.png"
site_web: "https://gnunet.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Autre"
langues:
    - "Autres langues"
description_courte: "GNUnet est à la fois le nom d'un programme mais aussi un réseau p2p informatique."
createurices: "Projet GNU"
alternative_a: "Mullvad"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
    - "gnu"
    - "réseau"
    - "p2p"
    - "chiffrement"
lien_wikipedia: "https://fr.wikipedia.org/wiki/GNUnet"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/gnunet"
---

Le client GNUnet inclut le partage de fichier ce qui est pour le moment l'aspect central du logiciel.
GNUnet est également un réseau alternatif décentralisé, développé dans le but de construire des applications protégeant la vie privée et s'inscrivant dans une logique de sécurité par la décentralisation. L'utilisation du p2p permet de résister à la censure, les messages chiffrés permettent aussi de répondre à cet objectif.
GNUnet considère le protocole internet comme vieillissant et non sécurisé et tente donc de proposer une alternative pertinente à cette problématique.

