---
nom: "Jamulus"
date_creation: "Lundi, 23 novembre, 2020 - 08:48"
date_modification: "Lundi, 23 novembre, 2020 - 08:52"
logo:
    src: "images/logo/Jamulus.png"
site_web: "https://jamulus.io"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Jouez de la musique en ligne, avec vos ami·e·s, librement et gratuitement."
createurices: "Volker Fischer"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "musique"
    - "internet"
    - "temps réel"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/jamulus"
---

Jamulus est un logiciel libre écrit par Volker Fischer et les contributeur·trice·s pour jouer de la musique, répéter, ou improviser en ligne avec vos ami·e·s, votre groupe ou n'importe qui que vous y rencontrerez.
Jamulus fonctionne sous Windows, macOS ou GNU/Linux.

