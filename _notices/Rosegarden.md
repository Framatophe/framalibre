---
nom: "Rosegarden"
date_creation: "Lundi, 20 mars, 2017 - 18:12"
date_modification: "Vendredi, 24 novembre, 2017 - 05:33"
logo:
    src: "images/logo/Rosegarden.png"
site_web: "http://www.rosegardenmusic.com/"
plateformes:
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Logiciel de création musicale et d'édition de partitions."
createurices: "Chris Cannam, Richard Bown, Guillaume Laurent"
alternative_a: "Cubase"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "création"
    - "mao"
    - "création musicale"
    - "éditeur de partition"
    - "midi"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Rosegarden"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/rosegarden"
---

Rosegarden est un logiciel libre de création musicale, commencé en 1993 par l'université de Bath, en Angleterre. Il permet notamment d'éditer des partitions et de s'interfacer avec des instruments via le standard MIDI.
Ainsi les compositeurs peuvent composer et les groupes s'enregistrer.
Vous pouvez avoir un bon aperçu de ce que l'on peut réaliser avec Rosegarden avec des vidéos de démonstration sur youtube !

