---
nom: "WinCDEmu"
date_creation: "Vendredi, 7 avril, 2017 - 21:35"
date_modification: "Lundi, 10 mai, 2021 - 14:21"
logo:
    src: "images/logo/WinCDEmu.jpg"
site_web: "http://wincdemu.sysprogs.org/"
plateformes:
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Montez vos CD sans avoir à les graver"
createurices: ""
alternative_a: "DAEMON Tools, Virtual CloneDrive, UltraISO, PowerISO, Alcohol"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
    - "iso"
    - "cd-rom"
    - "dvd"
    - "bande dessinée"
    - "émulateur"
lien_wikipedia: "https://en.wikipedia.org/wiki/WinCDEmu"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/wincdemu"
---

WinCDEmu est un outil qui va vous permettre de monter d'un simple clic droit des fichiers .iso, .img ou encore .cue sur Windows sans avoir à graver le moindre CD
Le fichier sera alors reconnu par votre ordinateur comme si il avait été insérer dans votre lecteur de CD.

