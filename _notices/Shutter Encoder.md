---
nom: "Shutter Encoder"
date_creation: "Lundi, 21 juin, 2021 - 12:01"
date_modification: "Lundi, 21 juin, 2021 - 17:13"
logo:
    src: "images/logo/Shutter Encoder.png"
site_web: "https://www.shutterencoder.com/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Convertisseur vidéo/image/audio sans limite d'utilisation"
createurices: "Paul Pacifico"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "convertisseur"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/shutter-encoder"
---

Shutter Encoder fait partie des meilleurs logiciels d'encodage vidéo, image, et audio actuels.
Il a été pensé par des monteurs vidéo dans le but d'être le plus accessible et efficace possible.
C'est un des rares outils professionnels gratuits.
Basé sur FFmpeg, il dispose de la plus grande bibliothèque de codecs disponibles.
Vous pouvez ainsi convertir vos fichiers dans de très nombreux formats, couper des vidéos et changer le format d'images, ainsi qu'ajouter votre logos et autres "watermarks", et même écrire des sous-titres.

