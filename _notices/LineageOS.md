---
nom: "LineageOS"
date_creation: "Vendredi, 5 avril, 2019 - 22:45"
date_modification: "Jeudi, 17 août, 2023 - 21:08"
logo:
    src: "images/logo/LineageOS.png"
site_web: "https://lineageos.org/"
plateformes:
    - "Android"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "La meilleure alternative aux Androids GAFAM-isés."
createurices: "Steve Kondik"
alternative_a: "ios, Microsoft Windows"
licences:
    - "Licence Apache (Apache)"
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
    - "système d'exploitation (os)"
    - "android"
lien_wikipedia: "https://fr.wikipedia.org/wiki/LineageOS"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/lineageos"
---

LineageOS est un système d'exploitation basé sur la version communautaire du défunt CyanogenMod. Il est livré par défaut sans applications Google (Play Store, Maps, Search...) et est compatible avec plus de 120 modèles de téléphone.

