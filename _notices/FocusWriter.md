---
nom: "FocusWriter"
date_creation: "Mardi, 17 février, 2015 - 00:32"
date_modification: "Jeudi, 20 juillet, 2017 - 21:59"
logo:
    src: "images/logo/FocusWriter.png"
site_web: "https://gottcode.org/focuswriter/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Écrivez sans distraction et concentrez-vous sur votre texte."
createurices: ""
alternative_a: "Notepad, Microsoft Word"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
    - "traitement de texte"
    - "texte"
    - "écriture"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/focuswriter"
---

FocusWriter est un traitement de texte centré sur la simplicité et l'absence de distraction. Son but est de vous immerger dans un environnement d'écriture vous permettant de vous concentrer au maximum. Il est extrêmement simple et rapide à prendre en main.
Vous pourrez également apprécier la personnalisation des thèmes, la gestion d'alarmes, la fixation d'objectifs journaliers (en nombre de mots ou minutes), la correction orthographique, les sessions, etc. FocusWriter est un logiciel indispensable pour les écrivains voulant travailler tous les jours !
Il offre un support basique des formats TXT, RTF et ODT.
Astuce : mettez la version portable pour Windows de FocusWriter sur votre clé USB et vous pourrez mettre en place votre environnement de travail n'importe où !

