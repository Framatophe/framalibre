---
nom: "Olive Video Editor"
date_creation: "Dimanche, 9 juillet, 2023 - 22:21"
date_modification: "Samedi, 15 juillet, 2023 - 09:08"
logo:
    src: "images/logo/Olive Video Editor.png"
site_web: "https://www.olivevideoeditor.org"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
description_courte: "Olive est un éditeur vidéo non linéaire"
createurices: ""
alternative_a: "Adobe Premiere Elements, Pinnacle Studio"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "création"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/olive-video-editor"
---

Olive est un éditeur vidéo non linéaire visant à fournir une alternative complète aux logiciels de montage vidéo professionnels haut de gamme.
Olive fait des progrès rapides et les utilisateurs produisent déjà des vidéos avec, mais il est encore actuellement en version alpha, ce qui signifie qu'il est incomplet et pas complètement stable. Quoi qu'il en soit, nous vous invitons à télécharger la dernière version et à l'essayer par vous-même.
De nouvelles fonctionnalités sont ajoutées chaque jour. Même s'il manque à Olive quelque chose dont vous avez besoin, revenez dans un mois ou deux et il est possible que cela ait été mis en œuvre.

