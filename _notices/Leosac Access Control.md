---
nom: "Leosac Access Control"
date_creation: "Mercredi, 28 septembre, 2022 - 11:53"
date_modification: "Mercredi, 20 septembre, 2023 - 11:00"
logo:
    src: "images/logo/Leosac Access Control.png"
site_web: "https://leosac.com"
plateformes:
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
description_courte: "Leosac Access Control est une application libre de contrôle d'accès physique."
createurices: "Maxime Chamley"
alternative_a: ""
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "sécurité"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/leosac-access-control"
---

Leosac Access Control est une solution libre de contrôle d'accès physique pour l'accès aux salles et aux bâtiments, aussi bien pour les individus avec une seule porte d'entrée que pour les entreprises avec plusieurs centaines de portes sur plusieurs zones géographiques. Conçu pour être installé sur des systèmes embarqués sous Linux, il gérera ensuite la logique d'un système de contrôle d'accès. Fondamentalement, il ouvre les portes.
Une installation Leosac Access Control pour un point de passage unique est généralement composée d'un pico-pc faisant fonctionner le logiciel, d'un boitier de protection, connecté à un lecteur RFID et à la gâche électrique d'une porte.

