---
nom: "Omeka S"
date_creation: "Mardi, 27 décembre, 2016 - 23:53"
date_modification: "Vendredi, 18 août, 2023 - 12:18"
logo:
    src: "images/logo/Omeka S.png"
site_web: "https://omeka.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Gestion de bibliothèques et de collections orientée vers le web de données"
createurices: "Roy Rosenzweig Center for History and New Media"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "métiers"
    - "cms"
    - "archives"
    - "collection"
    - "recherche"
    - "musée"
    - "bibliothèque"
    - "science"
    - "science ouverte"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Omeka"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/omeka-s"
---

Omeka S, constitue une refonte complète de Omeka, dit « classique » qui intègre les standards du web sémantique. Omeka S est une plateforme de publication web destinée aux institutions désireuses de relier les collections du patrimoine culturel, les corpus scientifiques, etc.
C'est à la fois un outils de gestion de fonds documentaires  et un système de publication multi-sites destinés à faciliter l'accès aux contenus et leur partage.
Il offre les fonctionnalités suivantes :

Gestion d'un ensemble important d'items (métadonnées et ressources associées) selon des critères scientifiques et documentaires « à la carte »,
gestion de sites publics  basés sur un choix d'items avec des contenus et des ressources complémentaires,
interopérabilité. Utilisation des vocabulaires du web sémantique et du standard IIIF interopérabilité des  images,
nombreux modules, notamment de connectivité,
mise à disposition d'API.


