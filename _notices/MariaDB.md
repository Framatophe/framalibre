---
nom: "MariaDB"
date_creation: "Lundi, 9 janvier, 2017 - 23:09"
date_modification: "Jeudi, 21 mars, 2019 - 10:12"
logo:
    src: "images/logo/MariaDB.png"
site_web: "https://mariadb.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Un des gestionnaires de base de données les plus populaires, créé par les développeurs de MySQL."
createurices: "Michael Widenius"
alternative_a: "Oracle Database, Microsoft SQL Server, MySQL, PostgreSQL"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
    - "Licence publique générale limitée GNU (LGPL)"
tags:
    - "système"
    - "base de données"
    - "gestion"
    - "manipulation de données"
    - "mariadb"
    - "mysql"
lien_wikipedia: "https://fr.wikipedia.org/wiki/MariaDB"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/mariadb"
---

Le gestionnaire de base de données relationnelle MariaDB est un fork de MySQL créé par le concepteur de ce dernier après la main mise de la société Oracle sur le fleuron libre des gestionnaires de données de l'époque : MySQL. MariaDB a rapidement séduit l'univers du libre pour prendre le flambeau. Il est disponible aujourd'hui sur pratiquement toutes les distributions Linux soit en version 5.5 soit en version 10.X.
Sur le site de l'éditeur, vous trouverez un gestionnaire d'installation de paquets très bien conçu pour installer l'une ou l'autre des versions très simplement.
De façon simplifiée, une base de données relationnelle, c'est une structure qui permet de mémoriser des informations (les données) en les organisant de telle sorte qu'il soit possible de les restituer en établissant des relations entre-elles à des fins de gestion ou d'analyse. SQL (Structured Query Language) est le langage couramment utilisé pour interroger les données.

