---
nom: "Katyusha! MCD"
date_creation: "Jeudi, 18 mars, 2021 - 15:35"
date_modification: "Dimanche, 21 mars, 2021 - 13:57"
logo:
    src: "images/logo/Katyusha! MCD.png"
site_web: "http://katyusha-mcd.projet-phosphore.anazaar.org"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Autre"
langues:
    - "Français"
description_courte: "Katyusha! MCD
Élégant outil de modélisation de bases dedonnées selon la méthode Merise."
createurices: ""
alternative_a: "JMerise, PowerAMC, Win'Design"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "développement"
    - "mysql"
    - "merise"
    - "postgresql"
    - "sqlite"
    - "modélisation"
    - "base de données"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/katyusha-mcd"
---

Katyusha! MCD est un logiciel de modélisation de bases de données s’appuyant sur la méthode Merise.
Le projet est né du constat que trop peu de logiciels semblables sont libres et encore maintenus.
Le logiciel est programmé en TCL/TK, fonctionnant donc sur toutes les plateformes disposant d'un interpréteur TCL et la bibliothèque TK.
Des binaires sont également disponibles pour Les systèmes GNU/Linux, *BSD, Windows.
Katyusha! MCD est encore en cours de développement et peut comporter quelques bugs mais fonctionne dans l'ensemble très bien.
Les fonctionnalités suivantes sont implémentées :
- Associations entités/relations
- Héritage
- Identifiants relatifs
- Génération de script SQL
Il permet la génération de scripts SQL pour les cinq SGBD suivant :
- MySQL
- SQLite3
- SQLserver
- Oracle
- Postgresql

