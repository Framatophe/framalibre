---
nom: "Gobby"
date_creation: "Mardi, 18 avril, 2017 - 11:45"
date_modification: "Samedi, 17 juin, 2017 - 09:58"
logo:
    src: "images/logo/Gobby.png"
site_web: "https://gobby.github.io/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Édition collaborative (comme Framapad, sans site web)"
createurices: ""
alternative_a: "MoonEdit"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
    - "traitement de texte"
    - "édition collaborative"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Gobby"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/gobby"
---

Vous connaissez sûrement les Framapads, eh bien Gobby est un logiciel de bureautique qui permet de faire la même chose: de l'édition collaborative de documents (documents text), mais sans passer par un site web, ce qui peut être plus pratique des fois. De la même façon, chaque utilisateur choisit une couleur et tout le monde voit les modifications en temps réel. Il est possible de discuter sur la messagerie instantanée et de partager des fichiers.
Lorsque les utilisateurs sont sur le même réseau wifi, Gobby les trouve et les liste automatiquement. C'est donc facile de se connecter au même document.

