---
nom: "ABStock"
date_creation: "Mercredi, 6 mai, 2020 - 15:01"
date_modification: "Vendredi, 5 août, 2022 - 13:41"
logo:
    src: "images/logo/ABStock.jpg"
site_web: "https://abstock.gitlab.io"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
langues:
    - "Français"
description_courte: "Consultation de stock et commandes en ligne"
createurices: "vindarel"
alternative_a: "lalibrairie.com, Amazon, placedeslibraires.fr"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
    - "livres"
    - "catalogue"
    - "librairie"
    - "dilicom"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/abstock"
---

ABStock permet de montrer le catalogue d'une librairie en ligne. Les clients peuvent ainsi voir ce qu'il y a en rayon, faire leur sélection et passer commande. Le libraire peut de plus choisir une liste de livres à mettre en avant.
Le site fonctionne par défaut avec le logiciel libre de gestion de librairies Abelujo et se synchronise avec sa base de données à intervalles réguliers. Abelujo est le logiciel de gestion pour le libraire: il gère les inventaires, la vente, etc, et sait se connecter au FEL à la demande de Dilicom.
ABStock a été développé pendant le confinement pour aider les librairies à garder une activité et un lien avec leurs clients, et est utilisé avec succès chez La Palpitante à Mens (38) ainsi que dans des associations. La documentation nécessaire pour l'installer soi-même est aussi en français, et le développeur propose un service d'hébergement sur ses serveurs.

