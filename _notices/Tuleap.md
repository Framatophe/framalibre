---
nom: "Tuleap"
date_creation: "Jeudi, 23 mars, 2017 - 17:16"
date_modification: "Mardi, 17 mars, 2020 - 11:13"
logo:
    src: "images/logo/Tuleap.png"
site_web: "https://www.tuleap.org/"
plateformes:
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
description_courte: "Tuleap est une plateforme web moderne de Gestion de Projet et Développement Collaboratif"
createurices: "Enalean"
alternative_a: "jira, Github, trello, collabnet, polarion"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "métiers"
    - "gestion de projet"
    - "kanban"
    - "git"
    - "tracker"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Tuleap"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/tuleap"
---

Tuleap est une plateforme très complète et libre qui permet aux équipes de mieux collaborer sur leur projets, agiles ou non. Tuleap permet d'héberger et suivre plusieurs projets:
Parmi les grandes fonctionnalités :
- Scrum: gestion du backlog, planning des sprints, burndown
- Kanban: tableaux de cartes, cliquer/glisser, limitation du Work In progress, personnalisation des colonnes
- Tracker: configuration par projet, permissions par tracker et par champs, workflow avancé (transitions, triggers, dépendances entre champs)
- Intégration Git et SVN
- Revue de code: Pull Request, Gerrit
- Gestion des Tests Manuels et Automatisés
- Gestionnaire de document et Wiki par projet
- Graphique de Gantt
- Reporting: camemberts, barres, tableaux
- Traçabilité complète entre artefacts, code, jobs, documents, versions
Le développement de l'outil est dynamique puisque une nouvelle version sort chaque mois avec des corrections d'anomalies et de nouvelles fonctionnalités.

