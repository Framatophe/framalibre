---
nom: "Redshift"
date_creation: "Lundi, 16 février, 2015 - 01:58"
date_modification: "mercredi, 10 avril, 2024 - 17:12"
logo:
    src: "images/logo/Redshift.png"
site_web: "http://jonls.dk/redshift/"
plateformes:
    - "Android"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
description_courte: "Redshift adapte le rouge dans la colorimétrie de votre écran par rapport à l'heure, la date et votre ville."
createurices: "Jon Lund Steffensen"
alternative_a: "f.lux, Twilight"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
    - "utilitaire"
    - "colorimétrie"
    - "yeux"
    - "vision"
    - "écran"
    - "lumière"
    - "couleur"
    - "santé"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Redshift_(logiciel)"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/redshift"
---

Redshift est un logiciel adaptant le rouge dans la colorimétrie de votre écran par rapport à l'heure, la date et à votre ville. Le logiciel calcule ainsi dans quelle ambiance vous êtes. Le but est qu'il y ait le moins de différences possibles entre les couleurs dominantes de votre écran et celles de votre environnement. Redshift sauvera réellement vos yeux si vous travaillez la nuit !
La version pour Windows est encore expérimentale. Sous GNU/Linux, vous pouvez également installer le paquet gtk-redshift pour avoir une icône d'activation/désactivation dans la barre des tâches, bien pratique lorsque l'on a besoin des vraies couleurs (vidéos/photos/etc.).
