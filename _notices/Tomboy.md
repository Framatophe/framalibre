---
nom: "Tomboy"
date_creation: "Dimanche, 14 avril, 2019 - 18:25"
date_modification: "Vendredi, 26 avril, 2019 - 18:37"
logo:
    src: "images/logo/Tomboy.png"
site_web: "https://wiki.gnome.org/Apps/Tomboy"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Logiciel de prise de note facile d'emploi !"
createurices: "Gnome"
alternative_a: "OneNote, Evernote"
licences:
    - "Licence publique générale limitée GNU (LGPL)"
tags:
    - "bureautique"
    - "prise de notes"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Tomboy_(logiciel)"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/tomboy"
---

Tomboy est un logiciel de prise de notes pour Linux, Unix, Windows et Mac OS X. Simple et facile à utiliser, il peut toutefois vous aider à organiser les idées et les informations que vous traitez chaque jour.

