---
nom: "Just-install"
date_creation: "Lundi, 19 août, 2019 - 16:01"
date_modification: "Lundi, 19 août, 2019 - 16:03"
logo:
    src: "images/logo/Just-install.png"
site_web: "https://just-install.github.io/"
plateformes:
    - "Windows"
langues:
    - "English"
description_courte: "just-install est un programme en ligne de commande qui automatise l'installation des logiciels sous Windows."
createurices: ""
alternative_a: "Ninite, Kapersky Software Updater, FileHippo App Manager"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
    - "installation"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/just-install"
---

just-install est un programme simple qui  essaie de faire une chose simple et de bien le faire : télécharger un fichier setup.exe et l'installer en silence.
Il suffit de lancer just-install [le nom du logiciel] et le téléchargement se lance puis l'installation enchaîne.
En août 2019, 262 logiciels sont pris en charge, dont de nombreux logiciels libres (taper just-install list pour en obtenir les noms).
C'est beau comme apt !

