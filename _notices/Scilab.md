---
nom: "Scilab"
date_creation: "Samedi, 31 décembre, 2016 - 17:08"
date_modification: "Vendredi, 23 décembre, 2022 - 00:34"
logo:
    src: "images/logo/Scilab.png"
site_web: "http://www.scilab.org"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Scilab est un logiciel de calcul numérique destiné à des applications scientifiques et d'ingénierie."
createurices: "F. Delebecque -- S. Steer -- J-Ph. Chancelier -- C. Gomez -- M. Goursat -- R. Nikoukhah"
alternative_a: "Matlab, Simulink"
licences:
    - "Licence CECILL (Inria)"
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "science"
    - "mathématiques"
    - "calcul"
    - "simulation"
    - "modélisation"
    - "statistiques"
    - "visualisation de données"
    - "développement"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Scilab"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/scilab"
---

Fort de plus de 30 ans d'existence, Scilab est un logiciel de calcul numérique destiné à des applications scientifiques et d'ingénierie. C'est aussi une plateforme de développement qui permet d'interagir avec d'autres langages de programmation (Java, C, C++, Fortran, Tcl Tk).
    En plus de ses nombreux outils de visualisation, de modélisation, de simulation et d'optimisation, il est extensible par le biais de modules externes spécialisés dans des domaines d'application aussi variés que l'acquisition de données (avec Arduino par exemple), le traitement d'images, le programme de mathématiques du lycée, la logique floue, le calcul d'orbites, ou l'analyse par ondelettes.
    Initialement développé par l'INRIA dans les années 80, il est utilisé par de nombreuses entreprises, laboratoires, universités, et lycées partout dans le monde.
Liens :
Centre Gitlab : https://gitlab.com/scilab
Pages d'aide : https://help.scilab.org/docs/current/fr_FR/index.html

