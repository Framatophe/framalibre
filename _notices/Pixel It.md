---
nom: "Pixel It"
date_creation: "vendredi, 22 mars, 2024 - 22:58"
date_modification: "vendredi, 22 mars, 2024 - 22:58"
logo:
    src: "images/logo/Pixel It.png"
site_web: "https://ciaconelli.forge.aeif.fr/pixel-it"
plateformes:
    - "le web"
langues:
    - "Français"
description_courte: "Pixel-It est une application web conçue pour transformer l’art du pixel en une expérience éducative et divertissante. Idéale pour les enseignants, les étudiants et tous les passionnés d’art numérique, Pixel-It permet de créer des œuvres et dessin en pixel !"
createurices: "Cyril Iaconelli avec La Forge des communs numériques de l'Éducation Nationale"
alternative_a: ""
licences:
    - "Creative Commons (CC-By)"
tags:
    - "Pixel Art"
    - "Grille"
    - "Dessin"
    - "Pixel"
    - "Coloriage"
lien_wikipedia: "https://ciaconelli.forge.aeif.fr/pixel-it/ReadMe.html"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

## Fonctionnalités
- **Grille Personnalisable :** Utilisateurs peuvent définir les dimensions de leur grille pour adapter leur projet à leurs besoins.
- **Sélection de Couleur :** Une palette pour choisir et appliquer des couleurs à chaque pixel et un code automatique pour chaque couleur sélectionnée.
- **Image de Fond :** Importez une image de référence pour vous guider dans votre création de pixel art.
- **Enregistrement/Chargement au format HTML :** Il est possible d’enregistrer son Pixel-Art au format HTML pour le reprendre ultérieurement en le chargeant dans l’application. Il faudra cependant veiller à réassocier chaque couleur à chaque lettre pour reprendre à partir de là où vous vous étiez arrêté.
