---
nom: "ZeroNet"
date_creation: "Mardi, 14 mars, 2017 - 22:29"
date_modification: "Mercredi, 12 mai, 2021 - 15:33"
logo:
    src: "images/logo/ZeroNet.png"
site_web: "https://zeronet.io/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Basée sur la blockchain Namecoin, ZeroNet est une plateforme et une application p2p."
createurices: ""
alternative_a: "Mullvad"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
    - "réseau"
    - "p2p"
    - "vie privée"
    - "chiffrement"
    - "tor"
    - "blockchain"
    - "décentralisation"
    - "anonymat"
lien_wikipedia: "https://fr.wikipedia.org/wiki/ZeroNet"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/zeronet"
---

L'application ZeroNet permet de se connecter via un script python au réseau ZeroNet où les gens hébergent leur contenu. L'utilisation du p2p rend théoriquement impossible la censure et pour protéger les utilisateurs, ZeroNet embarque également Tor.
ZeroNet propose plusieurs services comme les blogs, la messagerie et le mail, où le contenu est mis à jour dynamiquement, et bien d'autres choses à découvrir lorsque vous prendrez part à ce beau projet.

