---
nom: "Hedgewars"
date_creation: "Vendredi, 30 décembre, 2016 - 18:07"
date_modification: "Vendredi, 30 décembre, 2016 - 18:13"
logo:
    src: "images/logo/Hedgewars.png"
site_web: "http://hedgewars.org"
plateformes:
    - "Autre"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Jeu de stratégie au tour par tour dans lequel plusieurs équipes d'hérissons s'entretuent."
createurices: "unCORr"
alternative_a: "Worms"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "jeu"
    - "stratégie"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Hedgewars"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/hedgewars"
---

Hedgewars est un jeu de stratégie au tour par tour en 2D avec des graphismes cartoon.
Il possède
plusieurs règles de jeu,
plusieurs armements,
plusieurs types de carte.
Parmi les modes de jeu certains modifient grandement le jeu comme
capture le drapeau,
course,
space invaders.
Il est jouable seul ou à plusieurs
sur internet,
en réseau local,
sur le même poste.

