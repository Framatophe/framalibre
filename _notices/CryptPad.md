---
nom: "CryptPad"
date_creation: "Samedi, 3 novembre, 2018 - 15:34"
date_modification: "jeudi, 8 février, 2024 - 17:34"
logo:
    src: "images/logo/CryptPad.png"
site_web: "https://cryptpad.org"
plateformes:
    - "le web"
    - "Autre"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "CryptPad est une suite bureautique collaborative chifrée de bout en bout et open-source."
createurices: "XWiki SAS"
alternative_a: "Google Docs, Office 365"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "traitement de texte"
    - "suite bureautique"
    - "travail collaboratif"
    - "édition collaborative"
    - "texte"
    - "kanban"
    - "sondage"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/cryptpad"
---

## Une suite complète d'applications
CryptPad fournit une suite bureautique complète avec tous les outils nécessaires à une collaboration en ligne en temps réel. Les applications comprennent : texte enrichi, tableurs, code/markdown, kanban, diapositives, tableau blanc et formulaires.

## Chiffrée de bout en bout
Toutes les données sur CryptPad sont chiffrées dans le navigateur. Cela signifie qu'aucune donnée lisible ne quitte l'appareil de l'utilisateur·ice. Même les administrateur·ices du service ne peuvent pas voir le contenu des documents ou des données des utilisateur·ices.

## Collaboration privée
CryptPad est conçu pour favoriser la collaboration grâce à des fonctionnalités telles que les drives d'équipes, le calendrier et le partage. Il synchronise les modifications apportées aux documents en temps réel. Comme toutes les données sont chiffrées, le service et ses administrateur·ices n'ont aucun moyen de voir le contenu édité et stocké.

## Complètement libre et open source
CryptPad est un logiciel libre. Tout le monde peut héberger la plateforme à titre personnel ou professionnel. L'équipe de développement propose des abonnements et des contrats de support pour des déploiements prêts à l'emploi.

## Argent public = code public
CryptPad est soutenu depuis 2016 par des bourses de recherche françaises et européennes telles que BPI France, NLNet Foundation, NGI Trust, Mozilla Open Source Support. Nous pensons que l'argent public doit financer du code public, c'est pourquoi le service est entièrement open source. Cela signifie que tout le monde peut utiliser, héberger et modifier le logiciel.

## Financé par et pour les utilisateurs
Contrairement à d'autres plateformes, CryptPad n'offre pas de services "gratuits" pour vendre des données d'utilisateur·ices et encaisser des fonds pour ses investisseurs. Notre objectif actuel est d'être entièrement financé par les utilisateur·ices au moyen d'abonnements et de dons. Il n'y a pas d'investisseurs qui attendent de profiter des données des utilisateur·ices (elles sont de toute façon chiffrées), et il n'y a pas de "stratégie de sortie" puisque tout le code est déjà dans le domaine public.

Pour que CryptPad continue à se développer et reste gratuit pour tous, vous pouvez [vous abonner](https://cryptpad.org/pricing/), [faire un don](https://opencollective.com/cryptpad/contribute) ou [contribuer](https://docs.cryptpad.fr/fr/how_to_contribute.html).

## Fait par XWiki
L'instance officielle à l'adresse https://cryptpad.fr est administrée par XWiki SAS, l'entreprise française qui a créé CryptPad en 2017 et qui le maintient encore aujourd'hui.
