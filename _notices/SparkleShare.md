---
nom: "SparkleShare"
date_creation: "Mercredi, 18 mars, 2015 - 19:25"
date_modification: "Mardi, 10 janvier, 2017 - 04:04"
logo:
    src: "images/logo/SparkleShare.png"
site_web: "http://sparkleshare.org/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
    - "le web"
langues:
    - "Autres langues"
description_courte: "SparkleShare offre une interface graphique pour le stockage et la synchronisation de données dans le Cloud..."
createurices: ""
alternative_a: "Dropbox"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "synchronisation"
    - "partage"
    - "stockage"
lien_wikipedia: "https://en.wikipedia.org/wiki/SparkleShare"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/sparkleshare"
---

SparkleShare est un logiciel libre offrant une interface graphique permettant le stockage et la synchronisation de données dans les nuages ("le cloud"). Par défaut il utilise Git pour le stockage de données. Il offre la possibilité d’utiliser une plat-forme en ligne tel que Github mais aussi d'utiliser sa plate-forme auto-hébergé.
L'utilisation est comparable a Dropbox, une fois installé et configuré SparkleShare crée un dossier qui est synchronisé.

