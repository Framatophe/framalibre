---
nom: "Tux Timer"
date_creation: "dimanche, 18 février, 2024 - 20:33"
date_modification: "dimanche, 31 mars, 2024 - 08:48"
logo:
    src: "images/logo/Tux Timer.png"
site_web: "https://educajou.forge.apps.education.fr/tuxtimer/"
plateformes:
    - "le web"
    - "Apple iOS"
    - "Android"
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
description_courte: "Un compte à rebours graphique type \"time timer\""
createurices: "Arnaud Champollion"
alternative_a: "Time timer"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "timer"
    - "école"
    - "classe"
    - "temps"
    - "compte à rebours"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

Tuxtimer est un compte à rebours réglable de 0 à 60 minutes. Pour l'utiliser, choisir la durée au moyen des boutons +/- ou en écrivant le nombre de minutes au clavier.

Une fois lancé, vous pouvez suspendre l'écoulement du compte à rebours au moyen du bouton "pause".
Il est possible de personnaliser la couleur, le sens de rotation ainsi que le son de notification.

Vous pouvez aussi passer des paramètres dans l'URL, en ajoutant un point d'interrogation ? à la fin de l'URLet en séparant chaque paramètre par une esperluette &.

Par exemple :

https://achampollion.forge.aeif.fr/tuxtimer/?sens=horaire&minutes=25&couleur=yellow&autostart=true

lancera Tuxtimer dans le sens horaire, pour une durée de 25 minutes, avec une couleur jaune et un départ automatique.

**sens**=horaire/antihoraire
**minute**s=nombre de 1 à 60
**couleur**=nom de couleur ou code HTML (sans le #)
**autostart**=true/false

Tux Timer existe aussi en version Openboard, voir pour cela [sur la page du projet](https://forge.aeif.fr/achampollion/tuxtimer#openboard)
