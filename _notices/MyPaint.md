---
nom: "MyPaint"
date_creation: "Jeudi, 5 février, 2015 - 11:34"
date_modification: "lundi, 26 février, 2024 - 22:02"
logo:
    src: "images/logo/MyPaint.png"
site_web: "http://mypaint.app/"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "English"
description_courte: "MyPaint est un logiciel de dessin dont les principaux attraits sont la gestion des tablettes et des brosses."
createurices: ""
alternative_a: "Microsoft Paint, Pixelmator, MyBrushes"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "création"
    - "graphisme"
    - "infographie"
    - "dessin"
    - "brosse"
    - "peinture"
lien_wikipedia: "https://fr.wikipedia.org/wiki/MyPaint"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/mypaint"
---

MyPaint est un logiciel de dessin dont les principaux attraits sont la gestion des tablettes graphiques et une gestion avancée des brosses. Les brosses peuvent être dynamiques et changer en fonction de la pression, de la vitesse ou aléatoirement. Il propose une surface de dessin illimitée, est capable de zoomer/dézoomer et d’agrandir/réduire les brosses. Il génère des images au format PNG.
