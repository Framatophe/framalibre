---
nom: "Retroshare"
date_creation: "Mercredi, 4 janvier, 2017 - 12:02"
date_modification: "Mercredi, 12 mai, 2021 - 16:17"
logo:
    src: "images/logo/Retroshare.png"
site_web: "http://retroshare.cc/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Communication chiffrée et décentralisée: tchat, appels audio et vidéo, partage de fichiers, messages, etc"
createurices: ""
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
    - "partage de fichiers"
    - "communication chiffrée"
    - "p2p"
    - "messagerie instantanée"
    - "visioconférence"
    - "appels audio"
    - "client mail"
    - "forum de discussion"
lien_wikipedia: "https://fr.wikipedia.org/wiki/RetroShare"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/retroshare"
---

Retroshare permet de communiquer et de partager des fichiers entre personnes et groupes de manière anonyme et sécurisée. Il est basé sur les technologies pair-à-pair et est décentralisé (il fonctionne sans serveur central). On peut le coupler avec TOR ou I2P pour garantir son anonymité.
Il propose:
- messagerie instantanée (texte et images),
- appels audio et vidéo,
- messages privés (sorte de mails),
- partage de fichiers, entre amis ou à tous les membres du réseau,
- forums,
- partage de fichiers dans des chaînes thématiques

