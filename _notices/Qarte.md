---
nom: "Qarte"
date_creation: "Mardi, 6 juin, 2017 - 23:01"
date_modification: "Vendredi, 7 mai, 2021 - 10:54"
logo:
    src: "images/logo/Qarte.png"
site_web: "http://oqapy.eu/download?lang=fr"
plateformes:
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Pour re-revoir les replays d'Arte"
createurices: "Vinss"
alternative_a: "replay"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "enregistrement"
    - "télévision"
    - "flux"
    - "streaming"
    - "vidéo"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/qarte"
---

Qarte vous permet de récupérer les replays d'Arte et Arte Concert pendant leur durée de disponibilité afin de pouvoir les viualiser sur votre ordinateur avec votre lecteur vidéo préféré.
- Permet de consulter la liste des émissions en replay avec leur aperçu et notices
- Paramétrer la qualité de l'enregistrement, la langue et les sous-titres (si disponible)
- Programmer les enregistrements (notamment pour les émissions à venir)
- Faire une liste d'enregistrements

