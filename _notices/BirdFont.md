---
nom: "BirdFont"
date_creation: "Jeudi, 20 juillet, 2017 - 20:50"
date_modification: "Jeudi, 20 juillet, 2017 - 20:50"
logo:
    src: "images/logo/BirdFont.png"
site_web: "https://birdfont.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
description_courte: "Générateur de police de caractères."
createurices: "Johan Mattsson"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "création"
    - "police de caractères"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/birdfont"
---

Générateur de polices de caractères aux formats TTF, BF, EOF ou SVG.
ATTENTION : même si la licence affichée est la GNU GPL ce logiciel se présente avant tout comme une espèce de freemium. L'auteur semble sous-entendre que la version libre ne serait utilisable que pour générer des polices non commerciales !

