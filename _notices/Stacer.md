---
nom: "Stacer"
date_creation: "Samedi, 29 juillet, 2017 - 11:40"
date_modification: "Samedi, 29 juillet, 2017 - 11:42"
logo:
    src: "images/logo/Stacer.png"
site_web: "https://github.com/oguzhaninan/Stacer"
plateformes:
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "État des lieux complet du système en quelques onglets."
createurices: "Oguzhan Inan"
alternative_a: ""
licences:
    - "Licence MIT/X11"
tags:
    - "système"
    - "mémoire"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/stacer"
---

Stacer est un tableau de bord présentant une quantité impressionnante d'informations pratiques sur le système GNU/Linux utilisé.
Les parties sont les suivantes :
- tableau de bord ;
- nettoyeur système ;
- applications au démarrage ;
- services système ;
- processus en route ;
- désinstalleur paquets ;
- ressources CPU, internet, mémoire.

