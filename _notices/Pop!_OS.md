---
nom: "Pop!_OS"
date_creation: "Samedi, 20 juillet, 2019 - 13:26"
date_modification: "Jeudi, 20 mai, 2021 - 16:27"
logo:
    src: "images/logo/Pop!_OS.png"
site_web: "https://system76.com/pop"
plateformes:
    - "GNU/Linux"
langues:
    - "Autres langues"
description_courte: "Une distribution Gnu/Linux pour les développeurs, les créateurs et les professionnels de l'informatique."
createurices: ""
alternative_a: "Microsoft Windows, MacOS"
licences:
    - "Multiples licences"
tags:
    - "système"
    - "système d'exploitation (os)"
    - "distribution gnu/linux"
    - "ubuntu"
    - "linux"
    - "gnome"
lien_wikipedia: "https://en.wikipedia.org/wiki/System76#Pop!_OS"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/popos"
---

Pop!_OS est une distribution développée par la société System76 (vendeur d'ordinateurs dans le Colorado) basée sur Ubuntu et utilisant l'environnement de bureau GNOME.
Pop!_OS est conçu pour avoir un minimum d'encombrement sur le bureau sans distractions afin de permettre à l'utilisateur de se concentrer sur son travail.
Pop!_OS fournit un chiffrement complet du disque par défaut ainsi qu'une gestion rationalisée des fenêtres, des espaces de travail et des raccourcis clavier pour la navigation.
Pop!_OS offre quelques nouveautés majeures, dont certaines sont très intéressantes. Par exemple : GRUB a été remplacé par systemd-boot et un outil appelé kernelstub, et il y a une partition de récupération, donc une clé USB n'est plus nécessaire pour récupérer le système.

