---
nom: "Kodi"
date_creation: "Mardi, 21 mars, 2017 - 22:11"
date_modification: "Mercredi, 12 mai, 2021 - 15:08"
logo:
    src: "images/logo/Kodi.png"
site_web: "https://kodi.tv/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "Apple iOS"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Lecteur multimédia universel et multi-plateforme."
createurices: ""
alternative_a: "Windows Media Player, Quicktime, Windows Media Center, Plex"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "lecteur multimédia"
    - "lecteur vidéo"
    - "lecteur audio"
    - "réseau"
    - "windows"
    - "android"
    - "linux"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Kodi_Entertainment_Center"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/kodi"
---

Kodi, anciennement XBMC, est un lecteur multimédia, ainsi qu'un media center. Il offre un très large support de formats audios et vidéos, et rassemble d'autres fonctionnalités comme la météo, les diaporamas. Il supporte n'importe quelle source incluant les CD/DVD, le stockage et réseau local, ainsi qu'internet. Il peut se connecter à des sources tierces pour y récupérer les caractéristiques d'un film ou d'une musique (vignette, acteurs, chanteur, compositeur, etc) et permet d'écouter des webradios.
Il est possible d'y ajouter des extensions afin d'améliorer et de diversifier ses fonctionnalités.

