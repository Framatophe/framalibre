---
nom: "GParted"
date_creation: "Samedi, 7 janvier, 2017 - 19:43"
date_modification: "Samedi, 7 janvier, 2017 - 19:43"
logo:
    src: "images/logo/GParted.png"
site_web: "http://gparted.org/"
plateformes:
    - "GNU/Linux"
langues:
    - "Autres langues"
description_courte: "GParted est un outils Linux pour modifier/éditer des partions ou un disque."
createurices: ""
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
    - "stockage"
    - "administration système"
lien_wikipedia: "https://fr.wikipedia.org/wiki/GParted"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/gparted"
---

GParted ou "GNOME Partition Editor", est un logiciel disponible sur Linux qui permet de formater/éditer vos disque durs.
Vous pouvez par exemple créer des nouvelles partitions, les formater, les agrandir/réduire, ...
Il gère les formats suivants : btrfs, ext2/3/4, f2fs, FAT16/32, hfs/hfs+, linux-swap, lvm2 pv, nilfs2, NTFS, reiserfs/4, ufs et les fichiers xfs.

