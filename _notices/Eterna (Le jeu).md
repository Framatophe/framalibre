---
nom: "Eterna (Le jeu)"
date_creation: "Jeudi, 6 janvier, 2022 - 11:59"
date_modification: "Jeudi, 6 janvier, 2022 - 12:23"
logo:
    src: "images/logo/Eterna (Le jeu).jpg"
site_web: "https://eternagame.org/"
plateformes:

langues:
    - "English"
description_courte: "Un jeu vidéo où vous résolvez des puzzle d'énigmes de conception d'ARN"
createurices: "Jeehyung Lee"
alternative_a: ""
licences:
    - "Multiples licences"
tags:
    - "jeu"
    - "biologie"
    - "médecine"
    - "travail collaboratif"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/eterna-le-jeu"
---

Un jeu vidéo où vos solutions sont synthétisées et testées pour la médecine, la biochimie et la robotique. De nombreux challenges et niveaux de jeux.
Collectivement, des joueuses et joueurs ont synthétisé des milliers de molécules conçues, participant à publier plus de 20 articles universitaires abordant les questions fondamentales de la conception d'ARN et ouverant la voie au développement de médicaments à base d'ARN.

