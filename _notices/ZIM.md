---
nom: "ZIM"
date_creation: "Mercredi, 4 janvier, 2017 - 14:47"
date_modification: "Mercredi, 12 mai, 2021 - 15:51"
logo:
    src: "images/logo/ZIM.png"
site_web: "http://zim-wiki.org/"
plateformes:
    - "GNU/Linux"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Un wiki personnel sur votre bureau. Écrivez et organisez des petits textes, images et fichiers."
createurices: "Jaap G Karssenberg"
alternative_a: "Evernote"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
    - "wiki"
    - "organisation"
    - "notes"
    - "documentation"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Zim-wiki"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/zim"
---

Zim est un wiki de bureau. Vous créez facilement des pages de texte, que vous organisez. Vous pouvez lier des fichiers. C'est un outil utile pour organiser vos idées, des documents, des projets ou tenir un journal personnel. Le tout en local sur votre disque dur sous forme de dossiers et simples fichiers textes, et vous pouvez également rechercher dans votre wiki.
ZIM est de plus extensible avec des greffons qui ajoutent par exemple une gestion simple des tâches. Il peut également fonctionner avec un gestionnaire de version. Il peut même servir à générer un site internet.

