---
nom: "Shutter"
date_creation: "mercredi, 14 février, 2024 - 13:35"
date_modification: "mercredi, 14 février, 2024 - 13:35"
logo:
    src: "images/logo/Shutter.svg"
site_web: "https://shutter-project.org/"
plateformes:
    - "le web"
    - "GNU/Linux"
langues:
    - "English"
description_courte: "Shutter permet de capturer presque tout ce qui se trouve sur votre écran."
createurices: "https://shutter-project.org/team/"
alternative_a: "https://framalibre.org/notices/ksnip.html"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:

lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: "Q18088089"
mis_en_avant: "non"

---

Shutter est un outil de capture d'écran pour les systèmes d'exploitation basés sur Linux. Vous pouvez prendre une capture d'écran d'une zone spécifique, d'une fenêtre, de tout votre écran ou même d'un site web, lui appliquer différents effets, dessiner dessus pour mettre des points en évidence, puis la télécharger sur un site d'hébergement d'images, le tout dans une seule fenêtre.

Vous pouvez : 
- Capturer une zone spécifique
- Capturer votre bureau
- Capturer une fenêtre
- Capturer un menu ou une info-bulle
- Capturer un site web
