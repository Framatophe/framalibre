---
nom: "Tock"
date_creation: "Jeudi, 11 novembre, 2021 - 22:33"
date_modification: "Vendredi, 12 novembre, 2021 - 21:12"
logo:
    src: "images/logo/Tock.png"
site_web: "https://doc.tock.ai"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "FirefoxOS"
    - "Windows Mobile"
    - "Apple iOS"
    - "Autre"
    - "le web"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Tock est un chatbot totalement libre et indépendant"
createurices: "Julien Buret, François Nollen"
alternative_a: "DialogFlow, Watson, Azure Bot Service, DYDU, Clevy, Mindsay"
licences:
    - "Licence Apache (Apache)"
tags:

lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/tock"
---

Tock (The Open Conversation Kit) est une plateforme complète pour construire des agents conversationnels ou chatbots. Son gros avantage consiste à son indépendance totale par rapport à des API tierces.
La plateforme a été développé initialement par et pour la SNCF pour faire touner le OUIbot. Sa mise sous licence libre lui a permis d'être réutiliser dans plusieurs autres services, comme récemment AlloCOVID.
Le seul manque actuel de ce kit est la reconnaissance vocale, mais des connecteurs sont fournis pour interfacer le chatbot à des canaux externes prenant en charge la conversion voix-texte.

