---
nom: "OptGeo"
date_creation: "Samedi, 14 janvier, 2017 - 22:54"
date_modification: "Jeudi, 20 juillet, 2017 - 16:39"
logo:
    src: "images/logo/OptGeo.png"
site_web: "http://jeanmarie.biansan.free.fr/optgeo.html"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
description_courte: "Un logiciel de modélisation en optique géométrique (et physique)."
createurices: "Jean-Marie Biansan"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "science"
    - "géométrie"
    - "lumière"
    - "physique"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/optgeo"
---

OptGeo permet de créer des modèles d'optique géométrique (et ondulatoire) et d'afficher des schémas de réfraction, réflexion, lentilles et miroirs (microscopes, périscopes, télescopes...). Il permet aussi de montrer des aberrations géométriques et chromatiques. Sa prise en main est facile et ne nécessite pas de long apprentissage en particulier grâce aux nombreux exemples proposés.

