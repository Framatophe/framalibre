---
nom: "Nexi"
date_creation: "Jeudi, 10 septembre, 2020 - 08:06"
date_modification: "Dimanche, 17 septembre, 2023 - 13:25"
logo:
    src: "images/logo/Nexi.png"
site_web: "https://nexi.fiat-tux.fr"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "FirefoxOS"
    - "Windows Mobile"
    - "Apple iOS"
    - "Autre"
    - "le web"
langues:
    - "Autres langues"
description_courte: "Une application de gestion de listes simple et légère."
createurices: "Luc Didry"
alternative_a: "Bring!, Out of Milk"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:

lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/nexi"
---

Nexi est une application web de gestion de listes qui peut s’installer sur votre téléphone grâce à la technologie PWA.
Bien qu’étant une application web, vos données ne quittent pas votre navigateur.
Une présentation complète du logiciel est disponible sur le blog de l’auteur.

