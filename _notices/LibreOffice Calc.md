---
nom: "LibreOffice Calc"
date_creation: "Dimanche, 25 décembre, 2016 - 23:37"
date_modification: "Mercredi, 13 juillet, 2022 - 13:01"
logo:
    src: "images/logo/LibreOffice Calc.png"
site_web: "https://fr.libreoffice.org/discover/calc/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Tous les tableurs ne se ressemblent pas, la preuve."
createurices: ""
alternative_a: "Microsoft Excel"
licences:
    - "Licence Publique Mozilla (MPL)"
tags:
    - "bureautique"
    - "tableur"
    - "calcul"
    - "comptabilité"
    - "analyse"
lien_wikipedia: "https://fr.wikipedia.org/wiki/LibreOffice#Calc"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/libreoffice-calc"
---

LibreOffice Calc est le tableur de la suite LibreOffice. Gestion de feuilles de calcul, travail collaboratif (sans conflit de modification), macros, analyse et traitement de données, graphiques... Il est compatible avec les feuilles de calcul issues du format de Microsoft (jusqu'à un certain point). Des modèles de feuilles de calcul prêts à l'emploi sont disponibles en ligne.

