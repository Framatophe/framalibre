---
nom: "YesWiki"
date_creation: "Vendredi, 5 avril, 2019 - 15:59"
date_modification: "Lundi, 10 mai, 2021 - 13:31"
logo:
    src: "images/logo/YesWiki.png"
site_web: "https://yeswiki.net"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "L'outil libre faciliter la coopération ouverte !"
createurices: ""
alternative_a: ""
licences:
    - "Licence Publique Générale Affero (AGPL)"
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "cms"
    - "wiki"
    - "travail collaboratif"
    - "cartographie"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/yeswiki"
---

YesWiki est un wiki conçu pour rester simple, très facile à installer, en français traduit en anglais, espagnol, catalan, flamand...
Néanmoins, avec un YesWiki on peut fabriquer un site internet aux usages multiples :
  -  Rassembler toutes les infos d'un projet ou d'un groupe (fonction de "gare centrale")
  -  Cartographier des membres ou des lieux de façon participative
  -  Partager des ressources, des listes, des agendas grâce à des bases de données coopératives puissantes
  -  Faire communiquer des flux d'informations
  -  Cultiver un bout de liberté...

