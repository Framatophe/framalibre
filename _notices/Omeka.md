---
nom: "Omeka"
date_creation: "Mardi, 27 décembre, 2016 - 11:17"
date_modification: "Vendredi, 18 août, 2023 - 12:19"
logo:
    src: "images/logo/Omeka.png"
site_web: "http://omeka.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Un outil simple pour cataloguer et partager les ressources numériques des bibliothèques, musées, archives…"
createurices: "Roy Rosenzweig Center for History and New Media"
alternative_a: "Orphea, PastPerfect"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "cms"
    - "archives"
    - "collection"
    - "recherche"
    - "musée"
    - "bibliothèque"
    - "science"
    - "science ouverte"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Omeka"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/omeka"
---

Omeka est un logiciel libre de gestion et de valorisation de fonds d’archives et de collections de documents ou d’objets. Il est destiné aux bibliothèques, médiathèques, musées, écomusées, sociétés savantes ou historiques, organismes de recherche, établissements d’enseignement, groupe de citoyens, etc. désireux de partager leurs ressources ou d’en collecter de nouvelles.
Gérant ensemble notices de métadonnées et fichiers associés : textes, images, sons, etc. il se situe au croisement de différents besoins : publication de contenus, gestion de collections et éditorialisation des ressources. Il est basé sur une communauté de développeurs et d’utilisateurs venant des bibliothèques, des musées et de la recherche.
Omeka s'attache à respecter les recommandations du W3C, les normes 508 d'accessibilité et les standards de métadonnées (Dublin Core par défaut). Il propose une API en lecture comme en écriture. Omeka S, une version orientée vers le web de données est développée en parallèle.

