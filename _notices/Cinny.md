---
nom: "Cinny"
date_creation: "jeudi, 4 janvier, 2024 - 08:57"
date_modification: "jeudi, 4 janvier, 2024 - 08:57"
logo:
    src: "images/logo/Cinny.svg"
site_web: "https://cinny.in/"
plateformes:
    - "le web"
    - "GNU/Linux"
langues:

description_courte: "Cinny un client matrix qui se concentre principalement sur une interface simple et élégante."
createurices: ""
alternative_a: ""
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "messagerie instantanée"
    - "matrix"
    - "chat"
    - "chiffrement"
    - "internet"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

Cinny est un client matrix qui se concentre principalement sur une interface simple, élégante et sécurisée, protégée par e2ee avec la puissance de l'open source.

Caractéristiques : Design convivial, connexion et enregistrement, prise en charge de l'espace complet, cryptage et vérification des emoji, possibilité de créer de nouveaux espaces et de modifier les paramètres, prise en charge des réponses, markdown, édition des messages, thèmes sombre, clair et noir.
