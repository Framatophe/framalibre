---
nom: "HomeBank"
date_creation: "Mardi, 27 décembre, 2016 - 11:59"
date_modification: "Mardi, 27 décembre, 2016 - 11:59"
logo:
    src: "images/logo/HomeBank.png"
site_web: "http://homebank.free.fr/"
plateformes:
    - "Autre"
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Autres langues"
description_courte: "Logiciel de comptabilité personnelle."
createurices: "Maxime Doyen"
alternative_a: "Microsoft Money"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
    - "finances"
    - "budget"
    - "gestion"
    - "comptabilité"
lien_wikipedia: "https://fr.wikipedia.org/wiki/HomeBank"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/homebank"
---

HomeBank est un logiciel de comptabilité personnelle : il permet de gérer les opérations sur un ou plusieurs comptes bancaires, épargnes, etc. Il est possible de trier ses opérations par type (alimentation, essence, loyer, etc.) et d'affecter des budgets à chaque. Des statistiques peuvent aussi être facilement affichées (« combien ai-je dépensé en transports ce mois-ci ? »). Des opérations automatiques peuvent-être déclarées (salaire, assurance, loyer, etc.).
Bref, un logiciel pour gérer ses comptes facilement et avec une interface simple et claire.

