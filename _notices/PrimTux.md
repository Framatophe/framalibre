---
nom: "PrimTux"
date_creation: "Samedi, 13 mai, 2017 - 18:57"
date_modification: "Mercredi, 12 mai, 2021 - 15:37"
logo:
    src: "images/logo/PrimTux.png"
site_web: "https://primtux.fr/"
plateformes:
    - "GNU/Linux"
langues:
    - "Français"
description_courte: "Distribution GNU/Linux éducative adaptée à l'école primaire en France et aux enfants."
createurices: "l'équipe PrimTux"
alternative_a: "Microsoft Windows, MacOS"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "éducation"
    - "distribution gnu/linux"
    - "enfant"
    - "école"
    - "debian"
    - "apprentissage"
    - "réemploi"
    - "accessibilité"
    - "dyslexie"
    - "système d'exploitation (os)"
lien_wikipedia: "https://fr.wikipedia.org/wiki/PrimTux"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/primtux"
---

PrimTux est une distribution GNU/Linux complète et personnalisable destinée aux élèves de l'école primaire. Elle convient à tous types de PC, même anciens. Basée sur Debian stable et Ubuntu, sa consommation RAM est inférieure à 300 Mo au démarrage.
Les enseignants et parents disposeront avec elle d'une très riche logithèque pédagogique destinée aux enfants.
Grâce aux bureaux différents et paramétrables : Mini (maternelle), Super (CP-CE1), Maxi (CE2-CM1-CM2), Administrateur, chacun peut adapter son environnement de travail à ses capacités,  à ses besoins ou ses handicaps (dyslexie, dyspraxie, dyscalculie).

