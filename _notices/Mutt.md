---
nom: "Mutt"
date_creation: "mercredi, 13 décembre, 2023 - 19:17"
date_modification: "mercredi, 13 décembre, 2023 - 19:17"
logo:
    src: "images/logo/Mutt.gif"
site_web: "http://www.mutt.org/"
plateformes:
    - "Mac OS X"
    - "GNU/Linux"
    - "Autre"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Mutt est un client de messagerie en mode console hautement configurable"
createurices: "Michael Elkins"
alternative_a: "Outlook"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "client mail"
    - "console"
    - "terminal"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Mutt"
lien_exodus: ""
identifiant_wikidata: "Q1457398"
mis_en_avant: "non"


---

Mutt est dédié aux systèmes de type Unix. Il supporte différents formats et protocoles dédiés à la messagerie électronique. Son apprentissage peut être long mais le logiciel permet de nombreuses opérations, dont la création de macros, l'organisation des boîtes, l'arrangement de l'interface. Attention : on peut passer énormément de temps à configurer Mutt pour qu'il corresponde parfaitement à ses besoins tellement les possibilités sont étendues. Coupler Mutt avec un bon éditeur de texte vous permet une gestion rapide et efficace de votre messagerie.
