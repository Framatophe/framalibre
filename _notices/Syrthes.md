---
nom: "Syrthes"
date_creation: "Jeudi, 11 avril, 2019 - 13:18"
date_modification: "Jeudi, 11 avril, 2019 - 13:18"
logo:
    src: "images/logo/Syrthes.png"
site_web: "https://www.edf.fr/en/the-edf-group/world-s-largest-power-company/activities/res…"
plateformes:
    - "GNU/Linux"
langues:
    - "Français"
description_courte: "Logiciel de calcul thermique"
createurices: "Isabelle Rupp, Christophe Péniguel"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "science"
    - "ingénierie"
    - "calcul scientifique"
    - "solveur"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/syrthes"
---

Syrthes est un logiciel de calcul thermique d'EDF. C'est à ma connaissance le logiciel le plus complet dédié à la thermique, il gère très finement la conduction (les conductivités peuvent varié en fonction de tous les paramètres du calcul), le rayonnement (qui traverse le vide contrairement aux autres logiciels de calcul thermique où il faut mailler le vide).
Il peut être couplé avec les autres solveurs d'EDF, code_Saturne et code_Aster.

