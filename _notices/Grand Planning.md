---
nom: "Grand Planning"
date_creation: "Lundi, 13 mars, 2023 - 15:58"
date_modification: "Mardi, 19 septembre, 2023 - 09:51"
logo:
    src: "images/logo/Grand Planning.png"
site_web: "https://www.philnoug.com/planning"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "FirefoxOS"
    - "Windows Mobile"
    - "Apple iOS"
    - "Autre"
langues:
    - "Français"
description_courte: "Logiciel libre et coopératif de gestion de planification de salles et d'emplois du temps (EDT)"
createurices: "Philippe NOUGAILLON"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "éducation"
    - "planning"
    - "agenda"
    - "université"
    - "emploi du temps"
    - "école"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/grand-planning"
---

Logiciel libre et coopératif de gestion de planification de salles et d'emplois du temps (EDT),
à l’usage d’établissements de l’enseignement supérieur.
De la création des planning à l’édition des feuille d'émargement, en passant par les états de services, cette application assure la gestion complète et collaborative des processus de planification des cours, formation, intervenants, etc.
Collaborative, flexible et simple d'utilisation, cette application a été initialement développée pour l'IAE de Paris (Sorbonne Business School - Université Paris 1 Panthéon-Sorbonne) dans le but d'offrir une alternative à l'outil Hyperplanning, qui était complexe et surdimensionné pour un usage interne.
Planning accessible sur tous les supports, tout le temps. L’application est optimisée pour s’afficher sur toutes les tailles d’écrans. Un flux .ics permet à vos utilisateurs d’intégrer leur planning des cours dans leur outil d'agenda préféré.

