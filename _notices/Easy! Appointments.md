---
nom: "Easy! Appointments"
date_creation: "Mercredi, 28 juillet, 2021 - 10:31"
date_modification: "Mercredi, 28 juillet, 2021 - 10:48"
logo:
    src: "images/logo/Easy! Appointments.jpeg"
site_web: "https://easyappointments.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "le web"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Prise de rendez-vous, organisation d'évènements, inscriptions à des prestations individuelles ou collective..."
createurices: "Alex Tselegidis"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "planning"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/easy-appointments"
---

Logiciel simple et pratique (à installer sur un serveur web) pour gérer des inscriptions sur les créneaux horaires (salle de sport, prestations médicales, inscriptions à des rendez-vous...). Peut se synchroniser avec un Google Calendar.

