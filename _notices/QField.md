---
nom: "QField"
date_creation: "vendredi, 12 avril, 2024 - 15:54"
date_modification: "vendredi, 12 avril, 2024 - 15:54"
logo:
    src: "images/logo/QField.svg"
site_web: "https://qfield.org/"
plateformes:
    - "Apple iOS"
    - "Android"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "QField est une solution mobile pour la cartographie, basée sur QGIS"
createurices: "OPENGIS.ch et contributeurs QGIS"
alternative_a: "Mergin"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "QGIS"
    - "SIG"
    - "Cartographie"
    - "mobile"
    - "survey"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

QField est une solution mobile pour la cartographie, basée sur QGIS.

Préparez vos projets cartographiques dans QGIS Desktop, et exploitez les sur le terrain avec QField. Au delà des fonctionnalités classiques cartographiques ( géolocalisation, navigation, gestion des couches, formulaires de données attributaires, édition de données…), QField offre des fonctionnalités avancée : réutilisation des formulaires QGIS, édition hors-ligne, édition géométrique…

QField est également déployable avec un environnement QField.cloud pour la synchronisation des projets mobiles.
