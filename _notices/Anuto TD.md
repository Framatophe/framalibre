---
nom: "Anuto TD"
date_creation: "Mardi, 18 février, 2020 - 10:43"
date_modification: "Mercredi, 12 mai, 2021 - 16:52"
logo:
    src: "images/logo/Anuto TD.png"
site_web: "https://f-droid.org/en/packages/ch.logixisland.anuto/"
plateformes:
    - "Android"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Jeu mobile type Tower Defense aux graphismes époustouflants"
createurices: "reloZid"
alternative_a: "kingdom rush"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "jeu"
lien_wikipedia: ""
lien_exodus: "https://reports.exodus-privacy.eu.org/fr/reports/ch.logixisland.anuto/latest/"
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/anuto-td"
---

Anuto TD est un jeu mobile disponible sur la plate-forme F-Droid.
Du genre Tower Defense, le but est de résister aux vagues ennemis, face à des adversaires variés et de plus en plus forts. Pour cela, quatre types de défense sont proposés, que l'on peut améliorer grâce aux crédits gagnés en éliminant les ennemis.
Le jeu propose 8 cartes différentes pour plus de plaisir.

