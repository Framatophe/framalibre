---
nom: "Element (ex Riot)"
date_creation: "Dimanche, 3 décembre, 2017 - 14:51"
date_modification: "jeudi, 4 janvier, 2024 - 09:23"
logo:
    src: "images/logo/Element.io (ex Riot.im).png"
site_web: "https://element.io"
plateformes:
    - "le web"
    - "Apple iOS"
    - "Android"
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Element rassemble toutes vos conversations et intégrations applicatives en une seule application."
createurices: "Element"
alternative_a: "Slack, Discord, Whatsapp"
licences:
    - "Licence Apache (Apache)"
tags:
    - "internet"
    - "messagerie instantanée"
    - "chat"
    - "matrix"
    - "vie privée"
    - "chiffrement"
    - "visioconférence"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Element_(logiciel)"
lien_exodus: "https://reports.exodus-privacy.eu.org/fr/reports/im.vector.app/latest/"
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/elementio-ex-riotim"
---

Construit autour des salons de discussion de groupe, Element vous permet de partager des messages, des images, des vidéos et des fichiers – interagissez avec vos outils et accédez à toutes vos différentes communautés sous un même toit. Une identité et un lieu unique pour toutes vos équipes : nul besoin de changer de compte, de travailler et de dialoguer avec des personnes de différentes organisations dans des espaces publics ou privés : des projets professionnels aux voyages scolaires, Element deviendra le centre de toutes vos discussions !
Pour les organisations, Element utilise la technologie Matrix permettant de créer sa propre instance fédérable avec les autres instances, dont celle de matrix.org

Jusqu'en juillet 2020, Element s'appelait riot.im.



