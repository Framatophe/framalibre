---
nom: "Sound Converter"
date_creation: "Mardi, 16 mai, 2017 - 06:32"
date_modification: "Jeudi, 10 décembre, 2020 - 09:33"
logo:
    src: "images/logo/Sound Converter.png"
site_web: "http://soundconverter.org/"
plateformes:
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Conversion par lots de fichiers audio."
createurices: "Gautier Portet, Lars Wirzenius"
alternative_a: "Formatfactory"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "logiciel audio"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/sound-converter"
---

SoundConverter permet la conversion par lots de formats de fichiers audio.
Si, par exemple, vous avez sauvegardé vos CD des Fatals Picards en format sans dégradation (FLAC) et que vous souhaitez charger une clé USB de musiques en MP3 pour votre autoradio alors ouvrez SoundConverter, réglez «MP3» dans les préférences puis sélectionnez le répertoire contenant vos albums des Fatals picards avant de cliquer sur «convertir».
Pour deux albums cela vient de prendre moins de 30 secondes à SoundConverter pour me créer un répertoire de MP3 à partir de FLAC (voir capture).
SoundConverter peut s'utiliser également en ligne de commande ce qui permet de l'intégrer à ses propre scripts.

