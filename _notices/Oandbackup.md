---
nom: "Oandbackup"
date_creation: "Samedi, 25 mars, 2017 - 16:31"
date_modification: "Samedi, 25 mars, 2017 - 16:31"
logo:
    src: "images/logo/Oandbackup.png"
site_web: "https://github.com/jensstein/oandbackup"
plateformes:
    - "Android"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Outil de sauvegarde/restauration d'applications pour Android."
createurices: "jens stein"
alternative_a: "Super Backup & Restore"
licences:
    - "Licence MIT/X11"
tags:
    - "système"
    - "stockage"
    - "copie"
    - "sauvegarde"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/oandbackup"
---

Oandbackup vous permet de sauvegarder et restaurer vos applications. Il est possible de restaurer les données et/ou l'application elle-même (fichier apk).
Oandbackup offre la possibilité de sauvegarder et restaurer par lot et propose aussi une fonction de planification.
L'application nécessite les droits root ainsi que BusyBox pour fonctionner.

