---
nom: "Claroline Connect"
date_creation: "Mardi, 27 décembre, 2016 - 10:26"
date_modification: "Lundi, 10 mai, 2021 - 14:40"
logo:
    src: "images/logo/Claroline Connect.png"
site_web: "http://www.claroline.net"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "FirefoxOS"
    - "Windows Mobile"
    - "Autre"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Claroline Connect est un logiciel de gestion des apprentissages car \"apprendre est un acte de création\"."
createurices: ""
alternative_a: "360Learning, e-doceo, dokeos"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "éducation"
    - "espace numérique de travail (ent)"
    - "lms"
    - "didactique"
    - "pédagogie"
    - "forum de discussion"
    - "wiki"
    - "évaluation"
    - "exerciseur"
    - "blog"
    - "cms"
    - "partage de fichiers"
    - "devoirs"
    - "scorm"
    - "e-learning"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Claroline"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/claroline-connect"
---

Claroline Connect est un Learning Management System, c'est-à-dire un logiciel de gestion des apprentissages, libre et téléchargeable gratuitement. Il veut être un facilitateur d'accès au savoir en exploitant la puissance de la technologie, mettant comme seul limite votre créativité et en développant un outil collaboratif multi-niveaux.
Au contraire de la plupart des plateformes existantes aujourd'hui, Claroline Connect (re)met chaque personne au centre de sa formation en lui offrant la possibilité de créer, partager, choisir et organiser les éléments qui composent son apprentissage. La plateforme a aussi été pensée dans une vision collaborative, permettant à chacun d'interagir avec les autres utilisateurs dans le cadre d'un projet commun. Pourquoi avoir fait ces choix-là ? Parce qu'aujourd'hui plus que jamais, les plateformes doivent permettre de faire plus que donner un simple accès à du contenu que les apprenants peuvent déjà facilement trouver ailleurs.

