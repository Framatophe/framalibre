---
nom: "Authentic2"
date_creation: "mardi, 16 avril, 2024 - 00:28"
date_modification: "mardi, 16 avril, 2024 - 00:28"


site_web: "https://dev.entrouvert.org/projects/authentic"
plateformes:
    - "le web"
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
description_courte: "Plate-forme complète, multiprotocoles et libre, de gestion d'identité"
createurices: "Entr'ouvert"
alternative_a: ""
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "SSO"
    - "IDP"
    - "SAML"
    - "CAS"
    - "OpenID Connect"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

Authentic 2 est une plate-forme complète, multiprotocoles et libre, de gestion d'identité. Elle assure une gestion souple et homogénéisée des identités numériques au sein de votre organisme.

Fonctionnalités :

* Support du protocole SAML 2.0
* Support du protocole OpenIDConnect
* Support du raccordement à France Connect
* Support du protocole CAS (1.0 et 2.0 et mode proxy)
* Support des certificats SSL/TLS
* Support Kerberos et Active Directory
* Permet de servir de passerelle d'un protocole à un autre, par exemple entre OpenIDConnect et SAML 2.0
* Support des annuaires LDAP v2 et v3 (y compris Active Directory)
* Fonctionne nativement avec le système de gestion de bases de données PostgreSQL

Authentic 2 est développé en Python et Django.
