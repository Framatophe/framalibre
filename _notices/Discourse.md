---
nom: "Discourse"
date_creation: "Mardi, 23 octobre, 2018 - 14:25"
date_modification: "Lundi, 10 mai, 2021 - 14:40"
logo:
    src: "images/logo/Discourse.png"
site_web: "https://www.discourse.org/"
plateformes:
    - "GNU/Linux"
    - "le web"
langues:
    - "Autres langues"
description_courte: "Discourse est un logiciel de forum de discussion par internet."
createurices: ""
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "forum de discussion"
    - "discussion"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Discourse_(logiciel)"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/discourse"
---

Il dispose des fonctionnalités usuelles d’un forum de discussion : utilisateurs, discussions, recherche, messages privés, etc.
Le mode « liste de diffusion » permet d’utiliser la plupart des fonctionnalités du forum via des courriers électroniques.
Écrit en ruby et en JavaScript, il nécessite une base de données PostgreSQL et un serveur d’envoi de courrier électronique.

