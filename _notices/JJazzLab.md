---
nom: "JJazzLab"
date_creation: "mardi, 16 avril, 2024 - 16:01"
date_modification: "mardi, 16 avril, 2024 - 16:16"
logo:
    src: "images/logo/JJazzLab.png"
site_web: "https://www.jjazzlab.org"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Édition d'accompagnement musicaux"
createurices: "Jérôme Lelasseux"
alternative_a: "Band In Box"
licences:
    - "Licence publique générale limitée GNU (LGPL)"
tags:
    - "musique"
    - "création musicale"
    - "accords"
    - "partition"
    - "tablature"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

JJazzLab sert à (re)créer des accompagnements musicaux.
Ils utilisent des des "motifs" (patterns) de dizaines de styles musicaux différents avec des variantes (intro, couplet, break, outro…) ayant aussi des variantes et pouvant être réédité aussi bien sur le choix des notes que des instruments et leurs mixage.
Des connaissances musicales facilitent une utilisation experte, mais pour les novices, la prise en main technique est de moins d'une heure pour la base qui vous permettra de retranscrire n'importe quelle grille d'accords que vous trouverez ou inventerez.
Il est possible de créer vos propres motifs, d'en importer d'autres (basés sur le formats de style Yamaha .sty, .prs, .bcs, .sst, au format SFF1 ou SFF2). La page de ressources propose des dizaines de styles et un "roadbook" de plusieurs centaines de standards.
C'est un must pour répéter dans votre coin avec un orchestre ou un groupe jouant inlassablement des morceaux entiers ou seulement certains passages en boucle.

S'appuyant sur la bibliothèque de sons "fluidsynth", il donnera une couleur assez réaliste et nuancée à vos accompagnements.
