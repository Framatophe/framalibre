---
nom: "Soosyze"
date_creation: "Jeudi, 25 avril, 2019 - 11:38"
date_modification: "Lundi, 22 mars, 2021 - 18:38"
logo:
    src: "images/logo/Soosyze.jpg"
site_web: "https://soosyze.com/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "FirefoxOS"
    - "Windows Mobile"
    - "Apple iOS"
langues:
    - "Français"
    - "English"
description_courte: "CMS open source, sans base de données pour créer votre site facilement"
createurices: "Mathieu NOËL"
alternative_a: "wix, Blogspot, blogger, LightCMS"
licences:
    - "Licence MIT/X11"
tags:
    - "cms"
    - "php"
    - "flat-file"
    - "création de site web"
    - "responsive"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/soosyze"
---

Facile d'installation
Seulement deux étapes de configuration vous séparent de la mise en ligne de votre site.
Petit et complet
Couvre l’essentiel des besoins pour la gestion d'un site web (contenu, utilisateur, configuration, référencement...).
Responsive design
Affichage s'adaptant à la taille des écrans (PC, tablette, smartphone...).
Aucune base de données
Aucune base de données n'est nécessaire. Vos données sont gérées grâce à la bibliothèque Queryflatfile.
Socle de développement
Basé sur un micro framework PHP objet, le développement est normé pour maintenir et faire évoluer l'outil de façon structurée.
Extensible
Facilité de développement et d'installation de nouveaux modules afin d'enrichir les fonctionnalités de votre site.
Open source
Codes sources du framework , du CMS et de Queryflatfile disponibles sur Github.
Documentation fournie
Soosyze ainsi que les bibliothèques sont fournies avec une documentation complète.

