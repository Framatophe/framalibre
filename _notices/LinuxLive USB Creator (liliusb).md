---
nom: "LinuxLive USB Creator (liliusb)"
date_creation: "Dimanche, 15 janvier, 2017 - 11:18"
date_modification: "Mardi, 9 juillet, 2019 - 11:40"
logo:
    src: "images/logo/LinuxLive USB Creator (liliusb).png"
site_web: "http://www.linuxliveusb.com/"
plateformes:
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Ce logiciel permet de créer des liveusb de distributions simplement."
createurices: "Thibaut Lauzière"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
    - "livecd"
    - "installation"
    - "iso"
    - "utilitaire"
    - "gravure"
    - "usb"
    - "live usb"
lien_wikipedia: "https://fr.wikipedia.org/wiki/LinuxLive_USB_Creator"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/linuxlive-usb-creator-liliusb"
---

LinuxLive USB Creator permet de créer des liveUSB des distributions GNU/Linux les plus courantes de façon très simple en prenant en charge l'intégralité de la procédure : téléchargement des image ISO sur le site officiel, formatage de la clé USB, inscription de l'image et préparation de la clé pour qu'elle soit bootable.

