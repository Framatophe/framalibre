---
nom: "KeeWeb"
date_creation: "Mardi, 18 avril, 2017 - 22:40"
date_modification: "Lundi, 10 mai, 2021 - 13:39"
logo:
    src: "images/logo/KeeWeb.png"
site_web: "https://keeweb.info/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
    - "Android"
langues:
    - "Autres langues"
description_courte: "Gestionnaire de mots de passe pour ordinateur et outil en ligne."
createurices: "antelle"
alternative_a: "lastpass, 1password, enpass, dashlane"
licences:
    - "Licence MIT/X11"
tags:
    - "sécurité"
    - "gestionnaire de mots de passe"
    - "chiffrement"
    - "mot de passe"
    - "authentification"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/keeweb"
---

Un gestionnaire élégant et efficace pour gérer vos mots de passe. Disponible sous Windows, Mac et GNU/Linux, ainsi que via la page web https://app.keeweb.info/.
Il est possible de stocker le client sur Dropbox ou sur votre site web. Il existe également une application pour Nextcloud.
Le fichier contenant les mots de passe (.kdbx) peut vous suivre sur une clef USB et est compatible avec les autres gestionnaires de mots de passe comme KeePass ou KeePassX.

