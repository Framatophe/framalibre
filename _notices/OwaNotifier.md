---
nom: "OwaNotifier"
date_creation: "Dimanche, 10 septembre, 2017 - 09:40"
date_modification: "Dimanche, 10 septembre, 2017 - 15:03"
logo:
    src: "images/logo/OwaNotifier.png"
site_web: "https://owanotifier.github.io/"
plateformes:
    - "GNU/Linux"
    - "Windows"
langues:
    - "Français"
description_courte: "Un daemon qui affiche des notifications lorsque vous recevez des messages sur outlook 365"
createurices: "Mathieu GOULIN"
alternative_a: "Microsoft Outlook"
licences:
    - "Licence MIT/X11"
tags:
    - "bureautique"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/owanotifier"
---

Cette application vous permet de recevoir des notifications lors de l'arrivé de nouveaux emails sur Outlook 365. Ainsi plus besoin d'utiliser l'application Outlook pour être instantanément averti que vous avez de nouveaux messages.
De plus cette application fonctionne sur Windows et Linux.

