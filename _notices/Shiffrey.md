---
nom: "Shiffrey"
date_creation: "jeudi, 28 décembre, 2023 - 17:51"
date_modification: "mardi, 2 janvier, 2024 - 09:30"
logo:
    src: "images/logo/Shiffrey.svg"
site_web: "https://sultanrancho.github.io/Shiffrey/"
plateformes:
    - "le web"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
description_courte: "Logiciel de drive chiffré, auto-hébergé et léger écrit en PHP, JS et SQL."
createurices: ""
alternative_a: "Google drive"
licences:
    - "Licence MIT/X11 (MIT)"
tags:
    - "auto-hébergement"
    - "drive"
    - "fichier"
    - "chiffrement"
    - "php"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

**Shiffrey** est un logiciel de drive incluant des fonctionnalités de chiffrement. 
Écrit en PHP, il peut être déployé facilement sur des offres d'hébergements à moindre coups sans nécessiter de location de VPS. L'interface coté client embarque par défaut une fonctionnalité de chiffrement des fichiers avec **AES**. Il est également possible de traduire l'interface dans d'autres langues que celles proposées par défaut à l'aide de ficher JSON.

