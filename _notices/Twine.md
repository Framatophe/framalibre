---
nom: "Twine"
date_creation: "Jeudi, 29 décembre, 2016 - 23:44"
date_modification: "Mercredi, 12 mai, 2021 - 17:00"
logo:
    src: "images/logo/Twine.png"
site_web: "https://twinery.org/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Créez des histoires dont vous êtes le héros"
createurices: "Chris Klimas"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "création"
    - "histoire"
    - "écriture"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Twine"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/twine"
---

Twine est un petit logiciel tout simple qui permet de créer des histoires interactives du type « dont vous êtes le héros ». En quelques clics, votre histoire est créée et vous pouvez l’exporter tout aussi facilement en format HTML. Que ce soit pour l’utilisation en salle de classe pour la conception de récits, pour le développement de scénarios eLearning, ou encore pour exprimer votre créativité, ce logiciel rapide de prise en main vous permettra de créer efficacement une histoire.
Depuis la version 2, il est possible d'utiliser le logiciel directement en ligne depuis le site du logiciel.

