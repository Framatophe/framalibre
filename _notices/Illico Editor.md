---
nom: "Illico Editor"
date_creation: "Vendredi, 1 avril, 2016 - 19:12"
date_modification: "Mercredi, 31 mai, 2023 - 22:52"
logo:
    src: "images/logo/Illico Editor.png"
site_web: "http://illico.tuxfamily.org"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Autre"
langues:
    - "Français"
description_courte: "Couteau-suisse pour qualifier les données sans effort"
createurices: "Arnaud Coche"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "métiers"
    - "donnée"
    - "nettoyage"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/illico-editor"
---

Couteau-suisse de la qualification de données
dédié

aux acteurs métiers : services administratifs, etc.
aux acteurs techniques : développeurs, administrateurs de BD, etc.

lorsqu’il s’agit d’explorer des données, de les vérifier, de transformer leur format pour mieux les exploiter, analyser, valoriser et de les corriger en masse.
Possède plus de 140 fonctionnalités classiques ou complexes dédiées :

analyse de données (répartition des valeurs, valeurs non-saisies)
croisement de fichiers (comparaison, enrichissement, exclusion)
correction en masse (pivot, syntaxe, condition, etc.)
recherche et filtre (n-ième occurrence, doublon, isoler/exclure une cohorte, échantillonner)
restitution (enchaînements de tableaux croisé-dynamiques, tableaux de synthèse)


