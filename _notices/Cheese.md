---
nom: "Cheese"
date_creation: "Mercredi, 17 octobre, 2018 - 21:36"
date_modification: "Mercredi, 17 octobre, 2018 - 21:36"
logo:
    src: "images/logo/Cheese.png"
site_web: "https://wiki.gnome.org/Apps/Cheese"
plateformes:
    - "GNU/Linux"
langues:
    - "English"
description_courte: "Logiciel webcam avec effets"
createurices: ""
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Cheese_(logiciel)"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/cheese"
---

Logiciel permettant de faire des photos / vidéos avec différents effets : Mauve, Noir/Blanc, Saturation, Hulk, Miroir vertical, Miroir horizontal, Shagadélique !, Vertigo, Contour, En petits dés, Déformé.

