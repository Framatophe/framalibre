---
nom: "BleachBit"
date_creation: "Mercredi, 11 janvier, 2017 - 14:52"
date_modification: "Mercredi, 12 mai, 2021 - 15:49"
logo:
    src: "images/logo/BleachBit.png"
site_web: "https://www.bleachbit.org/"
plateformes:
    - "GNU/Linux"
    - "Windows"
langues:
    - "Français"
    - "Autres langues"
description_courte: "BleachBit est un logiciel libre permettant de nettoyer son système et ainsi protéger sa vie privée"
createurices: "Andrew Ziem"
alternative_a: "CCleaner"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
    - "nettoyage"
    - "vie privée"
    - "fichier"
    - "sécurité"
lien_wikipedia: "https://fr.wikipedia.org/wiki/BleachBit"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/bleachbit"
---

BleachBit permet de libérer de l'espace disque et de protéger sa vie privée en effaçant les fichiers inutiles du système.
il est même possible de supprimer "définitivement" les fichiers désirés afin d'empêcher toute tentative de récupération par un tiers.

