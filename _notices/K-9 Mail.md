---
nom: "K-9 Mail"
date_creation: "Mercredi, 4 janvier, 2017 - 11:51"
date_modification: "Mercredi, 12 mai, 2021 - 16:14"
logo:
    src: "images/logo/K-9 Mail.png"
site_web: "https://k9mail.app/"
plateformes:
    - "Android"
langues:
    - "Autres langues"
description_courte: "Client de courriel pour Android"
createurices: "Jesse Vincent"
alternative_a: "Gmail, Email"
licences:
    - "Licence Apache (Apache)"
tags:
    - "internet"
    - "client mail"
lien_wikipedia: "https://fr.wikipedia.org/wiki/K-9_Mail"
lien_exodus: "https://reports.exodus-privacy.eu.org/fr/reports/com.fsck.k9/latest/"
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/k-9-mail"
---

K-9 Mail est une application de courrier électronique libre pour Android.
Elle gère les protocoles POP3 et IMAP.
Elle permet aussi d'utiliser le chiffrement avec APG ou OpenKeychain.
On peut gérer plusieurs compte mail et avoir une boîte de réception unifiée.

