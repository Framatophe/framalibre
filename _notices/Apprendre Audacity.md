---
nom: "Apprendre Audacity"
date_creation: "Dimanche, 23 avril, 2017 - 09:46"
date_modification: "Dimanche, 23 avril, 2017 - 09:46"
logo:
    src: "images/logo/Apprendre Audacity.png"
site_web: "https://archive.org/details/Audacity_GPL_Diaporama"
plateformes:

langues:
    - "Français"
description_courte: "Former à Audacity, un logiciel libre de montage audio."
createurices: "Matthieu Giroux"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "audio"
    - "diaporama"
    - "libre"
    - "gpl"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/apprendre-audacity"
---

Audacity permet de monter des sons pour créer une bande sonore étoffée ou réparée. Nous vous proposons de créer des ateliers sur Audacity, grâce à ce diaporama, ainsi qu'avec les livres sur la créativité. On rappelle l'histoire du montage audio, qui coûtait cher avec beaucoup de pertes, alors qu'on peut maintenant enregistrer avec un micro XLR et sa carte son.

