---
nom: "Baserow"
date_creation: "Mercredi, 25 mai, 2022 - 09:46"
date_modification: "jeudi, 11 janvier, 2024 - 01:32"
logo:
    src: "images/logo/Baserow.png"
site_web: "https://baserow.io/"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
    - "Autre"
langues:
    - "Autres langues"
description_courte: "Une base de donnée no-code Open source alternative à Airtable"
createurices: "Bram Wiepjes"
alternative_a: "Airtable"
licences:
    - "Licence MIT/X11 (MIT)"
tags:
    - "internet"
    - "nocode"
    - "base de données"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/baserow"
---

Baserow est une base de données no-code open-source. Cette plateforme offre la possibilité à des équipes non techniques ou techniques de rassembler des données, de les organiser et de créer des liens logiques entre elles afin de faciliter la prise de décision ou d'automatiser des processus. Baserow est un outil qui se veut polyvalent qui peut être adapté à des projets de toutes tailles dans tous les domaines.


