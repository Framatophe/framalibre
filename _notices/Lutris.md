---
nom: "Lutris"
date_creation: "Jeudi, 29 décembre, 2016 - 19:51"
date_modification: "Mercredi, 12 mai, 2021 - 16:55"
logo:
    src: "images/logo/Lutris.png"
site_web: "https://lutris.net/"
plateformes:
    - "GNU/Linux"
langues:
    - "Autres langues"
description_courte: "La plateforme de jeu qui réunit toutes les possibilités pour jouer sous Linux."
createurices: ""
alternative_a: "Steam, Origin, Uplay"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "jeu"
    - "jeu vidéo"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/lutris"
---

Lutris est une plateforme de jeu vidéo libre qui contient une partie web, pour la recherche des jeux, et une partie client natif pour lancer les jeux. Lutris regroupe toutes les possibilités de jeu sur Linux :
Les jeux natifs
Les jeux Steam
Les jeux en émulation (consoles ou via Wine)
Les jeux web et bien plus.
À première vue, Lutris ne parait pas simple d'utilisation, mais après avoir compris, cela devient un jeu d'enfant, et tout joueur sous Linux ne pourra plus s'en passer.

