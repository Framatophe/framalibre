---
nom: "WinSCP"
date_creation: "lundi, 18 mars, 2024 - 12:19"
date_modification: "lundi, 18 mars, 2024 - 12:19"
logo:
    src: "images/logo/WinSCP.png"
site_web: "https://winscp.net/"
plateformes:
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "WinSCP est un client SFTP graphique pour Windows."
createurices: "Martin Prikryl"
alternative_a: "Cerberus, Transmettre 5, WS_FTP, CyberDuck, Commandant un, tectia"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "client ftp"
    - "ftp"
    - "transfert de fichiers"
    - "sftp"
    - "ssh"
lien_wikipedia: "https://fr.wikipedia.org/wiki/WinSCP"
lien_exodus: ""
identifiant_wikidata: "Q1160381"
mis_en_avant: "non"

---

WinSCP est un client SFTP graphique pour Windows. Il utilise SSH et est open source. Le protocole SCP est également supporté. Le but de ce programme est de permettre la copie sécurisée de fichiers entre un ordinateur local et un ordinateur distant. 

Sachant que la version MS Windows de [FileZilla](https://framalibre.org/notices/filezilla.html) est problématique (l'installateur MS Windows met en place des spywares), il est conseillé pour cet OS d'utiliser plutôt WinSCP.
