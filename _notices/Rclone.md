---
nom: "Rclone"
date_creation: "Mercredi, 26 août, 2020 - 02:54"
date_modification: "Mercredi, 26 août, 2020 - 02:54"
logo:
    src: "images/logo/Rclone.png"
site_web: "https://rclone.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "le web"
langues:
    - "English"
description_courte: "Rclone est un logiciel en ligne de commande qui permet de synchroniser ses fichiers sur les clouds."
createurices: ""
alternative_a: ""
licences:
    - "Licence MIT/X11"
tags:
    - "internet"
    - "synchronisation"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/rclone"
---

Rclone est un logiciel en ligne de commande qui permet de gerer ses fichiers sur les clouds. Il permet par exemple de synchroniser les fichiers de son disque dur sur un cloud, ou de synchroniser les fichiers de deux clouds différents. Rclone dispose de nombreuses commandes et certaines fonctionnalités avancées comme le chiffrement ou le partage par ftp sont disponibles.

