---
nom: "Quetoo"
date_creation: "Dimanche, 12 mars, 2017 - 13:13"
date_modification: "Vendredi, 7 mai, 2021 - 11:05"
logo:
    src: "images/logo/Quetoo.png"
site_web: "http://quetoo.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
description_courte: "Quetoo est un FPS inpiré de Quake 2 à l'ambiance sombre et au gameplay rapide."
createurices: "jdolan, Panjoo"
alternative_a: "Quake"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "jeu"
    - "fps"
    - "tir"
    - "action"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/quetoo"
---

Quetoo est le successeur de Quake2World, un FPS basé sur Quake 2 qui lui ajoutait un mode multijoueur complet. Le jeu est un FPS dans la lignée des Quake c'est à dire simple à prendre en main et très rapide. On retrouve les mêmes armes et les environnements fermés des jeux du genre.

