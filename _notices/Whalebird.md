---
nom: "Whalebird"
date_creation: "Samedi, 6 juin, 2020 - 16:39"
date_modification: "Samedi, 6 juin, 2020 - 16:39"
logo:
    src: "images/logo/Whalebird.png"
site_web: "https://whalebird.social/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Whalebird est un logiciel client bureau (GNU\\Linux, MacOS"
createurices: "h3poteto"
alternative_a: ""
licences:
    - "Licence MIT/X11"
tags:
    - "internet"
    - "mastodon"
    - "réseau social"
    - "décentralisation"
    - "microblog"
    - "fediverse"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/whalebird"
---

Whalebird est un logiciel client bureau (GNU\Linux, MacOS, Windows) pour les réseaux sociaux libres pleroma et mastodon. Il offre toutes les fonctionnalités attendues pour un logiciel de ce type (favoris, mentions, accès aux fils publics, repouet, notifications sur le bureau...)
À noter qu'il dispose de plusieurs thèmes, permettant de passer en affichage sombre pour préserver ses yeux ainsi que de paramètres d'affichages (taille des caractères, agrandissements des messages) pour l'accessibilité.

