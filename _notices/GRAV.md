---
nom: "GRAV"
date_creation: "Vendredi, 14 février, 2020 - 21:05"
date_modification: "Mardi, 11 mai, 2021 - 23:18"
logo:
    src: "images/logo/GRAV.png"
site_web: "https://getgrav.org/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
description_courte: "Grav est un CMS libre, il a été conçu pour être simple à installer et à maitriser."
createurices: "Andy Miller, Djamil Legato, Matias Griese"
alternative_a: "wix"
licences:
    - "Licence MIT/X11"
tags:
    - "cms"
    - "blog"
    - "développement web"
    - "hébergement"
lien_wikipedia: "https://en.wikipedia.org/wiki/Grav_(CMS)"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/grav"
---

Grav est un logiciel libre, avec un système d'auto-gestion (CMS).
Un logiciel CMS permet héberger son site web et de faire du développement web via une interface graphique et un minimum de code. Grav a été pensé pour avoir une courbe d'apprentissage facile, la prise en main et l'installation sont rapide.
Grav a été élu "Meilleur CMS open source" en 2016 et "Meilleur CMS de fichiers plats" en 2017.

