---
nom: "ext4magic"
date_creation: "Jeudi, 23 novembre, 2017 - 14:04"
date_modification: "Lundi, 10 mai, 2021 - 14:29"
logo:
    src: "images/logo/ext4magic.png"
site_web: "https://sourceforge.net/projects/ext4magic/"
plateformes:
    - "GNU/Linux"
langues:
    - "English"
description_courte: "Outil d'administration GNU/Linux qui permet de récupérer des fichiers et des dossiers supprimés (ext3/4)."
createurices: "Roberto Maar (robi)"
alternative_a: "Recuva"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
    - "dépannage"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/ext4magic"
---

Ext4magic est un outil d'administration GNU/Linux qui permet de  retrouver les informations nécessaires à la restauration de fichiers, de dossiers et de leur arborescence.
Cet outil  récupère la plupart des types de fichier supprimés ou écrasés  avec leur nom,  le propriétaire, le groupe, dans un système de fichiers ext3 ou ext4.
Interface en ligne de commande : voir le 'man' donné en lien dans cette notice (en).

