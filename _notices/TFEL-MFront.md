---
nom: "TFEL/MFront"
date_creation: "Mardi, 12 juin, 2018 - 20:58"
date_modification: "Mardi, 12 juin, 2018 - 20:58"
logo:
    src: "images/logo/TFEL-MFront.png"
site_web: "http://tfel.sourceforge.net"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Autre"
langues:
    - "Français"
    - "English"
description_courte: "TFEL/MFront:  un générateur de code dédié aux connaissance matériaux pour l'ingénieur en calcul des structures"
createurices: "Thomas Helfer"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "science"
    - "mécanique"
    - "éléments finis"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/tfelmfront"
---

Le plateforme PLEIADES vise à fournir une plate-forme unifiée de développement des applications de simulation des éléments combustible nucléaires du CEA et d’EDF. Au sein de ce projet, le projet TFEL permet de traiter les aspects liés à l’écriture et la capitalisation des connaissances matériau dans un cadre d’assurance qualité strict.
TFEL fournit de différents librairies C++ et exécutables dont MFront et MTest.
MFront est un générateur de code simplifiant l'écriture de propriétés matériaux, lois de comportement mécanique et modèles physico-chimiques simples. combustible, notamment pour les utilisateurs des codes Cast3M, Code_Aster, AMITEX_FTT, CalculiX, ZeBuLoN, Europlexus, Abaqus/Standard, Abaqus/Explicit et ANSYS. MFront a été mis en open-source dans l’espoir d’être utile aux ingénieurs et chercheurs de la communauté des matériaux et/ou du calcul de structure.
MTest est un outil de test sur point matériel permettant de simuler des essais mécaniques simples.

