---
nom: "FitoTrack"
date_creation: "jeudi, 8 février, 2024 - 15:41"
date_modification: "jeudi, 8 février, 2024 - 15:41"
logo:
    src: "images/logo/FitoTrack.png"
site_web: "https://codeberg.org/jannis/FitoTrack"
plateformes:
    - "Android"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "FitoTrack est une application mobile qui permet d'enregistrer et de visualiser vos séances d'entraînement. Que vous couriez, fassiez du vélo ou de la randonnée, FitoTrack vous montrera les informations les plus importantes, avec des graphiques et des statistiques détaillés."
createurices: "Jannis Scheibe"
alternative_a: "Strava"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "santé"
    - "tracker"
    - "sport"
    - "vélo"
    - "randonnée"
lien_wikipedia: ""
lien_exodus: "https://reports.exodus-privacy.eu.org/fr/reports/de.tadris.fitness/latest/"
identifiant_wikidata: ""
mis_en_avant: "non"

---

FitoTrack est une application permettant de suivre ses déplacements à pieds, en vélo, patins à roulettes ou compter votre nombres de pompes ou de… trampoline.
Vous pouvez suivre votre déplacement sur la carte en direct et voir le trajet complet à la fin, avec la durée totale, la distance, les heures de début et de fin, le rythme, la vitesse, le dénivelé, les calories, etc…
