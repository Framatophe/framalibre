---
nom: "NetSurf"
date_creation: "Lundi, 13 mars, 2017 - 12:31"
date_modification: "Mercredi, 12 mai, 2021 - 14:52"
logo:
    src: "images/logo/NetSurf.png"
site_web: "https://www.netsurf-browser.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Autre"
langues:
    - "Autres langues"
description_courte: "Netsurf est un navigateur web très léger qui dispose de son propre moteur d'affichage de page web."
createurices: ""
alternative_a: "Google Chrome"
licences:
    - "Licence publique générale limitée GNU (LGPL)"
tags:
    - "internet"
    - "navigateur web"
lien_wikipedia: "https://fr.wikipedia.org/wiki/NetSurf"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/netsurf"
---

Codé en C pour Risc OS, Netsurf est un navigateur web disponible sous différentes plateforme telles que Windows, Mac OSX, Linux, BSD et bien d'autres. Léger et rapide, il n'embarque que le strict nécessaire. Le projet Netsurf a pour objectif: la modularité, la portabilité, le respect des standards, proposer une interface claire et simple, et s'amuser en développant.

