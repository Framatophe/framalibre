---
nom: "Transportr"
date_creation: "mercredi, 1 novembre, 2023 - 13:19"
date_modification: "mercredi, 1 novembre, 2023 - 14:04"
logo:
    src: "images/logo/Transportr.png"
site_web: "https://transportr.app/"
plateformes:
    - "Android"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Transportr est une appli conçue pour faciliter autant que possible l’utilisation des transports en commun, pour tous."
createurices: "Torsten Grote"
alternative_a: "Google Maps"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "transport"
    - "horaire"
    - "carte"
lien_wikipedia: ""
lien_exodus: "https://reports.exodus-privacy.eu.org/fr/reports/de.grobox.liberario/latest/"
identifiant_wikidata: ""
mis_en_avant: "oui"
---

Transportr est une appli sans but lucratif développée par des personnes du monde entier pendant leur temps libre. Elles souhaitent faciliter autant que possible l’utilisation des transports en commun, pour tous.

Cette appli utilise les données de divers organismes locaux de transport en commun tout en fournissant une interface unifiée pour ces données.
Elle ne peut pas vérifier la justesse de ces données et par conséquent n’offre aucune garantie quant aux renseignements affichés.
