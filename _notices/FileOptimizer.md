---
nom: "FileOptimizer"
date_creation: "Samedi, 19 septembre, 2020 - 14:45"
date_modification: "Samedi, 19 septembre, 2020 - 14:48"
logo:
    src: "images/logo/FileOptimizer.png"
site_web: "https://nikkhokkho.sourceforge.io/static.php?page=FileOptimizer"
plateformes:
    - "Windows"
langues:
    - "Français"
    - "Autres langues"
description_courte: "FileOptimizer permet de réduire le poids des fichiers tout en conservant leurs qualités."
createurices: "Javier Gutiérrez Chamorro (Guti)"
alternative_a: ""
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "multimédia"
    - "compression de fichiers"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/fileoptimizer"
---

FileOptimizer intègre de nombreux plugins (plus de 90) lui permettant de prendre en charge une grande variété de formats de fichiers (plus de 400) : audio (MP3, OGG,...), images (GIF, JPEG, PNG, TIF,...), fichiers compressés (ZIP, 7Z,...), documents (PDF, DOCX, ODT, ...) et bien d’autres formats.

