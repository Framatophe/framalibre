---
nom: "Fpdfe"
date_creation: "Mardi, 13 avril, 2021 - 11:21"
date_modification: "Mardi, 13 avril, 2021 - 11:21"
logo:
    src: "images/logo/Fpdfe.png"
site_web: "https://github.com/DegrangeM/Fpdfe/"
plateformes:
    - "GNU/Linux"
    - "Windows"
langues:
    - "Français"
description_courte: "Fpdfe est un outil permettant d'extraire le contenu des champs de formulaires de fichiers pdf."
createurices: "Mathieu Degrange"
alternative_a: ""
licences:
    - "Licence MIT/X11"
tags:
    - "bureautique"
    - "pdf"
    - "formulaire"
    - "éducation"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/fpdfe"
---

Fpdfe (Form Pdf Exporter) est une application web permettant d'extraire le contenu des champs de formulaires de documents pdf et d'en générer un fichier tableur (csv).
L'application peut être utilisée via sa version en ligne ou bien être téléchargée pour une utilisation hors-ligne.

