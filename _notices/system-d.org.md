---
nom: "system-d.org"
date_creation: "Lundi, 23 décembre, 2019 - 10:43"
date_modification: "Lundi, 10 mai, 2021 - 14:56"
logo:
    src: "images/logo/system-d.org.png"
site_web: "https://www.system-d.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "FirefoxOS"
    - "Windows Mobile"
    - "Apple iOS"
    - "Autre"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Plateforme libre et chiffrée end to end de projets collaboratifs non-marchands"
createurices: "robin banquo"
alternative_a: "microsoft team, Google calendar, trello, groupes facebook, wrike"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "internet"
    - "travail collaboratif"
    - "projet"
    - "gestion"
    - "agenda"
    - "todo-list"
    - "forum de discussion"
    - "tâche"
    - "chiffrement"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/system-dorg"
---

Outil de gestion de projet libre, pratique et chiffré pour organiser vos collectifs, associations, groupes militants.
créez votre projet, invitez les autres membres, et disposez en un instant:
-d'un forum de discussion
-d'une carte interactive
-d'une todo liste
-d'un agenda partagé
-de tableurs en temps réels
Pour l'instant en béta avec déjà un outil pratique de gestion de projet, l'objectif est de faire à moyen terme de system-d.org un réseau de projet collaboratifs avec les deux prochaines phases de dev :
Création des interfaces faisant de la plateforme un espace de rencontre et de circulation des utilisateurs sur différents projets :
-Recherche géolocalisée par compétences-centres d’intérêts,
-messagerie chiffrée,
-blogs partagés, etc.
Déveloper un réseau de gratuité et de partage :
-Permettre à chaque utilisateur et chaque projets de mettre en partage les ressources qu'ils produisent afin de pouvoir réaliser une circulation non-marchande de grande ampleur

