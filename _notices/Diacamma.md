---
nom: "Diacamma"
date_creation: "Lundi, 21 mai, 2018 - 08:27"
date_modification: "Lundi, 25 juin, 2018 - 09:57"
logo:
    src: "images/logo/Diacamma.png"
site_web: "http://www.diacamma.org/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
description_courte: "Ensemble de logiciels libres de gestion administrative et financière pour les syndics ou associations."
createurices: ""
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "métiers"
    - "syndic"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/diacamma"
---

Ensemble de logiciels libres de gestion administrative et financière pour les syndics ou associations.
Diacamma Asso : Gérer son association (adhésions, cotisations, évènements, stocks, factures, etc.),
Diacamma Syndic : Gérer sa copropriété (envoi d'appels de fonds, situation des copropriétaires, conformité avec la réglementation française, ventilation des dépenses en fonction des tantièmes, etc.)
Permet de gérer la gestion comptable et de travailler en équipe (partage de documents, messagerie interne, gestion des rôles et accès).

