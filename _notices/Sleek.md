---
nom: "Sleek"
date_creation: "Jeudi, 4 août, 2022 - 22:58"
date_modification: "Jeudi, 4 août, 2022 - 22:58"
logo:
    src: "images/logo/Sleek.png"
site_web: "https://github.com/ransome1/sleek"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Gestionnaire de tâches au format todo.txt pour Linux, Windows and MacOS, libre et open-source"
createurices: ""
alternative_a: "Wunderlist"
licences:
    - "Licence MIT/X11"
tags:
    - "bureautique"
    - "todo-list"
    - "todo"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/sleek"
---

Sleek est un gestionnaire de tâches open-source basé sur la syntaxe todo.txt. Il a été réduit aux fonctions minimales nécessaires, avec une interface simple. Sleek vise à faciliter la concentration pour accomplir ses tâches.

