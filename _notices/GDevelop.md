---
nom: "GDevelop"
date_creation: "Jeudi, 29 décembre, 2016 - 18:18"
date_modification: "Mercredi, 12 mai, 2021 - 16:55"
logo:
    src: "images/logo/GDevelop.png"
site_web: "http://www.compilgames.net/"
plateformes:
    - "GNU/Linux"
    - "Windows"
    - "Android"
    - "Windows Mobile"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Logiciel de création de jeux, d'animations qui peut être utilisé sans aucune compétence en programmation."
createurices: "Florian Rival"
alternative_a: "Construct, Construct 2, Unity3D, Unity2D, GameMaker Studio, Unreal Engine"
licences:
    - "Licence MIT/X11"
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "création"
    - "html5"
    - "animation"
    - "jeu"
    - "développement de jeu vidéo"
    - "jeu vidéo"
    - "moteur de jeu"
lien_wikipedia: "https://fr.wikipedia.org/wiki/GDevelop"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/gdevelop"
---

Gdevelop permet de créer des jeux 2D ou 3D. La création se fait sur une scène où l'on place des objets. Un onglet appelé « Événements » permet de programmer les objets.
Le résultat final peut être exporté en HTML5 pour une publication sur Internet ou sous forme de fichier exécutable pour Windows ou Linux. Un export expérimental pour Android est possible.

