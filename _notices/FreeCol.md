---
nom: "FreeCol"
date_creation: "Mardi, 3 janvier, 2017 - 15:49"
date_modification: "Mercredi, 12 mai, 2021 - 16:53"
logo:
    src: "images/logo/FreeCol.png"
site_web: "http://www.freecol.org/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "FreeCol est un jeu de stratégie en tour par tour. Il est un clone amélioré de Colonization."
createurices: ""
alternative_a: "Colonization"
licences:
    - "Common Public License (CPL)"
tags:
    - "jeu"
    - "stratégie"
    - "jeu vidéo"
    - "tour par tour"
lien_wikipedia: "https://fr.wikipedia.org/wiki/FreeCol"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/freecol"
---

FreeCol est un jeu de stratégie en tour par tour. Il est un clone de Colonization, mais depuis à largement dépassé ce dernier
FreeCol débute en 1492. Le joueur commence alors la colonisation du Nouveau Monde. Il débute avec quelques colons, puis doit se développer. Toutefois, ses rivaux européens font de même.
Outre ses adversaires Européens, il devra composer avec les autochtones.

