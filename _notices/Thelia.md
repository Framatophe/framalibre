---
nom: "Thelia"
date_creation: "Mardi, 15 novembre, 2022 - 18:20"
date_modification: "Mardi, 6 décembre, 2022 - 07:44"
logo:
    src: "images/logo/Thelia.jpg"
site_web: "https://thelia.net"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Thelia est une solution pour la réalisation de sites e-commerce sur mesure."
createurices: "Franck Allimant, Vincent Lopes, Damien Foulhoux, Gilles Bourgeat"
alternative_a: "Shopify, Adobe Commerce, BigCommerce"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "métiers"
    - "e-commerce"
    - "cms"
    - "php"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Thelia"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/thelia"
---

Depuis 2005, Thelia a fait tourner des centaines de sites de vente en ligne en France et à l’étranger.
Thelia est une solution open source 100 % française dédiée à la création de sites e-commerce. Ce CMS, conçu intégralement à partir du Framework PHP Symfony, offre de nombreux atouts, notamment dans la conception d’une e-boutique sur-mesure.
Porté par OpenStudio et sa communauté depuis plus de 15 ans, le CMS Thelia bénéficie d’évolutions constantes afin d’assurer toujours plus de fonctionnalités et de services aux développeurs et e-commerçants.
Le CMS Thelia est extensible à l’infini. Avec son API REST et son système de templating, Thelia s’adapte à de nombreuses problématiques : liaison avec ERP/CRM, web2store, click & collect, store picking, drive, B2B, multisite, PIM, marketplace, logiciels de caisse, …

