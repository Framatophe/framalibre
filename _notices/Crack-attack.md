---
nom: "Crack-attack"
date_creation: "Vendredi, 23 décembre, 2016 - 23:37"
date_modification: "Vendredi, 24 février, 2017 - 12:14"
logo:
    src: "images/logo/Crack-attack.png"
site_web: "http://www.nongnu.org/crack-attack/"
plateformes:
    - "GNU/Linux"
    - "Windows"
langues:
    - "English"
description_courte: "Puzzle OpenGL multi-joueur à la « Tetris Attack ». Hautement addictif dès que vous passez la barre des 800 !;)"
createurices: "Lorien420"
alternative_a: "Tetris Attack!"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "jeu"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/crack-attack"
---

Crack Attack est un puzzle OpenGL basé sur le jeu « Tetris Attack » de Super Nintendo. Lentement, votre pile de blocs colorés s'élève à partir du bas et vous devez vous assurer qu'elle n'atteint jamais le haut. Si c'est le cas, vous avez perdu. Pour éliminer les blocs de la pile, vous devez en aligner au moins trois de la même couleur, horizontalement ou verticalement. Lorsque vous y arrivez, ces blocs disparaissent, ce qui repousse légèrement votre inévitable défaite.

