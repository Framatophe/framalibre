---
nom: "Lis pour moi"
date_creation: "dimanche, 31 mars, 2024 - 09:13"
date_modification: "dimanche, 31 mars, 2024 - 09:13"
logo:
    src: "images/logo/Lis pour moi.svg"
site_web: "https://educajou.forge.apps.education.fr/lispourmoi/"
plateformes:
    - "le web"
    - "Apple iOS"
    - "Android"
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
description_courte: "Entendre un texte par synthèse vocale et générer un code QR pour y accéder"
createurices: ""
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "éducation"
    - "synthèse vocale"
    - "code QR"
    - "inclusion"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

Lis pour moi permet d'obtenir une lecture par synthèse vocale d'un texte.
L'application génère un lien et un code QR permettant d'y accéder directement avec le texte spécifié.
La synthèse vocale appelée est celle de votre navigateur. La qualité de la voix peut donc différer d'un terminal à un autre.
