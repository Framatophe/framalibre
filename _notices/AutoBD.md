---
nom: "AutoBD"
date_creation: "vendredi, 26 janvier, 2024 - 09:53"
date_modification: "dimanche, 31 mars, 2024 - 09:14"
logo:
    src: "images/logo/AutoBD.png"
site_web: "https://educajou.forge.apps.education.fr/autobd"
plateformes:
    - "le web"
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
description_courte: "Un générateur rapide de bande dessinée."
createurices: "Arnaud Champollion"
alternative_a: "BDnF"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bande dessinée"
    - "production d'écrit"
    - "graphisme"
    - "pao"
    - "éducation"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

AutoBD est un générateur rapide de bande dessinée, sur le web.
## Principe
L'application mise sur la simplicité. Pas d'inscription ni d'installation. L'ajout de contenu, la mise en page et l'export s'effectuent sur une interface unique.
## Fonctionnement
Une galerie de personnages et fonds est affichée. Il suffit de les faire glisser vers les vignettes.
Tous les graphismes disponibles sont sous licence libre.
## Fonctionnalités
- opérations sur les personnages (redimensionnement, rotation, duplication, empilement ...)
- ajout de bulles, onomatopées et cartouches de narration
- ajout d'images personnelles depuis le disque dur (formats PNG ou SVG)
- opérations sur les vignettes et les bandes (redimensionnement, ajout, supression)
- gestion automatisée de la mise en page (ajustement des dimensions)
- historique d'annulation
- export de la planche en JPEG
- historique d'annulation
- ajout de titre à la BD
