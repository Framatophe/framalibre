---
nom: "FreeCAD"
date_creation: "Dimanche, 1 janvier, 2017 - 21:26"
date_modification: "Jeudi, 24 octobre, 2019 - 09:27"
logo:
    src: "images/logo/FreeCAD.png"
site_web: "http://www.freecadweb.org"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "FreeCAD est un modeleur 3D paramétrique fait pour concevoir des objets réels de toutes les tailles."
createurices: "Jurgen Riegel, Werner Mayer, Yorik Van Havre"
alternative_a: "Catia, SolidWorks, Fusion360"
licences:
    - "Licence publique générale limitée GNU (LGPL)"
tags:
    - "création"
    - "cao"
    - "3d"
    - "modélisation"
    - "mécanique"
lien_wikipedia: "https://fr.wikipedia.org/wiki/FreeCAD"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/freecad"
---

FreeCAD est un modeleur 3D paramétrique orienté vers le génie mécanique et la conception de produits, mais vise également d'autres disciplines, telles que l'architecture ou d'autres champs d'activité d'ingénierie.
La conception paramétrique permet d'éditer facilement votre design en remontant dans l'historique du modèle afin d'en changer les paramètres.
FreeCAD propose des fonctionnalités similaires à Catia, SolidWorks ou Solid Edge, et s'inscrit donc aussi dans les catégories CFAO/IAO/PLM. Il vise la conception paramétrique, avec une architecture modulaire qui permet l'ajout facile de fonctionnalités supplémentaires sans modifier le cœur du programme.
Comme plusieurs logiciels de CAO modernes, il offrira une composante 2D pour extraire des vues de dessin du modèle 3D et produire des mises en plan.
FreeCAD est libre et fortement personnalisable, scriptable et extensible.

