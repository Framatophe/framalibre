---
nom: "Cherrytree"
date_creation: "Dimanche, 29 janvier, 2017 - 21:58"
date_modification: "Dimanche, 29 janvier, 2017 - 21:58"
logo:
    src: "images/logo/Cherrytree.png"
site_web: "http://www.giuspen.com/cherrytree/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Windows"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Une application de prise de note avec organisation hiérarchique, coloration syntaxique et bien plus."
createurices: "Giuseppe Penone"
alternative_a: "Evernote"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
    - "prise de notes"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/cherrytree"
---

Cherrytree est une application de prise de note. Vos notes sont stockées au format .xml ou dans une base de données, chiffrée ou non. Les notes sont organisées hiérarchiquement au sein d'une arborescence dont les racines peuvent être multiples.
Cherrytree offre la coloration syntaxique (utile pour des notes contenant du code dans différents langages) ainsi que des formats de textes enrichis.
Cette application propose de nombreuses fonctionnalités de recherche, d'import et d'export des notes depuis et vers de nombreux formats (pdf, xm, txt, zim, html).

