---
nom: "TriSph"
date_creation: "Mardi, 18 avril, 2017 - 18:46"
date_modification: "Mardi, 18 avril, 2017 - 18:46"
logo:
    src: "images/logo/TriSph.png"
site_web: "http://gnomonique.fr/trisph/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Trisph résout les triangles sphériques (ou plans) et est configurable pour les applications pratiques"
createurices: "Yvon Massé"
alternative_a: ""
licences:
    - "Licence MIT/X11"
tags:
    - "science"
    - "gnomonique"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/trisph"
---

TriSph est une calculatrice configurable pour résoudre les triangles sphériques (ou plans) d’un point de vue mathématique mais surtout dans le cadre des domaines qui utilisent ces triangles, notamment :
- l’astronomie,
- la navigation,
- la géographie,
- la gnomonique (science des cadrans solaires).
Les grandeurs des côtés peuvent, par exemple, être exprimées en kilomètres pour le cas de la géographie.
De plus TriSph comporte 3 modules complémentaires :
- Éphémérides du Soleil, d’une grande précision sur plus de 2 siècles
- Hauteur du Soleil, pour apporter toutes les corrections nécessaires à cette mesure
- Projection gnomonique, projection de la sphère sur le plan
La documentation, très détaillée et très claire, rappelle les principes de la trigonométrie sphérique, comment renseigner le fichier de configuration et donne des exemples typiques de résolution, notamment :
- le point en mer
- l’heure de lever/coucher du Soleil
- l’heure de la prière islamique de l’Asr

