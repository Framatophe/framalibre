---
nom: "ImageOptim"
date_creation: "lundi, 25 décembre, 2023 - 21:18"
date_modification: "lundi, 25 décembre, 2023 - 21:18"
logo:
    src: "images/logo/ImageOptim.png"
site_web: "https://imageoptim.com/mac"
plateformes:
    - "le web"
    - "Mac OS X"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Un outil pour optimiser la taille de vos photos et images sans perdre en qualité"
createurices: "Kornel Lesiński"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "photo"
    - "image"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"

---

En un glissé-déposé, ImageOptim optimise le poids de votre image sans perdre la qualité. Simplissime à utiliser, rapide, vous n'aurez plus à vous demander comment réussir à envoyer vos photos trop lourdes.
