---
nom: "NetGuard"
date_creation: "Mardi, 25 avril, 2017 - 15:25"
date_modification: "Mercredi, 12 mai, 2021 - 15:41"
logo:
    src: "images/logo/NetGuard.png"
site_web: "http://www.netguard.me/"
plateformes:
    - "Android"
langues:
    - "Autres langues"
description_courte: "NetGuard permet de bloquer facilement l'accès Internet aux applications de votre choix. Root non requis."
createurices: "Marcel Bokhorst"
alternative_a: "NoRoot Firewall, LostNet, Mobiwol"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "sécurité"
    - "pare-feu"
    - "internet"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/netguard"
---

NetGuard est le seul pare-feu Android non root qui soit libre et open source.
Un pare-feu de qualité vous permet de réduire la consommation des données mobiles, d'économiser un peu de batterie et surtout d'améliorer la protection de votre vie privée.
NetGuard dispose de fonctionnalités basiques et avancées. Il séduira tous les utilisateurs Android soucieux de leurs données personnelles :
*Material Design
*Plusieurs thèmes
*Pas de traçage ni de statistiques sur votre utilisation
*Requiert Android 4.0 ou supérieur
*Gestion des applications systèmes
*Gestion des connexions par application (UID)
*Notification de demandes d'accès à Internet
*Journalisation des connexions
*Blocage des publicités en utilisant la version Github
*Développeur très réactif
La version gratuite offre beaucoup de possibilités mais il existe une version PRO pour quelques options supplémentaires mais aussi pour soutenir le développeur.

