---
nom: "TiddlyWiki"
date_creation: "Mardi, 21 mars, 2017 - 21:48"
date_modification: "Mardi, 21 mars, 2017 - 21:48"
logo:
    src: "images/logo/TiddlyWiki.png"
site_web: "http://tiddlywiki.com/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "FirefoxOS"
    - "Windows Mobile"
    - "Apple iOS"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "TiddlyWiki est un carnet de notes web non-linéaire"
createurices: "Jeremy Ruston"
alternative_a: "Evernote"
licences:
    - "Berkeley Software Distribution License (BSD)"
tags:
    - "cms"
    - "wiki"
    - "javascript"
    - "html5"
lien_wikipedia: "https://fr.wikipedia.org/wiki/TiddlyWiki"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/tiddlywiki"
---

TiddlyWiki est un carnet de notes web non-linéaire
pour saisir, organiser et partager des informations simples ou complexes.
Dans sa version autonome, c'est un simple fichier html que l'on peut ouvrir avec un navigateur web.
Firefox et son extension TiddlyFox est un compagnon idéal. Avec son système de tag, de filtres, de widget et bien d'autres mécanismes, il est possible de personnaliser et construire son propre outil.
Jeremy Ruston sort la première version de TiddlyWiki en septembre 2004. Une nouvelle branche, TW5 comme TiddlyWiki5, sort en novembre 2011. C'est une refonte complète du code, qui met l'accent sur une version HTML5 ainsi qu'une version Node.js. La précédente version est alors renommée en Classic. En 2014, TW5 sort de son caractère bêta pour les 10 ans du projet.

