---
nom: "Abiword"
date_creation: "Dimanche, 1 janvier, 2017 - 22:44"
date_modification: "Samedi, 23 mars, 2019 - 10:39"
logo:
    src: "images/logo/Abiword.png"
site_web: "http://www.abisource.com/"
plateformes:
    - "GNU/Linux"
    - "Autre"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Logiciel de traitement de texte avancé, léger et compatible."
createurices: "Eric Sink"
alternative_a: "Microsoft Word"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
    - "traitement de texte"
    - "format"
    - "open document"
lien_wikipedia: "https://fr.wikipedia.org/wiki/AbiWord"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/abiword"
---

Abiword est un logiciel de traitement de texte rapide et léger doté d'un système de plugins spécialisés pour différents usages. Il est compatible avec les formats .doc, .odt, .rtf., .html... Mais il permet aussi, par exemple, de collaborer sur des documents en ligne, de se connecter à Wikipédia, l'export/import de documents dans une grande diversité de formats (ePUB, LaTeX, Docbook, Wordperfect, Clarisworks, etc.),  de traduire des portions de texte... Moins étendu que LibreOffice Writer, Abiword assure aussi des fonctions différentes ou complémentaires. Abiword fait partie de l'ensemble Gnome Office (avec le tableur Gnumeric, notamment).

