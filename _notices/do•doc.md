---
nom: "do•doc"
date_creation: "jeudi, 15 février, 2024 - 23:11"
date_modification: "jeudi, 15 février, 2024 - 23:11"
logo:
    src: "images/logo/do•doc.png"
site_web: "https://dodoc.fr/"
plateformes:
    - "le web"
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "do•doc est un outil ouvert et modulaire qui permet de capturer des médias (photos, vidéos, sons et stop-motion), de les éditer, de les mettre en page et de les publier"
createurices: "L’Atelier des chercheurs"
alternative_a: ""
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "travail collaboratif"
    - "projet"
    - "stopmotion"
    - "vidéo"
    - "cartographie"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

Conçu par l'Atelier des chercheurs pour documenter et créer des récits à partir d'activités pratiques, do•doc (prononcer doudoc) est un outil libre et modulaire, qui permet de capturer des médias (photos, vidéos, sons et stop-motion), de les éditer, de les mettre en page et de les publier.
Son design composite permet de le reconfigurer de manière à ce qu'il soit le plus adapté possible à la situation dans laquelle il est déployé.
