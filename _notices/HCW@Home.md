---
nom: "HCW@Home"
date_creation: "mardi, 9 avril, 2024 - 20:17"
date_modification: "mardi, 9 avril, 2024 - 20:17"
logo:
    src: "images/logo/HCW@Home.svg"
site_web: "https://hcw-at-home.com/"
plateformes:
    - "le web"
langues:
    - "Français"
    - "English"
description_courte: "Logiciel de téléconsultation permettant à un médecin d'envoyer des invitations à ses patients"
createurices: "Iabsis SARL"
alternative_a: "Doxy.me"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "téléconsultation"
    - "santé"
    - "communication"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

HCW@Home est un système de téléconsultation sécurisé et évolutif pour des scénarios de télémédecine typiques, réalisé grâce à une étroite collaboration avec des professionnels de la santé. Il est entièrement open source et offre des fonctionnalités intégrées pour les appels de chat, audio et vidéo utilisant WebRTC.

Fonctionnalité

- Consultations voix et vidéo.
- Chat sécurisé.
- Invitation d'un patient via un lien SMS ou e-mail.
- Envoi de pièces jointe ou une image.
- Prise de photo directe.
- Rapports PDF
- API ouverte à des intégrations (calendrier externe, ...)
- Authentification SSO (OpenID et SAML)
