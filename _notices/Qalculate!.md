---
nom: "Qalculate!"
date_creation: "Jeudi, 2 mars, 2017 - 22:00"
date_modification: "Vendredi, 3 mars, 2017 - 10:43"
logo:
    src: "images/logo/Qalculate!.png"
site_web: "http://qalculate.github.io/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
langues:
    - "English"
description_courte: "La simplicité d'une calculatrice mais avec la conversion entre unités et affichage de courbes."
createurices: "Hanna Knutsson"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
    - "calculatrice"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/qalculate"
---

Commencé en 2005, ce petit utilitaire est vite devenu l'une des calculatrices les plus sympas à utiliser :
L'historique des dernières utilisations est conservé ;
Affichage en fraction ;
Formats hexadécimal, octal, binaire ;
Conversion entre les unités de mesure, mais aussi entre devises ;
Formules mathématiques ;
Simplification d'équations ;
Édition et ajout de nouvelles fonctions ;
Affichage de courbes ;
Et aussi le mode console interactive (CLI - Command Line Interface).
Au niveau de l'interface graphique, seul GTK+3 est pris en charge. Une version KDE (Qt) était disponible, mais cette version n'est plus maintenue. Le projet en C++ accueille les nouveaux contributeurs.

