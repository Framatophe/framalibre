---
nom: "Podcast Generator"
date_creation: "Vendredi, 17 mai, 2019 - 16:21"
date_modification: "Vendredi, 17 mai, 2019 - 16:58"
logo:
    src: "images/logo/Podcast Generator.png"
site_web: "http://www.podcastgenerator.net/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Autre"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Hébergez vous-même vos podcasts sur votre serveur web."
createurices: "Alberto Betella"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "podcast"
    - "audio"
    - "cms"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/podcast-generator"
---

Podcast Generator est une solution d’hébergement de podcasts codé en PHP (5.3 ou plus).
Aucune base de données n’étant utilisée (tout est stocké dans des fichiers XML et MP3) elle est très facile à héberger.
Elle gère le téléversement des fichiers MP3, l’extraction des tags ID3v2 (dont les images) et la génération du fichier RSS (compatible Apple™️ Podcast).
Bien que vieillot, son code source est très facile à personnaliser.

