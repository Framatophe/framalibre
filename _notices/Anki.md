---
nom: "Anki"
date_creation: "Mardi, 27 décembre, 2016 - 19:59"
date_modification: "jeudi, 14 mars, 2024 - 05:07"
logo:
    src: "images/logo/Anki.png"
site_web: "https://apps.ankiweb.net/"
plateformes:
    - "le web"
    - "Android"
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
    - "Autre"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Apprendre et réviser grâce aux cartes mémoire (mémorisation par « répétitions espacées »)."
createurices: "Damien Elmes"
alternative_a: "Cram, Quizlet, SuperMemo"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "éducation"
    - "apprentissage"
    - "mémoire"
    - "memorisation"
    - "cartes-memoire"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Anki"
lien_exodus: "https://reports.exodus-privacy.eu.org/fr/reports/com.ichi2.anki/latest/"
identifiant_wikidata: "Q557318"
mis_en_avant: "non"
redirect_from: "/content/anki"
---

Anki est un logiciel qui permet d'apprendre par la répétition. Il est basé sur le principe des cartes-mémoire ou « flashcards ». Il y a une face contenant la question et une face contenant la réponse. L'algorithme d'Anki décide de la fréquence de passage des cartes en fonction de  l'habileté de l'apprenant.
Il est simple de créer ses propres paquets de cartes. On peut également s'inscrire sur le site https://ankiweb.net et récupérer des paquets de cartes ou partager ses propres créations.
Le site permet également de synchroniser les applications entre elles et ainsi de démarrer son apprentissage sur un ordinateur puis le continuer sur son téléphone.
Le logiciel propose des statistiques via des histogrammes afin d'évaluer les progrès.
