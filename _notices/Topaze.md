---
nom: "Topaze"
date_creation: "Mardi, 14 mai, 2019 - 11:23"
date_modification: "Jeudi, 5 janvier, 2023 - 19:13"
logo:
    src: "images/logo/Topaze.png"
site_web: "https://doc.scenari.software/Topaze"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Español"
description_courte: "Topaze est destiné aux concepteurs de parcours personnalisés (formateurs, ingénieurs pédagogiques,...)"
createurices: ""
alternative_a: "Microsoft Office, Microsoft Word"
licences:
    - "Licence CECILL (Inria)"
    - "Licence Publique Générale GNU (GNU GPL)"
    - "Licence publique générale limitée GNU (LGPL)"
    - "Licence Publique Mozilla (MPL)"
tags:
    - "éducation"
    - "chaîne éditoriale"
    - "création de site web"
    - "traitement de texte"
    - "jeu sérieux"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/topaze"
---

Topaze est un modèle documentaire adapté à la création de module de formation avec personnalisation du parcours.
Plus largement, Topaze permet de créer facilement tout type de parcours comportant plusieurs itinéraires. Ainsi, on peut imaginer créer des parcours individualisés et personnalisés en fonction du niveau, des objectifs de l’apprenant ou autre.
Grâce à la technologie de Topaze, l’utilisateur évolue dans un environnement qui sera conditionné en fonction de ses propres choix. Ainsi, si l’apprenant recommence son parcours en faisant d’autres choix, il ne sera pas confronté aux mêmes situations.
Topaze est compatible avec la chaîne éditoriale Opale.
Topaze est un des modèles documentaires de Scenari.

