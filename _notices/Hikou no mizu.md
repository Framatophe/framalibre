---
nom: "Hikou no mizu"
date_creation: "Mercredi, 29 avril, 2020 - 22:20"
date_modification: "Lundi, 10 mai, 2021 - 14:42"
logo:
    src: "images/logo/Hikou no mizu.png"
site_web: "https://hikounomizu.org"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Autre"
langues:
    - "English"
description_courte: "Hikou no mizu est un jeu de combat en 2D aux graphismes inspirés par l'animation japonaise."
createurices: "Duncan Deveaux"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "jeu"
    - "arcade"
    - "jeu vidéo"
    - "dessin animé"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/hikou-no-mizu"
---

Hikou no mizu est un jeu de combat en 2D aux graphismes inspirés par l'animation japonaise.
Il peut mettre en scène des combats de deux à cinq joueurs dans des arènes interactives de tailles variées.
À l'heure actuelle, deux personnages sont jouables: Hikou et Takino. Le jeu supporte les joysticks et inclut une intelligence artificielle pour jouer en solo.

