---
nom: "Joomla!"
date_creation: "Mercredi, 6 mai, 2020 - 01:14"
date_modification: "jeudi, 16 mai, 2024 - 15:40"
logo:
    src: "images/logo/Joomla!.png"
site_web: "https://www.joomla.org"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Site public, intranet, extranet grâce au CMS Joomla! open source et libre"
createurices: "Communauté mondiale de développeurs"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "cms"
    - "création de site web"
    - "gestionnaire de contenus"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Joomla!"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/joomla"
---

Joomla! est un système de gestion de contenu (CMS - content managment system), qui vous permet de créer des sites internet de qualité professionnelle. De nombreux aspects dont sa facilité d’utilisation et son extensibilité ont fait de Joomla! un des logiciels les plus populaires et meilleurs de tous.
Joomla! est une solution open source et gratuite, accessible à tout le monde.
Plus de...
- 110 millions de téléchargements sur le site officiel
- 1500 volontaires
- 10 000 extensions et templates
