---
nom: "Jekyll"
date_creation: "Mercredi, 29 décembre, 2021 - 11:16"
date_modification: "Lundi, 17 janvier, 2022 - 14:05"
logo:
    src: "images/logo/Jekyll.png"
site_web: "https://jekyllrb.com/"
plateformes:
    - "GNU/Linux"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Jekyll est un générateur de site statique."
createurices: ""
alternative_a: ""
licences:
    - "Licence MIT/X11"
tags:
    - "cms"
    - "ruby"
    - "création site web"
    - "site web statique"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/jekyll"
---

Jekyll est un générateur de site statique. Il prend du texte écrit dans votre langage de balisage préféré et utilise des mises en page pour créer un site Web statique. Vous pouvez modifier l'apparence du site, les URL, les données affichées sur la page. Plus de bases de données, de modération de commentaires ou de mises à jour embêtantes à installer, juste votre contenu prêt à être servi par Apache, Nginx ou un autre serveur Web.
C'est un des premiers générateurs de sites statiques, toujours utilisé. Aujourd'hui, d'autres peuvent avoir une plus grande collection de thèmes disponibles tout en étant plus faciles à installer et plus rapides à utiliser (par exemple, Hugo).

