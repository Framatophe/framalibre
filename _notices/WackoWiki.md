---
nom: "WackoWiki"
date_creation: "Dimanche, 28 août, 2016 - 11:46"
date_modification: "mercredi, 24 avril, 2024 - 09:21"
logo:
    src: "images/logo/WackoWiki.png"
site_web: "https://wackowiki.org/"
plateformes:
    - "le web"
    - "Apple iOS"
    - "Android"
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
    - "Autre"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "WackoWiki est un moteur de wiki petit, léger, maniable, extensible, multilingue."
createurices: "WackoWiki Team"
alternative_a: "DokuWiki, MediaWiki, PmWiki"
licences:
    - "Berkeley Software Distribution License (BSD)"
tags:
    - "cms"
    - "wiki"
    - "web"
    - "documentation"
lien_wikipedia: "https://fr.wikipedia.org/wiki/WackoWiki"
lien_exodus: ""
identifiant_wikidata: "Q1304185"
mis_en_avant: "non"
redirect_from: "/content/wackowiki"
---

WackoWiki est un moteur de wiki petit, léger, maniable, extensible, multilingue.
Il est écrit en langage de programmation PHP, et il utilise MySQL pour stocker les pages. WackoWiki a un éditeur semblable à un WYSIWYG, un programme d'installation facile, de nombreuses localisations ; il supporte les courriels de notification sur les changements ou les commentaires, divers niveaux de cache, les designs (skins) ; il est conforme au HTML5, il gère les droits de page (ACL) ainsi que les pages de commentaire. WackoWiki est distribué sous la licence BSD.
