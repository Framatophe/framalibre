---
nom: "Markor"
date_creation: "Samedi, 14 octobre, 2017 - 21:44"
date_modification: "Lundi, 20 novembre, 2023 - 10:41"
logo:
    src: "images/logo/Markor.png"
site_web: "https://gsantner.net/project/markor.html"
plateformes:
    - "Android"
langues:
    - "Autres langues"
description_courte: "Markor est un éditeur de texte markdown très complet"
createurices: "Gregor Santner"
alternative_a: ""
licences:
    - "Licence MIT/X11"
tags:
    - "bureautique"
    - "markdown"
    - "traitement de texte"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/markor"
---

Markor est un éditeur de fichier en Markdown.
Il gère également une todo-list en utilisant le format todo.txt.
Complet, son interface permet de lister l'ensemble des fichiers présents dans les répertoires, qu'ils soient au format texte, markdown, image.... On peut effectuer des recherches sur leurs contenus, les visualiser et surtout les modifier.
Il gère un système de modèle de fichiers ("templates") qui permet d'éditer des fichiers de blogs SGG tels que Jekyll ou Hugo.
Couplé à une solution de synchronisation de dossier telle que Syncthing [https://www.syncthing.net], vos fichiers deviennent accessibles sur tous vos appareils.
C'est donc un logiciel Idéal pour de la prise de note simple en mobilité, mais également un couteau suisse idéal à avoir sur son appareil Android !

