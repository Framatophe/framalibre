---
nom: "Zotero"
date_creation: "Mercredi, 4 janvier, 2017 - 13:02"
date_modification: "mardi, 26 décembre, 2023 - 13:40"
logo:
    src: "images/logo/Zotero.png"
site_web: "https://www.zotero.org/"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Autres langues"
description_courte: "Zotero permet de collecter, annoter, gérer et citer vos différentes sources, documents électroniques ou papier"
createurices: "Dan Cohen"
alternative_a: "EndNote"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "bureautique"
    - "bibliographie"
    - "navigateur web"
    - "add-on"
    - "extension"
    - "référence bibliographique"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Zotero"
lien_exodus: ""
identifiant_wikidata: "Q226915"
mis_en_avant: "oui"
redirect_from: "/content/zotero"
---

Zotero est un logiciel de gestion de références gratuit, libre et open source. À l’origine, il est une extension pour le navigateur Firefox ; il est maintenant disponible en version autonome (standalone)
Il permet de gérer des données bibliographiques et des documents de recherche, de les organiser, partager, synchroniser. Zotero génère aussi les citations (notes et bibliographies) dans un texte rédigé depuis les logiciels de traitement de textes comme LibreOffice Writer grâce à un plugin.


