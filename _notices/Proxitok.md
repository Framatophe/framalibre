---
nom: "Proxitok"
date_creation: "Samedi, 4 juin, 2022 - 00:02"
date_modification: "Samedi, 4 juin, 2022 - 16:14"
logo:
    src: "images/logo/Proxitok.png"
site_web: "https://proxitok.herokuapp.com/"
plateformes:
    - "Autre"
    - "le web"
langues:
    - "English"
description_courte: "TikTok mais en plus respectueux de la vie privée !"
createurices: "Pablo Ferreiro"
alternative_a: "TikTok"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "frontend"
    - "vidéo"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/proxitok"
---

Ce logiciel est un frontend opensource qui permet de consulter TikTok sans avoir de compte. Il est écrit en PHP. On peut s'abonner à un compte TikTok avec un flux RSS. Et il est constitué d'instances.

