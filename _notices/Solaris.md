---
nom: "Solaris"
date_creation: "mardi, 13 février, 2024 - 15:42"
date_modification: "mardi, 13 février, 2024 - 15:42"
logo:
    src: "images/logo/Solaris.png"
site_web: "https://solaris.games"
plateformes:
    - "le web"
    - "Android"
langues:
    - "English"
description_courte: "Un jeu de stratégie spatiale en ligne plein de conquête, de trahison et de subterfuge."
createurices: ""
alternative_a: "Neptune's Pride , Subterfuge"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "jeu vidéo"
    - "multi-joueur"
    - "espace"
    - "stratégie"
    - "2D"
lien_wikipedia: ""
lien_exodus: "https://reports.exodus-privacy.eu.org/fr/reports/com.voxel.solaris_android/latest/"
identifiant_wikidata: "Q122373985"
mis_en_avant: "non"

---

Découvrez un jeu de stratégie spatiale rempli de conquête, de trahison et de subterfuge. Construisez des alliances, faites-vous des ennemis et frayez-vous un chemin vers la victoire et la domination galactique.

Allez-vous conquérir la galaxie ?

- Les jeux durent 2-3 semaines, et vous n'avez pas besoin de consacrer beaucoup de temps à jouer !
- Construisez une infrastructure économique, industrielle et scientifique pour améliorer votre empire.
- Construisez des transporteurs pour voyager vers de nouvelles étoiles ou pour combattre vos ennemis.
- Recherchez de nouvelles technologies pour prendre l'avantage sur vos adversaires.
- Embauchez des spécialistes pour vous donner un avantage tactique au combat.
- Établissez des échanges avec vos alliés pour prendre une longueur d'avance.
- Participez à des discussions de groupe avec vos alliés pour discuter de stratégie.
- Combattez d'autres joueurs et capturez des étoiles pour gagner la partie.
- Jouez à des jeux avec jusqu'à 32 joueurs à la fois.
- Jouez sur n'importe quel appareil doté d'un navigateur Web.
- C'est entièrement gratuit et open source !

* Compte tiers requis pour jouer.
