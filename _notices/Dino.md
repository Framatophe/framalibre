---
nom: "Dino"
date_creation: "Mercredi, 28 mars, 2018 - 23:31"
date_modification: "Mercredi, 12 mai, 2021 - 16:27"
logo:
    src: "images/logo/Dino.PNG"
site_web: "https://dino.im"
plateformes:
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
description_courte: "Dino est un client XMPP moderne et minimaliste."
createurices: ""
alternative_a: "Facebook Messenger, WhatsApp, Skype, Google Talk"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
    - "messagerie instantanée"
    - "xmpp"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/dino"
---

Dino est un client XMPP moderne et minimaliste.
Le logiciel est très minimaliste : la gestion des conversations et identités est fonctionnelle, mais il n'y a pas d'interface de configuration du compte (changement du mot de passe, avatar, …)
Dino supporte le chiffrement GPG et l'extension OMEMO. Il est nécessaire d'avoir importé au préalable les clés de ses correspondants pour que le chiffrement soit activable pour chaque conversation.

