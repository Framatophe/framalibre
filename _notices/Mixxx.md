---
nom: "Mixxx"
date_creation: "Jeudi, 11 avril, 2019 - 20:48"
date_modification: "Jeudi, 11 avril, 2019 - 20:49"
logo:
    src: "images/logo/Mixxx.png"
site_web: "https://mixxx.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Le logiciel de mixage pour DJs"
createurices: ""
alternative_a: "Traktor, VirtualDJ"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "mixage"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/mixxx"
---

Mixxx est un logiciel multiplateforme de mixage pour les DJs, il permet aux utilisateurs de mixer de la musique comme le ferait un DJ avec des platines vinyles. Il comporte 4 platines et une table de mixage.
ll est devenu très populaire dès sa sortie en 2011.
Voyez aussi sa notice sur linuxmao: https://linuxmao.org/mixxx

