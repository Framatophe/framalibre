---
nom: "WriterCMS"
date_creation: "Samedi, 11 mars, 2023 - 15:10"
date_modification: "Mardi, 21 mars, 2023 - 22:44"
logo:
    src: "images/logo/WriterCMS.png"
site_web: "https://www.writercms.com"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Autre"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "WriterCMS, le CMS pour les écrivains"
createurices: "Christopher Fournier"
alternative_a: ""
licences:
    - "Licence Publique Union Européenne (EUPL)"
tags:
    - "cms"
    - "écriture"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/writercms"
---

Vous êtes écrivain et vous voulez un site web spécialement conçu pour vous.
WriterCMS est l’outil qu’il vous faut : il vous permet de créer votre propre site Internet avec toutes les rubriques liées à votre travail d’auteur et ce sans connaissance poussée en informatique.
Parmi les fonctionnalités proposées, vous pourrez :
- mettre sa biographie ;
- créer une page pour chacun de vos livres avec résumé, informations générales, lieux d’achat en ligne, en version papier et électronique, extrait ;
- mettre des histoires gratuites à disposition de vos lecteurs ;
- gérer votre blog en proposant des posts quand il le souhaite.
Les visiteurs peuvent aussi s’inscrire à votre newsletter et peuvent vous contacter via un formulaire de contact.
WriterCMS est entièrement gratuit, est disponible en français, anglais, allemand et espagnol.
De nombreux tutoriels sont disponibles sur le site Web.
Il est publié sous licence EUPL (dans le dossier data/licences)

