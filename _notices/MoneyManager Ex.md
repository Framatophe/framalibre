---
nom: "MoneyManager Ex"
date_creation: "Jeudi, 4 août, 2022 - 22:19"
date_modification: "Jeudi, 1 septembre, 2022 - 09:07"
logo:
    src: "images/logo/MoneyManager Ex.png"
site_web: "https://moneymanagerex.org"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Money Manager Ex est un logiciel libre de gestion de comp"
createurices: ""
alternative_a: "Moneydance, Quicken, MoneyWell, Tous Comptes Faits, Ciel Comptes Personnels"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
    - "comptabilité"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/moneymanager-ex"
---

Money Manager Ex est un logiciel libre de gestion de comptes personnels destiné aussi bien aux particuliers qu'aux associations qui souhaitent gérer leurs comptes sur un ordinateur. Il permet de gérer de façon conviviale ses finances. Son but est de vous aider à organiser vos finances et de conserver la trace de où, quand et comment circule votre argent.
Il faut considérer Money Manager Ex (MMEX) comme un livre de comptes informatique qui vous permet d'équilibrer vos comptes, de les organiser, de les gérer et de vous fournir un état de vos avoirs.
C'est aussi un bon moyen de vous tenir au courant de la santé de votre situation financière.
Le premier objectif de MMEX est de simplifier le suivi de vos finances sans le rendre complexe comme c'est le cas pour certains logiciels de gestion de comptes personnels bien plus connus.
…la version Android n'est pas disponible ni sur Gogool ni sur F-droid…

