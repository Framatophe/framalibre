---
nom: "Lulu"
date_creation: "Samedi, 13 mars, 2021 - 18:25"
date_modification: "Vendredi, 5 août, 2022 - 12:00"
logo:
    src: "images/logo/Lulu.png"
site_web: "https://www.objective-see.com/products/lulu.html"
plateformes:
    - "Mac OS X"
langues:
    - "English"
description_courte: "LuLu est un pare-feu gratuit et open-source."
createurices: "Patrick Wardle"
alternative_a: "Little Snitch, Hands Off!"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "sécurité"
    - "pare-feu"
    - "cybersecurite"
    - "firewall"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/lulu"
---

Dans le monde connecté d'aujourd'hui, il est rare de trouver une application ou un logiciel malveillant qui ne communique pas avec un serveur distant.
LuLu est un pare-feu gratuit et open-source qui vise à bloquer les connexions sortantes inconnues, protégeant ainsi votre vie privée et votre Mac !

