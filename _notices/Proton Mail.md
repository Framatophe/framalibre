---
nom: "Proton Mail"
date_creation: "Lundi, 16 janvier, 2017 - 10:17"
date_modification: "lundi, 11 mars, 2024 - 11:56"
logo:
    src: "images/logo/ProtonMail.png"
site_web: "https://proton.me/fr/mail"
plateformes:
    - "le web"
    - "Apple iOS"
    - "Android"
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
    - "Autre"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "ProtonMail est un service de messagerie sécurisé respectant votre vie privée."
createurices: "Andy Yen, Jason Stockman, Wei Sun"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
    - "client mail"
    - "vie privée"
    - "sécurité"
    - "mail"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/protonmail"
---

Avertissement : Seuls les interfaces (webmails) et applications de Proton Mail sont libres, mais pas la partie serveur. Proton Mail est un service de messagerie sécurisé fondé par des membres du CERN (Conseil européen pour la recherche nucléaire) et profite ainsi d'une grande expérience en matière de gestion de système sécurisé à grande échelle.
Il supporte un chiffrement de bout en bout et ses serveurs sont situés en Suisse sous leur juridiction.
Depuis peu, Proton Mail propose une application "Bridge" permettant d'utiliser les protocoles IMAP/POP dans un logiciel de gestions de mail comme Thunderbird par exemple.
Il est aussi possible d'y accéder via mobile via votre app store préféré. Proton Mail propose des comptes gratuits en offre de base, mais il est possible d'étendre l'espace de stockage ou d'utiliser son nom de domaine moyennant un abonnement. Proton Mail est accessible depuis le réseau TOR depuis l'adresse https://protonirockerxow.onion/.
