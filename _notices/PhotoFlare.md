---
nom: "PhotoFlare"
date_creation: "Vendredi, 31 août, 2018 - 15:39"
date_modification: "Mercredi, 2 octobre, 2019 - 11:45"
logo:
    src: "images/logo/PhotoFlare.jpeg"
site_web: "http://photoflare.io/"
plateformes:
    - "GNU/Linux"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Éditeur d'image multiplateforme facile d'accès"
createurices: "Dylan Coakley"
alternative_a: "Photofiltre, Adobe Photoshop, Microsoft Paint"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "création"
    - "photo"
    - "retouche"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/photoflare"
---

Photoflare est un logiciel libre sous license GPL de retouche et d'édition d'image (pixel par pixel) très proche dans son interface à Photofiltre, le projet s’appelait d'ailleurs à l'origine Photofiltre LX.
Le but du logiciel est de proposer un éditeur d'image simple pour les habitués de Photofiltre qui veulent un équivalent sur leur système GNU/Linux.
Le logiciel dispose de beaucoup de filtres et autres outils permettant une retouche d'image rapide et simple. Photoflare est cependant moins fourni en fonctionnalités que son grand frère Photofiltre, le projet étant en développement depuis 2015.

