---
nom: "DigiKam"
date_creation: "Dimanche, 19 mars, 2017 - 22:57"
date_modification: "Vendredi, 15 décembre, 2023 - 21:49"
logo:
    src: "images/logo/DigiKam.png"
site_web: "https://www.digikam.org/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Gestionnaire de collection de photos"
createurices: ""
alternative_a: "Picasa, Adobe Lightroom"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "photothèque"
    - "photo"
    - "galerie photo"
    - "partage d'image"
lien_wikipedia: "https://fr.wikipedia.org/wiki/DigiKam"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/digikam"
---

DigiKam est un gestionnaire de collection de photos.
Il permet d'organiser ses photos, de leur ajouter des tags, de les classer. Il permet également de modifier les données EXIF.
Il peut également les manipuler et leur appliquer des filtres, puis les exporter (création de diaporamas, de tables d'impression, de calendriers, partage sur des sites web de partage de photo et les réseaux sociaux, etc).
Parmi les fonctionnalités : table lumineuse pour comparer les clichés, détection des doublons, reconnaissance des visages, géolocalisation, etc.
La plupart des appareils photos et des formats d'images sont reconnus par DigiKam, qu'elles soient stockées sur disque interne ou réseau. Initialement destiné à l'environnement graphique KDE (de GNU/Linux). Il fut plus récemment porté sur Windows et Mac osx.

