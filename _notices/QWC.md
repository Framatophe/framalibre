---
nom: "QWC"
date_creation: "vendredi, 12 avril, 2024 - 15:46"
date_modification: "vendredi, 12 avril, 2024 - 15:46"


site_web: "https://github.com/qgis/qwc2"
plateformes:
    - "le web"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "QWC ( QGIS Web Client ) est une application et un framework web permettant de publier facilement des cartes sur le web."
createurices: "Communauté de développeurs QGIS"
alternative_a: "ArcMap, GEO"
licences:
    - "Licence MIT/X11 (MIT)"
tags:
    - "SIG"
    - "géographie"
    - "cartographie"
    - "QGIS"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

QWC ( QGIS Web Client ) est une application et un framework web permettant de publier facilement des cartes sur le web. Basé sur QGIS Server, QWC permet de créer son projet sur QGIS, et de le publier directement sur le web.

QWC a une architecture en composants, permettant d'ajouter des fonctionnalités côté serveur comme côté client.

On retrouve les éléments d'interface cartographique classique : navigation cartographique, gestion des couches, recherche de localisation, interrogation des objets, table attributaire, édition de données, formulaires de saisie, annotation & dessin, impression. QWC propose également des fonctionnalités avancées comme le filtrage temporel des données, outil de profil en Z.
