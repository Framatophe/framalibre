---
nom: "MapMap"
date_creation: "Lundi, 20 août, 2018 - 13:55"
date_modification: "Lundi, 20 août, 2018 - 14:33"
logo:
    src: "images/logo/MapMap.png"
site_web: "https://mapmapteam.github.io//"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
description_courte: "Logiciel de mapping vidéo"
createurices: "Sofian Audry"
alternative_a: "Resolume Arena, HeavyM"
licences:
    - "Open Software License (OSL)"
tags:
    - "création"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/mapmap"
---

Logiciel gratuit et open-source permettant le mapping vidéo. Très utile pour de la projection vidéo en spectacle sur des objets plus petits que l'image maximum du projecteur et pas forcément bien positionné.

