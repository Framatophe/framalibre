---
nom: "Sésamath"
date_creation: "Dimanche, 20 septembre, 2020 - 13:19"
date_modification: "Dimanche, 20 septembre, 2020 - 13:19"


site_web: "https://manuel.sesamath.net/index.php?page=telechargement"
plateformes:

langues:

description_courte: ""
createurices: ""
alternative_a: ""
licences:
    - "Creative Commons -By-Sa"
tags:
    - "éducation"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/s%C3%A9samath"
---


