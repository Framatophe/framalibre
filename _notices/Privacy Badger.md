---
nom: "Privacy Badger"
date_creation: "Mercredi, 4 janvier, 2017 - 22:01"
date_modification: "dimanche, 14 janvier, 2024 - 21:42"
logo:
    src: "images/logo/Privacy Badger.png"
site_web: "https://github.com/EFForg/privacybadger"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
    - "Autre"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Une extension anti-tracking permettant de ne pas vous faire pister par les annonceurs."
createurices: ""
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
    - "vie privée"
    - "anonymat"
    - "extension"
    - "add-on"
    - "navigateur web"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Privacy_Badger"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/privacy-badger"
---

* Privacy Badger est une extension anti-tracking, vous permettant de lutter contre les tentatives de pistage des différents annonceurs, en les détectant d'une part, puis en bloquant ceux que vous n'autorisez pas.
* Certains sites présentent en effet des contenus issus d'autres sites qui, lorsque vous surfez, engrangent vos données de navigation à l'aide de cookies. Typiquement, c'est le comportement des liens Facebook qui, une fois chargés et même si vous ne cliquez pas dessus, renvoient des données vous concernant à Facebook.
* L'extension a été développée par l'Electronic Frontier Foundation (EFF), une ONG qui œuvre pour la liberté d'expression sur Internet.
* Privacy Badger fait partie des extensions recommandées par Mozilla Firefox.


