---
nom: "compta.libremen.com"
date_creation: "Vendredi, 24 avril, 2020 - 18:36"
date_modification: "Mercredi, 3 juin, 2020 - 16:31"
logo:
    src: "images/logo/compta.libremen.com.png"
site_web: "https://compta.libremen.com"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Windows"
langues:
    - "Français"
description_courte: "Logiciel libre de comptabilité générale en partie double"
createurices: "Vincent Veyron"
alternative_a: "Sage Ciel Cegid"
licences:
    - "Licence CECILL (Inria)"
tags:
    - "métiers"
    - "comptabilité"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/comptalibremencom"
---

Logiciel libre de comptabilité en partie double.
Fonctionnalités:
- Plan de comptes et journaux
- Saisie des écritures comptables
- Balance, Grand Livre
- Déclaration TVA (calcul du CA3)
- Lettrage, pointage
- Numérotation automatique des pièces comptables
- Enregistrement automatique dans le journal de banque du paiement des écritures enregistrées dans le journal des fournisseurs
- Paramétrage de l'affichage des dates
- Calcul du résultat
- Reports à nouveaux
- Calculs du document 2033-B (Compte de résultats simplifié)
- Reconduite du plan comptable de l'année précédente
- Importation d'un plan comptable
- Importation d'écritures
- Fichier des écritures comptables ou FEC (Article A47 A-1)
- Archivage et exportation des écritures, mensuels et incrémentiels

