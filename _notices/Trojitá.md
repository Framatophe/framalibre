---
nom: "Trojitá"
date_creation: "Dimanche, 12 mars, 2017 - 13:26"
date_modification: "Lundi, 10 mai, 2021 - 13:40"
logo:
    src: "images/logo/Trojitá.png"
site_web: "http://trojita.flaska.net"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Windows"
langues:
    - "English"
description_courte: "Trojitá est un client de messagerie léger et multiplateforme."
createurices: ""
alternative_a: "Microsoft Outlook"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
    - "client mail"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/trojit%C3%A1"
---

Trojitá se définit comme une client de messagerie rapide et codé en Qt. Il respecte les standards et facilite donc l'ajout de nouvelles technologies. Il ne monopolise que très peu de bande passante et de CPU et il est multiplateforme.

