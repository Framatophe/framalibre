---
nom: "SPUI"
date_creation: "Lundi, 19 novembre, 2018 - 17:13"
date_modification: "Mercredi, 12 mai, 2021 - 14:52"
logo:
    src: "images/logo/SPUI.png"
site_web: "https://spui.tuxfamily.org"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
description_courte: "SPUI est un logiciel qui, avec un client et un serveur et en utilisant le SPUI, permet d'afficher une page."
createurices: "Nifou"
alternative_a: ""
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "développement"
    - "programmation"
    - "navigateur web"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/spui"
---

SPUI est un logiciel (client et serveur) et un langage de programmation :
-Le client qui permet d'accéder aux pages qui sont sur un serveur SPUI et le serveur permet en codant en SPUI, en mettant des images et en codant en SPUIS (un langage de script pour SPUI) de créer un joli site qui sera accessible de partout par le client SPUI.
-Un langage de programmation qui à travers des mots clés (comme TEXT, IMG ou BUTTON) sera lu par le client SPUI et affiché sous forme de texte, d'image et de boutons.
SPUI est codé en Python avec la bibliothèque wxpython pour l'interface graphique et avec HttpLib pour la partie serveur (serveur qui utilise donc le protocole HTTP).

