---
nom: "Logseq"
date_creation: "mardi, 16 avril, 2024 - 13:31"
date_modification: "mardi, 16 avril, 2024 - 13:31"
logo:
    src: "images/logo/Logseq.png"
site_web: "https://logseq.com/"
plateformes:
    - "Apple iOS"
    - "Android"
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Español"
description_courte: "Gestionnaire de notes interconnecté, pratique pour gérer ses connaissances."
createurices: "Logseq"
alternative_a: "Notion, Obsidian"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "prise de notes"
    - "todo"
    - "journal"
    - "gestion de tâches"
    - "base de connaissances"
lien_wikipedia: ""
lien_exodus: "https://reports.exodus-privacy.eu.org/fr/reports/com.logseq.app/latest/"
identifiant_wikidata: "Q116748304"
mis_en_avant: "non"

---

Gestionnaire de notes interconnecté, pratique pour gérer ses connaissances.
