---
nom: "Nanazip"
date_creation: "Jeudi, 29 juin, 2023 - 22:27"
date_modification: "Jeudi, 29 juin, 2023 - 22:48"
logo:
    src: "images/logo/Nanazip.png"
site_web: "https://apps.microsoft.com/store/detail/nanazip/9N8G7TSCL18R?hl=fr-fr&gl=fr&rtc=…"
plateformes:
    - "Windows"
langues:
    - "Autres langues"
description_courte: "NanaZip est un logiciel open source de compression et décompression de fichiers"
createurices: "Kenji Mouri"
alternative_a: "winzip, winrar"
licences:
    - "Licence MIT/X11"
tags:
    - "système"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/nanazip"
---

NanaZip est à la fois puissant et facile à utiliser avec support des codecs Brotli, Fast-LZMA2, Lizard, LZ4, LZ5 et Zstandard.
NanaZip se distingue par ses performances et sa flexibilité. Qu’il s’agisse de compresser des fichiers pour économiser de l’espace de stockage, protéger des données sensibles par un mot de passe ou encore extraire des fichiers d’archives compressées, grâce à des algorithmes de compression avancés, il offre la possibilité de protéger vos fichiers compressés avec un mot de passe robuste pour garantir leur sécurité

