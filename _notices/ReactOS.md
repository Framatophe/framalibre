---
nom: "ReactOS"
date_creation: "Lundi, 16 janvier, 2017 - 20:32"
date_modification: "Mercredi, 12 mai, 2021 - 15:37"
logo:
    src: "images/logo/ReactOS.png"
site_web: "https://www.reactos.org/"
plateformes:
    - "Autre"
langues:
    - "Autres langues"
description_courte: "ReactOS : l'OS NT libre (GNU) compatible avec les applications et drivers Microsoft Windows."
createurices: "ReactOS Foundation et la communauté"
alternative_a: "Microsoft Windows, OS/2, MacOS"
licences:
    - "Berkeley Software Distribution License (BSD)"
    - "Licence Publique Générale GNU (GNU GPL)"
    - "Licence publique générale limitée GNU (LGPL)"
tags:
    - "système"
    - "système d'exploitation (os)"
    - "gnu"
    - "gpl"
    - "bsd"
lien_wikipedia: "https://fr.wikipedia.org/wiki/ReactOS"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/reactos"
---

Les motivations peuvent être les mêmes que celles ayant amené à la création de Linux soit un clone libre d'UNIX.
ReactOS est un système d'exploitation libre (GNU/GPL/LGPL) qui se veut compatible Microsoft Windows avec des librairies dynamiques et des exécutables. Donc avec des logiciels et des pilotes également.
Il est en cours de développement et n'est pas encore conseillé pour la production.

