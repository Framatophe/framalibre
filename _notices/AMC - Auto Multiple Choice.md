---
nom: "AMC - Auto Multiple Choice"
date_creation: "Mercredi, 10 avril, 2019 - 13:12"
date_modification: "Mercredi, 10 avril, 2019 - 14:12"
logo:
    src: "images/logo/AMC - Auto Multiple Choice.png"
site_web: "https://www.auto-multiple-choice.net"
plateformes:
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Un ensemble d'utilitaires permettant de créer, gérer et corriger automatiquement des QCM"
createurices: "Alexis Bienvenüe, Frédéric Bréal, Benjamin Abel"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "éducation"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/amc-auto-multiple-choice"
---

Auto Multiple Choice est un ensemble d'utilitaires permettant de créer, gérer et corriger automatiquement des questionnaires à choix multiples (QCM). C'est un logiciel libre distribué sous la licence GPLv2+.
Fonctionnalités
- Mise en forme par LaTeX
- Questions et réponses mélangées
- Correction automatique à partir des scans des copies
- Notation

