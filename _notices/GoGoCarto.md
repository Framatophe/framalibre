---
nom: "GoGoCarto"
date_creation: "Jeudi, 7 novembre, 2019 - 13:55"
date_modification: "vendredi, 22 décembre, 2023 - 13:09"
logo:
    src: "images/logo/GoGoCarto.png"
site_web: "https://gogocarto.fr/projects"
plateformes:
    - "le web"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Créez des cartes géographiques personnalisées, seul·e ou de manière collaborative."
createurices: ""
alternative_a: "Google Maps"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "édition de cartes géographiques"
    - "cartographie"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/gogocarto"
---

GoGoCarto permet de publier un site web affichant des données sur une carte, avec des fonctionnalités de contribution et d'import de données.

Le site peut contenir une page d'accueil, la carte personnalisée, un formulaire d'ajout, etc.

Les visiteurs peuvent parcourir la carte pour visualiser les données, sous forme de marqueurs disposés sur la carte. Un clic sur un marqueur permet d'afficher sa fiche contenant ses informations détaillés (adresse, catégorie du marqueur, commentaires, horaires d'ouverture, etc.) ainsi que d'afficher les actions liées à ce marqueur (Enregistrer le marqueur, générer un itinéraire, proposer des modifications, signaler une erreur, envoyer un mail si disponible, etc.).

Lors de la visualisation de la carte, les résultats peuvent être filtrés en utilisant les catégories et sous catégories personnalisés assignées aux marqueurs.

Un moteur de recherche est proposé pour trouver facilement des marqueurs précis. Il est également possible de se géolocaliser pour trouver des résultats proches de sa position.

Il est possible de se créer un compte pour pouvoir proposer des modifications ou de nouveaux marqueurs, ainsi que pour pouvoir les enregistrer (les mettre en favoris) et être informé·es des nouveaux marqueurs ajoutés.

L'administration de GoGoCarto permet de gérer le site, modérer les contributions et les contributeur·rices ainsi que d'ajouter, modifier et supprimer librement des marqueurs.

Des tutoriels sont proposés pour bien prendre en main le logiciel : https://video.colibris-outilslibres.org/video-channels/gogocarto_channel...
