---
nom: "Celestia"
date_creation: "Mardi, 14 mars, 2017 - 21:14"
date_modification: "Jeudi, 5 octobre, 2023 - 08:39"
logo:
    src: "images/logo/Celestia.png"
site_web: "https://celestiaproject.space"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Un simulateur d'espace pour découvrir notre univers en 3D"
createurices: "Chris Laurel"
alternative_a: "Coelix"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "science"
    - "astronomie"
    - "étoile"
    - "planète"
    - "espace"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Celestia"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/celestia"
---

Célestia est un simulateur d'espace qui permet de partir à la découverte de l'univers, en 3D. Avec un zoom exponentiel, il permet de façon fluide d'explorer pour aller observer des éléments de tailles très variées, de l'astéroïde à l'amas de galaxies.
Très utilisé à des fins pédagogiques par des écoles ou des planétariums, il est également extensible grâce à de nombreuses extensions qui permettent des ajouts variés, de surfaces en haute résolution aux astéroïdes, en passant par des objets de fonctions comme des personnages, ou des vaisseaux spatiaux.

