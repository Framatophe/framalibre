---
nom: "Ancestromania"
date_creation: "lundi, 1 avril, 2024 - 15:18"
date_modification: "lundi, 1 avril, 2024 - 15:18"
logo:
    src: "images/logo/Ancestromania.png"
site_web: "https://ancestromania.eu/"
plateformes:
    - "le web"
    - "Apple iOS"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
description_courte: "Ancestromania permet de créer sa généalogie, des documents de suivi, des arbres, d’intégrer à partir ou vers Geneanet, à partir ou vers des GEDCOMS ( il exporte et importe les images ). On peut aussi créer son site web gratuit avec Ancestroweb."
createurices: "Matthieu Giroux, André Langlet"
alternative_a: "Ancestrologie"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "Généalogie"
    - "gedcom"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: "Q75050"
mis_en_avant: "non"

---

Conçu en 1995 par les développeurs autour de Philippe Cazaux-Moutou, Ancestromania GNU-GPL s'est sans cesse amélioré depuis Ancestrologie GPL grâce à la contribution de développeurs bénévoles, notamment de André Langlet et Matthieu Giroux.
