---
nom: "Firefox"
date_creation: "Lundi, 2 février, 2015 - 19:39"
date_modification: "mardi, 9 janvier, 2024 - 20:57"
logo:
    src: "images/logo/Firefox.png"
site_web: "https://www.mozilla.org/fr/firefox/"
plateformes:
    - "Apple iOS"
    - "Android"
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
    - "Autre"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Le navigateur web épris de liberté distribué par la Fondation Mozilla."
createurices: ""
alternative_a: "Internet Explorer, Opera, Google Chrome, Microsoft Edge, Apple Safari"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
    - "Licence publique générale limitée GNU (LGPL)"
    - "Licence Publique Mozilla (MPL)"
tags:
    - "internet"
    - "navigateur web"
    - "web"
    - "sécurité"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Mozilla_Firefox"
lien_exodus: ""
identifiant_wikidata: "Q698"
mis_en_avant: "oui"
redirect_from: "/content/firefox"
---

Diffusé depuis 2002, Mozilla Firefox est un navigateur web respectueux des standards et des données privées. Il est personnalisable grâce à des centaines d'extensions disponibles au téléchargement depuis le site officiel. Grâce aux milliers de contributeurs à ce logiciel libre, Firefox est en constante évolution et les mises à jour sont fréquentes.
À noter toutefois que la fondation Mozilla est presque entièrement financée par des accords avec de grands moteurs de recherche (comme Yahoo ou Google) pour que ces moteurs apparaissent dans la barre de recherche de Firefox. Mais ces conditions ne sont pas exclusives : en 2016, Mozilla a noué un partenariat avec Qwant qui partage les mêmes points de vue concernant le respect de la vie privée. On peut changer très facilement le réglage du moteur par défaut dans Firefox pour installer les moteurs de recherche que l'on souhaite.


