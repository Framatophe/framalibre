---
nom: "Grafana"
date_creation: "Samedi, 25 avril, 2020 - 15:24"
date_modification: "Samedi, 25 avril, 2020 - 15:48"
logo:
    src: "images/logo/Grafana.png"
site_web: "https://grafana.com/oss/grafana/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
    - "le web"
langues:
    - "English"
description_courte: "Grafana est une application web libre qui permet de créer des visualisations de données dynamiques."
createurices: "Torkel Ödegaard"
alternative_a: ""
licences:
    - "Licence Apache (Apache)"
tags:
    - "statistiques"
    - "visualisation de données"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Grafana"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/grafana"
---

Grafana est une application web libre sous licence Apache qui permet d'agréger une ou plusieurs sources de données, afin d'en réaliser des visualisations à travers de tableaux de bord qui se composent de graphiques dynamiques.
De nombreux types de bases de données sont supportés, tels que MySQL, PostgreSQL, InfluxDB et OpenTSDB.
Grafana permet également de créer des alertes personnalisées, par SMS ou par Email notamment.
L'application est extensible à souhait, grâce à un grand nombre de plug-ins disponibles.

