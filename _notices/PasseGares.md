---
nom: "PasseGares"
date_creation: "Lundi, 26 novembre, 2018 - 22:54"
date_modification: "Samedi, 28 décembre, 2019 - 15:52"
logo:
    src: "images/logo/PasseGares.png"
site_web: "https://framagit.org/JonathanMM/passegares"
plateformes:
    - "Android"
langues:
    - "Français"
    - "English"
description_courte: "Voyager à travers les gares pour tamponner votre visa !"
createurices: "JonathanMM"
alternative_a: ""
licences:
    - "Licence MIT/X11"
tags:
    - "jeu"
lien_wikipedia: ""
lien_exodus: "https://reports.exodus-privacy.eu.org/fr/reports/fr.nocle.passegares/latest/"
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/passegares"
---

PasseGares est une nouvelle forme pour découvrir de nouveaux horizons ferroviaires de façon ludique. Voyager de gare en gare afin de remplir votre visa de tampons. Le but ultime ? Visiter toutes les gares du monde !
Contient actuellement l'ensemble des transports d'Île-de-France, ainsi que Lille Métropôle, Reims, Besançon, Dijon, Montpellier, Toulouse, Le Mans, Nantes et Angers, ainsi que toutes les gares de France métropolitaine continentale. À l'étranger, la Suisse et la région de Londres sont couvertes. Vous voulez avoir l'application pour votre agglomération ? Envoyez nous votre demande par e-mail et elle fera peut-être partie des mises à jour ;)
L'application est amie de votre vie privée, aucune information sur les gares tamponnées ne sort de votre appareil :)
Le logo de l'application a été fait avec des icônes de fontawesome.io

