---
nom: "Ice Cubes"
date_creation: "mercredi, 3 janvier, 2024 - 11:31"
date_modification: "mercredi, 3 janvier, 2024 - 11:31"
logo:
    src: "images/logo/Ice Cubes.png"
site_web: "https://apps.apple.com/us/app/ice-cubes-for-mastodon/id6444915884"
plateformes:
    - "Apple iOS"
    - "Mac OS X"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Un client Mastodon pour les plateformes Apple"
createurices: "Thomas Ricouard"
alternative_a: ""
licences:
    - "Licence Apache (Apache)"
tags:
    - "mastodon"
    - "fediverse"
    - "réseau social"
    - "microblog"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

Ice Cubes est un client Mastodon écrit en SwiftUI pour les plateformes Apple (iOS, iPadOS, macOS, visionOS). Il permet de se connecter à n'importe quelle instance de Mastodon, de consulter sa timeline, ses favoris, de publier des toots, de commenter et de modifier son compte.

Le code source est disponible sur GitHub : https://github.com/Dimillian/IceCubesApp
