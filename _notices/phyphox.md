---
nom: "phyphox"
date_creation: "Mardi, 21 avril, 2020 - 11:24"
date_modification: "Lundi, 10 mai, 2021 - 14:14"
logo:
    src: "images/logo/phyphox.png"
site_web: "https://phyphox.org/"
plateformes:
    - "Android"
    - "Apple iOS"
langues:
    - "Autres langues"
description_courte: "Application qui  permet de faire des mesures de physique en utilisant les capteurs du smartphone."
createurices: "Sebastian Staacks"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "éducation"
    - "apprentissage"
    - "physique"
lien_wikipedia: ""
lien_exodus: "https://reports.exodus-privacy.eu.org/fr/reports/122168/"
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/phyphox"
---

Phyphox est une application qui  permet de faire des mesures de physique en utilisant les capteurs du smartphone.Phyphox vous donne accès aux capteurs de votre téléphone, analyse vos données et vous permet de les exporter aux formats CSV et Excel.

