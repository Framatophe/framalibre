---
nom: "Hugin"
date_creation: "Mardi, 10 février, 2015 - 17:00"
date_modification: "Mercredi, 12 mai, 2021 - 15:27"
logo:
    src: "images/logo/Hugin.png"
site_web: "http://hugin.sourceforge.net"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
description_courte: "Hugin est un logiciel de création de panoramas."
createurices: ""
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "création"
    - "graphisme"
    - "photo"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Hugin_%28logiciel%29"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/hugin"
---

Hugin est un logiciel de création de panoramas. Le principe est de « coller » plusieurs photos prises du même endroit de manière panoramique.
L'avantage d'utiliser un logiciel spécialisé pour cette tâche est qu'il propose une détection automatique des pixels en commun, qu'il va recoller les photos en utilisant une projection, et qu'il propose beaucoup d'options.
Hugin est un logiciel très puissant qui pourra faire tout ce que vous voulez. Cependant la création d'un panorama reste assez complexe et longue. Mais pas d'inquiétude, la procédure reste bien guidée.

