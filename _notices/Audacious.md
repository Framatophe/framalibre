---
nom: "Audacious"
date_creation: "Mercredi, 10 avril, 2019 - 11:52"
date_modification: "Mercredi, 10 avril, 2019 - 16:21"
logo:
    src: "images/logo/Audacious.png"
site_web: "https://audacious-media-player.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Windows"
langues:
    - "English"
description_courte: "Extrêmement léger et customisable à volonté : Audacious est au libre ce que Winamp était à Windows."
createurices: ""
alternative_a: "winamp, Windows Media Player, iTunes"
licences:
    - "Multiples licences"
tags:
    - "multimédia"
    - "lecteur audio"
    - "lecteur musique"
    - "codecs"
lien_wikipedia: "https://en.wikipedia.org/wiki/Audacious_(software)"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/audacious"
---

Audacious est un lecteur de musique puissant, modulable, incroyablement rapide et léger. Il est basé sur une série de projets (X11Amp, renommé XMMS, suivi par Beep Media Player) visant initialement à créer un équivalent libre de ce que représentait le lecteur Winamp, tout en ajoutant de nouvelles fonctionnalités, comme la gestion de multiples listes de lecture.
Ses fonctionnalités peuvent être augmentées via un grand nombre de plugins (y compris last.fm) et il supporte un très grand nombre formats audio (dont évidemment mp3, ogg, flac, aac, wma, alac, wav et midi, pour ne citer qu'eux) ainsi que de fichiers liste de lecture (dont m3u).
Présentant une interface GTK par défaut, il est peut être configuré pour utiliser des skins XMMS ou Winamp 2 (voir captures d'écran).

