---
nom: "Déjà Dup"
date_creation: "Jeudi, 22 décembre, 2016 - 16:02"
date_modification: "Jeudi, 22 décembre, 2016 - 16:02"
logo:
    src: "images/logo/Déjà Dup.png"
site_web: "https://launchpad.net/deja-dup"
plateformes:
    - "GNU/Linux"
langues:
    - "Autres langues"
description_courte: "Déjà Dup est un logiciel simplifié de sauvegarde de données. Il permet les sauvegardes programmées chiffrées."
createurices: "Michael Terry"
alternative_a: "time machine"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "sécurité"
    - "sauvegarde"
    - "chiffrement"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/d%C3%A9j%C3%A0-dup"
---

Déjà Dup est un outil de sauvegarde simplifié. C'est une interface graphique à Duplicity. Il permet de construire des sauvegardes sécurisées de vos données dans un dossier local, un disque externe, un réseau local, distant ou de type "cloud".
Il permet aussi le chiffrement complet et la protection par mot de passe de votre sauvegarde.
Déjà Dup dispose d'une interface très claire ne nécessitant aucune connaissance informatique.
Il est disponible dans la plupart des dépôts des distributions libres.

