---
nom: "Kanboard"
date_creation: "Mercredi, 7 février, 2018 - 20:06"
date_modification: "jeudi, 29 février, 2024 - 10:26"
logo:
    src: "images/logo/Kanboard.png"
site_web: "https://kanboard.org"
plateformes:
    - "le web"
    - "GNU/Linux"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Kanboard, un logiciel libre pour gérer ses projets avec la méthode Kanban"
createurices: "Frédéric Guillot"
alternative_a: "trello"
licences:
    - "Licence MIT/X11 (MIT)"
tags:
    - "kanban"
    - "gestion de projet"
    - "tableau interactif"
    - "php"
    - "auto-hébergement"
    - "todo-list"
    - "travail collaboratif"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/kanboard"
---

Kanboard est un gestionnaire de tâches visuel qui permet de gérer facilement des petits projets de manière collaborative. L'outil est particulièrement adapté aux personnes qui utilisent la méthode Kanban. On peut voir Kanboard comme une alternative (simplifiée) au logiciel propriétaire Trello.
Kanboard est un logiciel minimaliste, il se concentre uniquement sur les fonctionnalités réellement nécessaires. L'interface utilisateur est simple et claire.
D'un point de vue technique, Kanboard est une application web développée en PHP et utilise Sqlite pour enregistrer ses données. Son installation est vraiment simple et se résume à un copier/coller du code source. L'outil est prévu pour fonctionner sur une petite machine tel qu'un Raspberry Pi ou un serveur virtuel privé (VPS). Il n'y a aucune dépendance externe, le glisser-déposer des tâches utilise les nouvelles API de HTML5.
