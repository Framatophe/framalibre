---
nom: "Bibisco"
date_creation: "Samedi, 4 février, 2017 - 19:03"
date_modification: "Mercredi, 12 mai, 2021 - 15:52"
logo:
    src: "images/logo/Bibisco.png"
site_web: "http://www.bibisco.com/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Un logiciel d'aide à l'écriture pour faciliter la gestion des personnages, lieux et même du chapitrage."
createurices: "Andra Feccomandi"
alternative_a: "Scrivener"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "création"
    - "écriture"
    - "nouvelle"
    - "roman"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/bibisco"
---

Ce logiciel vous aide lors de l'écriture de vos nouvelles ou de vos romans, en gérant des fiches personnages, des fiches lieux, ainsi que leur utilisation sur les différents chapitres. À travers des questions ciblées, il vous aide à ne rien oublier et à optimiser la construction narrative en structurant les personnages, leurs interactions, les lieux récurrents, etc.
De la structure initiale de l'idée au plan de l'écriture, en passant par le détail des personnages, de leurs caractères et leurs intentions, cet outil pensé pour l'écriture vous aide à construire votre nouvelle dans les meilleures conditions.

