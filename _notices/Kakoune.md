---
nom: "Kakoune"
date_creation: "Dimanche, 15 octobre, 2017 - 13:53"
date_modification: "Dimanche, 15 octobre, 2017 - 13:57"
logo:
    src: "images/logo/Kakoune.png"
site_web: "http://kakoune.org"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
langues:
    - "English"
description_courte: "Kakoune est un éditeur de texte au design orthogonal, dans la lignée de vim."
createurices: "Maxime Coste"
alternative_a: "Notepad, Sublime Text"
licences:
    - "Domaine public"
tags:
    - "développement"
    - "traitement de texte"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/kakoune"
---

Kakoune est un éditeur de texte modal qui met l'accent sur les points suivants:
les sélections multiples sont au cœur de l'expérience, avec la possibilité de les filtrer, les aligner, les diviser, manipuler leur contenu, tout ça via des expressions rationnelles
une architecture client / serveur: plusieurs fenêtres collaborent sur une même session et leur disposition est laissée au gestionnaire habituel
l'outil tire parti de son environnement au maximum en se reposant sur le shell et ses commandes de base (ex: coreutils)
le confort de développement moderne: coloration syntaxique, aide contextuelle, completion automatique, indentation…
pour aller plus loin: personnalisation poussée des touches, ajout de nouvelles commandes, extensions…

