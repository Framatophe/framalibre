---
nom: "Maxima"
date_creation: "Mardi, 14 mars, 2017 - 17:32"
date_modification: "Mercredi, 2 octobre, 2019 - 16:49"
logo:
    src: "images/logo/Maxima.png"
site_web: "http://maxima.sourceforge.net/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
description_courte: "Maxima est un logiciel de calcul formel: à vous les polynômes, matrices et autres équations différentielles !"
createurices: ""
alternative_a: "Mathematiqua, Maple"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "science"
    - "calcul"
    - "calcul formel"
    - "calcul numérique"
    - "mathématiques"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Maxima_(logiciel)"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/maxima"
---

Maxima est un logiciel de calcul formel: il permet de faire du calcul sur les polynômes, les matrices, de faire de l'intégration, du calcul de séries, de résoudre des équations différentielles, etc, et de tracer des graphs en 2 ou 3 dimensions. Il est spécialisé dans la manipulation de symboles mais peut bien entendu produire des résultats numériques (entiers, fractions).
Il est écrit dans le language Lisp et par conséquent ses possibilités d'extension sont très riches.

