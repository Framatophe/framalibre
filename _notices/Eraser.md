---
nom: "Eraser"
date_creation: "Vendredi, 7 avril, 2017 - 22:46"
date_modification: "Samedi, 20 mai, 2017 - 09:35"
logo:
    src: "images/logo/Eraser.png"
site_web: "https://eraser.heidi.ie/"
plateformes:
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Enfin un logiciel pour réellement effacer vos données de votre ordinateur."
createurices: "Garrett Trant, Joel Low, Dennis van Lith"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "sécurité"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Eraser"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/eraser"
---

Avant tout, il faut savoir que lorsque que vous videz votre corbeille, les fichiers que vous venez de supprimer ne sont pas réellement supprimés, c'est juste que votre ordinateur ne sait plus les retrouver et note que l'espace qui était occupé par vos fichiers est de nouveau disponible . C'est pour cela qu'il existe des programmes capables de retrouver des fichiers que vous avez malencontreusement effacés.
Imaginez maintenant que vous souhaitez vous séparer de votre ordinateur en le vendant sur un site de petites annonces. Rien n’empêchera l'acquéreur de votre machine de rechercher les fichiers que vous aviez déposés dessus. Et cela même si vous l'avez réinitialisée.
C'est la qu’intervient Eraser, il va vous permettre d'effacer réellement vos données en les écrasant plusieurs fois avec de fausses données, ainsi, il va rendre vos fichiers irrécupérables, un peu comme si vous aviez gommé puis réécris puis re-gommer du texte sur une feuille de papier.

