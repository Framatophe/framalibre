---
nom: "Shotwell"
date_creation: "Mercredi, 18 janvier, 2017 - 13:46"
date_modification: "Mercredi, 14 décembre, 2022 - 19:03"
logo:
    src: "images/logo/Shotwell.png"
site_web: "https://wiki.gnome.org/Apps/Shotwell"
plateformes:
    - "GNU/Linux"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Un logiciel de gestion de photothèque avancé, avec des options de retouche."
createurices: ""
alternative_a: "iPhoto, Photos"
licences:
    - "Licence publique générale limitée GNU (LGPL)"
tags:
    - "multimédia"
    - "photothèque"
    - "photo"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Shotwell"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/shotwell"
---

Shotwell est un gestionnaire de photos associé au projet GNOME. Il permet d'importer et d'organiser de larges bibliothèques de photos, et de les trier par date, nom, ou par étiquette (tag). Shotwell contient aussi quelques options de retouche basique : rotation, recadrage, ajustement automatique, yeux rouges, ajustements, redressement, etc., qui permettent de ne pas avoir à ouvrir un logiciel de traitement spécialisé comme GIMP. Shotwell permet aussi de basculer en mode "Diaporama", pratique pour les présentations de photos, et offre un outil permettant d'écrire des métadonnées dans une photo (commentaire, tag, etc.). Vous pouvez aussi importer des photos de n'importe quelle location, par exemple un appareil photo que vous auriez branché, ou encore une carte mémoire. Shotwell supporte les formats d'image suivants : TIFF, JPEG, PNG, BMP, RAW, ainsi que les fichiers vidéo. Il intègre aussi un gestionnaire de plugins, et peut exporter vos photos vers différents réseaux sociaux.

