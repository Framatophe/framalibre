---
nom: "uBlock Origin"
date_creation: "Mardi, 17 janvier, 2017 - 23:32"
date_modification: "Mercredi, 27 juillet, 2022 - 16:24"
logo:
    src: "images/logo/uBlock Origin.png"
site_web: "https://github.com/gorhill/uBlock"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Autre"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Une extension vous laissant le choix de bloquer, ou non, les publicités des sites web que vous rencontrez."
createurices: "Raymond Hill"
alternative_a: "AdBlock, Adblock Plus, Ghostery, AdBlocker Ultimate, AdGuard AdBlocker, AdBlocker for YouTube™"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
    - "vie privée"
    - "anonymat"
    - "bloqueur de publicité"
    - "add-on"
    - "extension"
    - "navigateur web"
lien_wikipedia: "https://fr.wikipedia.org/wiki/UBlock_Origin"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/ublock-origin"
---

• uBlock Origin est une extension simple d'utilisation (avec un bouton "ON / OFF"), vous permettant d'avoir le choix de bloquer, ou non, les publicités provenant des sites web que vous rencontrez.
• Ainsi, cela vous permet d'être maître de ce qui peut s'afficher dans votre navigateur web, et voici quelques exemples d'utilisation :
"OFF" : Soutenir un moteur de recherche écologique, en laissant afficher les premiers résultats sponsorisés.
"OFF" : Soutenir une chaîne YouTube, en visionnant les publicités avant la vidéo.
"ON" : Bloquer les différentes nuisances publicitaires de la part des sites web qui vous gênent.
• De plus, l'extension vous permet d'être protégé contre un certain nombre de pisteurs et de domaines malveillants (via des listes de filtrage ajoutées à l'extension, et qui sont maintenues fréquemment à jour par la communauté).
• uBlock Origin fait partie des extensions recommandées par Mozilla Firefox.

