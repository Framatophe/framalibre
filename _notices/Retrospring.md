---
nom: "Retrospring"
date_creation: "Samedi, 11 juillet, 2020 - 18:09"
date_modification: "Samedi, 11 juillet, 2020 - 18:09"
logo:
    src: "images/logo/Retrospring.png"
site_web: "https://retrospring.net/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "FirefoxOS"
    - "Windows Mobile"
    - "Apple iOS"
    - "Autre"
    - "le web"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Proposez à votre entourage virtuel de vous poser des questions et répondez-y !"
createurices: "nilsding, pixeldesu"
alternative_a: "curiouscat, curiouscat.me, curiouscat.qa"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "social"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/retrospring"
---

Avec Retrospring, vous pouvez proposer à votre entourage de vous poser des questions et y répondre. Les questions restent privée tant que vous n'y avez pas répondu, et peuvent être publiées (avec les réponses) si vous avez activé l'option dans vos préférences.
Il n'est pas obligatoire d'avoir un compte sur la plateforme pour poser une question, si vous n'avez pas désactivé l'option.
Curiouscat est une alternative propriétaire à ce logiciel open-source, libre, et génial \o/

