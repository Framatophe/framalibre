---
nom: "Pygbag"
date_creation: "mercredi, 8 novembre, 2023 - 11:19"
date_modification: "mercredi, 8 novembre, 2023 - 11:19"
logo:
    src: "images/logo/Pygbag.png"
site_web: "https://pygame-web.github.io"
plateformes:
    - "Web"
    - "Apple iOS"
    - "Android"
    - "Windows"
    - "GNU/Linux"
langues:
    - "English"
description_courte: "Outil de portage pour langage Python vers le Navigateur ( via WebAssembly) Terminal / 2D (pygame/raylib) / 3D ( Panda3D / Harfang3D ) / Matplotlib et bien d'autres ...."
createurices: "pythonseverywhere"
alternative_a: "pyodide replit brython"
licences:
    - "Licence MIT/X11 (MIT)"
tags:
    - "python"
    - "navigateur web"
    - "webassembly"
    - "2D"
    - "3D"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Outil de portage pour langage Python 3 vers le Navigateur (via WebAssembly) 

Terminal : ncurses batgrl pyTermTk

2D : pygame-ce raylib matplotlib et bien d'autres ....

3D : Panda3D Harfang3D

Moteur physiques : bullet3 ode chipmunk box2d nova-physic
Connectivité : Utilisable avec Arduino (wifi préféré) pour les labo.

