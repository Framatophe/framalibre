---
nom: "Lagrange"
date_creation: "mercredi, 10 avril, 2024 - 20:31"
date_modification: "mercredi, 10 avril, 2024 - 20:31"
logo:
    src: "images/logo/Lagrange.png"
site_web: "https://gmi.skyjake.fi/lagrange/"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Un client Gemini graphique"
createurices: "skyjake"
alternative_a: ""
licences:
    - "Berkeley Software Distribution License (BSD)"
tags:
    - "gemini"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

Lagrange est le meilleur client Gemini graphique. Simple, léger, rapide, un plaisir pour naviguer sur les sites (les capsules) de Gemini.
