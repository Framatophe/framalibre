---
nom: "Claws Mail"
date_creation: "Mercredi, 21 décembre, 2016 - 20:23"
date_modification: "Mercredi, 12 mai, 2021 - 16:25"
logo:
    src: "images/logo/Claws Mail.png"
site_web: "http://www.claws-mail.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Un client de courriel léger, rapide et hautement configurable."
createurices: ""
alternative_a: "Microsoft Outlook, Mail"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
    - "client mail"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Claws_Mail"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/claws-mail"
---

Le développement de Claws Mail (anciennement nommé Sylpheed-Claws) est basé sur les principes de maniabilité, facilité d'utilisation et compatibilité. La légèreté de ce client de courriel lui permet de tourner sur des ordinateurs peu puissants, mais il sait s'intégrer au mieux à tous les environnements de bureau.
Claws Mail supporte la majorité des protocoles dans le domaine du courrier électronique, avec un accent sur la configuration de la sécurité des transmissions (y compris GNU GPG). Plusieurs modules gérant l'affichage des courriels, des pièces jointes ou ajoutant d'autres fonctionnalités, sont installables facilement, au choix de l'utilisateur. Il en va de même pour les nombreux thèmes d'affichage disponibles. La gestion de l'apparence  (fenêtres, courriels, couleurs, etc.) se fait avec un nombre important d'options.

