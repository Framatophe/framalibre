---
nom: "Nicotine+"
date_creation: "Mardi, 27 décembre, 2016 - 22:30"
date_modification: "Mercredi, 31 août, 2022 - 23:12"
logo:
    src: "images/logo/Nicotine+.png"
site_web: "http://nicotine-plus.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Un client Soulseek, réseau de téléchargement pair-à-pair spécialisé pour la musique."
createurices: "gfarmerfr"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
    - "Licence publique générale limitée GNU (LGPL)"
tags:
    - "internet"
    - "p2p"
    - "téléchargement de musique"
    - "téléchargement"
lien_wikipedia: "https://fr.wikipedia.org/wiki/SoulSeek"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/nicotine"
---

Nicotine, ou Nicotine+, est un client Soulseek de téléchargement de fichiers pair-à-pair. Soulseek est à la fois le nom du réseau d'échange de fichiers mono-source (on télécharge chez une personne) et le nom du logiciel client officiel.
Le réseau Soulseek est particulièrement apprécié par les amateurs de musiques alternatives ou underground, qui sont moins présentes sur les réseaux P2P plus connus.
Nicotine permet également de:
discuter entre utilisateurs,
discuter sur des groupes,
voir les fichiers partagés par un-e utilisateur-trice,
rentrer des listes de mots-clefs pour ses goûts et de voir des suggestions de groupes, selon les informations d'utilisateurs similaires,
établir des "listes de vœux" lorsqu'un fichier est dur à trouver,
gérer des droits d'accès pour listes d'amis,
etc

