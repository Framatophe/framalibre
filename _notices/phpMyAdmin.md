---
nom: "phpMyAdmin"
date_creation: "Mardi, 10 janvier, 2017 - 12:38"
date_modification: "Lundi, 10 mai, 2021 - 13:54"
logo:
    src: "images/logo/phpMyAdmin.jpg"
site_web: "https://www.phpmyadmin.net/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Interface utilisateur libre, développée en PHP, qui masque la complexité de gestion des bases de données."
createurices: ""
alternative_a: ""
licences:
    - "FSF Unlimited License"
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
    - "gestionnaire de base de données"
    - "base de données"
lien_wikipedia: "https://fr.wikipedia.org/wiki/PhpMyAdmin"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/phpmyadmin"
---

phpMyAdmin est une interface utilisateur libre qui masque la complexité de gestion des bases de données. Développé en langage PHP, le gestionnaire permet facilement de créer, modifier, consulter ou supprimer les bases de données MYSQL ou MARIADB ainsi que leurs composants (tables de données, index, champs de données, ...).
phpMyAdmin est disponible sur la majeure partie des distributions linux. On le trouve aussi sur toutes les plateformes de développement Web de type AMP (WAMP, LAMP, XAMP, ...). Il s'installe facilement. La sécurisation de l'interface reste néanmoins un peu plus compliquée.
PhpMyAdmin autorise la gestion d'une ou plusieurs bases de données réparties sur un ou plusieurs serveurs.

