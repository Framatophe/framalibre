---
nom: "Signal"
date_creation: "Jeudi, 22 décembre, 2016 - 00:52"
date_modification: "Mercredi, 12 mai, 2021 - 16:16"
logo:
    src: "images/logo/Signal.png"
site_web: "https://signal.org"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "Apple iOS"
    - "Autre"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Application de messagerie et téléphonie mobile respectueuse de la vie privée."
createurices: ""
alternative_a: "Facebook Messenger, WhatsApp, iMessage, Google Hangouts, Viber, WeChat"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
    - "messagerie instantanée"
    - "téléphonie sur ip"
    - "vie privée"
    - "chiffrement"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Signal_(application)"
lien_exodus: "https://reports.exodus-privacy.eu.org/fr/reports/search/org.thoughtcrime.secures…"
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/signal"
---

Signal remplace avantageusement des applications de messagerie et téléphonie mobile par internet comme Facebook Messenger, WhatsApp et Skype, avec les avantages d'offrir le chiffrement de bout en bout par défaut, d'être un logiciel libre, et d'être sécurisé par défaut.
Signal permet de créer des conversations 1 à 1 et de groupes, et de passer des appels audio et vidéo de 1 à 1 et de groupes. La messagerie offre des fonctionnalités similaires à ses concurrents, comme les emojis, le partage de photos, vidéos, documents, localisations, gif animés, contacts, etc. Chaque fonctionnalité est conçue de manière à respecter la vie privée des utilisateurs, à protéger ses communications, et à avoir une bonne sécurité par défaut.
Le chiffrement utilisé pour envoyer messages et appels audio est généralement considéré comme étant le meilleur actuellement.

