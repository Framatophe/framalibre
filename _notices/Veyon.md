---
nom: "Veyon"
date_creation: "Mardi, 14 mai, 2019 - 09:43"
date_modification: "Mardi, 14 mai, 2019 - 09:55"
logo:
    src: "images/logo/Veyon.png"
site_web: "https://veyon.io/"
plateformes:
    - "GNU/Linux"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Pour que l'enseignant puisse prendre la main sur des ordinateurs d'une salle informatique."
createurices: "Tobias Doerffel"
alternative_a: "iTALC, MasterEye, Netop School, Netsupport"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "éducation"
    - "gestion de parc informatique"
lien_wikipedia: "https://fr.wikipedia.org/wiki/ITALC"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/veyon"
---

Veyon est un logiciel libre permettant à un professeur de prendre la main pour effectuer des démonstrations sur les postes des étudiants dans une salle de cours informatisée mise en réseau. Il succède à iTALC.

