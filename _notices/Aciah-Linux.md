---
nom: "Aciah-Linux"
date_creation: "Mardi, 17 septembre, 2019 - 08:46"
date_modification: "Lundi, 10 janvier, 2022 - 21:09"
logo:
    src: "images/logo/Aciah-Linux.jpg"
site_web: "https://aciah-linux.org"
plateformes:
    - "GNU/Linux"
langues:
    - "Français"
description_courte: "Un système simple, gratuit, mettant l'informatique à la portée de tous, avec une nouveauté : le Aciah-Menu."
createurices: "Christophe Lévêque, modifié par l'équipe de ACIAH"
alternative_a: "Microsoft Windows, MacOS"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/aciah-linux"
---

Aciah-Linux est une distribution adaptée aux débutants de tout âge, intégrant des fonctions facilitatrices, une base complète de logiciels pour quasiment tous les domaines. Elle est surtout et principalement une distribution destinée aux personnes en situation de handicap, notamment visuel, et propose pour cela un grand nombre d’outils et de développements pour la compensation du handicap.
Sa simplicité d’utilisation et l’importance des logiciels proposés, en fait une distribution de choix pour tous les utilisateurs, y compris les utilisateurs chevronnés.
Base logicielle : Les principaux :
- navigateur Firefox avec mode lecture
- messagerieThunderbird.
- lecteur d’écran ORCA,
- suite libre-Office
- éditeur de texte Mousepad
- machine à lire
- lecteur de musique ou de vidéo, lecteur Daisy,
- The Gimp, Audacity,
L'association ACIAH propose en outre des ateliers d'initiation, un accompagnement personnalisé, et des formations en informatique adaptée

