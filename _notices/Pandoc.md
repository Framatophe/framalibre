---
nom: "Pandoc"
date_creation: "Jeudi, 29 décembre, 2016 - 16:15"
date_modification: "mardi, 26 décembre, 2023 - 13:49"
logo:
    src: "images/logo/Pandoc.png"
site_web: "http://pandoc.org"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "English"
description_courte: "Convertissez toutes sortes de documents avec ce couteau suisse multi-formats."
createurices: "John MacFarlane"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
    - "format"
    - "markdown"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Pandoc"
lien_exodus: ""
identifiant_wikidata: "Q2049294"
mis_en_avant: "oui"
redirect_from: "/content/pandoc"
---

Pandoc est un puissant logiciel de conversion de documents en ligne de commande.  Il convertit vers et depuis de multiples formats. Mais son intérêt réside surtout dans l’automatisation de tâches et la possibilité d’utiliser des modèles (templates) pour obtenir des produits de conversion. Ainsi, par exemple, il est possible d’éditer un fichier dans un format Markdown amélioré (le « Pandoc Markdown ») et obtenir des sorties LaTeX, HTML, ePub, ODT ou DOCX avec des mises en page prédéfinies. De même, en utilisant LaTeX, il est possible de produire des documents PDF (ou même des présentations) en se contentant de rédiger un document en Markdown et sans toucher une ligne de LaTeX. La commande Pandoc supporte plusieurs options servant à conditionner la conversion ou mettre en page la sortie demandée.


