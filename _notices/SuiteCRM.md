---
nom: "SuiteCRM"
date_creation: "Mercredi, 5 avril, 2017 - 21:10"
date_modification: "Mardi, 11 mai, 2021 - 23:14"
logo:
    src: "images/logo/SuiteCRM.png"
site_web: "https://suitecrm.com/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "SuiteCRM est un outil de gestion de la relation client (GRC) - en Anglais CRM-, basé sur le célèbre SugarCRM."
createurices: "SalesAgility"
alternative_a: "sugarcrm, vtiger"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "métiers"
    - "crm"
    - "gestion de la relation client"
lien_wikipedia: "https://en.wikipedia.org/wiki/SuiteCRM"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/suitecrm"
---

SuiteCRM a été développé à partir de 2015 suite à l'annonce par l'éditeur SugarCRM de son intention de ne plus mettre à jour sa version communautaire du produit Sugar.
Les équipes de Sales Agility ont repris le flambeau au sein d'une nouvelle version en y ajoutant des fonctionnalités inédites, en modernisant le code technique, et en incluant une toute nouvelle interface utilisateur.
Pour gérer son fichier client/prospect, ses devis, et automatiser la force de vente, le marketing et le département support des entreprises, SuiteCRM est très simple d'usage et son architecture ouverte permet de le personnaliser à volonté.

