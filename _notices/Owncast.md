---
nom: "Owncast"
date_creation: "jeudi, 28 décembre, 2023 - 20:01"
date_modification: "jeudi, 28 décembre, 2023 - 20:01"
logo:
    src: "images/logo/Owncast.svg"
site_web: "https://owncast.online/"
plateformes:
    - "le web"
langues:
    - "English"
description_courte: "Owncast est un serveur de diffusion vidéo et de tchat en direct gratuit et libre, compatible avec les logiciels de diffusion populaires existants."
createurices: "Gabe Kangas"
alternative_a: "Twitch, YouTube, PeerTube"
licences:
    - "Licence MIT/X11 (MIT)"
tags:
    - "vidéo"
    - "fediverse"
    - "streaming"
    - "décentralisation"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: "Q104533790"
mis_en_avant: "non"

---

Owncast est un serveur de diffusion vidéo et de tchat en direct gratuit et libre, compatible avec les logiciels de diffusion populaires existants.
Le principe du logiciel est d'offrir un site de streaming pour une seule personne qui soit similaire aux offres grand public. Il offre le plein contrôle du contenu, de l'interface, de la modération et de l'audience.
Il peut être installé par un·e administrateurice web, soit directement sur son serveur, soit de manière simplifiée chez certains hébergeurs.
