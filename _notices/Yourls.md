---
nom: "Yourls"
date_creation: "jeudi, 14 mars, 2024 - 11:08"
date_modification: "jeudi, 14 mars, 2024 - 11:08"
logo:
    src: "images/logo/Yourls.svg"
site_web: "https://yourls.org/"
plateformes:
    - "le web"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "YOURLS est un puissant paquet de scripts PHP pour vous permettre de mettre en œuvre, sur votre serveur, votre propre raccourcisseur de liens.."
createurices: "YOURLS Contributors"
alternative_a: "bit.ly, goo.gl"
licences:
    - "Licence MIT/X11 (MIT)"
tags:
    - "raccourci"
    - "url"
    - "anonymat"
    - "raccourcisseur de liens"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

YOURLS (Your Own URL Shortener) est un raccourcisseur de liens avec un espace de gestion (en bref, vous pouvez corriger un ancien lien tout en gardant le même lien raccourci…).

Très facile à installer sur son propre serveur et à mettre à jour si besoin. Un incontournable pour qui a besoin de donner des liens cours par téléphone ou lors de visios.
