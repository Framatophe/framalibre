---
nom: "Bitwarden"
date_creation: "Lundi, 5 février, 2018 - 16:23"
date_modification: "dimanche, 14 janvier, 2024 - 21:46"
logo:
    src: "images/logo/Bitwarden.png"
site_web: "https://bitwarden.com/"
plateformes:
    - "le web"
    - "Apple iOS"
    - "Android"
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Résolvez tous vos problèmes de gestion de mots de passe. Une solution  simple et sécurisée."
createurices: "Kyle Spearrin"
alternative_a: "1password, dashlane, enpass, SafeInCloud, lastpass, Pleasant Password Server"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "sécurité"
    - "gestionnaire de mots de passe"
    - "authentification"
    - "mot de passe"
    - "auto-hébergement"
lien_wikipedia: "https://fr.m.wikipedia.org/wiki/Bitwarden"
lien_exodus: "https://reports.exodus-privacy.eu.org/fr/reports/search/com.x8bit.bitwarden/"
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/bitwarden"
---

Bitwarden vous propose un espace sécurisé dans le cloud (gratuit pour les particuliers) pour stocker et synchroniser votre coffre-fort (vault) accessible via navigateur, mobile, web. Vous pouvez également vous auto-hébergez en récupérant le code 100% open-source. Une installation via Docker est également possible.
Les principales fonctions de Bitwarden :
- Partage sécurisé
- Groupes d'utilisateurs
- Collections
- Synchronisation
- Stockage de fichiers (version payante ou auto-hébergement)
- Données exportables au format JSON.



