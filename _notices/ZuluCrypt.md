---
nom: "ZuluCrypt"
date_creation: "mardi, 16 avril, 2024 - 17:00"
date_modification: "mercredi, 17 avril, 2024 - 13:14"


site_web: "https://mhogomchungu.github.io/zuluCrypt/"
plateformes:
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "ZuluCrypt permet de créer et d'accéder à des conteneurs chiffrés (disques, clés USB...), mais aussi de vérifier leur intégrité."
createurices: "mhogomchungu"
alternative_a: "TrueCrypt, VeraCrypt"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "chiffrement"
    - "sécurité"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

ZuluCrypt est conçu pour fournir aux utilisateurs à la fois une interface graphique et une interface en ligne de commande pour une gestion facile des dispositifs de stockage chiffrés. 

Il permet de gérer les volumes chiffrés avec LUKS (Linux Unified Key Setup), crypt ou TrueCrypt. 

ZuluCrypt est principalement utilisé pour chiffrer les disques durs, les clés USB et créer/lire des fichiers chiffrés avec LUKS, TrueCrypt ou GNUPGP. 

ZuluCrypt se compose de deux composants principaux : le backend en ligne de commande appelé zuluCrypt-cli, qui est écrit en C et dépend des bibliothèques zuluCrypt-cli, et le frontend graphique appelé zuluCrypt-gui, qui est une application Qt écrite en C++/Qt. Les composants graphiques sont optionnels et le logiciel peut être utilisé en ligne de commande uniquement.

A noter que ZuluCrypt est actuellement disponible uniquement pour les systèmes d'exploitation GNU/Linux (il est disponible sous forme de packages pour différentes distributions, y compris Ubuntu/Debian) et ne prend pas en charge les autres plateformes.
