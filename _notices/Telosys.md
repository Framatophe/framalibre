---
nom: "Telosys"
date_creation: "Vendredi, 24 juin, 2022 - 12:25"
date_modification: "Vendredi, 24 juin, 2022 - 12:25"
logo:
    src: "images/logo/Telosys.png"
site_web: "https://www.telosys.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
description_courte: "N’écrivez plus le code répétitif de vos projets, Telosys le fait pour vous."
createurices: ""
alternative_a: ""
licences:
    - "Licence publique générale limitée GNU (LGPL)"
tags:
    - "développement"
    - "code"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Telosys"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/telosys"
---

Telosys est un générateur de code open source simple et léger.
Il peut être utilisé pour générer tout type de code source, quel que soit le langage (Java, PHP, Python, Javascript, Golang, etc…)
Il est souvent utilisé pour initialiser les projets, il permet de démarrer rapidement en réduisant la charge de développement liée aux tâches répétitives.

