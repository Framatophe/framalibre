---
nom: "VotAR"
date_creation: "Dimanche, 21 mai, 2017 - 19:53"
date_modification: "Lundi, 10 mai, 2021 - 13:31"
logo:
    src: "images/logo/VotAR.png"
site_web: "https://votar.libre-innovation.org/index.fr.html"
plateformes:
    - "Android"
langues:
    - "Français"
    - "English"
description_courte: "VotAR est un système de Vote en Réalité augmenté (ou de sondage d'audience)."
createurices: "Stéphane Poinsart"
alternative_a: "Plickers"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "bureautique"
    - "sondage"
    - "vote"
    - "travail collaboratif"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/votar"
---

VotAR est un système de Vote en Réalité augmenté (ou de sondage d'audience). Votre public tient des feuilles imprimées avec un symbole précis, et en tant qu'orateur vous en prenez une photo sur votre smartphone avec cette application Android, qui l'analysera et comptera les votes.

