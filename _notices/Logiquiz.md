---
nom: "Logiquiz"
date_creation: "Mercredi, 28 juillet, 2021 - 15:01"
date_modification: "Mercredi, 28 juillet, 2021 - 17:36"
logo:
    src: "images/logo/Logiquiz.png"
site_web: "https://ladigitale.dev/logiquiz/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Créez des contenus interactifs H5P pour la classe en présence et à distance."
createurices: "Emmanuel ZIMMERT"
alternative_a: "Anki, eXeLearning"
licences:
    - "Licence Apache (Apache)"
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "éducation"
    - "exerciseur"
    - "présentation"
    - "contenu interactif"
    - "h5p"
    - "qcm"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/logiquiz"
---

La puissance des outils H5P
H5P est la référence du logiciel libre pour la création de contenus interactifs. La boîte à outils H5P contient plus de 40 types d'activités : quiz, vidéos interactives, textes à trous, présentations, mots mêlés, etc.
Une utilisation facilitée
Logiquiz simplifie l'accès aux outils de création H5P en permettant la création et la lecture de contenus hors ligne sur votre ordinateur Windows, macOS ou Linux sans avoir à passer par une plateforme Moodle ou un CMS.
Pour lire les contenus en ligne et générer un lien de partage, il y a Digiquiz.
L'outil Logiquiz est similaire à LUMI.

