---
nom: "MongoDB"
date_creation: "Jeudi, 16 février, 2017 - 16:37"
date_modification: "Lundi, 10 mai, 2021 - 13:53"
logo:
    src: "images/logo/MongoDB.jpg"
site_web: "https://www.mongodb.com/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Windows"
    - "Autre"
langues:
    - "Autres langues"
description_courte: "MongoDB, une base de données NoSQL."
createurices: ""
alternative_a: "Oracle NoSQL, DynamoDB, BigTable"
licences:
    - "Licence Apache (Apache)"
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "système"
    - "base de données"
    - "manipulation de données"
lien_wikipedia: "https://fr.wikipedia.org/wiki/MongoDB"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/mongodb"
---

MongoDB est un système de gestion de bases de données orienté documents. Il fait partie de la catégorie des bases de données dites NoSQL par rapport aux bases de données dites SQL.
MongoDB est une base de données qui offre des performances élevées ainsi que de la haute disponibilité.
Les documents sont des enregistrements à taille variable qui associent des valeurs à des noms (ex : {Nom: "Jean", Age: 36}). Les couples "nom/valeur" ainsi obtenus sont plus proche des langages de programmation courant (PHP, C, ...).
Le langage d'interrogation est assez riche. Il est basé sur les opérations traditionnelles - (Création, suppression, mise à jour, lecture et écriture de masse), l'agrégation, la recherche de texte et les requêtes géospatiales.

