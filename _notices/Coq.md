---
nom: "Coq"
date_creation: "Mardi, 3 mars, 2015 - 22:15"
date_modification: "Jeudi, 29 décembre, 2016 - 22:55"
logo:
    src: "images/logo/Coq.png"
site_web: "https://coq.inria.fr/"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "English"
description_courte: "Logiciel assistant de preuve Utilisant le langage Gallina (par Inria)."
createurices: "Thierry Coquand"
alternative_a: ""
licences:
    - "Licence publique générale limitée GNU (LGPL)"
tags:
    - "science"
    - "mathématiques"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Coq_%28logiciel%29"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/coq"
---

Le logiciel Coq est un assistant à la preuve formelle développé à l'Inria. Il est fondé sur le Calcul des Constructions de Thierry Coquand, une théorie des types d'ordre supérieur, et exploite largement la correspondance de Curry-Howard.
Coq peut manipuler des énoncés du calcul et permet de faire une vérification mécaniques de ces énoncés. Il peut aussi aider à la recherche de preuves formelles d'énoncés mathématiques (par exemple, le théorème des quatre couleurs) ainsi que à la vérification formelle de programmes informatiques (par exemple, le compilateur CompCert C).

