---
nom: "DarkMoon"
date_creation: "Dimanche, 18 août, 2019 - 17:21"
date_modification: "Dimanche, 18 août, 2019 - 17:21"
logo:
    src: "images/logo/DarkMoon.png"
site_web: "https://www.dark-moon.org/"
plateformes:
    - "Windows"
langues:
    - "English"
description_courte: "Dark Moon est un sous-système portable émulant les fonctionnalités POSIX grâce à Cygwin.dll."
createurices: "Mehdi Boutayeb Ferkatou"
alternative_a: "WSL, cygwin, virtualbox, VMWARE"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "développement"
    - "linux"
    - "windows"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/darkmoon"
---

Framalibre:
DarkMoon est un sous système Linux portable libre pour Windows sous licence GNU GPv2 d créé en 2013 sur la base de Cygwin.
L’ensemble des fonctionnalités POSIX sont des applications Linux converties en Win32 piloté par le noyau de DarkMoon avec l'aide de Cygwin.dll à bas niveau sur la couche système.
DarkMoon possède une riche bibliothèques de fonctionnalités Linux.
C'est un utilitaire qui a la grande particularité d'être le seul sous système Linux pour Windows à être aussi portable.
N'utilisant pas d'Hyperviseur pour virtualiser un système, DarkMoon est natif et fonctionne entièrement avec du binaire Windows et peut être lancé en mode console ou en mode GUI avec XFCE.
DarkMoon est largement utilisé comme support de développement logiciel Linux libre sous Windows intégrant de nombreux compilateurs (Perl, Python, TCL, C, C++, Fortran, Ada..).
Les commandes Linux habituelles sont présentes (ls, grep, mc, rsync, wget, git..)

