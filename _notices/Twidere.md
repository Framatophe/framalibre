---
nom: "Twidere"
date_creation: "Jeudi, 9 février, 2017 - 22:50"
date_modification: "Jeudi, 9 février, 2017 - 22:50"
logo:
    src: "images/logo/Twidere.png"
site_web: "https://github.com/TwidereProject/"
plateformes:
    - "Android"
    - "Apple iOS"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Application de micro-blogging Twitter sous Android et iOS."
createurices: "Mariotaku Lee"
alternative_a: "Twitter, tweetdeck, tweetbot, echofon, twitterrific"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
    - "twitter"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/twidere"
---

Twidere est un client Twitter/StatusNet/Fanfou libre, sans publicité, muti-comptes et totalement personnalisable. Il comprend un mode d'affichage de nuit, des onglets personnalisable, des filtres de désactivations, entre autres choses.

