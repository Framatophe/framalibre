---
nom: "Akira"
date_creation: "Samedi, 15 février, 2020 - 03:58"
date_modification: "Samedi, 15 février, 2020 - 03:58"
logo:
    src: "images/logo/Akira.png"
site_web: "https://github.com/akiraux/Akira"
plateformes:
    - "GNU/Linux"
langues:
    - "English"
description_courte: "Logiciel Linux d'UX design."
createurices: "Alessandro Castellani"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "création"
    - "ux design"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/akira"
---

Akira est un logiciel natif Linux de design de l'expérience utilisateur (UX design).
Akira propose une approche moderne et rapide de la conception de l'interface utilisateur. Ce logiciel s'adresse donc principalement aux graphistes et concepteurs Web. L'objectif est d'offrir une solution valide et professionnelle aux concepteurs qui souhaitent utiliser Linux comme système d'exploitation principal.

