---
nom: "Tableaunoir"
date_creation: "Mercredi, 4 novembre, 2020 - 13:00"
date_modification: "Mercredi, 4 novembre, 2020 - 13:00"
logo:
    src: "images/logo/Tableaunoir.png"
site_web: "https://tableaunoir.github.io/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "FirefoxOS"
    - "Autre"
langues:
    - "English"
description_courte: "Tableaunoir est un tableau interactif en ligne. Des \"magnets\" permettent de créer du contenu interactif."
createurices: "François Schwarzentruber"
alternative_a: "jamboard"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "éducation"
    - "tableau interactif"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/tableaunoir"
---

Tableaunoir est un tableau interactif en ligne. Des "magnets" permettent de créer du contenu interactif. L'outil offre quelques exemple de magnets. On peut en créer en les dessinant directement dans l'outil ou en important des images.  La barre d'outil peut être caché pour que l'auditoire se concentre sur le contenu. Un tableau peut être sauvegardé. On peut aussi partager un tableau pour écrire à plusieurs.
Voici une vidéo qui montre les fonctionnaltés de l'outil :https://www.youtube.com/watch?v=P6_lhqiPBow

