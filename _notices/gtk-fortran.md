---
nom: "gtk-fortran"
date_creation: "Mercredi, 20 mars, 2019 - 17:44"
date_modification: "Lundi, 25 avril, 2022 - 12:23"
logo:
    src: "images/logo/gtk-fortran.png"
site_web: "https://github.com/vmagnin/gtk-fortran/wiki"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
description_courte: "gtk-fortran permet de créer des interfaces graphiques GTK multi-plateformes en Fortran, et d'utiliser la GLib."
createurices: "Vincent Magnin et al."
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "science"
    - "graphisme"
    - "gtk"
    - "fortran"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/gtk-fortran"
---

Le projet gtk-fortran, sous licence GNU GPL 3, permet aux programmeurs Fortran de créer des interfaces graphiques GTK multi-plateformes directement en Fortran. Il utilise les capacités d'interfaçage avec le C du Fortran moderne. Environ 10000 fonctions GTK 4 sont disponibles (GTK, GDK, GdkPixbuf, Cairo, Pango, GLib, GObject, GIO...).
A noter que gtk-fortran donne également accès aux fonctions de la librairie GLib et de la librairie de tracé scientifique PLplot.
gtk-fortran est développé sous Linux, mais fonctionne également sous Windows via MSYS2, sous BSD et macOS. Il peut être installé avec CMake, conda ou utilisé comme dépendance fpm (Fortran Package Manager).

