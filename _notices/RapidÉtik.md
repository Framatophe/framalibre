---
nom: "RapidÉtik"
date_creation: "Mercredi, 29 novembre, 2023 - 07:38"
date_modification: "Mercredi, 29 novembre, 2023 - 07:45"
logo:
    src: "images/logo/RapidÉtik.png"
site_web: "https://achampollion.forge.aeif.fr/rapidetik/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "FirefoxOS"
    - "Windows Mobile"
    - "Apple iOS"
langues:
    - "Français"
description_courte: "RapidÉtik permet de créer très rapidement une exercice de type \"phrase mélangée\"."
createurices: "Arnaud Champollion"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "éducation"
    - "dictée"
    - "lecture"
    - "cycle 2"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/rapid%C3%A9tik"
---

RapidÉtik permet de créer très rapidement une exercice de type "phrase mélangée".
On entre une phrase, et l'application crée une série d'étiquettes déplaçables, que l'élève est invité à positionner sur les emplacements, puis à valider.
La phrase de référence peut être écoutée via synthèse vocale, ainsi que chaque étiquette séparément.
On peut partager l'exercice créé par lien / QR-code. Rien n'est stocké en ligne, le contenu de l'exercice est contenu dans l'URL.
Possibilité d'imprimer l'exercice en format étiquettes à découper, avec consigne et nombre d'exemplaires paramétrables, ainsi que l'ajout de QR-code de correction (fonctionne hors-ligne) et d'accès à l'exercice en ligne.
Et rien n'mpêche de le détourner pour des phrases en anglais, de la numération (série de nombres à ranger) ou tout autre type de série (par exemple les planètes du Système Solaire), il suffit de séparer les éléments par un espace.

