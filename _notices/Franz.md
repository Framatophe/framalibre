---
nom: "Franz"
date_creation: "Mardi, 27 septembre, 2022 - 23:30"
date_modification: "Mercredi, 28 septembre, 2022 - 02:35"
logo:
    src: "images/logo/Franz.png"
site_web: "https://meetfranz.com"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Franz est un client de messagerie qui supporte plusieurs réseaux sociaux différents ..."
createurices: ""
alternative_a: "rambox, hootsuite, tweetdeck, crowdfire"
licences:
    - "Licence Apache (Apache)"
tags:
    - "internet"
    - "client mail"
    - "communication"
    - "twitter"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/franz"
---

Franz est un client de messagerie qui supporte plusieurs réseaux sociaux différents. Il vous permettra en une seule application d'accéder à vos différents comptes en ligne tel que Twitter, Messenger, GMail ou tant d'autres.

