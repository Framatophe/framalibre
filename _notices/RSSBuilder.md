---
nom: "RSSBuilder"
date_creation: "Dimanche, 26 mars, 2017 - 14:53"
date_modification: "Mercredi, 12 mai, 2021 - 16:40"
logo:
    src: "images/logo/RSSBuilder.jpg"
site_web: "https://sourceforge.net/projects/rss-builder/"
plateformes:
    - "Windows"
langues:
    - "English"
    - "Autres langues"
description_courte: "Logiciel de création et de publication de flux RSS"
createurices: "Wim Bokkers"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
    - "lecteur de flux rss"
    - "veille"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/rssbuilder"
---

RSSBuilder est un logiciel pour les systèmes Windows sous licence GPL développé en .Net, vous permettant la création et la publication de flux RSS. Il intègre un gestionnaire FTP ainsi qu'un éditeur HTML assez rustique.
Le développement de ce logiciel a été arrêté en mars 2008.

