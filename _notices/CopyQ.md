---
nom: "CopyQ"
date_creation: "Jeudi, 20 avril, 2017 - 14:58"
date_modification: "Lundi, 26 mars, 2018 - 11:18"
logo:
    src: "images/logo/CopyQ.png"
site_web: "https://hluk.github.io/CopyQ/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "CopyQ est un gestionnaire de presse-papiers avancé avec des fonctions d'édition et de script."
createurices: "Lukas Holecek"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/copyq"
---

Ce logiciel vous permet de garder en sécurité tout votre presse-papiers, d'avoir toujours a portée de main vos derniers copier-collers, et de pouvoir consulter l'historique.
Quelques fonctionnalités:
- Les éléments sauvegardés peuvent être modifiés par un éditeur de texte inclus,
- Support du glisser-déposer,
- Marquer ou tagger les éléments sauvegardés

