---
nom: "UniConvertor"
date_creation: "Mercredi, 21 août, 2019 - 11:07"
date_modification: "Lundi, 10 mai, 2021 - 14:39"
logo:
    src: "images/logo/UniConvertor.png"
site_web: "https://sk1project.net/modules.php?name=Products&product=uniconvertor"
plateformes:
    - "GNU/Linux"
    - "Windows"
langues:
    - "English"
    - "Autres langues"
description_courte: "Interopérabilité des fichiers vectoriels"
createurices: ""
alternative_a: "yamb, PVA Instrumento, X Muxer, TMPGENC"
licences:
    - "Licence publique générale limitée GNU (LGPL)"
tags:
    - "multimédia"
    - "convertisseur"
    - "format"
    - "image"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/uniconvertor"
---

UniConvertor est un convertisseur universel de fichiers vectoriels. Il permet de convertir un fichier d'un format à un autre.
Formats qu'il est possible d'importer : CDR, CDRX, CDT, CMX, AI, CGM, WMF, XFIG, SVG, SK, SK1, AFF.
Formats d'export disponibles : AI, SVG, SK, SK1, CGM, WMF.

