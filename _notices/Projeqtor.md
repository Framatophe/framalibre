---
nom: "Projeqtor"
date_creation: "Jeudi, 22 décembre, 2016 - 16:08"
date_modification: "Lundi, 10 mai, 2021 - 14:48"
logo:
    src: "images/logo/Projeqtor.png"
site_web: "http://www.projeqtor.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Autre"
    - "le web"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "ProjeQtOr regroupe dans un outil unique toutes les fonctionnalités nécessaires à la gestion de projets."
createurices: "Pascal BERNARD"
alternative_a: "Microsoft Project, Redmine, Sciforma, Primavera"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "gestion de projet"
    - "gestion"
    - "projet"
    - "php"
lien_wikipedia: "https://en.wikipedia.org/wiki/ProjeQtOr"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/projeqtor"
---

ProjeQtOr est un Logiciel de gestion de projet open source, qui regroupe dans un outil unique toutes les fonctionnalités nécessaires à la gestion de projets. Que ce soit au niveau des tâches, du (rétro-)planning, l'utilisation des ressources, le pointage et les rapports d'activité (...)
Sa particularité est aussi d'être orienté qualité et a pour but de faciliter la mise en conformité aux principaux standards de gestion de la qualité comme ISO, CMMI, ITIL ou autre.
Le logiciel est proposé en licence AGPL, vous pouvez donc l'installer sur votre propre serveur. Il est cependant possible de payer pour obtenir un hébergement, du support, de la formation, voire des développements spécifiques.
Au niveau technique, les dépendances sont classiques pour un serveur: ProjeQtOr est codé en PHP, et supporte MySQL et PostgreSQL. (cf. les spécifications techniques)

