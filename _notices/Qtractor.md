---
nom: "Qtractor"
date_creation: "Mardi, 27 décembre, 2016 - 13:19"
date_modification: "Dimanche, 9 avril, 2017 - 17:58"
logo:
    src: "images/logo/Qtractor.png"
site_web: "https://qtractor.sourceforge.io"
plateformes:
    - "GNU/Linux"
langues:
    - "Autres langues"
description_courte: "Qtractor est un séquenceur multi-pistes AUDIO et MIDI."
createurices: "Rui Nuno Capela"
alternative_a: "Cubase, Sonar, Reaper"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "daw"
    - "musique"
    - "mao"
    - "midi"
    - "création musicale"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/qtractor"
---

Qtractor est un séquenceur multi-pistes AUDIO et MIDI. Enregistre et joue tout signal audio ou MIDI. Compatible avec tous les formats de modules (plugins) de génération et de traitement audio et MIDI disponibles sous Linux : LV2, DSSI, LADSPA, VST. Transport maître/esclave Jack, report des connections audio et MIDI, import/export par piste/bus, etc.

