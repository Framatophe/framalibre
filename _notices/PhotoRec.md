---
nom: "PhotoRec"
date_creation: "Mardi, 5 janvier, 2021 - 13:38"
date_modification: "Mercredi, 12 mai, 2021 - 15:08"
logo:
    src: "images/logo/PhotoRec.png"
site_web: "https://www.cgsecurity.org/wiki/PhotoRec_FR"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
description_courte: "PhotoRec permet de récupérer des fichiers supprimés."
createurices: "Christophe Grenier"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
    - "fichier"
    - "restauration"
    - "dépannage"
lien_wikipedia: "https://fr.wikipedia.org/wiki/PhotoRec"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/photorec"
---

Contrairement à son nom, il ne récupère pas que des fichiers images mais aussi des fichiers archives (ZIP, RAR, etc.), bureautique (openDocuments, Microsoft Office, PDF, etc.), multimédia (photo, vidéo et son), etc.
Au total il couvre plus de 180 extensions.

