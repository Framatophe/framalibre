---
nom: "Trisquel GNU/Linux"
date_creation: "Samedi, 31 décembre, 2016 - 15:07"
date_modification: "Lundi, 22 juillet, 2019 - 13:26"
logo:
    src: "images/logo/Trisquel GNU-Linux.png"
site_web: "https://trisquel.info/"
plateformes:
    - "GNU/Linux"
langues:
    - "Autres langues"
description_courte: "Une distribution 100% libre, basée sur Ubuntu et ne contenant que des logiciels libres."
createurices: ""
alternative_a: "Microsoft Windows, MacOS"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
    - "système d'exploitation (os)"
    - "distribution gnu/linux"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Trisquel"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/trisquel-gnulinux"
---

Trisquel GNU/Linux est une distribution GNU/Linux complètement libre, qui est basée sur Ubuntu (14.04 pour la dernière version), et sur le même principe que Parabola GNU/Linux et gNewSense. Tous les logiciels qui sont inclus dans l'installation de base sont complètement libres (The GIMP, LibreOffice, Firefox, etc.). Elle utilise le gestionnaire de paquets apt, de Debian, et reprend la plupart des éléments présents dans Ubuntu 14.04 : l'installateur, certains drivers, etc, tout en retirant les éléments non-libres : le noyau contenant des morceaux propriétaires, les drivers propriétaires ainsi que les dépôts de logiciels non-libres, garantissant à l'utilisateur une liberté totale. Le bureau par défaut est GNOME Flashback (reprenant l'apparence de Gnome 2 mais basé sur Gnome 3). Elle se décline en plusieurs versions : Trisquel "normale", Trisquel Mini (bureau léger LXDE, pour les vieux ordinateurs) et Trisquel Sugar Toast, avec le bureau Sugar pour les écoles et l'éducation.

