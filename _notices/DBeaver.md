---
nom: "DBeaver"
date_creation: "Dimanche, 22 janvier, 2017 - 18:21"
date_modification: "Lundi, 10 mai, 2021 - 13:55"
logo:
    src: "images/logo/DBeaver.png"
site_web: "http://dbeaver.jkiss.org/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
    - "Autre"
langues:
    - "English"
description_courte: "L'outil universel de développement et d'administration de bases de données SQL"
createurices: "Serge Rider"
alternative_a: "SQL Server Management Studio (SSMS), Oracle Enterprise Manager Database Management"
licences:
    - "Licence Apache (Apache)"
tags:
    - "système"
    - "mysql"
    - "postgresql"
    - "sqlite"
    - "gestionnaire de base de données"
    - "sql"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Dbeaver"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/dbeaver"
---

DBeaver est un client SQL multi-plateformes en Java qui se veut universel et vise à simplifier la vie des développeurs et des administrateurs de base de données. Il supporte les moteurs de bases de données les plus populaires : MySQL, Access, SQL Server, Oracle, Db2, Firebird , Postgres, Informix, SQLite, MongoDB, Cassandra, Redis, Sybase et tout ce qui dispose d'un pilote JDBC.
Comme les autres logiciels de sa catégorie, DBeaver propose un éditeur SQL, un gestionnaire de requêtes, l'import/export des données et une vue modèle entité-association de la base. L'une des particularités de ce logiciel, est la gestion intégrée de tunnels SSH.
À noter qu'il existe une version entreprise qui n'est pas libre qui ajoute le support des bases NoSQL ainsi que des extensions Eclipse supplémentaires.

