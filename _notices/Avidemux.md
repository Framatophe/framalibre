---
nom: "Avidemux"
date_creation: "Jeudi, 29 avril, 2021 - 16:15"
date_modification: "Mardi, 18 mai, 2021 - 22:41"
logo:
    src: "images/logo/Avidemux.png"
site_web: "http://avidemux.sourceforge.net/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Un éditeur vidéo linéaire qui permet de couper, encoder ou appliquer des filtres sur des vidéos."
createurices: ""
alternative_a: "VirtualDub"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "vidéo"
    - "éditeur vidéo"
    - "utilitaire"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Avidemux"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/avidemux"
---

Avidemux est un logiciel de manipulation de vidéos qui ne permet pas de faire du montage multipistes, mais plutôt de découper, encoder et ré-enconder ou appliquer des filtres..
C'est un vénérable logiciel très connu dans le monde des logiciels libres, et qui est toujours activement développé. Il a même été intégré, sous le statut "en observation", à la liste 2020 des logiciels libres préconisés par l'État français dans le cadre de sa modernisation. C'est dire !
Voici des exemples de tâches que l'on peut effectuer avec Avidemux:
 extraire des bouts de vidéo
 l'encoder dans un autre format
 réduire la taille de la (ou des) vidéos (Avidemux permet le traitement par lots)
 incruster des sous-titres
 extraire la bande son de la vidéo
Il n'est plus dans les dépôts d'Ubuntu, mais on peut l'installer avec un PPA ou avec Flatpak. Nous vous laissons consulter la documentation officielle.

