//@ts-check

import './types.js'

/**
 * 
 * @param {string} text 
 * @returns {string}
 */
function escapeQuotesInYAMLValue(text){
    return text ?
        text.trim()
        .replace(/\\/g, '\\\\')
        .replace(/"/g, '\\"')
        : ''
}


/**
 * 
 * @param {Notice} notice
 * @returns {string}
 */
export default function createNoticeContent({
    nom,
    date_creation,
    date_modification,
    logo,
    tags,
    site_web,
    plateformes,
    langues,
    description_courte,
    createurices,
    alternative_a,
    licences,
    lien_wikipedia,
    lien_exodus,
    identifiant_wikidata,
    description_longue,
    mis_en_avant,
    redirect_from
}) {
    return `---
nom: "${escapeQuotesInYAMLValue(nom)}"
date_creation: "${escapeQuotesInYAMLValue(date_creation)}"
date_modification: "${escapeQuotesInYAMLValue(date_modification)}"
${ logo && logo.src ? `logo:` : ''}
${ logo && logo.src ? `    src: "${escapeQuotesInYAMLValue(logo.src)}"` : ''}
site_web: "${escapeQuotesInYAMLValue(site_web)}"
plateformes:
${plateformes.map(plateforme => `    - "${escapeQuotesInYAMLValue(plateforme)}"`).join('\n')}
langues:
${langues.map(langue => `    - "${escapeQuotesInYAMLValue(langue)}"`).join('\n')}
description_courte: "${escapeQuotesInYAMLValue(description_courte)}"
createurices: "${escapeQuotesInYAMLValue(createurices)}"
alternative_a: "${escapeQuotesInYAMLValue(alternative_a)}"
licences:
${licences.map(licence => `    - "${escapeQuotesInYAMLValue(licence)}"`).join('\n')}
tags:
${[...tags].map(t => `    - "${escapeQuotesInYAMLValue(t)}"`).join('\n')}
lien_wikipedia: "${escapeQuotesInYAMLValue(lien_wikipedia)}"
lien_exodus: "${escapeQuotesInYAMLValue(lien_exodus)}"
identifiant_wikidata: "${escapeQuotesInYAMLValue(identifiant_wikidata)}"
mis_en_avant: "${mis_en_avant ? 'oui' : 'non'}"
${redirect_from ? `redirect_from: "${escapeQuotesInYAMLValue(redirect_from)}"` : ""}
---

${description_longue.trim()}
`
}
