## Format des notices

Les notices sont au format markdown avec une en-tête frontmatter (Yaml)

### Données

Tous les champs sont optionnels sauf le `nom` et les `licences`.

`nom` : nom du logiciel\
`date_creation` : date de création de la notice\
`date_modification`: date de dernière modification de la notice\
`logo`: informations sur le logo\.
`logo.src`: url de l'image du logo\
`site_web`: url vers le site web du logiciel\
`plateformes`: systèmes d'exploitation où le logiciel est disponible (valeurs possibles : `GNU/Linux`, `BSD`, `Mac OS X`, `Windows`, `Web`, `Apple iOS`, `Android`, `Windows Mobile` `Autre`)\
`langues`: langues humaines pour lesquelles le logiciel est disponible (valeurs possibles : `Français`, `English`, `Español`, `autres langues`)\
`description_courte`: description courte\
`createurices`: Personnes ou collectif.s qui ont créé le logiciel\
`alternative_a`: Logiciels dont ce logiciel est une alternative\
`licences`: Licences du logiciel (valeurs possibles :

````
Berkeley Software Distribution License (BSD)
Cern Open Hardware LicenceCommon Public License (CPL)
Creative Commons (CC-By)
Creative Commons (CC-By-Sa)
Creative Commons Zero (CC0-1.0)
Design Science License (DSL)
Domaine publicFSF Unlimited LicenseLicence Apache (Apache)
Licence Art Libre (LAL)
Licence Beerware
Licence CECILL (Inria)
Licence de base de données ouverte (OdbL)
Licence de Documentation Libre GNU (GNU FDL)
Licence de Publication Ouverte (OPL)
Licence Libre Académique (AFL)
Licence MIT/X11Licence Open Source NCSA/Université de l'Illinois (NCSA)
Licence Publique Eclipse (EPL)
Licence publique f***-en ce que vous voulez (WTFPL)
Licence Publique Générale Affero (AGPL)
Licence Publique Générale GNU (GNU GPL)
Licence publique générale limitée GNU (LGPL)
Licence publique LaTeX (LPPL)
Licence Publique Mozilla (MPL)
Licence publique Sun (SPL)
Licence Publique Union Européenne (EUPL)
Licence Zlib
Multiples licences
ODC Public Domain Dedication and Licence (PDDL)
Open Font License (SIL-OFL)
Open Software License (OSL)
TAPR Open Hardware License
Unlicense
````
)

`tags`: Liste de mots-clefs. On utilise `tags` pour bénéficier du [travail fourni par Jekyll](https://jekyllrb.com/docs/posts/#tags-and-categories)\
`lien_wikipedia`: lien vers la page [Wikipedia](https://fr.wikipedia.org/) du logiciel\
`lien_exodus`: lien vers la page [Exodus](https://reports.exodus-privacy.eu.org/fr/) du logiciel\
`identifiant_wikidata`: identifiant du logiciel sur [wikidata](wikidata.org/)\
`mis_en_avant`: Par défaut, la valeur est à "non". Si on souhaite mettre la
notice en avant, mettre la valeur "oui".\

